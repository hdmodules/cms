<?php

  namespace frontend\models;
  
  use yii\rbac\DbManager;
  use common\models\User;
  use yii\base\Model;
  use Yii;

  /**
   * Signup form
   */
  class SignupForm extends Model {

    public $name;
    public $surname;
    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules() {
      return [
          ['name', 'filter', 'filter' => 'trim'],
          ['name', 'required'],
          ['name', 'string', 'min' => 2, 'max' => 255],
          ['surname', 'filter', 'filter' => 'trim'],
          ['surname', 'required'],
          ['surname', 'string', 'min' => 2, 'max' => 255],
          ['email', 'filter', 'filter' => 'trim'],
          ['email', 'required'],
          ['email', 'email'],
          ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address exist'],
          ['password', 'required'],
          ['password', 'string', 'min' => 6],
      ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
      return [
          'name' => 'Name',
          'surname' => 'Surname',
          'email' => 'E-mail',
          'password' => 'Password',
      ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup() {
      if ($this->validate()) {
        $user = new User();
        $user->name = $this->name;
        $user->surname = $this->surname;
        $user->username = $this->surname . ' ' . $this->name;
        $user->email = $this->email;
        $user->role = User::ROLE_USER;
        $user->status = User::STATUS_ENABLED;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->save();
        
           
        $r=new DbManager;
        $r->init();
        $userRole = $r->getRole('user');
        $r->assign($userRole, $user->id);
        
        return $user;
      }

      return null;
    }

  }
  