<?php
namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class MaterialForm extends Model
{   
    const LOGIN = 'graphickit';
    const PASSWORD = 'sQ6R3VNJ4g';
    
    public $material_login;
    public $material_password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['material_login', 'material_password'], 'required']
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->material_login == self::LOGIN && $this->material_password == self::PASSWORD) {
            return true;
        } else {
            $this->addError('material_password', \Yii::t('site', 'Password or login wrong'));
            return false;
        }
    }

}
