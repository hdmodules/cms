<?php
namespace frontend\behaviors;

use yii\base\Behavior;
use yii\helpers\Json;
use backend\models\GameReview;
use yii\db\BaseActiveRecord;


class SaveReview extends Behavior
{

    public function events()
    {
        return [
            BaseActiveRecord::EVENT_AFTER_INSERT => 'saveReview'
        ];
    }

    public function saveReview()
    {
        $review = new GameReview();

        $json_attributes = Json::decode($this->owner->json_attributes);

        $review->title = 'From :'.$this->owner->name;
        $review->type = isset($json_attributes['type_review']) ? (int) $json_attributes['type_review'] : null;
        $review->url = isset($json_attributes['url']) ? $json_attributes['url'] : null;
        $review->status = GameReview::STATUS_NEW;
        $review->game_id = $this->owner->model_id;
        $review->request_id = $this->owner->primaryKey;

        if(!$review->save()){
            echo '<pre>';
            var_dump($review->errors);
            echo '</pre>';
            die();
        }

    }
}