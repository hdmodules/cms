<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\UniversalForm\UniversalForm;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;

$code = isset($exception->statusCode) ? $exception->statusCode : 404;

if ($code == 404) {
    $class = 'error-404';
    $form_class = 'form-light';
    $message_class = 'text-light';
} elseif ($code == 403) {
    $class = 'error-403 bg-dark';
    $form_class = '';
    $message_class = '';
} elseif ($code == 500) {
    $class = 'error-500 bg-dark';
    $form_class = '';
    $message_class = '';
}

$this->params['breadcrumbs'][] = ['label' => sprintf('<span>%s</span>', $this->title), "encode" => false];

?>

<main class="page-error <?= $class ?>">

    <section class="breadcrumbs">
        <div class="container">
            <?=
                \yii\widgets\Breadcrumbs::widget([
                    'homeLink' => [
                        'label' => Yii::t('site', 'Home'),
                        'url' => ['site/index'],
                    ],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
            ?>
        </div>
    </section>

	<section class="content">
		<div class="container">
                    <h1><span><span class="orange">oops! </span><?= Html::encode($this->title) ?></span></h1>
                    <?php if($code == 404){ ?>
			<img src="/media/images/error/error-404.png" alt="">
                        <p class="big"><?= Yii::t('error', 'Try to get back or visit our home page!') ?></p>
                        <p><?= nl2br(Html::encode($message)) ?></p>
                    <?php } elseif($code == 403){ ?>    
                        <img src="/media/images/error/error-403.png" alt="">
                        <p><?= Yii::t('error', 'Please, visit our') ?> <a href="<?= Url::to(['site/index']) ?>"><?= Yii::t('error', 'home page') ?></a></p>
                    <?php } elseif($code == 500){ ?> 
                        <img src="/media/images/error/error-500.png" alt="">
                        <p><?= Yii::t('error', 'Don’t panic! Our team is already fixing the problem') ?></p>
			<p class="big"><?= Yii::t('error', 'Please, try back later') ?></p>
                    <?php } ?>
  
		</div>
	</section>

    <section class="report-error send-email">
        <div class="container">
            <div class="form-wrapper">
                <?= UniversalForm::widget(['view' => 'error_page', 'type' => 'error-page', 'title' => Yii::t('site', 'News subscription'), 'form_class' => $form_class]) ?>
                <div class="success-msg">
                    <div class="info">
                        
                    <?php if($code == 404){ ?>
                        <img src="/media/images/icon/icon-envelope-white-big.png" alt="">
                    <?php } else { ?>    
                        <img src="/media/images/icon/icon-envelope-big.png" alt="">
                    <?php } ?>
                        
                        <h3 class="<?= $message_class ?>"><?= Yii::t('site', 'Your message has been sent successfully') ?></h3>
                    </div>
                </div>
                <div class="error-msg">
                    <div class="info">
                        
                    <?php if($code == 404){ ?>
                        <img src="/media/images/icon/icon-envelope-white-big-error.png" alt="">
                    <?php } else { ?>    
                        <img src="/media/images/icon/icon-envelope-big-error.png" alt="">
                    <?php } ?>
                        <h3 class="<?= $message_class ?>"><?= Yii::t('site', 'Something went wrong!') ?></h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
