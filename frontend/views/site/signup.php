<?php

  use yii\helpers\Html;
  use yii\bootstrap\ActiveForm;
  use common\widgets\Alert;

/* @var $this yii\web\View */
  /* @var $form yii\bootstrap\ActiveForm */
  /* @var $model \frontend\models\SignupForm */

  $this->title = 'Registration';
//$this->params['breadcrumbs'][] = $this->title;
?>


<section id="hero" class="login">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                <div id="login">
                    <div class="text-center"><img src="/img/logo.png" alt="" data-retina="true" ></div>
                    <hr>
                    
                    <div class="row">
                        <div class="col-lg-12">
                            <?=
                              Alert::widget([
                                  'options' => [
                                      'align' => 'center',
                                      'style' => 'margin: 0px 0px 10px;'
                                  ],
                              ])
                            ?>
                        </div>
                    </div>
                    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                        <?= $form->field($model, 'name') ?>
                        
                        <?= $form->field($model, 'surname') ?>
                        
                        <?= $form->field($model, 'email') ?>
                        
                        <?= $form->field($model, 'password')->passwordInput() ?>

                        <div id="pass-info" class="clearfix"></div>
                        <?= Html::submitButton('Register', ['class' => 'btn_full']) ?>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

