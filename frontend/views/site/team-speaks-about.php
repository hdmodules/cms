<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\UniversalForm\UniversalForm;

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Endorphina team speaks about EiG show and EiG 2016 slot game they created</title>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <link rel="icon" type="image/png" href="/images/icons/favicon.png">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

        
        <meta property="og:url"                content="<?= Url::to(['site/team-speaks-about'], true) ?>" />
        <meta property="og:title"              content="Endorphina team speaks about EiG show and EiG 2016 slot game they created" />
        <meta property="og:description"        content="Endorphina team speaks about EiG show and EiG 2016 slot game they created" />
        <meta property="og:image"              content="http://endorphina.com/media/images/logo/logo-main.png" />
        
        <?= Html::csrfMetaTags() ?>
        
</head>

<body style="background-color:#fff; height:100%; margin-left:0px; margin-bottom:0px; margin-right:0px; margin-top:0px">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
	<tbody>
	<tr>
		<td height="40">
		</td>
	</tr>
	</tbody>
</table>
<table style="width: 600px; margin: auto; text-align: center; background: #ffffff url(https://endorphinamarketing.com/public/userfiles/templates/402/bg_main_14775742548461.jpg) repeat-y center top; font-family: Tahoma, sans-serif; color: white"
	   align="center"
	   background="https://endorphinamarketing.com/public/userfiles/templates/402/bg_main_14775742548461.jpg"
	   cellpadding="0" cellspacing="0" width="600">
	<tbody>
	<tr>
		<td colspan="2" height="45">
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center" valign="middle">
			<a href="http://www.endorphina.com" style="display: inline-block">
				<img src="https://endorphinamarketing.com/public/userfiles/templates/402/logo_main_14775742548571.png"
					 alt="" style="display: block;">
			</a>
		</td>
	</tr>
	<tr>
		<td colspan="2" height="25">
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="https://endorphinamarketing.com/public/userfiles/templates/402/title_1477574459959.png"
				 alt="Endorphina team speaks about EiG show and EiG 2016 slot game they created">
		</td>
	</tr>
	<tr>
		<td colspan="2" height="20">
		</td>
	</tr>
	<tr>
		<td colspan="2" style="padding: 0 45px">
			<table style="">
				<tbody>
				<tr>
					<td>
						<p style="color: #ffffff; padding-left: 5px; text-align: center; font-size: 13px; text-decoration: none; display: block; line-height: 1.4; margin: 0;">
							Year 2016 is a significant year for Endorphina, not only it was the first year they were
							exhibiting at the expo, they also introduced completely new and unusual project.
						</p>
					</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" height="30">
		</td>
	</tr>
	<tr>
		<td width="50%" style="width: 50%; padding-left: 30px; text-align: left">
			<img src="https://endorphinamarketing.com/public/userfiles/templates/402/cubes_14775742548562.png"
				 alt="CUBES" style="display: inline-block">
		</td>
		<td width="50%" style="width: 50%; padding-right: 30px; text-align: left">
			<img src="https://endorphinamarketing.com/public/userfiles/templates/402/eig_2016_14775742548564.png"
				 alt="EiG 2016" style="display: inline-block">
			<p style="color: #ffffff; padding-top: 8px; padding-bottom: 12px; text-align: left; font-size: 11px; text-decoration: none; display: block; line-height: 1.5; margin: 0;">
				A first ever slot developed exclusively to support an igaming event was impossible to miss. Endorphina
				team introduced the game matching the theme of the EiG expo this year to entertain attendees
				waiting for the show. Hundreds of players enjoyed beautiful space design and unique mathematics of the
				game and had a chance to win attractive prizes provided by Clarion events and Endorphina.
			</p>
			<a href="https://eig2016slot.com/" style="display: inline-block">
				<img src="https://endorphinamarketing.com/public/userfiles/templates/402/button_02_1477574254856.png"
					 alt="TRY THE GAME">
			</a>
		</td>
	</tr>
	<tr>
		<td colspan="2" height="35">
		</td>
	</tr>
	<tr>
		<td width="50%" style="width: 50%; padding-left: 30px; text-align: center">
			<img src="https://endorphinamarketing.com/public/userfiles/templates/402/img_honza_14775742548567.png"
				 alt="JAN" style="display: inline-block">
		</td>
		<td width="50%" style="width: 50%; padding-right: 30px; text-align: center">
			<img src="https://endorphinamarketing.com/public/userfiles/templates/402/img_lucie_14775742548569.png"
				 alt="LUCIE" style="display: inline-block">
		</td>
	</tr>
	<tr>
		<td colspan="2" height="15">
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table>
				<tbody>
				<tr>
					<td style="width: 50%; text-align: left; padding: 0 10px 0 30px; vertical-align: top">
						<img src="https://endorphinamarketing.com/public/userfiles/templates/402/name_honza_1477575514612.png"
							 alt="" style="display: inline-block">
						<p style="color: #ffffff; padding-top: 15px; text-align: left; font-size: 12px; text-decoration: none; display: block; line-height: 1.6; margin: 0;">
							"EiG was really successful show for us, we met old and new partners and moved opened
							projects forward. The space themed 2016 EiG Slot that we created for the event was a huge
							success, everybody loved it and we already have a lot of inspiration for unique project that
							we are preparing for ICE next year."
						</p>
					</td>
					<td style="width: 50%; text-align: left; padding: 0 30px 0 10px; vertical-align: top">
						<img src="https://endorphinamarketing.com/public/userfiles/templates/402/name_lucie_14775755146122.png"
							 alt="" style="display: inline-block">
						<p style="color: #ffffff; padding-top: 14px; text-align: left; font-size: 12px; text-decoration: none; display: block; line-height: 1.84; margin: 0;">
							"EiG slot had even a space themed prizes to win. We managed to get a genuine space food for
							astronauts and it instantly became a big hit. We also chose EiG show to officially release a
							Twerk game. The game is available in casinos and as free demo on our brand new website."
						</p>
					</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" height="40">
		</td>
	</tr>
	<tr>
		<td colspan="2" style="text-align: center">
			<a href="http://www.endorphina.com" style="display: inline-block">
				<img src="https://endorphinamarketing.com/public/userfiles/templates/402/button_01_14775742548464.png"
					 alt="www.endorphina.com">
			</a>
		</td>
	</tr>
	<tr>
		<td colspan="2" height="20">
		</td>
	</tr>
	<tr>
		<td colspan="2" style="padding: 20px 0 0">
			<a href="https://twitter.com/EndorphinaGames"
			   style="display: inline-block; margin: 0 12px; vertical-align: middle">
				<img src="https://endorphinamarketing.com/public/userfiles/templates/339/twitter_14634026347528.png"
					 alt="">
			</a>

			<a href="http://periscope.tv/EndorphinaGames"
			   style="display: inline-block; margin: 0 12px; vertical-align: middle">
				<img src="https://endorphinamarketing.com/public/userfiles/templates/339/periscope_14634026347518.png"
					 alt="">
			</a>

			<a href="https://www.youtube.com/channel/UCraVme6dFGEYh0CB9eov2Ng"
			   style="display: inline-block; margin: 0 12px; vertical-align: middle">
				<img src="https://endorphinamarketing.com/public/userfiles/templates/339/youtube_14634026347533.png"
					 alt="">
			</a>

			<a href="https://www.linkedin.com/company/5324249?trk=tyah&amp;trkInfo=tarId%3A1422003201710%2Ctas%3Aendorphina%2Cidx%3A2-1-2"
			   style="display: inline-block; margin: 0 12px; vertical-align: middle">
				<img src="https://endorphinamarketing.com/public/userfiles/templates/339/linkedin_146340263475.png"
					 alt="">
			</a>

			<a href="https://www.facebook.com/endorphinagames"
			   style="display: inline-block; margin: 0 12px; vertical-align: middle">
				<img src="https://endorphinamarketing.com/public/userfiles/templates/339/facebook_14634026347472.png"
					 alt="">
			</a>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="padding: 5px 0 10px">

			<p style="margin: 20px 0 15px; color: #ffffff; font-size: 11px">
				<a style="color: white; font-size: 11px; padding: 0 5px">
					<b>
						Mail
					</b>
					mail@endorphina.com
				</a>
				&nbsp;&nbsp;|&nbsp;&nbsp;
				<a style="color: white; font-size: 11px; padding: 0 5px">
					<b>
						Skype
					</b>
					endorphina.com
				</a>
				&nbsp;&nbsp;|&nbsp;&nbsp;
				<a style="color: white; font-size: 11px; padding: 0 5px">
					<b>
						Phone
					</b>
					+420 222 564 222
				</a>
			</p>
		</td>
	</tr>
	<tr>
		<td colspan="2" height="25">
		</td>
	</tr>
	</tbody>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
	<tbody>
	<tr>
		<td height="40">
		</td>
	</tr>
	</tbody>
</table>

</body>

</html>