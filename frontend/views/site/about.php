<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use frontend\widgets\UniversalForm\UniversalForm;

if(isset($page)){
    $this->title = $page->seo('title') ? $page->seo('title') : $page->title;
    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['name' => 'keywords', 'content' => $page->seo('keywords')]);
    $this->registerMetaTag(['name' => 'description', 'content' => $page->seo('description')]);

    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['property' => 'og:url', 'content' => Url::base(true).Url::current()]);
    $this->registerMetaTag(['property' => 'og:description', 'content' => $page->seo('description')]);
}

$this->params['breadcrumbs'][] = ['label' => sprintf('<span>%s</span>', $page->title), "encode" => false];

$this->registerJs(" 
var tag = document.createElement('script');  
  
tag.src = \"https://www.youtube.com/iframe_api\";  
var firstScriptTag = document.getElementsByTagName('script')[0];  
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);  
  
// 2. This function creates an <iframe> (and YouTube player) after the API code downloads.  
var player;  
function onYouTubeIframeAPIReady() {  
    player = new YT.Player('player', {  
        height: '1280',  
        width: '720',  
        videoId: '" . $page->attribute_1 . "',  
        events: {  
            'onReady': onPlayerReady  
        }  
    });  
}  
  
// 3. The API will call this function when the video player is ready.  
function onPlayerReady(event) {  
    player.setPlaybackQuality('hd1080'); // Here we set the quality (yay!)  
    //event.target.playVideo(); // Optional. Means video autoplays  
}  ", View::POS_END);

$this->registerJsFile('/media/js/libs/particles.min.js');
$this->registerJs("	
        particlesJS.load('plexus01', '/media/json/particlesjs.json');
	particlesJS.load('plexus02', '/media/json/particlesjs.json');", yii\web\View::POS_END);
?>

<main class="page-about-us bg-dark">

    <section class="breadcrumbs">
        <div class="container">
            <?=
                \yii\widgets\Breadcrumbs::widget([
                    'homeLink' => [
                        'label' => Yii::t('site', 'Home'),
                        'url' => ['site/index'],
                    ],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
            ?>
        </div>
    </section>

	<section class="content">
		<div class="container">
			<h1 class="title-line title-light title-bg">
				<span><?= $page->title ?></span>
				<strong class="line"></strong>
			</h1>

			<h2 class="title-dots title-orange"><span><?= Yii::t('about', 'Who we are') ?></span></h2>

			<div class="video">
                            <div id="player"></div>
			</div>

			<h3><?= $page->short ?></h3>
			<div class="text"><?= $page->text ?></div>
		</div>
	</section>

	<section class="subscriber subscriber-light">
                <div id="plexus01" class="plexus"></div>
		<div class="container">
			<?= UniversalForm::widget(['view' => 'about_page_subscription', 'type' => 'about-page-subscription', 'title' => $page->title]) ?>
		</div>
	</section>

	<section class="our-awards">
		<div class="container">
			<h2 class="title-dots"><span><?= Yii::t('about', 'Our awards') ?></span></h2>

			<ul>
				<li>
					<div class="illustration" data-animation="anim04">
						<img src="/media/images/gif/circle-award.png" alt="">
                                                <img src="/media/images/gif/circle-award.png" alt="" class="gif">
					</div>
					<p><?= Yii::t('about', 'Entertainment Arena Expo:<br>Product of the Year') ?></p>
				</li>
				<li>
					<div class="illustration">
						<img src="/media/images/gif/circle-slot.gif" alt="">
                                                <img src="/media/images/gif/circle-slot.png" alt="" class="gif">
					</div>
					<p><?= Yii::t('about', 'Slots Guide: The Ninja and Satoshi’s<br>Secret Reader\'s choice Slot 2015') ?></p>
				</li>
				<li>
					<div class="illustration">
						<img src="/media/images/gif/circle-stars.gif" alt="">
                                                <img src="/media/images/gif/circle-stars.png" alt="" class="gif">
					</div>
					<p><?= Yii::t('about', 'Slots Guide: The Ninja second place<br>in the Best New Online Slot 2015') ?></p>
				</li>
				<li>
					<div class="illustration">
						<img src="/media/images/gif/circle-check.gif" alt="">
                                                <img src="/media/images/gif/circle-check.png" alt="" class="gif">
					</div>
					<p><?= Yii::t('about', 'Slots Guide: Best Online<br>Slot Developer 2015') ?></p>
				</li>

			</ul>
		</div>
	</section>

	<?php if($employees){ ?>
		<section class="our-team">
                        <div id="plexus02" class="plexus"></div>
			<div class="container">
				<h2 class="title-dots"><span><?= Yii::t('about', 'Some of our team') ?></span></h2>

				<ul class="team-members-list slider slider-light">
					<?php foreach($employees as $employe){ ?>
						<li class="member">
							<div class="flipper">
								<div class="front">
									<div class="illustration">
										<img src="<?= $employe->image ? $employe->image : '/media/images/team/team-member-01.png' ?>" alt="">
									</div>
									<div class="details">
										<h3><?= $employe->title ?></h3>
										<p><?= $employe->short ?></p>
									</div>
								</div>
								<div class="back">
									<div class="info">
										<div class="details">
											<h3><?= $employe->title ?></h3>
											<p><?= $employe->short ?></p>
										</div>
										<ul class="contact">
                                                                                    <li><a class="fa fa-envelope" href="mailto:<?= $employe->attribute_1 ?>"><?= $employe->attribute_1 ?></a></li>
                                                                                        <li><span class="fa fa-skype"><?= $employe->attribute_2 ?></span></li>
											<li>
												<a class="linkedin" target="_blank" href="https://www.linkedin.com/in/<?= $employe->attribute_3 ?>">
													<img src="/media/images/icon/icon-linkedin.png" alt="">
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</li>
					<?php } ?>
				</ul>
			</div>
		</section>
	<?php } ?>

</main>