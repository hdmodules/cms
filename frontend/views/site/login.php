<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;


/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;


?>

<main class="page-contact">

    <section class="breadcrumbs">
        <div class="container">
            <?=
                \yii\widgets\Breadcrumbs::widget([
                    'homeLink' => [
                        'label' => Yii::t('site', 'Main'),
                        'url' => Yii::$app->homeUrl,
                    ],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
            ?>
        </div>
    </section>

	<section class="content">
		<div class="container">
			<h1 class="title-line">
				<span><?= $this->title ?></span>
			</h1>

                    <div class="contact-form send-email">
    <div class="form-wrapper" align="center">
                    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                        <?php // echo \nodge\eauth\Widget::widget(['action' => 'site/login']); ?>

                        <?= $form->field($model, 'email', ['template' => '{label}<div class="form_field_wrap">{input}</div>{error}'])->textInput(['placeholder' => 'Enter e-mail']) ?>
                        
                        <?= $form->field($model, 'password', ['template' => '{label}<div class="form_field_wrap">{input}</div>{error}'])->passwordInput(['placeholder' => 'Enter password']) ?>

                        <p class="small">
                            <?= Html::a('Forgot your password', ['site/request-password-reset']) ?>
                        </p>
                        
                        <?= Html::submitButton('Sign in', ['class' => 'btn_full']) ?>
                        <?= Html::a('Register', ['site/signup'], ['class' => 'btn_full_outline']) ?>
                    <?php ActiveForm::end(); ?>
    </div>
                        </div>

		</div>
	</section>

<div class="modal-wrapper">
    <section class="modal modal-form" data-modal="event-modal">
        <button class="close" data-close-modal><?= Yii::t('site', 'Close') ?><span>×</span></button>

        <div class="modal-body add-review">
            
                <?= \frontend\widgets\UniversalForm\UniversalForm::widget([
                    'view' => 'event_subscribe',
                    'type' => 'event-subscribe'
                ]); ?>

        </div>
    </section>
</div>
</main>




