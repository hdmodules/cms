<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\UniversalForm\UniversalForm;

/* @var $this yii\web\View */

if(isset($page)){
    $this->title = $page->seo('title') ? $page->seo('title') : $page->title;
    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['name' => 'keywords', 'content' => $page->seo('keywords')]);
    $this->registerMetaTag(['name' => 'description', 'content' => $page->seo('description')]);

    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['property' => 'og:url', 'content' => Url::base(true).Url::current()]);
    $this->registerMetaTag(['property' => 'og:description', 'content' => $page->seo('description')]);
}
?>


<main class="page-main">

	<section class="advantages">
		<div class="container">
			<ul>
				<li>
					<div class="circle">
						<img src="/media/images/gif/icon-gamepad.gif" alt="icon">
                                                <img src="/media/images/gif/icon-gamepad.png" class="gif" alt="icon">
					</div>
					<h3><?= Yii::t('site', '35+ Games') ?></h3>
				</li>
				<li>
					<div class="circle">
						<img src="/media/images/gif/icon-html5.gif" alt="icon">
                                                <img src="/media/images/gif/icon-html5.png" class="gif" alt="icon">
					</div>
					<h3><?= Yii::t('site', 'HTML5') ?></h3>
				</li>
				<li>
					<div class="circle">
						<img src="/media/images/gif/icon-star.gif" alt="icon">
                                                <img src="/media/images/gif/icon-star.png" class="gif" alt="icon">
					</div>
					<h3><?= Yii::t('site', 'Certification') ?></h3>
				</li>
				<li>
					<div class="circle">
						<img src="/media/images/gif/icon-badge.gif" alt="icon">
                                                <img src="/media/images/gif/icon-badge.png" class="gif" alt="icon">
					</div>
					<h3><?= Yii::t('site', 'Games Quality') ?></h3>
				</li>
			</ul>
		</div>
	</section>

</main>