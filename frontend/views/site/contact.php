<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use hdmodules\base\models\Setting;
use frontend\widgets\UniversalForm\UniversalForm;

if(isset($page)){
    $this->title = $page->seo('title') ? $page->seo('title') : $page->title;
    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['name' => 'keywords', 'content' => $page->seo('keywords')]);
    $this->registerMetaTag(['name' => 'description', 'content' => $page->seo('description')]);

    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['property' => 'og:url', 'content' => Url::base(true).Url::current()]);
    $this->registerMetaTag(['property' => 'og:description', 'content' => $page->seo('description')]);
}

$this->params['breadcrumbs'][] = ['label' => "<span>" . $page->title . "</span>", "encode" => false];


$this->registerJs('
    $( ".event-data" ).on( "click", function() {
        $("#request-model_id").val($(this).data("id")); 
    });
    
    	$(".events-slider li .buttons .btn").on("click", function(){
		var thisDataAttr = $(this).closest("li").data("meeting");
		$("#request-answer_subject").val(thisDataAttr);
	});
', View::POS_READY); 

$this->registerJsFile('/media/js/libs/particles.min.js');
$this->registerJs("	
        particlesJS.load('plexus01', '/media/json/particlesjs.json');
	particlesJS.load('plexus02', '/media/json/particlesjs02.json');", yii\web\View::POS_END);
?>

<main class="page-contact">

    <section class="breadcrumbs">
        <div class="container">
            <?=
                \yii\widgets\Breadcrumbs::widget([
                    'homeLink' => [
                        'label' => Yii::t('site', 'Home'),
                        'url' => ['site/index'],
                    ],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
            ?>
        </div>
    </section>

	<section class="content">
		<div class="container">
			<h1 class="title-line">
				<span><?= $page->title ?></span>
			</h1>

			<div class="contact-details">
                            <div id="plexus02" class="plexus"></div>
				<ul>
					<li>
						<p class="label"><?= Yii::t('site', 'Phone') ?>:</p>
						<a class="fa fa-phone" href="tel:<?= Setting::get('main-page-phone') ?>"><?= Setting::get('main-page-phone') ?></a>
					</li>
					<li>
						<p class="label"><?= Yii::t('site', 'Email') ?>:</p>
						<a class="fa fa-envelope" href="mailto:<?= Setting::get('main-page-email') ?>"><?= Setting::get('main-page-email') ?></a>
					</li>
					<li>
						<p class="label">skype:</p>
						<a class="fa fa-skype" href="skype:<?= Setting::get('main-page-skype') ?>?chat"><?= Setting::get('main-page-skype') ?></a>
					</li>
					<li>
						<p class="label"><?= Yii::t('site', 'Follow us') ?>:</p>
						<div class="social">
							<ul>
                                                            <li><a class="fa fa-twitter" href="<?= Setting::get('social-link-twitter') ?>" target="_blank"></a></li>
                                                            <li><a class="fa fa-periscope" href="<?= Setting::get('social-link-periscope') ?>" target="_blank"></a></li>
                                                            <li><a class="fa fa-youtube" href="<?= Setting::get('social-link-youtube') ?>" target="_blank"></a></li>
                                                            <li><a class="fa fa-linkedin" href="<?= Setting::get('social-link-linkedin') ?>" target="_blank"></a></li>
                                                            <li><a class="fa fa-facebook" href="<?= Setting::get('social-link-facebook') ?>" target="_blank"></a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>

			<?= UniversalForm::widget(['view' => 'main_contact_form', 'type' => 'main-contact-form', 'title' => $page->title]) ?>
			
		</div>
	</section>

        <?php if($events){ ?>
	<section class="events-slider">
                <div id="plexus01" class="plexus"></div>
		<div class="container">
			<ul class="slider-big slider-dark" data-animation="anim03">
                            <?php foreach($events as $event){ ?>
				<li data-meeting="<?= $event->title ?>">

					<div class="illustration">
						<img src="<?= $event->image ?>" alt="<?= $event->title ?>">
					</div>
					<div class="details">
						<div class="text">
							<p class="ellipsis"><?= $event->short ?></p>
							<strong>
                                                            <span class="place"><?= $event->attribute_3 ?></span>
                                                            
                                                            <?php if($event->attribute_1 && $event->attribute_2){ ?>
                                                                <span class="dash">/</span>
                                                                <span class="date"><?= date('d', $event->attribute_1) ?> - <?= date('d.m.Y', $event->attribute_2) ?></span>
                                                            <?php } ?>
                                                            <?php if($event->attribute_1 && !$event->attribute_2){ ?>
                                                                <span class="dash">/</span>
                                                                <span class="date"><?= date('d.m.Y', $event->attribute_1) ?></span>
                                                            <?php } ?>
                                                        </strong>
						</div>
					</div>
					<div class="buttons">
                                            <a href="" class="btn btn-default event-data" data-open-modal="event-modal" data-id="<?= $event->id ?>"><?= Yii::t('site', 'Arrange a meeting') ?></a>
					</div>
				</li>
                            <?php } ?>
			</ul>
		</div>
	</section>
        <?php } ?>
<div class="modal-wrapper">
    <section class="modal modal-form" data-modal="event-modal">
        <div class="links">
			<button data-close-modal="" class="close btn-close-game"><?= Yii::t('site', 'Close') ?><span>×</span></button>
	</div>
        <div class="modal-body add-review">
            
                <?= \frontend\widgets\UniversalForm\UniversalForm::widget([
                    'view' => 'event_subscribe',
                    'type' => 'event-subscribe',
                    'button_class' => 'default'
                ]); ?>

        </div>
    </section>
</div>
</main>

