<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use frontend\widgets\UniversalForm\UniversalForm;
use yii\widgets\ActiveForm;

$this->title = Yii::t('site', 'Download materials');

$this->params['breadcrumbs'][] = ['label' => sprintf('<span>%s</span>', $this->title), "encode" => false];
?>
<main class="page-about-us bg-dark">

    <section class="breadcrumbs">
        <div class="container">
            <?=
            \yii\widgets\Breadcrumbs::widget([
                'homeLink' => [
                    'label' => Yii::t('site', 'Home'),
                    'url' => ['site/index'],
                ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ])
            ?>
        </div>
    </section>

    <div class="container" style="padding-bottom: 30px;">
        <h2 class="title-line title-light title-bg">
            <span><?= Yii::t('site', 'Download materials') ?></span>
            <strong class="line"></strong>
        </h2>
        <div class="form-wrapper">

            <?php
            $form = ActiveForm::begin([
                        'id' => 'materials-form',
                        'options' => ['class' => 'form-light'],
                        //'enableClientValidation' => true,
                        //'enableAjaxValidation' => false,
                        //'validateOnBlur' => false,
                        'errorCssClass' => 'error'
            ]);
            ?>

            <?= $form->field($model, 'material_login', ['options' => ['class' => 'input', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag' => 'div'], 'template' => '{input} <span class="icon"><i class="fa fa-user"></i></span> {error}'])->input('text', ['placeholder' => Yii::t('site', 'Login') . ' *', 'class' => '']); ?>

            <?= $form->field($model, 'material_password', ['options' => ['class' => 'input', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag' => 'div'], 'template' => '{input} <span class="icon"><i class="fa fa-key"></i></span> {error}'])->input('password', ['placeholder' => Yii::t("site", 'Password') . ' *', 'class' => '']); ?>

            <?= Html::submitButton(Yii::t('site', 'Get GraphicKit.zip'), ['class' => 'btn btn-primary', 'style' => 'float: right;']) ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>


</main>