<?php

use yii\helpers\Url;
use yii\web\View;

if(isset($page_seo)){
	$this->title = $page_seo->seo('title') ? $page_seo->seo('title') : $page_seo->title;
	$this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
	$this->registerMetaTag(['name' => 'keywords', 'content' => $page_seo->seo('keywords')]);
	$this->registerMetaTag(['name' => 'description', 'content' => $page_seo->seo('description')]);

	$this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
	$this->registerMetaTag(['property' => 'og:url', 'content' => Url::base(true).Url::current()]);
	$this->registerMetaTag(['property' => 'og:description', 'content' => $page_seo->seo('description')]);
}

$action = $this->context->action->id;

$this->params['breadcrumbs'][] = ['label' => "<span>".Yii::t('site', "Let's Talk")."</span>", "encode" => false];

?>

<main class="page-lets-talk">

	<?php //$pjax = \yii\widgets\Pjax::begin(); ?>

	<?php
		$this->registerJs('
			function select() {
				$("#lt-type").not(".multiselect").selectric({
					disableOnMobile: false,
					onChange: function () {
						window.location.href = "'.Url::to(['site/lets-talk']).'/"+$("#lt-type").val();									
						//$("#"+$("#lt-type").val()).click();
					}
				});
    		}
    		
    		select();
    		 
            /*$.fn.formHeightFunction = function () {
        
                setTimeout(function () {
        
                    $(".form-wrapper").each(function (key, thisForm) {
        
                        $(thisForm).css("height", "").removeClass("transition");
                        $(thisForm).find("form").css("height", "");
        
                        $(thisForm).find(".success-msg, .error-msg").css({
                            display: "none"
                        });
        
                        var realWrapperHeight = $(thisForm).height();
        
                        $(thisForm).css("height", realWrapperHeight);
                        $(thisForm).find("form").css("height", realWrapperHeight);
        
                        $(thisForm).find(".success-msg, .error-msg").css("display", "table");
        
                        var successHeight = $(thisForm).find(".success-msg").height();
                        var errorHeight = $(thisForm).find(".error-msg").height();
                        var highestHeight = Math.max(realWrapperHeight, successHeight, errorHeight);
        
                        $(thisForm).css("height", highestHeight);
                        $(thisForm).find("form").css("height", highestHeight);
                        $(thisForm).find(".success-msg, .error-msg").css("height", highestHeight);
        
                        setTimeout(function () {
                            $(thisForm).addClass("transition");
                        }, 50);
        
                    });
        
                }, 50);
            };
        
            $(window).formHeightFunction();
        
            $("form").on("afterValidateAttribute", function (event, attribute, messages) {
                $(window).formHeightFunction();
            });*/
            
		', View::POS_READY);
	?>


	<section class="breadcrumbs">
		<div class="container">
			<?=
                \yii\widgets\Breadcrumbs::widget([
                    'homeLink' => [
                        'label' => Yii::t('site', 'Home'),
                        'url' => ['/'],
                    ],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
			?>
		</div>
	</section>

	<section class="content">
		<div class="container">
			<h1 class="title-line title-grey">
				<span><?= Yii::t('site', "Let's Talk") ?></span>
                                
				<?php $types = [
					6=>Yii::t('site', "career"),
					7=>Yii::t('site', "sales"),
					8=>Yii::t('site', "marketing"),
					9=>Yii::t('site', "other"),
				]?>

				<select class="theme" name="" id="lt-type">
					<?php foreach ($types as $k=>$t){ ?>
						<option value="<?= $t ?>" <?= (Yii::$app->request->get('type') ==  $t) ? 'selected="selected"' : '' ?>>
							<?= $t ?>
						</option>
					<?php } ?>
				</select>

				<?php foreach ($types as $k=>$t){ ?>
					<a id="<?= $t ?>" href="<?= Url::to(["site/lets-talk", "type"=>$t]) ?>" style="display: none"></a>
				<?php } ?>
			</h1>


			<div class="form-wrapper">

				<?= \frontend\widgets\UniversalForm\UniversalForm::widget([
					'view' => 'lets_talk',
                    'type' => $type ? $type->name : 'lets-talk-career',
                    'title' => Yii::t('site', "Let's Talk"),
					'request' => new \common\models\RequstLetsTalk()
				]); ?>

				<div class="success-msg">
					<img src="/media/images/logo/logo-main-no-text.png" alt="">

					<strong>
						<?= Yii::t('site', "thank you!") ?>
					</strong>

					<p class="title">
						<?= Yii::t('site', "We’ll get back to you soon") ?>
					</p>

					<p>
						<?= Yii::t('site',
							"Your email has been sent! We appreciate you reaching out and are stoked about the opportunity to work together.") ?>
					</p>

					<a class="read-more" href=""><?= Yii::t('site', "Send another message") ?> >></a>
				</div>

				<div class="error-msg">
					<div class="info">
						<img src="/media/images/icon/icon-envelope-big-error.png" alt="">
						<h2><?= Yii::t('site', 'Something went wrong!') ?></h2>
					</div>
				</div>

			</div>
		</div>
	</section>

	<?php //\yii\widgets\Pjax::end(); ?>
</main>

