<?php

use yii\helpers\Url;
use \yii\helpers\Html;
use yii\web\View;
use \hdmodules\content\models\Item;
use \backend\models\GamePaylineItem;

if(isset($page_seo)){
    $this->title = $page_seo->seo('title') ? $page_seo->seo('title') : $page_seo->title;
    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['name' => 'keywords', 'content' => $page_seo->seo('keywords')]);
    $this->registerMetaTag(['name' => 'description', 'content' => $page_seo->seo('description')]);

    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['property' => 'og:url', 'content' => Url::base(true).Url::current()]);
    $this->registerMetaTag(['property' => 'og:description', 'content' => $page_seo->seo('description')]);
}

$action = $this->context->action->id;

$this->params['breadcrumbs'][] = ['label' => Yii::t('site', 'Games'), 'url'=>['games/index']];

if(in_array($action, ['paylines', 'themes', 'features'])){
    $this->params['breadcrumbs'][] = ['label' => $action, 'url'=>['games/'.$action]];
}
if(Yii::$app->request->get('slug')){
    $this->params['breadcrumbs'][] = ['label' => "<span>".$page_seo->title."</span>", "encode" => false];
}

?>

<?php $this->registerJs('                                               
      $(document).on("click", ".game-starter", function (e) {
            e.preventDefault();                        
            gameId = $(this).data("id");   
            gameUrl = $(this).data("url");
            slug = $(this).data("slug");
            html5 = $(this).data("html5");
            $.ajax({
                type: "POST",
                url: "' . Url::to(['games/get-game-demo-url']) . '",
                data: {gameId:gameId,slug:slug,html5:html5},
                dataType: "json",
                    beforeSend:function () {
                        $(".modal-play-now").addClass("loading");
                    },
                    error:function () {
                        //$("#event-form").closest(".form-wrapper").addClass("error");
                    },
                    success : function (data) {
                        if(data.status){
                            $(".modal-play-now").removeClass("loading"); 
                            $("#game-info").attr("href", gameUrl); 
                            $("#game-holder").html("<iframe src=\'" + data.url + "\' style=\'width:100%;height:100%;border:4px;\'></iframe>");                               
                        }
                        else{
                           $(".modal-play-now").removeClass("loading"); 
                           $("#game-info").attr("href", gameUrl); 
                           $("#game-holder").html("<div style=\'width:100%;height:50%;\'></div><div style=\'text-align:center;font-size:35px;\'>'.Yii::t('site','Game is unavailable').'</div>"); 
                               
                        }
                    }
            });                   
       });                            
', View::POS_END); 

$this->registerJs('                                               
      $(document).on("click", "#btn-close-game", function (e) {
            $("#game-holder").html("");
       });                            
', View::POS_END); 

$this->registerJsFile('/media/js/libs/particles.min.js');
$this->registerJs("particlesJS.load('plexus01', '/media/json/particlesjs.json');", yii\web\View::POS_END);

?>

<?php ?>

<main class="page-game">

    <?php //$pjax = \yii\widgets\Pjax::begin(); ?>

        <section class="breadcrumbs">
            <div class="container">
                <?=
                    \yii\widgets\Breadcrumbs::widget([
                        'homeLink' => [
                            'label' => Yii::t('site', 'Home'),
                            'url' => ['site/index'],
                        ],
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ])
                ?>
            </div>
        </section>

        <section class="content">
            <div class="container">
                <h1 class="title-line">
                    <span><?= Yii::t('site', 'Games') ?></span>
                </h1>
                <div class="filters <?= ($action=='cat' || $action=='index')  ? 'no-submenu' : '' ?>" data-filters="<?= Yii::t('site', 'Filter games')?>">
                    <ul>
                        <li <?= ($action ==  'index') ? 'class="active"' : ''?>>
                            <a href="<?= Url::to(['games/index']) ?>"><?= Yii::t('site', 'all') ?></a>
                        </li>
                        <li <?= $action ==  'paylines' ? 'class="active"' : ''?>>
                            <a class="active" href="<?= Url::to(['games/paylines']) ?>">paylines</a>

                            <?php $paylines = GamePaylineItem::find()->block('games-payline')->status(Item::STATUS_ON)->all();?>
                            <ul>
                                <?php foreach ($paylines as $line){ ?>
                                    <li <?= (($action ==  'paylines') && (Yii::$app->request->get('slug') ==  $line->slug)) ? 'class="active"' : ''?>>
                                        <a href="<?= Url::to(['games/paylines', 'slug'=>$line->slug]) ?>"><?= $line->title ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li <?= $action ==  'themes' ? 'class="active"' : ''?>>

                            <a href="<?= Url::to(['games/themes']) ?>"><?= Yii::t('site', 'themes') ?></a>
                            <?php $themes = GamePaylineItem::find()->block('games-theme')->status(Item::STATUS_ON)->all(); ?>

                            <ul>
                                <?php foreach($themes as $theme){ ?>
                                    <li <?= (($action ==  'themes') && (Yii::$app->request->get('slug') ==  $theme->slug)) ? 'class="active"' : ''?>>
                                        <a href="<?= Url::to(['games/themes', 'slug'=>$theme->slug]) ?>"><?= $theme->title ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li <?= $action ==  'features' ? 'class="active"' : ''?>>
                            <a href="<?= Url::to(['games/features']) ?>"><?= Yii::t('site', 'features') ?></a>
                            <?php $features = GamePaylineItem::find()->block('games-feature')->status(Item::STATUS_ON)->all();?>

                            <ul>
                                <?php foreach ($features as $feature){ ?>
                                    <li <?= (($action ==  'features') && (Yii::$app->request->get('slug') ==  $feature->slug)) ? 'class="active"' : ''?>>
                                        <a href="<?= Url::to(['games/features', 'slug'=>$feature->slug]) ?>"><?= $feature->title ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li <?= (($action ==  'cat') && (Yii::$app->request->get('slug') ==  'popular')) ? 'class="active"' : ''?>>
                            <a href="<?= Url::to(['games/cat', 'slug'=>'popular']) ?>"><?= Yii::t('site', 'popular') ?></a>
                        </li>
                        <li <?= (($action ==  'cat') && (Yii::$app->request->get('slug') ==  'new')) ? 'class="active"' : ''?>>
                            <a href="<?= Url::to(['games/cat/', 'slug'=>'new']) ?>"><?= Yii::t('site', 'new') ?></a>
                        </li>
                    </ul>
                </div>


                <?php
                    if($dataProvider->count) {
                        echo \yii\widgets\ListView::widget([
                            'dataProvider' => $dataProvider,
                            'options' => [
                                'tag' => 'ul',
                                'class' => 'games-list cols-3',
                            ],
                            'itemOptions' => [
                                'tag' => 'li',
                                'class' => 'item',
                            ],
                            'itemView' => '_item_game',
                            'summary' => false,
                            'layout' => '{items}<div class="pagination-wrap">{pager}</div>',
                            'pager' => [
                                'class' => \darkcs\infinitescroll\InfiniteScrollPager::className(),
                                'paginationSelector' => '.pagination-wrap',
                                'containerSelector' => '.games-list',
                                'wrapperSelector' => '.games-list',
                                //'pjaxContainer' => $pjax->id,
                            ],
                        ]);
                    }
                ?>

            </div>
        </section>

    <?php //\yii\widgets\Pjax::end(); ?>

    <section class="subscriber subscriber-light">
        <div id="plexus01" class="plexus"></div>
        <div class="container">
            <?= \frontend\widgets\UniversalForm\UniversalForm::widget(['view' => 'main_page_subscription', 'type' => 'main-page-subscription', 'title' => Yii::t('site', 'News subscription'), 'button_class'=>'default']) ?>
        </div>
    </section>

</main>

<div class="modal-wrapper">
    <section class="modal modal-play-now" data-modal="play-now">
        <div class="links">
                        <a href="" class="game-info" id="game-info" target="blank"><?= Yii::t('site', 'Game info') ?><span>i</span></a>
			<button id="btn-close-game" data-close-modal="" class="close"><?= Yii::t('site', 'Close') ?><span>×</span></button>
	</div>
        <div class="modal-body" id="game-holder">
            
        </div>
    </section>
</div>