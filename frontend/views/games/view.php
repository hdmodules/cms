<?php

use yii\helpers\Url;
use hdmodules\base\helpers\Image;
use \common\helpers\Helper;
use \yii\helpers\Html;
use \hdmodules\content\models\Item;
use \backend\models\GamePaylineItem;
use yii\web\View;

if(isset($game)){
    $this->title = $game->seo('title') ? $game->seo('title') : $game->title;
    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['name' => 'keywords', 'content' => $game->seo('keywords')]);
    $this->registerMetaTag(['name' => 'description', 'content' => $game->seo('description')]);

    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['property' => 'og:url', 'content' => Url::base(true).Url::current()]);
    $this->registerMetaTag(['property' => 'og:description', 'content' => $game->seo('description')]);
}

$action = $this->context->action->id;

$this->params['breadcrumbs'][] = ['label' => Yii::t('site', 'Games'), 'url'=>['games/index']];

if(Yii::$app->request->get('slug')){
    $this->params['breadcrumbs'][] = ['label' => "<span>".$game->title."</span>", "encode" => false];
}

$this->registerJs('
    $(".game-play-now").on("click", function (event) {
        gameId = $(this).data("id");
        slug = $(this).data("slug");
        html5 = $(this).data("html5");
        $.ajax({
            type: "POST",
            url: "' . Url::to(['games/get-game-demo-url']) . '",
            data: {gameId:gameId,slug:slug,html5:html5},
            dataType: "json",
                beforeSend:function () {
                    $(".modal-play-now").addClass("loading");
                },
                error:function () {
                    //$("#event-form").closest(".form-wrapper").addClass("error");
                },
                success : function (data) {
                    if(data.status){
                        console.log("Status OK!");
                            $(".modal-play-now").removeClass("loading"); 
                            $("#game-holder").html("<iframe src=\'" + data.url + "\' style=\'width:100%;height:100%;border:4px;\'></iframe>"); 
                    }
                    else {
                       $(".modal-play-now").removeClass("loading"); 
                       $("#game-holder").html("<div style=\'width:100%;height:50%;\'></div><div style=\'text-align:center;font-size:35px;\'>'.Yii::t('site','Game is unavailable now').'</div>"); 
                    }
                }
        });
    });', View::POS_READY);

/*TO DO CHANGE ALL GAME CALLING LIKE THIS*/
/*$this->registerJs('
    $(".game-play-html").on("click", function (event) {
        gameUrl = $(this).data("id");

        $("#game-holder").html("<iframe src=\'" + gameUrl + "\' style=\'width:100%;height:100%;border:4px;\'></iframe>");
    });', View::POS_READY);*/

$this->registerJsFile('/media/js/libs/particles.min.js');
$this->registerJs("particlesJS.load('plexus01', '/media/json/particlesjs02.json');", yii\web\View::POS_END);
?>

<?php $this->registerJs('                                               
      $(document).on("click", ".close", function (e) {
            $("#game-holder").html("");
            $("#game-video-holder").html("");
       });                            
', View::POS_END); ?>

    <main class="page-game-detail">

        <section class="breadcrumbs">
            <div class="container">
                <?=
                \yii\widgets\Breadcrumbs::widget([
                    'homeLink' => [
                        'label' => Yii::t('site', 'Home'),
                        'url' => ['/'],
                    ],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
                ?>
            </div>
        </section>

        <section class="content">
            <div class="container">
                <h1 class="title-line">
                    <span><?= $game->title ?></span>
                </h1>
                <div class="game-slider">

                    <?php if($game->photos){ ?>
                        <ul class="slider-for">
                            <?php foreach ($game->photos as $photo){ ?>
                                <li><img src="<?= $photo->image ?>" alt=""></li>
                            <?php }?>
                        </ul>
                        <ul class="slider-nav">
                            <?php foreach ($game->photos as $photo){ ?>
                                <li><img src="<?= Image::thumb($photo->image, 120, 89) ?>" alt=""></li>
                            <?php }?>
                        </ul>

                    <?php } elseif($game->image && !(empty($game->image))){ ?>
                        <img src="<?= Image::thumb($game->image, 640, 476) ?>" alt="">
                    <?php } ?>

                </div>

                <div class="game-details">
                    <ul>
                        <?php foreach ($points as $point){ ?>
                            <li><span><?= $point->point->name ?></span></li>
                        <?php }?>
                    </ul>
                </div>
                
                <div class="buttons">
                    <?php if($isMobile && $game->game->html_game && $gameHtmlUrl){ ?>
                        <a class="btn btn-primary game" data-id="<?= $game->game->key ?>"  data-slug="<?= $game->game->content->slug ?>" data-html5="1" href="<?= $gameHtmlUrl ?>" target="_blank"><?= Yii::t('site', 'Play now') ?></a>
                    <?php } else {
                        if(!$isMobile) { ?>
                            <a class="btn btn-primary game game-play-now" data-open-modal="play-now"  href="" data-id="<?= $game->game->key ?>" data-slug="<?= $game->game->content->slug ?>" data-html5="0"><?= Yii::t('site', 'Play now') ?></a>
                            <?php if($game->game->html_game && $gameHtmlUrl){ ?>
                                <br>
                                <a class="game-play-html"  href="<?=$gameHtmlUrl?>" target="_blank" data-id="<?= $game->game->key ?>" data-slug="<?= $game->game->content->slug ?>" data-html5="1" style="cursor: pointer;font-size: 20px;"><?= Yii::t('site', 'Try HTML5 version') ?>&nbsp &nbsp &nbsp</a>
                            <?php }
                        } ?>
                    <?php } ?>
                </div>
                
                <div class="game-info text">
                    <?= $game->text ?>
                </div>

                <?php if(isset($tags) && $tags){ ?>
                    <div class="tags">
                        <span><?= Yii::t('site', 'Tags:') ?></span>
                        <ul>
                            <li><?= implode(', ', $tags) ?></li>
                        </ul>
                    </div>
                <?php } ?>
                
            </div>
        </section>

        <?php if($review_big_wins){ ?>

            <section class="big-wins">
                <div class="container">
                    <h2 class="title-dots title-light"><span><?= Yii::t('site', 'Big wins:') ?></span></h2>
                    <ul class="games-list slider">
                        <?php foreach($review_big_wins as $review){ ?>

                            <li>

                                <a class="game game-video-review" data-open-modal="play-video" data-code="<?= Helper::getYotubeCode($review->url) ?>" href="<?= $review->url ?>">
                                    <img src="<?= Image::thumb($review->image ? $review->image : $game->image, 313, 313) ?>" alt="<?= $review->title ?>">

                                    <div class="hover">
                                        <div class="icon-play">
                                            <img src="/media/images/icon/icon-play.png" alt="play">
                                            <span><?= Yii::t('site', 'Play now') ?></span>
                                        </div>
                                    </div>
                                </a>
                                <p class="game-info info-light"><?= $review->title ?></p>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </section>
        <?php } ?>

        <section class="reviews">
            <div class="container">
                <h2 class="title-dots">
                    <span><?= Yii::t('site', 'Video reviews') ?></span>
                </h2>
                <?php if($review_video){ ?>
                    <div class="games-slider">
                        <ul class="games-list cols-3 slider slider-dark">
                            <?php foreach($review_video as $review){ ?>
                                <li>
                                    <a class="game game-video-review" data-open-modal="play-video" data-code="<?= Helper::getYotubeCode($review->url) ?>" href="<?= $review->url ?>">

                                        <img src="<?= Image::thumb($review->image ? $review->image : $game->image, 225, 225) ?>" alt="<?= $review->title ?>">

                                        <div class="hover">
                                            <div class="icon-play">
                                                <img src="/media/images/icon/icon-play.png" alt="play">
                                                <span><?= Yii::t('site', 'Play now') ?></span>
                                            </div>
                                        </div>
                                    </a>
                                    <p class="game-info"><?= $review->title ?></p>

                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>
                <div class="add-button tiles">
                    <a href="" class="add-game-review" data-open-modal="modal-01" data-type-review="<?= \backend\models\GameReview::REVIEW_VIDEO ?>">
                        <div class="icon-add">
                            <i class="fa fa-plus"></i>
                            <span><?= Yii::t('site', 'Add review') ?></span>
                        </div>
                    </a>
                </div>
            </div>
        </section>

        <section class="reviews">
            <div class="container">
                <h2 class="title-dots">
                    <span><?= Yii::t('site', 'Reviews') ?></span>
                </h2>
                <?php if($review_simple){ ?>
                    <div class="games-slider">
                        <ul class="games-list cols-3 slider slider-dark">
                            <?php foreach($review_simple as $review){ ?>
                                <li>
                                    <a class="game" href="<?= $review->url ?>" target="_blank">
                                        <img src="<?= Image::thumb($review->image ? $review->image : $game->image, 313, 313) ?>" alt="<?= $review->title ?>">
                                    </a>
                                    <p class="game-info"><?= $review->title ?></p>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>
                <div class="add-button tiles">
                    <a href="" class="add-game-review" data-open-modal="modal-01" data-type-review="<?= \backend\models\GameReview::REVIEW_SIMPLE ?>">
                        <div class="icon-add">
                            <i class="fa fa-plus"></i>
                            <span><?= Yii::t('site', 'Add review') ?></span>
                        </div>
                    </a>
                </div>
            </div>
        </section>

        <section class="subscriber subscriber-dark">
            <div id="plexus01" class="plexus"></div>
            <div class="container">
                <?= \frontend\widgets\UniversalForm\UniversalForm::widget([
                    'view' => 'main_page_subscription',
                    'type' => 'main-page-subscription',
                    'title' => Yii::t('site', 'News subscription'),
                    'button_class'=>'primary']); ?>
            </div>
        </section>

        <?php if($similar_games){?>
            <section class="similar-games">
                <div class="container">


                    <h2 class="title-dots">
                        <span><?= Yii::t('site', 'Similar games') ?></span>
                    </h2>
                    <ul class="games-list slider slider-dark">

                        <?php foreach($similar_games as $similar_game){ ?>
                            <li>
                                <a class="game" href="<?= Url::to(['games/view', 'slug'=>$similar_game['slug']]); ?>">
                                    <img src="<?= Image::thumb($similar_game['image'], 313, 313) ?>" alt="<?= $similar_game['title'] ?>">
                                    <div class="hover"></div>
                                </a>
                            </li>

                        <?php } ?>

                    </ul>
                </div>
            </section>
        <?php } ?>

    </main>

    <div class="modal-wrapper">
        <section class="modal modal-form" data-modal="modal-01">
            <div class="links">
                <button data-close-modal="" class="close btn-close-game">close<span>×</span></button>
            </div>
            <div class="modal-body add-review">

                <?= \frontend\widgets\UniversalForm\UniversalForm::widget([
                    'view' => 'add_review',
                    'type' => 'add-review',
                    'title' => Yii::t('site', 'Add Review'),
                    'request'=>new \common\models\RequestReview(),
                    'model_id'=>$game->game->id,
                    'button_class' => 'default'
                ]); ?>

            </div>
        </section>


        <section class="modal modal-play-now" data-modal="play-now">
            <div class="links">
                <a href="#" data-close-modal=""  class="game-info close"><?= Yii::t('site', 'Game info') ?><span>i</span></a>
                <button data-close-modal="" class="close btn-close-game"><?= Yii::t('site', 'Close') ?><span>×</span></button>
            </div>
            <div class="modal-body" id="game-holder">
            </div>
        </section>


        <section class="modal modal-play-video" data-modal="play-video">
            <div class="links">
                <button data-close-modal="" class="close btn-close-game"><?= Yii::t('site', 'Close') ?><span>×</span></button>
            </div>
            <div class="modal-body" id="game-video-holder"></div>
        </section>
    </div>

<?php $this->registerJs('

    $(".add-game-review").on("click", function (event) {               
        $("#requestreview-type_review").val($(this).data("type-review"));       
    });
    
    $(".game-video-review").on("click", function (event) {                  
        $("#game-video-holder").html("<iframe src=\'//www.youtube.com/embed/" + $(this).data("code") + "\' width=\'680\' height=\'380\' frameborder=\'0\' allowfullscreen></iframe>"); 
    });
    
', \yii\web\View::POS_READY); ?>