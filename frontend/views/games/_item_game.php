<?php
use yii\helpers\Url;
use hdmodules\base\helpers\Image;

$isMobile = \common\helpers\Helper::isMobileDevice();
?>

 <?php if(!$isMobile){ ?>
        <a class="game game-starter" data-open-modal="play-now" href="" data-id="<?= $model->key ?>" data-slug="<?= $model->content->slug ?>" data-html5="<?= $model->html_game ?>" data-url="<?= Url::to(['games/view', 'slug'=>$model->content->slug]); ?>">
 <?php } else{ ?>
        <a class="game" href="<?= Url::to(['games/view', 'slug'=>$model->content->slug]);?>" data-url="<?= Url::to(['games/view', 'slug'=>$model->content->slug]);?>">
 <?php } ?>
            
    <img src="<?= Image::thumb($model->content->image, 313, 313) ?>" alt="game">
 <?php if(!$isMobile){ ?>
    <div class="hover">
        <div class="icon-play">
            <img src="<?= Url::base() ?>/media/images/icon/icon-play.png" alt="play">
            <span>Play now</span>
        </div>
    </div>
 <?php } ?>

</a>
<h2><a href="<?= Url::to(['games/view', 'slug'=>$model->content->slug]);?>"><?= $model->content->title; ?></a></h2>
<a class="btn btn-tertiary" href="<?= Url::to(['games/view', 'slug'=>$model->content->slug]);?>" style="cursor:pointer"><span>find out </span>more</a>
<span class="fa fa-heart-o"></span>
<span class="fa fa-eye"></span>
