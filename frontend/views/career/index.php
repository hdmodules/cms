<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\Slider\Slider;
use frontend\widgets\UniversalForm\UniversalForm;
use \hdmodules\base\helpers\Image;
use backend\models\CareerItemContent;
/* @var $this yii\web\View */

if(isset($page)){
    $this->title = $page->seo('title') ? $page->seo('title') : $page->title;
    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['name' => 'keywords', 'content' => $page->seo('keywords')]);
    $this->registerMetaTag(['name' => 'description', 'content' => $page->seo('description')]);

    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['property' => 'og:url', 'content' => Url::base(true).Url::current()]);
    $this->registerMetaTag(['property' => 'og:description', 'content' => $page->seo('description')]);
}

$counter = 1;

$this->params['breadcrumbs'][] = ['label' => sprintf('<span>%s</span>', $page->seo('h1') ? $page->seo('h1') : $page->title), "encode" => false];

$dependency = [
    'class' => 'yii\caching\DbDependency',
    'sql' => 'SELECT MAX(update_time) FROM carousel_item',
];

$this->registerJsFile('/media/js/libs/particles.min.js');
$this->registerJs("particlesJS.load('plexus01', '/media/json/particlesjs02.json');", yii\web\View::POS_END);
?>

<main class="page-career">

    <section class="breadcrumbs">
        <div class="container">
            <?=
                \yii\widgets\Breadcrumbs::widget([
                    'homeLink' => [
                        'label' => Yii::t('site', 'Home'),
                        'url' => ['site/index'],
                    ],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
            ?>
        </div>
    </section>

	<section class="content">
		<div class="container">

			<h1 class="title-line">
				<span><?=  $page->seo('h1') ? $page->seo('h1') : $page->title?></span>
			</h1>

			<h2 class="title-dots">
				<span><?= Yii::t('career', 'Why join us?') ?></span>
			</h2>

			<ol class="four-points">
				<li>
					<strong class="number" data-animation="anim02">1</strong>
					<p><?= Yii::t('career', 'Career why join text 1') ?></p>
				</li>
				<li>
					<strong class="number">2</strong>
					<p><?= Yii::t('career', 'Career why join text 2') ?></p>
				</li>
				<li>
					<strong class="number">3</strong>
					<p><?= Yii::t('career', 'Career why join text 3') ?></p>
				</li>
				<li>
					<strong class="number">4</strong>
					<p><?= Yii::t('career', 'Career why join text 4') ?></p>
				</li>
			</ol>
                    
                    <?php if($careers){ ?>    
			<ul class="vacancy-list">
                            <?php foreach($careers as $data){ ?>
                                <li class="block-0<?= $counter ?> <?= $data->attribute_2 ?>">
                                    <?php if($data->attribute_1 == CareerItemContent::TYPE_VACANCY){ ?>
					<h3><?= $data->title ?></h3>
					<p><?= $data->short ?></p>
					<a class="btn <?= $data->attribute_3 ?>" href="<?= Url::to(['view', 'slug' => $data->slug]) ?>"><?= Yii::t('career', 'More info') ?></a>
                                    <?php } elseif($data->attribute_1 == CareerItemContent::TYPE_BLOCK){ ?>
                                        <img src="<?= $data->image ?>" alt="">
					<?php $title = explode('-', $data->title) ?>
                                        <h3><?= $title[0] ?> <span><?= $title[1] ?></span></h3>
					<p class="quote"><?= $data->short ?></p>
                                    <?php } elseif($data->attribute_1 == CareerItemContent::TYPE_IMAGE){ ?>
                                        <?php if($data->text_1){ ?>
                                            <a href="<?= $data->text_1 ?>" target="_blank">
						<img src="<?= $data->image ?>" alt="">
                                            </a>
                                        <?php } else { ?>
                                            <img src="<?= $data->image ?>" alt="">
                                        <?php } ?>
                                    <?php } ?>
                                </li>

                                <?php $counter++ ?>
                            <?php } ?>
			</ul>
                    <?php } ?>
                    
			<section class="subscriber subscriber-dark">
                                <div id="plexus01" class="plexus"></div>
				<div class="container">
                                    
                                    <?= UniversalForm::widget(['view' => 'career_page_subscription', 'type' => 'career-page-subscription-form', 'title' => Yii::t('site', 'Order product presentation'), 'button_class' => 'primary']) ?>

				</div>
			</section>
		</div>
	</section>

	<section class="our-life">
		<div class="container">
			<h2 class="title-dots">
				<span><?= Yii::t('career', 'Our life') ?></span>
			</h2>
                        <?php if ($this->beginCache('career-our-life', ['dependency' => $dependency, 'variations' => [Yii::$app->language]])) { ?>
                            <?= Slider::widget(['slug' => 'career-our-life', 'view' => 'our_life']); ?>
                            <?php $this->endCache(); ?>
                        <?php } ?>
		</div>
	</section>

</main>