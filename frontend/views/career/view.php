<?php

use yii\helpers\Url;
use hdmodules\base\helpers\Image;
use \common\helpers\Helper;
use \yii\helpers\Html;
use \hdmodules\content\models\Item;
use \backend\models\GamePaylineItem;
use frontend\widgets\UniversalForm\UniversalForm;

if(isset($page)){
    $this->title = $page->seo('title') ? $page->seo('title') : $page->title;
    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['name' => 'keywords', 'content' => $page->seo('keywords')]);
    $this->registerMetaTag(['name' => 'description', 'content' => $page->seo('description')]);

    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['property' => 'og:url', 'content' => Url::base(true).Url::current()]);
    $this->registerMetaTag(['property' => 'og:description', 'content' => $page->seo('description')]);
}

$this->params['breadcrumbs'][] = ['label' => Yii::t('career', 'Career'), 'url'=>['career/index']];
$this->params['breadcrumbs'][] = ['label' => "<span>".$this->title."</span>", "encode" => false];

$this->registerJsFile('/media/js/libs/particles.min.js');
$this->registerJs("particlesJS.load('plexus01', '/media/json/particlesjs.json');", yii\web\View::POS_END);
?>

<main class="page-career-detail bg-dark">

    <section class="breadcrumbs">
        <div class="container">
            <?=
            \yii\widgets\Breadcrumbs::widget([
                'homeLink' => [
                    'label' => Yii::t('site', 'Home'),
                    'url' => ['site/index'],
                ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ])
            ?>
        </div>
    </section>

	<section class="content">
		<div class="container">
			<h1 class="title-line title-light title-bg">
				<span><?= Yii::t('career', 'Career') ?></span>
				<strong class="line"></strong>
			</h1>

			<h2 class="title-dots title-orange"><span><?= $page->seo('h1') ? $page->seo('h1') : $page->title ?></span></h2>

			<div class="text">
                            <?= $page->text ?>
			</div>

			<div class="buttons">
				<a href="" class="btn btn-secondary" data-open-modal="recomend-modal"><?= Yii::t('career', 'Recommend a friend') ?></a>
				<?= Html::a(Yii::t('career', 'Apply for this position'), ['site/lets-talk'], ['class' => 'btn btn-primary']) ?>
			</div>
		</div>
	</section>

	<section class="subscriber subscriber-light">
                <div id="plexus01" class="plexus"></div>
		<div class="container">
			<?= UniversalForm::widget(['view' => 'career_page_subscription', 'type' => 'career-page-subscription', 'button_class'=>'default']) ?>
		</div>
	</section>

	<section class="other-jobs">
		<div class="container">
			<h2 class="title-dots"><span><?= Yii::t('career', 'Other job vacancy') ?></span></h2>
                        
			<ul class="games-list slider slider-dark">
                            
				<?php foreach($careers as $data){ ?>
					<li>
						<h3 class="job-title">
							<a class="game" href="<?= Url::to(['career/view', 'slug' => $data->slug]) ?>"><?= $data->title ?></a>
						</h3>
						<!--<span class="fa fa-heart-o">2039</span>
						<span class="fa fa-eye">38412</span>-->
					</li>
				<?php } ?>
			</ul>
		</div>
	</section>
<div class="modal-wrapper">
    <section class="modal modal-form" data-modal="recomend-modal">
        <div class="links">
			<button data-close-modal="" class="close btn-close-game"><?= Yii::t('site', 'Close') ?><span>×</span></button>
	</div>
        <div class="modal-body add-review">
			<?= UniversalForm::widget(['view' => 'career_friend_recommend', 'type' => 'career-friend-recommend', 'button_class'=>'default']) ?>
        </div>
    </section>
</div>
</main>