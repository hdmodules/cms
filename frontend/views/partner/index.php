<?php

use yii\helpers\Url;
use \yii\helpers\Html;
use \hdmodules\content\models\Item;
use \backend\models\GamePaylineItem;

if(isset($page)){
    $this->title = $page->seo('title') ? $page->seo('title') : $page->title;
    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['name' => 'keywords', 'content' => $page->seo('keywords')]);
    $this->registerMetaTag(['name' => 'description', 'content' => $page->seo('description')]);

    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['property' => 'og:url', 'content' => Url::base(true).Url::current()]);
    $this->registerMetaTag(['property' => 'og:description', 'content' => $page->seo('description')]);
}

$this->params['breadcrumbs'][] = ['label' => sprintf('<span>%s</span>', $this->title), "encode" => false];

$this->registerJsFile('/media/js/libs/particles.min.js');
$this->registerJs("particlesJS.load('plexus01', '/media/json/particlesjs.json');", yii\web\View::POS_END);
?>

<main class="page-partners bg-dark">

    <section class="breadcrumbs">
        <div class="container">
            <?=
                \yii\widgets\Breadcrumbs::widget([
                    'homeLink' => [
                        'label' => Yii::t('site', 'Home'),
                        'url' => ['/'],
                    ],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
            ?>
        </div>
    </section>
    
    <?php if($partners){ ?>
        <?php foreach($partners as $data){ ?>
      
        <?php if($data->slug == 'our-clients' && $data->publishItems) { ?>
            <section class="content">
		<div class="container">
			<h1 class="title-line title-light title-bg">
				<span><?= $page->seo('h1') ? $page->seo('h1') : $page->title  ?></span>
				<strong class="line"></strong>
			</h1>

			<h2 class="title-dots title-orange"><span><?= $data->title ?></span></h2>

			<ul class="partners-list">
                                <?php foreach($data->publishItems as $item){ ?>
				<li>
					<div class="illustration">
                                            <a href="<?= $item->attribute_2 ?>" target="_blank"><img src="<?= $item->image ?>" alt=""></a>
					</div>
					<div class="details">
						<p><?= $item->short ?></p>
						<strong>
							<span class="name"><?= $item->attribute_1 ?>,</span>
							<span class="company"><?= $item->title ?></span>
						</strong>
					</div>
				</li>
                                <?php } ?>
			</ul>

			<div class="buttons">
				<a class="btn btn-primary" href="<?= Url::to(["site/lets-talk", "type"=>'sales']) ?>"><?= Yii::t('site', 'Become a client') ?></a>
			</div>
		</div>
            </section>
        <?php } elseif($data->slug == 'media-partners' && $data->publishItems){ ?>

	<section class="media-partners">
		<div class="container">
			<h2 class="title-dots"><span><?= $data->title ?></span></h2>

			<ul class="partners-img-list">
                                <?php foreach($data->publishItems as $item){ ?>
                                    <li><a href="<?= $item->attribute_2 ?>" target="_blank"><img src="<?= $item->image ?>" alt="<?= $item->title ?>"></a></li>
                                <?php } ?>
			</ul>
                        
			<div class="buttons">
				<a class="btn btn-default" href="<?= Url::to(["site/lets-talk", "type"=>'marketing']) ?>"><?= Yii::t('site', 'Become a partner') ?></a>
			</div>
		</div>
	</section>
    
        <?php } ?>

     <?php } ?>

        <?php if($meetings){ ?>
	<section class="exhibitions">
            <div id="plexus01" class="plexus"></div>
		<div class="container">
                    <h2 class="title-dots"><span><?= Yii::t('site', 'Exhibitions') ?></span></h2>
			<ul class="exhibitions-list slider slider-dark">
                            <?php foreach($meetings as $item){ ?>
				<li><a href="<?= $item->attribute_2 ?>" target="_blank"><img src="<?= $item->image ?>" alt="<?= $item->title ?>"></a></li>
                            <?php } ?>    
			</ul>
		</div>
	</section>
        <?php } ?>

  <?php } ?>
</main>