<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use hdmodules\base\models\Menu;
use frontend\widgets\MyMenu\MyMenu;
use yii\helpers\Url;
use hdmodules\base\models\Setting;
use common\models\User;
use yii\widgets\ActiveForm;

$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
$model = new common\models\LoginForm();

AppAsset::register($this);

$dependency = [
    'class' => 'yii\caching\DbDependency',
    'sql' => 'SELECT MAX(update_time) FROM menu_item',
];
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="/images/icons/favicon.png">

        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <header>
            <div class="header-main">
                <div class="container">
                    <a href="<?= Url::to(['/']);?>" class="logo-main">
                        <img src="/media/images/logo/logo-main.png" alt="">
                    </a>
                    <div class="buttons">
                        <?php if(Yii::$app->user->isGuest){ ?>
                            <a href="" class="fa fa-unlock-alt" data-open-modal="login-modal"><?= Yii::t('site', 'Log in') ?></a>
                        <?php } elseif(Yii::$app->user->identity->role == User::ROLE_ADMIN) { ?>
                            <a href="/adminpanel/" class="fa fa-gears">Admin panel</a>
                        <?php } ?>
                        <a href="tel:<?= Setting::get('main-page-phone') ?>" class="fa fa-phone"><?= Setting::get('main-page-phone') ?></a>
                        <a href="<?= Url::to(['site/lets-talk', 'type' => 'sales']) ?>" class="btn btn-default btn-lets-talk"><?= Yii::t('site', 'LET’s talk') ?></a>
                    </div>
                </div>
            </div>

            <div class="header-nav <?= ($controller=='site' && $action=='index') ? 'nav-main' : '' ?>">
                <div class="open-menu-wrapper">
                    <button class="open-menu"><span></span></button>
                </div>
                <a class="logo-menu" href="<?= Url::to(['site/index']) ?>">
                    <img src="/media/images/logo/logo-main-white.png" alt="">
                </a>
                <div class="container">
                    <?= MyMenu::widget(['items' => Menu::getMenuItemsBySlug("main-menu")]); ?>
                </div>
            </div>
        </header>

        <?= Alert::widget() ?>

        <?= $content ?>

        <div class="social social-dark social-vertical social-fixed">
            <ul>
                <li><a class="fa fa-twitter" href="<?= Setting::get('social-link-twitter') ?>" target="_blank"></a></li>
                <li><a class="fa fa-periscope" href="<?= Setting::get('social-link-periscope') ?>" target="_blank"></a></li>
                <li><a class="fa fa-youtube" href="<?= Setting::get('social-link-youtube') ?>" target="_blank"></a></li>
                <li><a class="fa fa-linkedin" href="<?= Setting::get('social-link-linkedin') ?>" target="_blank"></a></li>
                <li><a class="fa fa-facebook" href="<?= Setting::get('social-link-facebook') ?>" target="_blank"></a></li>
            </ul>
        </div>
        <div style="display:none">Deploy working!</div>
        <footer>
            <div class="container">
                <?= MyMenu::widget(['items' => Menu::getMenuItemsBySlug("footer-menu"), 'view' => 'footer']); ?>
                <div class="contacts">
                    <ul>
                        <li><a class="fa fa-envelope" href="mailto:<?= Setting::get('main-page-email') ?>"><?= Setting::get('main-page-email') ?></a></li>
                        <li><a class="fa fa-skype" href="skype:<?= Setting::get('main-page-skype') ?>?chat"><?= Setting::get('main-page-skype') ?></a></li>
                        <li><a class="fa fa-phone" href="tel:<?= Setting::get('main-page-phone') ?>"><?= Setting::get('main-page-phone') ?></a></li>
                    </ul>
                </div>
                <div class="buttons">
                    <!--                    <a class="btn btn-secondary" href=""><?= Yii::t('site', 'Client Login') ?></a>-->
                </div>
                <div class="social social-footer">
                    <ul>
                        <li><a class="fa fa-twitter" href="<?= Setting::get('social-link-twitter') ?>" target="_blank"></a></li>
                        <li><a class="fa fa-periscope" href="<?= Setting::get('social-link-periscope') ?>" target="_blank"></a></li>
                        <li><a class="fa fa-youtube" href="<?= Setting::get('social-link-youtube') ?>" target="_blank"></a></li>
                        <li><a class="fa fa-linkedin" href="<?= Setting::get('social-link-linkedin') ?>" target="_blank"></a></li>
                        <li><a class="fa fa-facebook" href="<?= Setting::get('social-link-facebook') ?>" target="_blank"></a></li>
                    </ul>
                </div>

            </div>
        </footer>

        <?php if (\Yii::$app->user->isGuest) { ?>
            <div class="modal-wrapper">
                <section class="modal modal-form" data-modal="login-modal">
                    <div class="links">
                        <button data-close-modal="" class="close btn-close-game"><?= Yii::t('site', 'Close') ?><span>×</span></button>
                    </div>
                    <div class="modal-body log-in">
                        <div class="form-wrapper">
                            <?php
                            $form = ActiveForm::begin([
                                'id' => 'login-form',
                                'options' => ['class' => 'form-dark'],
                                'action' => '/site/login',
                                'validationUrl' => Url::to(['site/ajax-login-validation']),
                                // 'enableClientValidation' => true,
                                'enableAjaxValidation' => true,
                                'errorCssClass' => 'error'
                            ]);
                            ?>

                            <h2><?= Yii::t('site', 'Log in') ?></h2>

                            <?= $form->field($model, 'email', ['options' => ['class' => 'input', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag' => 'div'], 'template' => '{input} <span class="icon"><i class="fa fa-envelope-o"></i></span> {error}'])->input('text', ['placeholder' => Yii::t("site", 'E-mail') . ' *', 'class' => '']); ?>

                            <?= $form->field($model, 'password', ['options' => ['class' => 'input', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag' => 'div'], 'template' => '{input} <span class="icon"><i class="fa fa-gear"></i></span> {error}'])->passwordInput(['placeholder' => Yii::t("site", 'Password') . ' *', 'class' => '']); ?>

                            <!--                        <div align="center">
                                <?= Html::a('Forgot your password', ['site/request-password-reset']) ?>
                            </div>-->

                            <div class="buttons">
                                <?= Html::submitButton(Yii::t('site', 'Enter'), ['class' => 'btn btn-default']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>

                            <div class="success-msg">
                                <div class="info">
                                    <img src="/media/images/icon/icon-envelope-big.png" alt="">
                                    <h2><?= Yii::t('site', 'Your message has been sent successfully') ?></h2>
                                </div>
                            </div>
                            <div class="error-msg">
                                <div class="info">
                                    <img src="/media/images/icon/icon-envelope-big-error.png" alt="">
                                    <h2><?= Yii::t('site', 'Something went wrong!') ?></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        <?php } ?>
        <?php $this->endBody() ?>


        <script>
                <!--Start of Zopim Live Chat Script-->
                window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set._.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
                    $.src='//v2.zopim.com/?2RXxH9rYJLOD3CZtD3lWFXpR8mQa32EU';z.t=+new Date;$.type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
                <!--End of Zopim Live Chat Script—>


                $zopim(function() {
                    $zopim.livechat.setLanguage('en');
                });
        </script>


        <?php if(!YII_DEBUG && $_SERVER['HTTP_HOST'] == 'endorphina.com'){ ?>
            <script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                ga('create', 'UA-47950639-1', 'endorphina.com');
                ga('send', 'pageview');
            </script>
        <?php } ?>

    </body>
    </html>
<?php $this->endPage() ?>