<?php

use yii\helpers\Url;
use hdmodules\base\helpers\Image;
use \common\helpers\Helper;
use \yii\helpers\Html;
use \hdmodules\content\models\Item;
use \backend\models\GamePaylineItem;

$page_seo = $news;

if(isset($page_seo)){
    $this->title = $page_seo->seo('title') ? $page_seo->seo('title') : $page_seo->title;
    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['name' => 'keywords', 'content' => $page_seo->seo('keywords')]);
    $this->registerMetaTag(['name' => 'description', 'content' => $page_seo->seo('description')]);

    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['property' => 'og:url', 'content' => Url::base(true).Url::current()]);
    $this->registerMetaTag(['property' => 'og:description', 'content' => $page_seo->seo('description')]);
    
    
    
    if(isset($page_seo->image)){
        $image = Url::base(true) . $page_seo->image;
    } else {
        $image = 'http://endorphina.com/media/images/logo/logo-main.png';
    }
    $this->registerMetaTag(['property' => 'og:image', 'content' => $image]);
    
}

$action = $this->context->action->id;

$this->params['breadcrumbs'][] = ['label' => Yii::t('site', 'News'), 'url'=>['news/index']];
if($news->block->slug <> 'no-category'){
    $this->params['breadcrumbs'][] = ['label' => $news->block->title, 'url'=>['news/view', 'slug'=>$news->block->slug]];    
}
$this->params['breadcrumbs'][] = ['label' => "<span>".$news->title."</span>", "encode" => false];

$this->registerJsFile('/media/js/libs/particles.min.js');
$this->registerJs("particlesJS.load('plexus01', '/media/json/particlesjs02.json');", yii\web\View::POS_END);
?>

<main class="page-news-detail">

    <section class="breadcrumbs">
        <div class="container">
            <?=
            \yii\widgets\Breadcrumbs::widget([
                'homeLink' => [
                    'label' => Yii::t('site', 'Home'),
                    'url' => ['/'],
                ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ])
            ?>
        </div>
    </section>

    <section class="content">
        <div class="container">

            <h1 class="title-line">
                <span><?= $news->title ?></span>
            </h1>

            <div class="news-slider">

                <?php if($news->photos){ ?>
                    <ul class="slider-for">
                        <?php if (!empty($news->news->video) && Helper::getYotubeCode($news->news->video)){ ?>
                            <li>
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?= Helper::getYotubeCode($news->news->video) ?>" frameborder="0" allowfullscreen></iframe>
                            </li>
                        <?php }?>
                        <?php foreach ($news->photos as $photo){ ?>
                            <li><img src="<?= $photo->image ?>" alt=""></li>
                        <?php }?>
                    </ul>
                    <ul class="slider-nav">
                        <?php if (!empty($news->news->video) && Helper::getYotubeCode($news->news->video)){ ?>
                            <?php
                                if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $news->news->video, $match)) {
                                    $image = 'http://img.youtube.com/vi/'.$match[1].'/mqdefault.jpg';
                                }
                            ?>
                            <li><img src="<?= $image ?>" alt=""></li>
                        <?php }?>
                        <?php foreach ($news->photos as $photo){ ?>
                            <li><img src="<?= Image::thumb($photo->image, 153, 86) ?>" alt=""></li>
                        <?php }?>
                    </ul>
                <?php } elseif(!empty($news->news->video) && Helper::getYotubeCode($news->news->video)){ ?>
                    <ul class="slider-for">
                        <li>
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?= Helper::getYotubeCode($news->news->video) ?>" frameborder="0" allowfullscreen></iframe>
                        </li>
                    </ul>
                <?php }?>
            </div>

            <time class="fa fa-calendar"><?= \Yii::$app->formatter->asDatetime($news->time, "php:d/m/y")?></time>

            <div class="news-info text">

                <?= $news->text ?>
            </div>

            <?php if($news->tags){ ?>
                <div class="tags">
                    <span><?= Yii::t('site', 'Tags:') ?></span>
                    <ul>
                        <?php $count_tags = count($news->tags)?>
                        <?php foreach ($news->tags as $k=>$tag){ ?>
                            <li><?= $tag->name ?><?= ($count_tags == $k+1) ? '' : ', ' ?></li>
                        <?php }?>
                    </ul>
                </div>
            <?php } ?>
        </div>
    </section>

    <section class="subscriber subscriber-dark">
        <div id="plexus01" class="plexus"></div>
        <div class="container">
            <?= \frontend\widgets\UniversalForm\UniversalForm::widget(['view' => 'main_page_subscription', 'type' => 'main-page-subscription', 'title' => Yii::t('site', 'News subscription'), 'button_class'=>'primary']) ?>
        </div>
    </section>

    <section class="recommended-news">
        <div class="container">
            <?php if($recommended_news){?>

                <h2 class="title-dots">
                    <span><?= Yii::t('site', 'Recommended for you') ?></span>
                </h2>
                <ul class="articles-list slider slider-dark">

                    <?php foreach($recommended_news as $rec_news){ ?>

                        <li>
                            <a href="<?= Url::to(['news/view', 'slug'=>$rec_news->slug]); ?>">
                                <img src="<?= $rec_news->news->cropped_3 ?>" alt="<?= $rec_news->title ?>">
                            </a>
                            <div class="details ellipsis">
                                <h3>
                                    <a href="<?= Url::to(['news/view', 'slug'=>$rec_news->slug]); ?>">
                                        <?= $rec_news->title ?>
                                    </a>
                                </h3>
                                <p class="ellipsis">
                                    <?= $rec_news->short ?>
                                </p>
                            </div>
                            <a class="read-more" href="<?= Url::to(['news/view', 'slug'=>$rec_news->slug]); ?>">
                                <?= Yii::t('site', 'Read More') ?>
                            </a>
                            <span class="fa fa-heart-o">2039</span>
                            <span class="fa fa-eye">38412</span>
                        </li>

                    <?php } ?>

                </ul>

            <?php } ?>
        </div>
    </section>

</main>