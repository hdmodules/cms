<?php

use yii\helpers\Url;
use \yii\helpers\Html;
use \yii\widgets\ListView;
use frontend\widgets\UniversalForm\UniversalForm;

if(isset($page_seo)){
    $this->title = $page_seo->seo('title') ? $page_seo->seo('title') : $page_seo->title;
    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['name' => 'keywords', 'content' => $page_seo->seo('keywords')]);
    $this->registerMetaTag(['name' => 'description', 'content' => $page_seo->seo('description')]);

    $this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
    $this->registerMetaTag(['property' => 'og:url', 'content' => Url::base(true).Url::current()]);
    $this->registerMetaTag(['property' => 'og:description', 'content' => $page_seo->seo('description')]);
}

$action = $this->context->action->id;

$this->params['breadcrumbs'][] = ['label' => Yii::t('site', 'News'), 'url'=>['news/index']];

if($action=='index'){
    $this->params['breadcrumbs'][] = ['label' => "<span>".Yii::t('site', 'All news')."</span>", "encode" => false];
}elseif($action=='cat' && Yii::$app->request->get('slug')){
    $this->params['breadcrumbs'][] = ['label' => "<span>".$page_seo->title."</span>", "encode" => false];
}

$this->registerJsFile('/media/js/libs/particles.min.js');
$this->registerJs("particlesJS.load('plexus01', '/media/json/particlesjs02.json');", yii\web\View::POS_END);
?>

<main class="page-news">

    <?php //$pjax = \yii\widgets\Pjax::begin(); ?>

    <section class="breadcrumbs">
        <div class="container">
            <?=
            \yii\widgets\Breadcrumbs::widget([
                'homeLink' => [
                    'label' => Yii::t('site', 'Home'),
                    'url' => ['/#news'],
                    'id'=>'home-news'
                ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ])
            ?>
        </div>
    </section>

    <section class="content">
        <div class="container">
            <h1 class="title-line title-bg">
                <span><?= Yii::t('site', 'News')?></span>
                <strong class="line"></strong>
            </h1>

            <div class="filters filters-news" data-filters="<?= Yii::t('site', 'Filter news')?>">
                <ul>
                    <li <?= $action ==  'index' ? 'class="active"' : ''?>>
                        <a href="<?= Url::to(['news/index'])?>">
                            <?= Yii::t('site', 'All news') ?>
                        </a>
                    </li>
                    <?php $categories = $cat_news->children()->all(); ?>
                    <?php foreach ($categories as $c){?>
                        <?php if ($c->slug != 'no-category'){?>
                            <li <?= (($action ==  'cat') && (Yii::$app->request->get('slug') ==  $c['slug'])) ? 'class="active"' : ''?>>
                                <a href="<?= Url::to(['news/cat', 'slug'=>$c['slug']])?>">
                                    <?= $c['title']; ?>
                                </a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>

            <?php if($dataProvider->count) { 
                    echo ListView::widget([
                        'dataProvider' => $dataProvider,
                        'options' => [
                            'tag' => 'div'
                        ],
                        'itemView'=>'_item_news',
                        'itemOptions' => [
                            'tag' => false,
                        ],
                        'summary' => false,
                        'viewParams'=>['count'=>count($dataProvider->keys)],
                        'layout' => '<ul class="news-list">{items}</ul><div class="pagination-wrap">{pager}</div>',
                        'pager' => [
                            'class' => \kop\y2sp\ScrollPager::className(),
                            'container'=>'.news-list',
                            'paginationSelector' => '.pagination-wrap .pagination',
                            'noneLeftText'=>'',
                            'triggerText'=>Yii::t('site', 'Show more news'),
                            'triggerTemplate'=>'<div class="buttons"><button class="btn btn-default" >{text}</button></div>',
                            'eventOnRendered'=>'function(){$(window).initDotDotDot();$(window).windowWidthCheck();}'
                        ]
                    ]);
                    } else { ?>
                    <div align="center">
                        <h3><?= Yii::t('site', 'Not found.') ?></h3>
                    </div>
              <?php  } ?>

        </div>
    </section>

    <?php //\yii\widgets\Pjax::end(); ?>

</main>