<?php
use yii\helpers\Url;
use hdmodules\base\helpers\Image;


$arr_key = [1, 3, 4, 4, 2, 1, 3, 4, 4, 2];
$arr_class = [0=>'news-grey', 4=>'news-orange', 5=>'news-grey', 9=>'news-blue'];
$class = isset($arr_class[$index]) ? $arr_class[$index] : '';

switch ($arr_key[$index]) {
    case 3:
        //$crop = Image::thumb($model->news->{"cropped_2"}, 275, 144);
        $crop = $model->news->{"cropped_2"};
        break;
    case 4:
        $crop = $model->news->{"cropped_3"};
        break;
    default:
        $crop = $model->news->{"cropped_$arr_key[$index]"};
}
?>

<li class="item news-0<?= $arr_key[$index]?> <?= $class?>">
    <a class="illustration" href="<?= Url::to(['news/view', 'slug'=>$model->slug])?>">
        <img src="" alt="<?= $model->title ?>" data-img-src="<?= $crop ?>">
    </a>
    <div class="details ellipsis">
        <h2>
            <a href="<?= Url::to(['news/view', 'slug'=>$model->slug])?>">
                <?= $model->title ?>
            </a>
        </h2>
        <time class="fa fa-calendar"><?= \Yii::$app->formatter->asDatetime($model->time, "php:d/m/y")?></time>
        <p><?= $model->short ?></p>
    </div>
    <a class="read-more" href="<?= Url::to(['news/view', 'slug'=>$model->slug])?>">
        <?= Yii::t('site', 'Read more') ?>
    </a>
</li>


<?php if(!Yii::$app->request->isAjax && (($count==($index+1) && $index<4)  || $index==4)){ ?>
    <li class="subscriber subscriber-dark">
        <div id="plexus01" class="plexus"></div>
        <div class="container">
            <?= \frontend\widgets\UniversalForm\UniversalForm::widget(['view' => 'main_page_subscription', 'type' => 'main-page-subscription', 'title' => Yii::t('site', 'News subscription'), 'button_class'=>'primary']) ?>
        </div>
    </li>
<?php } ?>
