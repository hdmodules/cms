<?php

if (YII_DEBUG) {
    $params = array_merge(
            require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
    );
} else {
    $params = array_merge(
            require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/params.php')
    );
}

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'admin' => [
            'class' => 'mdm\admin\Module',
            'layout' => 'left-menu', // avaliable value 'left-menu', 'right-menu' and 'top-menu'
            'controllerMap' => [
                'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    'userClassName' => 'common\models\User',
                    'idField' => 'id'
                ]
            ],
            'menus' => [
                'assignment' => [
                    'label' => 'Grand Access' // change label
                ],
            // 'route' => null, // disable menu
            ],
        ]
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'debug/*',
            'site/index',
            'site/contact',
            'site/login',
            'site/captcha',
            'site/signup',
            'site/about',
            'news/*',
            'site/*',
            'games/*',
            'service/*',
            'partner/*',
            'career/*'
        ]
    ],
    'components' => [
        'devicedetect' => [
            'class' => 'alexandernst\devicedetect\DeviceDetect'
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest'],
        ],
        'request' => [
            'baseUrl' => '/',
            'class' => 'common\components\LangRequest',
            'noCsrfRoutes' => [
                //'transaction/uah-callback'
            ],
            'cookieValidationKey' => 'M0NYURR7h--VRr39ZdSz1wrV368v6BOZ',
        ],
        'urlManager' => [
            'class' => 'common\components\LangUrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => '/',
            'rules' => [
                '' => 'site/index',
                'materials/GraphicKit.zip' => 'site/materials',
                'lets-talk/<type:(career|sales|marketing|partner|other)>' => 'site/lets-talk',
                'lets-talk' => 'site/lets-talk',
                'login' => 'site/login',
                'login/<service:vkontakte|facebook>' => 'site/login',
                'sign-up' => 'site/signup',
                'logout' => 'site/logout',
                'contact-us' => 'site/contacts',
                'about' => 'site/about',
                'terms' => 'site/terms',
                'privacy' => 'site/privacy',
                'request-password-reset' => 'site/request-password-reset',
                'reset-password' => 'site/reset-password',
                'team-speaks-about-EiG' => 'site/team-speaks-about',

                'games'=>'games/index',
                'games/get-game-demo-url' => 'games/get-game-demo-url',
                'games/get-game-demo-url-test' => 'games/get-game-demo-url-test',
                'games/<slug:(popular|new|3d)>'=>'games/cat',
                'games/<action:(paylines|themes|features)>/<slug:[\w-]+>' => 'games/<action>',
                'games/<action:(paylines|themes|features)>' => 'games/<action>',
                'games/<slug:[\w-]+>' => 'games/view',

                'news'=>'news/index',
                'news/<slug:(events|releases|big-wins|interviews)>'=>'news/cat',
                'news/<slug:[\w-]+>' => 'news/view',
                
                'partners'=>'partner/index',
                
                'career' => 'career/index',
                'career-detail/<slug:[\w-]+>' => 'career/view',
                
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                'module/<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>'
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['/site/index']
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
