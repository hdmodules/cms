<?php

namespace frontend\controllers;

use hdmodules\request\models\RequestType;
use Yii;
use common\models\LoginForm;
use common\models\User;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use lajax\translatemanager\helpers\Language;
use common\models\GameThemeItem;
use backend\models\Game;
use hdmodules\content\models\Item;
use yii\helpers\ArrayHelper;
use backend\models\NewsItemContent;
use backend\models\MeetingItemContent;
use hdmodules\base\models\Setting;
use backend\models\GameItemContent;
use backend\models\PartnerItemContent;
use backend\models\EmployeItemContent;
use common\models\Request;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class SiteController extends Controller {

    public function init() {

        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
               // 'errorAction' => 'site/error'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function actionError(){
        $exception = Yii::$app->errorHandler;
        if ($exception !== null) {
            return $this->render('error', ['exception' => $exception]);
        }
    }

    
    public function actionIndex() {

        if(isset($_GET['clearCache']) && $_GET['clearCache']){
            Yii::$app->cache->flush();
        }
        
        $page = Yii::$app->cache->get('main-page' . Yii::$app->language);
        if ($page === false) {
          $page = Item::find()->with(['seo'])->where(['slug' => 'main-page'])->one();
          Yii::$app->cache->set('main-page' . Yii::$app->language, $page);
        }

        return $this->render('index',[
            'page' => $page
        ]);
    }
        
    public function actionLogin() {

        $model = new LoginForm();
        
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            \common\models\Language::setCurrentById(\Yii::$app->user->identity->language_id);

            Yii::$app->session->setFlash('success', sprintf('Welcome text, %s', \Yii::$app->user->identity->name . ' ' . \Yii::$app->user->identity->surname));
            \Yii::info('SiteController.actionLogin(): Site login OK!!!');

            if (\Yii::$app->user->identity->role == User::ROLE_ADMIN) {
                return $this->redirect('/adminpanel/');
            } else {
                return $this->redirect(['site/index']);
            }
        }

       /* return $this->render('login', [
                    'model' => $model,
        ]);*/
    }

    public function actionAjaxLoginValidation() {

        $model = new LoginForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\bootstrap\ActiveForm::validate($model);
        }
    }
    
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContacts() {
        $page = Item::find()->with(['seo'])->where(['slug' => 'contact-us'])->one();
        
        //Get meetings
        $events = MeetingItemContent::getDb()->cache(function ($db) {
            return MeetingItemContent::find()->joinWith(['block'])->where(['`content_block`.slug' => 'meeting'])->publish()->all();
        }, null, MeetingItemContent::getCacheDependency('meeting'));

        return $this->render('contact', [
            'page' => $page, 
            'events' => $events
        ]);
    }

    public function actionAbout() {
        
        $page = Item::find()->with(['seo'])->where(['slug' => 'about-us'])->one();
        
        $employees = EmployeItemContent::getDb()->cache(function ($db) {
            return EmployeItemContent::find()->joinWith(['block'])->where(['`content_block`.slug' => 'our-team', '`content_blocks_item`.status' => Item::STATUS_ON])->publish()->all();
        }, null, EmployeItemContent::getCacheDependency());
        
        return $this->render('about', [
            'page' => $page,
            'employees' => $employees
        ]);
    }

    public function actionMaterials() {

        $model = new \frontend\models\MaterialForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $file = Yii::getAlias('@frontend/materials') . '/GraphicKit.zip';
            Yii::info('File:' . $file);
            if (file_exists($file)) {
                Yii::info('File exist!');
                Yii::info('File is_readable:' . is_readable($file));
                Yii::info('File is_writable:' . is_writable($file));
               
                Yii::info('Content-Disposition: attachment; filename="' . basename($file) . '"');
                
                ini_set('memory_limit', '400M');
                
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . basename($file) . '"');
                header('Content-Length: ' . filesize($file));
                header('Cache-Control: must-revalidate');
                header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
                header('Pragma: public');

                readfile($file);
                
            } else {
                Yii::info('File NOT exist!');
            }
        }

        return $this->render('materials', [
            'model' => $model
        ]);
    }

    public function actionLetsTalk($type=null) {

        $page_seo = Item::find()->with(['seo'])->where(['slug' => 'lets-talk'])->one();
        
        $type = RequestType::find()->where(['name'=>'lets-talk-'.$type])->one();

        return $this->render('lets-talk', ['page_seo'=>$page_seo, 'type'=>$type]);
    }

    public function actionSignup() {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
                    'model' => $model,
        ]);
    }

    public function actionRequestPasswordReset() {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
                    'model' => $model,
        ]);
    }

    public function actionResetPassword($token) {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
                    'model' => $model,
        ]);
    }

}
