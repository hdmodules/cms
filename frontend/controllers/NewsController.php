<?php
namespace frontend\controllers;

use Yii;
use backend\models\NewsItemContent;
use hdmodules\content\models\Block;
use hdmodules\content\models\Item;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class NewsController extends Controller
{
    public function actionIndex()
    {
        $cat_news = $this->findBlock('news');
        $categories = $cat_news->children()->asArray()->all();

        if(!$categories)
            throw new NotFoundHttpException('The requested page does not exist.');

        $dataProvider = new ActiveDataProvider([
            'query' => NewsItemContent::find()->with(['block'])
                    ->where(['`content_blocks_item`.status'=>Item::STATUS_ON, 'block_id'=>ArrayHelper::map($categories, 'id', 'id')])
                    //->orderBy(['`content_blocks_item`.time' => SORT_DESC, '`content_blocks_item`.order_num' => SORT_DESC]),
                    ->orderBy(['`content_blocks_item`.time' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $page_seo =  $cat_news;

        return $this->render('index',[
            'cat_news'=>$cat_news,
            'dataProvider'=>$dataProvider,
            'page_seo'=>$page_seo

        ]);

    }

    public function actionCat($slug)
    {
        $cat_news = $this->findBlock('news');

        $cat = $this->findBlock($slug);

        $dataProvider = new ActiveDataProvider([
            'query' => NewsItemContent::find()->with(['block'])
                ->where(['`content_blocks_item`.status'=>Item::STATUS_ON, 'block_id'=>$cat->id])
                ->orderBy(['`content_blocks_item`.time' => SORT_DESC, '`content_blocks_item`.order_num' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $page_seo =  $cat;

        return $this->render('index',[
            'cat_news'=>$cat_news,
            'dataProvider'=>$dataProvider,
            'page_seo'=>$page_seo

        ]);
    }

    public function actionView($slug)
    {
        $news = $this->findNews($slug);

        /*//Get last news
        $recommended_news = Yii::$app->cache->get($slug);
        if ($recommended_news === false) {

            $newsBlocks = \hdmodules\content\models\Block::find()->leftJoin('`content_block` b', '`content_block`.tree = b.id')
                ->where('b.slug = :slug AND `content_block`.slug <> :slug', ['slug' => 'news'])->asArray()->all();
            $newsBlocksIds = ArrayHelper::getColumn($newsBlocks, 'id');

            $newsCount = Setting::get('main-page-news-count');
            $newsCount = isset($newsCount) ? $newsCount : 8;
            $news = NewsItemContent::find()
                ->with(['newsDetail'])
                ->where(['block_id' => $newsBlocksIds])
                ->orderBy(['time' => SORT_DESC, 'order_num' => SORT_DESC])
                ->publish()
                ->limit($newsCount)
                ->all();
            Yii::$app->cache->set('main-page-news', $news, Yii::$app->params['cachLiveTime']);
        }*/

        $recommended_news = NewsItemContent::find()
            ->joinWith(['tags'=>function ($q) {$q->from('tag t');}])
            ->where(['content_blocks_item.status'=>Item::STATUS_ON, 't.id'=>ArrayHelper::map($news->tags , 'id', 'id')])
            ->andWhere(['block_id'=>$news->block->id])
            ->andWhere(['not', ['content_blocks_item.id' => $news->id]])
            ->orderBy('t.frequency asc')
            ->limit(9)
            ->all();

        return $this->render('view',[
            'news'=>$news,
            'recommended_news'=>$recommended_news

        ]);

    }

    protected function findBlock($slug)
    {

        if (($model = Block::find()->where('slug = :slug',[':slug' => $slug])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findNews($slug)
    {
        $newsBlocks = \hdmodules\content\models\Block::find()->leftJoin('`content_block` b', '`content_block`.tree = b.id')
            ->where('b.slug = :slug AND `content_block`.slug <> :slug', ['slug' => 'news'])->asArray()->all();

        $newsBlocksIds = ArrayHelper::getColumn($newsBlocks, 'id');

        if (($model = NewsItemContent::find()->where('slug = :slug',[':slug' => $slug])->andWhere(['block_id'=>$newsBlocksIds])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



}
