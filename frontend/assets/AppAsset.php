<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700&subset=latin,cyrillic',
        'media/css/font-awesome.min.css',
        'media/css/main.css',
    ];
    public $js = [
        //'media/js/libs/jquery-2.2.3.min.js',
        'media/js/libs/jquery.dotdotdot.min.js',
        'media/js/libs/masonry.pkgd.min.js',
        'media/js/libs/jquery.selectric.js',
        'media/js/libs/slick.min.js',
        'media/js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset'
    ];
}
