<?php

namespace frontend\widgets\MyMenu;

use yii\base\Widget;
use yii\helpers\Json;

/**
 * Menu widget
 * @package frontend\widgets\MyMenu
 */
class MyMenu extends Widget {

    public $items = [];
    public $options = [];
    public $view = 'main';

    /**
     * Renders the widget
     */
    public function run() {
        return $this->render($this->view, ['items' => $this->items, 'options' => $this->options]);
    }
    
    /**
     * Create data params for URL
     */
    public static function createUrlData($route, $params) {
        $data[] = $route;
        if(isset($params) && !empty($params)){
            $arrayParams = Json::decode($params);
            foreach($arrayParams as $key => $value){
                $data[$key] = $value;
            }
        }
        return $data;
    }
    /**
     * Initiate the widget
     *
     * @param $model Model
     * @param $view string View file
     * @param array $config Configuration array
     * @return string|void
     * @throws \Exception
     */
    public static function widget($config = []) {
        return parent::widget($config);
    }

}
