<?php 

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use frontend\widgets\MyMenu\MyMenu;

?>
<?php if ($items) { ?>
    <nav>
        <ul>
            <?php foreach ($items as $item) { ?>
            <li class="<?= \Yii::$app->controller->route == $item->route ? 'active' : '' ?>">
                <?= Html::a($item->label, MyMenu::createUrlData($item->route, $item->params)) ?>
            </li>
            <?php } ?>
        </ul>
    </nav>
<?php } ?>
