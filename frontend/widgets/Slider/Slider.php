<?php

namespace frontend\widgets\Slider;

use yii\base\Widget;
use yii\helpers\Json;
use hdmodules\carousel\models\Carousel;

/**
 * Slider widget
 * @package app\widgets\ContactForm
 */
class Slider extends Widget {

    public $items = [];
    public $options = [];
    public $view = 'main';
    public $slug;

    /**
     * Renders the widget
     */
    public function run() {

        $this->items = Carousel::getCarouselItemsBySlug($this->slug);
        return $this->render($this->view, ['items' => $this->items, 'options' => $this->options]);
    }
    
    /**
     * Create data params for URL
     */
    public function createUrlData($route, $params) {
        $data[] = $route;
        if(isset($params) && !empty($params)){
            $arrayParams = Json::decode($params);
            foreach($arrayParams as $key => $value){
                $data[$key] = $value;
            }
        }
        return $data;
    }
    /**
     * Initiate the widget
     *
     * @param $model Model
     * @param $view string View file
     * @param array $config Configuration array
     * @return string|void
     * @throws \Exception
     */
    public static function widget($config = []) {
        return parent::widget($config);
    }

}
