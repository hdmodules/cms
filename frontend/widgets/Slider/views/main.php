<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\Slider\Slider;
use hdmodules\carousel\models\CarouselItem;
?>
<?php if (isset($items) && $items) { ?>
    <section class="main-slider">
        <div class="container">
            <ul class="slider-big">

                <?php foreach ($items as $item) { ?>
                    <li>
                        <?php if ($item->type == CarouselItem::TYPE_DEFAULT) { ?>

                            <div class="slide">
                                <div class="illustration">
                                    <img src="<?= $item->image ?>" alt="">
                                </div>
                                <div class="details">
                                    <h2>
                                        <?= $item->title ?>
                                        <?= $item->title_url ? Html::a($item->title_url, $item->external_link ? $item->external_link : Slider::createUrlData($item->route, $item->params), ['class' => 'title-link', 'target' => $item->external_link ? '_blank' :  '']) : '' ?>
                                    </h2>
                                    <p><?= $item->text ?></p>
                                </div>
                            </div>

                        <?php } elseif ($item->type == CarouselItem::TYPE_IMAGE) { ?>

                            <div class="slide">
                                <div class="illustration">
                                    <img src="<?= $item->image ?>" alt="">
                                </div>
                            </div>

                        <?php } elseif ($item->type == CarouselItem::TYPE_BIG_IMAGE) { ?>

                            <div class="slide slide-big">
                                <img src="<?= $item->image ?>" alt="<?= $item->title ?>">
                                <?= Html::a(Yii::t('site', 'Learn more'), $item->external_link ? $item->external_link : Slider::createUrlData($item->route, $item->params), ['class' => 'game-link', 'target' => $item->external_link ? '_blank' :  '']) ?>
                            </div>

                        <?php } elseif ($item->type == CarouselItem::TYPE_BIG_IMAGE_GENERAL) { ?>

                            <div class="slide slide-big slide-big-general">
                                <img src="<?= $item->image ?>" alt="<?= $item->title ?>">
                                <?= Html::a(Yii::t('site', 'Learn more'), $item->external_link ? $item->external_link : Slider::createUrlData($item->route, $item->params), ['class' => 'game-link', 'target' => $item->external_link ? '_blank' :  '']) ?>
                            </div>
                        
                        <?php } ?>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </section>
<?php } ?>
