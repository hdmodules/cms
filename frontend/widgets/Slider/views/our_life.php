<?php 

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\Slider\Slider;
use hdmodules\carousel\models\CarouselItem;

?>
<?php if(isset($items) && $items){ ?>

	<ul class="games-list slider slider-dark">
            <?php foreach($items as $item){ ?>
		<li><img src="<?= $item->image ?>" alt="<?= $item->title ?>"></li>
            <?php } ?>
	</ul>

<?php } ?>
