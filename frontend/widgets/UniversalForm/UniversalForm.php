<?php
/**
 * UniversalForm widget
 * @package frontend\widgets\UniversalForm
 */

namespace frontend\widgets\UniversalForm;

use Yii;
use yii\base\Widget;
use common\models\Request;
use hdmodules\request\models\RequestType;

class UniversalForm extends Widget {

    public $view;

    public $type;

    public $title=null;

    public $button=null;

    public $button_class=null;
    
    public $key=null;

    public $request=null;

    public $model_id=null;
    
    public $form_class=null;
    
    public function run() {

        $model = $this->request ?  $this->request :  new Request() ;

        $model->setScenarioByType($this->type);

        return $this->render($this->view, [
            'model' => $model,
            'title'=>$this->title,
            'key'=>$this->key,
            'button'=>$this->button,
            'button_class'=>$this->button_class,
            'model_id'=>$this->model_id,
            'form_class'=>$this->form_class
        ]);
    }

    public static function widget($config = []) {
        return parent::widget($config);
    }
}