<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use hdmodules\base\widgets\ReCaptcha;
use yii\web\View;
use yii\validators\Validator;

$this->registerJs(
        '$("#contact-form").on("beforeSubmit", function (event) {
            $.ajax({
                type: "POST",
                url: "/service/save-universal-data",
                data: $("#contact-form").serialize(),
                dataType: "json",
                beforeSend:function () {
                    $("#contact-form").closest(".form-wrapper").addClass("loading");
                },
                error:function () {
                    $("#contact-form").closest(".form-wrapper").addClass("error");
                },
                success : function (data) {
                    if(data.status){
                        $("#contact-form").closest(".form-wrapper").removeClass("loading");
                        $("#contact-form").closest(".form-wrapper").addClass("success");
                    }
                }
            });
            return false;
        });', View::POS_READY);
?>

<div class="contact-form send-email">
    <div class="form-wrapper">


        <?php
        $form = ActiveForm::begin([
                    'id' => 'contact-form',
                    'options' => ['class' => 'form-dark'],
                    'action' => '/service/save-universal-data',
                    'enableClientValidation' => true,
                    'enableAjaxValidation' => false,
                    'validateOnBlur'=>false,
                    'errorCssClass' => 'error'
        ]);
        ?>
        <h2 class="title-line">
            <span><?= Yii::t('site', 'We are just a click away') ?></span>
        </h2>
        <p class="title-info"><?= Yii::t('site', 'We would really appreciate your feedback on both our website and games we offer.') ?></p>

        <?= $form->field($model, 'title')->hiddenInput(['value' => $title])->label(false) ?>
        <?= $form->field($model, 'type_id')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'page')->hiddenInput(['value' => Yii::$app->request->getUrl()])->label(false) ?>
        <?= $form->field($model, 'lang')->hiddenInput(['value' => Yii::$app->language])->label(false) ?>
                
        <div class="col-2">
            <?= $form->field($model, 'name', ['options' => ['class' => 'input', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag' => 'div'], 'template' => '{input} <span class="icon"><i class="fa fa-user"></i></span> {error}'])->input('text', ['placeholder' => Yii::t('site', 'Name') . ' *', 'class' => '']); ?>

            <?= $form->field($model, 'email', ['options' => ['class' => 'input', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag' => 'div'], 'template' => '{input} <span class="icon"><i class="fa fa-envelope-o"></i></span> {error}'])->input('text', ['placeholder' => Yii::t("site", 'Your e-mail') . ' *', 'class' => '']); ?>
        </div>
        <div class="col-2">
            <?= $form->field($model, 'text', ['options' => ['class' => 'input input-textarea', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag' => 'div'], 'template' => '{input} <span class="icon"><i class="fa fa-commenting-o"></i></span> {error}'])->textarea(['placeholder' => Yii::t("site", 'Comment') . ' *']); ?>
        </div>
        
        <p class="form-info"><span>*</span> <?= Yii::t('site', 'Required fields') ?></p>

        <div class="buttons">

            <?= $form->field($model, 'recaptcha', ['options' => ['class' => 'input input-recaptcha', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag' => 'div']])->widget(ReCaptcha::className(), ['theme' => ReCaptcha::THEME_LIGHT])->label(false); ?>

            <?= Html::submitButton(Yii::t('site', 'Send'), ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
        
        <div class="success-msg">
            <div class="info">
                <img src="/media/images/icon/icon-envelope-big.png" alt="">
                <h2><?= Yii::t('site', 'Your message has been sent successfully') ?></h2>
            </div>
        </div>
        <div class="error-msg">
            <div class="info">
                <img src="/media/images/icon/icon-envelope-big-error.png" alt="">
                <h2><?= Yii::t('site', 'Something went wrong!') ?></h2>
            </div>
        </div>

    </div>
</div>