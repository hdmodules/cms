<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use hdmodules\base\widgets\ReCaptcha;
use yii\web\View;

$this->registerJs('
    $("#lets-talk-form").on("beforeSubmit", function (event) {
        form = $(this);
        $.ajax({
            type: "POST",
            url: "/service/save-lets-talk",
            data: form.serialize(),
            dataType: "json",
            beforeSend: function(){               
                form.parent().addClass("loading");
            },
            error:function () {
            form.parent().removeClass("loading");
                form.parent().addClass("error");                
            },
            success : function (data) {     
                form.parent().removeClass("loading");
                if(data.status){
                    form.parent().addClass("success")
                }else{
                    form.parent().addClass("error");
                }
            },           
        });
        return false;
    });
', View::POS_READY); ?>

<?php $form = ActiveForm::begin([
    'id' => 'lets-talk-form',
    'options' => ['class' => 'form-dark'],
    'enableClientValidation'=>true,
    'validateOnBlur'=>false,
    'errorCssClass' => 'error'
]); ?>

    <p class="info-text"><?= Yii::t('site', 'Would you like to speak with an Endorphina representative? Just send us yourcontact information and we would be happy to call you shortly')?></p>

    <div class="col-2">
        <?= $form->field($model, 'title')->hiddenInput(['value' => $title])->label(false) ?>
        <?= $form->field($model, 'type_id')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'page')->hiddenInput(['value' => Yii::$app->request->getUrl()])->label(false) ?>
        <?= $form->field($model, 'lang')->hiddenInput(['value' => Yii::$app->language])->label(false) ?>

        <?= $form->field($model, 'name', ['options'=>['class'=>'input', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag'=>'div'], 'template' => '{input} <span class="icon"><i class="fa fa-user"></i></span> {error}'])->input('text', ['placeholder'=>Yii::t("site", 'name') . ' *', 'class' => '']); ?>
        <?= $form->field($model, 'email', ['options'=>['class'=>'input', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag'=>'div'], 'template' => '{input} <span class="icon"><i class="fa fa-envelope-o"></i></span> {error}'])->input('email', ['placeholder'=>Yii::t("site", 'e-mail address') . ' *', 'class' => '']); ?>
        <?= $form->field($model, 'phone', ['options'=>['class'=>'input', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag'=>'div'], 'template' => '{input} <span class="icon"><i class="fa fa-phone"></i></span> {error}'])->input('text', ['placeholder'=>Yii::t("site", 'tel number') . ' *', 'class' => '']); ?>
    </div>

    <div class="col-2">
        <?= $form->field($model, 'text', ['options'=>['class'=>'input input-textarea', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag'=>'div'], 'template' => '{input} <span class="icon"><i class="fa fa-commenting-o"></i></span> {error}'])->textArea(['placeholder'=>Yii::t("site", 'comment') . ' *', 'class' => '']); ?>
    </div>


    <div class="col-1">
        <p class="form-info"><span>*</span> <?= Yii::t('site', 'Required fields')?></p>

        <div class="buttons">
            <?= $form->field($model, 'recaptcha', ['options'=>['class'=>'input input-recaptcha', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag'=>'div']])->widget(ReCaptcha::className(), ['theme' => ReCaptcha::THEME_LIGHT])->label(false); ?>
            <?= Html::submitButton(Yii::t('site', 'Send'), ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>

