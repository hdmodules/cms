<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use hdmodules\base\widgets\ReCaptcha;
use yii\web\View;

$this->registerJs(
        '$("#subscription-form").on("beforeSubmit", function (event) {
            $.ajax({
                type: "POST",
                url: "/service/save-universal-data",
                data: $("#subscription-form").serialize(),
                dataType: "json",
                beforeSend:function () {
                    $("#subscription-form").closest(".form-wrapper").addClass("loading");
                },
                error:function () {
                    $("#subscription-form").closest(".form-wrapper").addClass("error");
                },
                success : function (data) {
                    if(data.status){
                        $("#subscription-form").closest(".form-wrapper").removeClass("loading");
                        $("#subscription-form").closest(".form-wrapper").addClass("success");
                    }
                }
            });
            return false;
        });', View::POS_READY);
?>
<div class="form-wrapper">
    <?php
    $form = ActiveForm::begin([
                'id' => 'subscription-form',
                'options' => ['class' => 'form-light'],
                'action' => '/service/save-universal-data',
                'enableClientValidation' => true,
                'validateOnBlur'=>false,
                'errorCssClass' => 'error'
    ]);
    ?>

    <?= $form->field($model, 'title')->hiddenInput(['value' => Yii::t('career', 'Jobs subscription')])->label(false) ?>
    <?= $form->field($model, 'type_id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'page')->hiddenInput(['value' => Yii::$app->request->getUrl()])->label(false) ?>
    <?= $form->field($model, 'lang')->hiddenInput(['value' => Yii::$app->language])->label(false) ?>

    <h3><?= Yii::t('career', 'Jobs subscription') ?></h3>

    <?= $form->field($model, 'email', ['options' => ['class' => 'input', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag' => 'div'], 'template' => '{input} <span class="icon"><i class="fa fa-envelope-o"></i></span> {error}'])->input('text', ['placeholder' => Yii::t("site", 'Your e-mail') . ' *', 'class' => '']); ?>

    <div class="buttons">

        <?= $form->field($model, 'recaptcha', ['options' => ['class' => 'input input-recaptcha', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag' => 'div']])->widget(ReCaptcha::className(), ['theme' => ReCaptcha::THEME_LIGHT])->label(false); ?>

        <?= Html::submitButton(Yii::t('site', 'Send'), ['class' => 'btn btn-'.$button_class]) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <div class="success-msg">
        <div class="info">
            <img alt="" src="/media/images/icon/icon-envelope.png">
            <strong><?= Yii::t('site', 'Thank you!') ?></strong>
            <p><?= Yii::t('site', 'We’ll get back to you soon') ?></p>
        </div>
    </div>
    <div class="error-msg">
        <div class="info">
            <img alt="" src="/media/images/icon/icon-envelope-error.png">
            <strong><?= Yii::t('site', 'Something went wrong!') ?></strong>
            <p><?= Yii::t('site', 'We’ll fix it as soon as possible.') ?></p>
        </div>
    </div>
</div>