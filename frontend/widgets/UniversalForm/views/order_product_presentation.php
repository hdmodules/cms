<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use hdmodules\base\widgets\ReCaptcha;
use yii\web\View;

$this->registerJs(
        '$("#order-product-presentation-form").on("beforeSubmit", function (event) {
            $.ajax({
                type: "POST",
                url: "/service/save-universal-data",
                data: $("#order-product-presentation-form").serialize(),
                dataType: "json",
                beforeSend:function () {
                    $(".product-presentation").addClass("loading");
                },
                error:function () {
                    $(".product-presentation").addClass("error");
                },
                success : function (data) {
                    if(data.status){
                        $(".product-presentation").removeClass("loading");
                        $(".product-presentation").addClass("success");
                    }
                }
            });
            return false;
        });', View::POS_READY);
?>

<div class="container stage-02">
    <?php
    $form = ActiveForm::begin([
                'id' => 'order-product-presentation-form',
                'options' => ['class' => ''],
                'action' => '/service/save-universal-data',
                'enableClientValidation' => true,
                'enableAjaxValidation' => false,
                'validateOnBlur'=>false,
                'errorCssClass' => 'error'
    ]);
    ?>

    <?= $form->field($model, 'title')->hiddenInput(['value' => $title])->label(false) ?>
    <?= $form->field($model, 'type_id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'page')->hiddenInput(['value' => Yii::$app->request->getUrl()])->label(false) ?>
    <?= $form->field($model, 'lang')->hiddenInput(['value' => Yii::$app->language])->label(false) ?>

    <?= $form->field($model, 'name', ['options' => ['class' => 'input', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag' => 'div'], 'template' => '{input} <span class="icon"><i class="fa fa-user"></i></span> {error}'])->input('text', ['placeholder' => Yii::t('site', 'Name') . ' *', 'class' => '']); ?>

    <?= $form->field($model, 'email', ['options' => ['class' => 'input', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag' => 'div'], 'template' => '{input} <span class="icon"><i class="fa fa-envelope-o"></i></span> {error}'])->input('text', ['placeholder' => Yii::t("site", 'Your e-mail') . ' *', 'class' => '']); ?>

    <p class="form-info"><span>*</span> <?= Yii::t('site', 'Required fields') ?></p>

    <div class="buttons">

        <?= $form->field($model, 'recaptcha', ['options' => ['class' => 'input input-recaptcha', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag' => 'div']])->widget(ReCaptcha::className(), ['theme' => ReCaptcha::THEME_LIGHT])->label(false); ?>

        <?= Html::submitButton(Yii::t('site', 'Send'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>			
</div>
<div class="container stage-03">
    <img src="/media/images/icon/icon-envelope.png" alt="">
    <strong><?= Yii::t('site', 'Thank you!') ?></strong>
    <p><?= Yii::t('site', 'We’ll get back to you soon') ?></p>
</div>
<div class="container stage-04">
    <img alt="" src="/media/images/icon/icon-envelope-error.png">
    <strong><?= Yii::t('site', 'Something went wrong!') ?></strong>
    <p><?= Yii::t('site', 'We’ll fix it as soon as possible.') ?></p>
</div>
