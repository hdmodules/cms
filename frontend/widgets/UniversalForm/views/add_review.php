<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use hdmodules\base\widgets\ReCaptcha;
use yii\web\View;

$this->registerJs('
    $("#add-review-form").on("beforeSubmit", function (event) {
        form = $(this);
        $.ajax({
            type: "POST",
            url: "/service/save-review",
            data: $("#add-review-form").serialize(),
            dataType: "json",
            beforeSend: function(){               
                form.parent().addClass("loading");
            },
            error:function () {
                form.parent().removeClass("loading");
                form.parent().addClass("error");                
            },
            success : function (data) {   
                form.parent().removeClass("loading");
                if(data.status){
                    $("#add-review-form")[0].reset();
                    form.parent().addClass("success")
                }else{
                    form.parent().addClass("error");
                }
            }           
        });
        return false;
    });
', View::POS_READY); ?>

<div class="form-wrapper">



    <?php $form = ActiveForm::begin([
        'id' => 'add-review-form',
        'options' => ['class' => 'form-dark'],
        'action' => '/service/save-review',
        'enableClientValidation'=>true,
        'validateOnBlur'=>false,
        'errorCssClass' => 'error'
    ]); ?>

    <h2><?= Yii::t('site', 'Add Review') ?></h2>

    <?= $form->field($model, 'title')->hiddenInput(['value' => $title])->label(false) ?>
    <?= $form->field($model, 'type_id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'page')->hiddenInput(['value' => Yii::$app->request->getUrl()])->label(false) ?>
    <?= $form->field($model, 'lang')->hiddenInput(['value' => Yii::$app->language])->label(false) ?>
    <?= $form->field($model, 'model_id')->hiddenInput(['value' => $model_id])->label(false) ?>
    <?= $form->field($model, 'type_review')->hiddenInput()->label(false) ?>



    <?= $form->field($model, 'name', ['options'=>['class'=>'input', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag'=>'div'], 'template' => '{input} <span class="icon"><i class="fa fa-user"></i></span> {error}'])->input('text', ['placeholder'=>Yii::t("site", 'name') . ' *', 'class' => '']); ?>

    <?= $form->field($model, 'email', ['options'=>['class'=>'input', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag'=>'div'], 'template' => '{input} <span class="icon"><i class="fa fa-envelope-o"></i></span> {error}'])->input('text', ['placeholder'=>Yii::t("site", 'e-mail address') . ' *', 'class' => '']); ?>

    <?= $form->field($model, 'url', ['options'=>['class'=>'input', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag'=>'div'], 'template' => '{input} <span class="icon"><i class="fa fa-link"></i></span> {error}'])->input('text', ['placeholder'=>Yii::t("site", 'review url') . ' *', 'class' => '']); ?>

    <p class="form-info"><span>*</span> <?= Yii::t('site', 'Required fields') ?></p>

    <div class="buttons">

        <?= $form->field($model, 'recaptcha', ['options'=>['class'=>'input input-recaptcha', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag'=>'div']])->widget(ReCaptcha::className(), ['theme' => ReCaptcha::THEME_LIGHT])->label(false); ?>

        <?= Html::submitButton(Yii::t('site', 'Send'), ['class' => 'btn btn-'.$button_class]) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <div class="success-msg">
        <div class="info">
            <img src="/media/images/icon/icon-envelope-big.png" alt="">
            <h2><?= Yii::t('site', 'Your message has been sent successfully') ?></h2>
        </div>
    </div>
    <div class="error-msg">
        <div class="info">
            <img src="/media/images/icon/icon-envelope-big-error.png" alt="">
            <h2><?= Yii::t('site', 'Something went wrong!') ?></h2>
        </div>
    </div>
</div>