<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use hdmodules\base\widgets\ReCaptcha;
use yii\web\View;
use yii\validators\Validator;

$this->registerJs(
    '$("#error-form").on("beforeSubmit", function (event) {
            $.ajax({
                type: "POST",
                url: "/service/save-universal-data",
                data: $("#error-form").serialize(),
                dataType: "json",
                beforeSend:function () {
                    $("#error-form").closest(".form-wrapper").addClass("loading");
                },
                error:function () {
                    $("#error-form").closest(".form-wrapper").addClass("error");
                },
                success : function (data) {
                    if(data.status){
                        $("#error-form").closest(".form-wrapper").removeClass("loading");
                        $("#error-form").closest(".form-wrapper").addClass("success");
                    }
                }
            });
            return false;
        });', View::POS_READY);

?>



    <?php
    $form = ActiveForm::begin([
                'id' => 'error-form',
                'options' => ['class' => $form_class],
                'action' => '/service/save-universal-data',
                'enableClientValidation' => true,
                'enableAjaxValidation' => false,
                'validateOnBlur' => false,
                'errorCssClass' => 'error'
    ]);
    ?>
<h2 class="title-dots">
    <span><?= Yii::t('error', 'Report this issue') ?></span>
</h2>
<p class="title-info"><?= Yii::t('error', 'If you are seeing this, please let us know.') ?></p>
    <?= $form->field($model, 'title')->hiddenInput(['value' => $title])->label(false) ?>
    <?= $form->field($model, 'type_id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'page')->hiddenInput(['value' => Yii::$app->request->getUrl()])->label(false) ?>
    <?= $form->field($model, 'lang')->hiddenInput(['value' => Yii::$app->language])->label(false) ?>

    <?= $form->field($model, 'name', ['options' => ['class' => 'input', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag' => 'div'], 'template' => '{input} <span class="icon"><i class="fa fa-user"></i></span> {error}'])->input('text', ['placeholder' => Yii::t('site', 'Name') . ' *', 'class' => '']); ?>
    <?= $form->field($model, 'email', ['options' => ['class' => 'input', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag' => 'div'], 'template' => '{input} <span class="icon"><i class="fa fa-user"></i></span> {error}'])->input('text', ['placeholder' => Yii::t('site', 'Email') . ' *', 'class' => '']); ?>

    <?= $form->field($model, 'text', ['options' => ['class' => 'input input-textarea', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag' => 'div'], 'template' => '{input} <span class="icon"><i class="fa fa-commenting-o"></i></span> {error}'])->textarea(['placeholder' => Yii::t("site", 'Comment') . ' *']); ?>

    <p class="form-info"><span>*</span> Required fields</p>

    <div class="buttons">

        <?= $form->field($model, 'recaptcha', ['options' => ['class' => 'input input-recaptcha', 'tag' => 'div'], 'errorOptions' => ['class' => 'error-message', 'tag' => 'div']])->widget(ReCaptcha::className(), ['theme' => ReCaptcha::THEME_LIGHT])->label(false); ?>

        <?= Html::submitButton(Yii::t('site', 'Send'), ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>


