$(document).ready(function () {

    initTabs();

    /*DOTDOTDOT*/
    $.fn.initDotDotDot = function () {
        $('.ellipsis').dotdotdot();
    };

    /*VIEWPORT*/
    $("html head").append('<meta id="viewport" name="viewport" content="width=device-width, initial-scale=0.666666666, maximum-scale=2, minimum-scale=0.666666666">');

    $.fn.viewport = function () {
        if ($(window).width() < 558) {
            $("#viewport").attr("content", "width=device-width, initial-scale=0.666666666, maximum-scale=2, minimum-scale=0.666666666");
        } else {
            $("#viewport").attr("content", "width=device-width, initial-scale=1, maximum-scale=2, minimum-scale=1");
        }
    };

    $(window).viewport();

    /*SLICK*/
    $(".people-info ul").slick({
        draggable: true,
        dots: false,
        autoplay: true,
        autoplaySpeed: 5000,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        responsive: [
            {
                breakpoint: 5000,
                settings: 'unslick'
            },
            {
                breakpoint: 550
            }
        ]
    });

    $(".sponsors-slider .slider").slick({
        draggable: true,
        dots: false,
        autoplay: true,
        autoplaySpeed: 5000,
        rows: 4,
        slidesPerRow: 12,
        prevArrow: "<button type='button' class='slick-arrow fa fa-angle-left'></button>",
        nextArrow: "<button type='button' class='slick-arrow fa fa-angle-right'></button>",
        responsive: [
            {
                breakpoint: 900,
                settings: {
                    slidesPerRow: 10
                }
            },
            {
                breakpoint: 700,
                settings: {
                    slidesPerRow: 8
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesPerRow: 6
                }
            },
            {
                breakpoint: 500,
                settings: {
                    slidesPerRow: 4
                }
            }
        ]
    });

    $(".people-slider .slider").slick({
        draggable: true,
        dots: false,
        autoplay: true,
        autoplaySpeed: 5000,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: "<button type='button' class='slick-arrow fa fa-angle-left'></button>",
        nextArrow: "<button type='button' class='slick-arrow fa fa-angle-right'></button>"
    });

    /*TABS*/
    $(document).on("click", "[data-tab-group]", function () {
        var group = $(this).attr("data-tab-group");
        var item = $(this).attr("data-tab-link");
        toggleTab(group, item);
    });

    function initTabs() {
        $("[data-tab]").each(function () {
            var wrapper = $(this);
            wrapper.find("[data-tab-group]").each(function (index) {
                var item = $(this).attr("data-tab-link");

                if (index != 0) {
                    wrapper.find("[data-tab-item='" + item + "']").hide();
                }
            })
        })
    }

    function toggleTab(wrapper, tab) {
        var el = $("[data-tab='" + wrapper + "']");
        el.find("[data-tab-item]").hide();
        el.find("[data-tab-item='" + tab + "']").show();
        el.find("[data-tab-group]").removeClass("active");
        el.find("[data-tab-group][data-tab-link='" + tab + "']").addClass("active");
    }

    /*DAYS TABS*/
    $(".program .days .tab-day").on("click", function () {
        var whichTab = $(this).attr('data-type');
        $(".program .days .tab-day").each(function (key, thisLi) {
            $(thisLi).removeClass("open");
        });
        $(this).addClass("open");
        $(".program .tab").each(function (key, thisTab) {
            $(thisTab).find(".program-list").each(function (key, thisList) {
                $(thisList).removeClass("open");
            });
        });
        $(".program .tab .program-list." + whichTab + "").addClass("open");
    });

    /*H0VER*/
    $(".main-page .news .illustration, .main-page .news .details strong a").on("mouseover", function () {
        $(this).closest("li").addClass("hover");
    }).on("mouseout", function () {
        $(this).closest("li").removeClass("hover");
    });

    /*SPONSOR PACKS*/
    $.fn.checkResolution = function () {
        participantsColumns = null;

        $('.pack-detail').remove();
        var $sponsorPack = $(".sponsor-page .sponsor-packs > section");
        $sponsorPack.each(function (key, thisA) {
            $(thisA).removeClass('active');
        });

        var windowWidth = window.innerWidth;

            if ((windowWidth >= 900)) {
            packsColumns = 3;
        } else if ((windowWidth >= 550)) {
            packsColumns = 2;
        } else {
            packsColumns = 1;
        }

        $sponsorPack.each(function (key, item) {
            $(item).removeClass('last');
        });

        $(".sponsor-page .sponsor-packs > section:nth-of-type(" + packsColumns + "n), .sponsor-page .sponsor-packs > section:last-of-type").addClass("last");

    };

    $(window).checkResolution();

    $('.sponsor-page .sponsor-packs > section').on('click', function (e) {
        e.preventDefault();
        $('.pack-detail').remove();
        $(this).siblings('.active').removeClass('active');
        $(this).addClass('active');
        if ($(this).hasClass('last')) {
            $(this).after("<div class='pack-detail'><button class='close'></button></div>");
            $(this).find('.pack-info').clone().appendTo('.pack-detail');
        } else {
            $(this).nextAll('.last:first').after("<div class='pack-detail'><button class='close'></button></div>");
            $(this).find('.pack-info').clone().appendTo('.pack-detail');
        }
    });

    $('.sponsor-packs').on('click', 'button.close', function (e) {
        $('.sponsor-page .sponsor-packs > section').each(function () {
            $(this).removeClass('active');
        });
        $('.pack-detail').remove();
    });

    /*WHICH POPUP*/
    $('.btn-popup').on('click', function (e) {
        e.preventDefault();
        var popupType = $(this).attr('data-type');
        var whichPopup = $(".popup." + popupType + "");
        $('.popup-overlay').addClass('active');
        whichPopup.addClass('active');
        if (popupType === "popup-bio") {
            var $thisLi = $(this).closest("li");
            $thisLi.find(".illustration img").clone().insertBefore(".popup-bio .info");
            $thisLi.find(".details strong, .details span, .details .logo").clone().prependTo(".popup-bio .info");
            $thisLi.find(".details .text").clone().appendTo(".popup-bio");
        } else if (popupType === "popup-meeting") {
            $(this).closest("li").find(".illustration img, .details strong, .details span").clone().appendTo(".popup-meeting header");
        }
    });

    /*POPUP CLOSE*/
    $("button.close").on("click", function () {
        $(".popup-overlay").removeClass('active').find(".popup").removeClass('active');
        if ($(this).closest(".popup").hasClass("popup-bio")) {
            $(".popup.popup-bio").find(".photo, .info strong, .info span, .info .logo, .text").remove();
        } else if ($(this).closest(".popup").hasClass("popup-meeting")) {
            $(".popup.popup-meeting").find(".photo, header strong, header span").remove();
        }
    });

    $(".popup-overlay").on("click", function (data, handler) {
        if (data.target == this) {
            var $activePopup = $(".popup.active");
            if ($activePopup.hasClass("popup-bio")) {
                $(".popup.popup-bio").find(".photo, .info strong, .info span, .info .logo, .text").remove();
            } else if ($activePopup.hasClass("popup-meeting")) {
                $(".popup.popup-meeting").find(".photo, header strong, header span").remove();
            }
            $(this).removeClass('active').find(".popup").removeClass('active');
        }
    });

    /*POPUP RADIO BUTTON*/
    $(".time-intervals .time time").on("click", function () {
        var $thisTime = $(this).closest(".time");
        $thisTime.siblings().removeClass("active");
        $thisTime.addClass("active");
        $thisTime.find('input[type="radio"]').prop("checked", true);
        $thisTime.siblings().each(function (key, thisInput) {
            $(thisInput).find('input[type="submit"]').attr("disabled", "disabled");
        });
        $thisTime.find('input[type="submit"]').removeAttr("disabled");
    });

    /*OPEN MENU*/
    $(".btn-mobile-menu").on("click", function () {
        if ($(this).hasClass("open")) {
            $(this).removeClass("open");
            $("header .menu").removeClass("open");
        } else {
            $(this).addClass("open");
            $("header .menu").addClass("open");
        }
    });

    /*MENU CLASS*/
    $(".menu nav > ul > li").each(function (key, thisLi) {
        if ($(thisLi).find("ul").length) {
            $(thisLi).addClass("roll");
        }
    });

    /*MOBILE MENU ROLL*/
    $(".menu nav > ul > li > a").on("click", function (e) {
        var windowWidth = window.innerWidth;

        if ((windowWidth < 700) && ($(this).parent().hasClass("roll"))) {
            e.preventDefault();
            if ($(this).parent().hasClass("open")) {
                $(this).parent().removeClass("open");
            } else {
                $(this).parent().addClass("open");
            }
        }
    });

    /*RESIZE ONLY X*/
    var width = $(window).width();

    $(window).resize(function () {
        if ($(window).width() == width) return;
        width = $(window).width();
        $(window).checkResolution();
        $(window).initDotDotDot();
    });

});

$(window).load(function () {

    $(window).initDotDotDot();

    $(".program .tab").each(function (key, thisTab) {
        $(thisTab).show();
        $(thisTab).find(".program-list").each(function (key, thisList) {
            var thisHeight = $(thisList).outerHeight();
            $(thisList).css("margin-top", -thisHeight);
        });
        if (!$(thisTab).hasClass("active")) {
            $(thisTab).hide();
        }
    });

});

$(window).on('scroll', function () {

});

$(window).on('resize', function () {

    $(window).viewport();
});

$(window).on('orientationchange', function () {

    $(window).initDotDotDot();
    $(window).viewport();
    $(window).checkResolution();

});