<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL ^ E_STRICT ^ E_DEPRECATED ^E_NOTICE);
//ini_set('display_errors', 'On');
//ini_set('display_startup_errors', 'On');
//error_reporting(E_ALL);

defined('LOCALHOST') or define('LOCALHOST', $_SERVER['REMOTE_ADDR'] == "127.0.0.1");

if(LOCALHOST){
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    defined('YII_ENV') or define('YII_ENV', 'dev');
} else {
    defined('YII_DEBUG') or define('YII_DEBUG', false);
    defined('YII_ENV') or define('YII_ENV', 'prod');
}

//Redirect from www to without
if(isset($_SERVER['HTTP_HOST']) && preg_match('(^www2.)', $_SERVER['HTTP_HOST'])){
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: https://endorphina.com".$_SERVER['REQUEST_URI']);
	exit();
}
if(isset($_SERVER['HTTP_HOST']) && preg_match('(^www.)', $_SERVER['HTTP_HOST'])){
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: https://endorphina.com".$_SERVER['REQUEST_URI']);
	exit();
}
require(__DIR__ . '/../../vendor/autoload.php');
require(__DIR__ . '/../../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../../common/config/bootstrap.php');
require(__DIR__ . '/../config/bootstrap.php');

if(LOCALHOST){
    $config = yii\helpers\ArrayHelper::merge(
        require(__DIR__ . '/../../common/config/main.php'),
        require(__DIR__ . '/../../common/config/main-local.php'),
        require(__DIR__ . '/../config/main.php'),
        require(__DIR__ . '/../config/main-local.php')
    );
} else {
    $config = yii\helpers\ArrayHelper::merge(
        require(__DIR__ . '/../../common/config/main.php'),
        require(__DIR__ . '/../config/main.php')
    );
}

$application = new yii\web\Application($config);
$application->run();
