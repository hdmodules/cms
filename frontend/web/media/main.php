<!DOCTYPE html>
<html lang="ru">

<?php include ("head.html"); ?>

<body>

<?php include("header-main.html"); ?>

<main class="page-main">

	<section class="main-slider">
		<div class="container">
			<ul class="slider-big">
				<li>
					<div class="slide slide-big slide-big-general">
						<img src="images/slider/endorphina.png" alt="">
						<a class="game-link" href="">learn more</a>
					</div>
				</li>
				<li>
					<div class="slide slide-big">
						<img src="images/slider/DiamondVapor_banner.jpg" alt="">
						<a class="game-link" href="">learn more</a>
					</div>
				</li>
				<li>
					<div class="slide slide-big">
						<img src="images/slider/Jetsetter_banner.jpg" alt="">
						<a class="game-link" href="">learn more</a>
					</div>
				</li>
				<li>
					<div class="slide slide-big">
						<img src="images/slider/Maori_banner.jpg" alt="">
						<a class="game-link" href="">learn more</a>
					</div>
				</li>
				<li>
					<div class="slide slide-big">
						<img src="images/slider/twerk_banner.jpg" alt="">
						<a class="game-link" href="">learn more</a>
					</div>
				</li>
				<li>
					<div class="slide slide-big">
						<img src="images/slider/Voodoo_banner.jpg" alt="">
						<a class="game-link" href="">learn more</a>
					</div>
				</li>
				<li>
					<div class="slide">
						<div class="illustration">
							<img src="images/logo/logo-main-big.png" alt="">
						</div>
						<div class="details">
							<h2>endorphina <a class="title-link" href="">about us</a></h2>
							<p>Endorphina is an online gambling content provider.The Endorphina team of highly qualified
								professionals unites the best online gaming experience and cutting-edge technology to
								create.....</p>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</section>

	<h1>main page</h1>

	<section class="games-categories">
		<div class="container">
			<ul class="tiles">
				<li class="block-01">
					<a href="">
						<img src="images/game/game-group-01.jpg" alt="">
						<h3>Fruit Games</h3>
						<p>just play</p>
					</a>
				</li>
				<li class="block-02">
					<a href="">
						<img src="images/game/game-group-02.jpg" alt="">
						<h3>Horror Games</h3>
						<p>just play</p>
					</a>
				</li>
				<li class="block-01">
					<a href="">
						<img src="images/game/game-group-03.jpg" alt="">
						<h3>3d Games</h3>
						<p>just play</p>
					</a>
				</li>
				<li class="block-03">
					<a href="">
						<img src="images/game/game-group-04.jpg" alt="">
						<h3>Unique Games</h3>
						<p>just play</p>
					</a>
				</li>
				<li class="block-01">
					<a href="">
						<img src="images/game/game-ninja.png" alt="">
						<img src="images/game/game-ninja.gif" alt="">
						<h3>Cute Games</h3>
						<p>just play</p>
					</a>
				</li>
				<li class="block-01">
					<a href="">
						<img src="images/game/game-group-06.jpg" alt="">
						<h3>Country Related</h3>
						<p>just play</p>
					</a>
				</li>
				<li class="block-03">
					<a href="">
						<img src="images/game/game-group-07.jpg" alt="">
						<h3>Oriental Games</h3>
						<p>just play</p>
					</a>
				</li>
				<li class="block-02">
					<a href="">
						<img src="images/game/game-group-08.jpg" alt="">
						<h3>Classic Games</h3>
						<p>just play</p>
					</a>
				</li>
			</ul>
		</div>
	</section>

	<section class="product-presentation">
		<div id="plexus01" class="plexus"></div>

		<div class="container stage-01">
			<button class="btn btn-default" type="button">order product presentation</button>
			<p>Receive full product catalogue straight to your email</p>
		</div>
		<div class="container stage-02">
			<form action="">
				<div class="input error">
					<input type="text" placeholder="name*">
					<span class="icon">
						<i class="fa fa-user"></i>
					</span>

					<div class="error-message">Input cannot be blank.</div>
				</div>
				<div class="input">
					<input type="email" placeholder="e-mail*">
					<span class="icon">
						<i class="fa fa-envelope-o"></i>
					</span>

					<div class="error-message">Input cannot be blank.</div>
				</div>
				<p class="form-info"><span>*</span> Required fields</p>
				<div class="buttons">
					<div class="input input-recaptcha">
						<!--<input type="hidden" id="subscriber-recaptcha-1" name="Subscriber[reCaptcha]">-->
						<!--<div id="recaptcha-1" data-sitekey="6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU"-->
						<!--data-recaptcha-object="recaptcha2" data-callback="recaptchaCallback">-->
						<!--<div>-->
						<!--<div style="width: 304px; height: 78px;">-->
						<!--<iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU&amp;co=aHR0cDovL3ByZXByb2QuY2FzZXhlLmNvbTo4MA..&amp;hl=en&amp;v=r20160502112552&amp;theme=light&amp;size=normal&amp;cb=ftudccmhzbdi"-->
						<!--title="recaptcha widget" width="304" height="78" role="presentation"-->
						<!--frameborder="0" scrolling="no" name="undefined"></iframe>-->
						<!--</div>-->
						<!--<textarea id="g-recaptcha-response-1" name="g-recaptcha-response"-->
						<!--class="g-recaptcha-response"-->
						<!--style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea>-->
						<!--</div>-->
						<!--</div>-->
						<!--<p class="error-message"></p>-->
					</div>
					<button class="btn btn-default" type="submit">send</button>
				</div>
			</form>
		</div>
		<div class="container stage-03">
			<img src="images/icon/icon-envelope.png" alt="">
			<strong>thank you!</strong>
			<p>We’ll get back to you soon</p>
		</div>
		<div class="container stage-04">
			<img src="images/icon/icon-envelope-error.png" alt="">
			<strong>Something went wrong!!!</strong>
			<p>We’ll fix it as soon as possible.</p>
		</div>
	</section>

	<section class="news">
		<div class="container">
			<h2 class="title-line">
				<span>news</span>
				<a href="">all news</a>
			</h2>
			<!--Add class="slider" to init slick.-->
			<ul class="articles-list slider slider-dark">
				<li>
					<a href=""><img src="images/article/article-square.png" alt=""></a>
					<div class="details ellipsis">
						<h3><a href="">Endorphina is heading to Sweden</a></h3>
						<p>i-Gaming Forumin Stockholm is the next stop on our unmissable events calendar. We are
							thrilled to announce that Endorphina has become a partner of this even</p>
					</div>
					<a class="read-more" href="">Read More</a>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a href=""><img src="images/article/article-square.png" alt=""></a>
					<div class="details ellipsis">
						<h3><a href="">Endorphina is heading to Sweden</a></h3>
						<p class="ellipsis">i-Gaming Forumin Stockholm is the next stop on our unmissable events
							calendar. We are thrilled to announce that Endorphina has become a partner of this even</p>
					</div>
					<a class="read-more" href="">Read More</a>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a href=""><img src="images/article/article-square.png" alt=""></a>
					<div class="details ellipsis">
						<h3><a href="">Endorphina is heading to Sweden</a></h3>
						<p class="ellipsis">i-Gaming Forumin Stockholm is the next stop on our unmissable events
							calendar. We are thrilled to announce that Endorphina has become a partner of this even</p>
					</div>
					<a class="read-more" href="">Read More</a>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a href=""><img src="images/article/article-square.png" alt=""></a>
					<div class="details ellipsis">
						<h3><a href="">Endorphina is heading to Sweden</a></h3>
						<p class="ellipsis">i-Gaming Forumin Stockholm is the next stop on our unmissable events
							calendar. We are thrilled to announce that Endorphina has become a partner of this even</p>
					</div>
					<a class="read-more" href="">Read More</a>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a href=""><img src="images/article/article-square.png" alt=""></a>
					<div class="details ellipsis">
						<h3><a href="">Endorphina is heading to Sweden</a></h3>
						<p class="ellipsis">i-Gaming Forumin Stockholm is the next stop on our unmissable events
							calendar. We are thrilled to announce that Endorphina has become a partner of this even</p>
					</div>
					<a class="read-more" href="">Read More</a>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
			</ul>
		</div>
	</section>

	<section class="subscriber">
		<div class="container">
			<div class="form-wrapper">
				<form class="form-light" action="">
					<h3>news subscription</h3>
					<p>Be the first to know about upcoming releases, our new partners, industry events and a lot
						more!</p>
					<div class="input">
						<input type="text" placeholder="e-mail address">
					<span class="icon">
						<i class="fa fa-envelope-o"></i>
					</span>

						<div class="error-message">Input cannot be blank.</div>
					</div>
					<div class="buttons">
						<div class="input input-recaptcha">
							<!--<input type="hidden" id="subscriber-recaptcha-2" name="Subscriber[reCaptcha]">-->
							<!--<div id="recaptcha-2" data-sitekey="6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU"-->
							<!--data-recaptcha-object="recaptcha2" data-callback="recaptchaCallback">-->
							<!--<div>-->
							<!--<div style="width: 304px; height: 78px;">-->
							<!--<iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU&amp;co=aHR0cDovL3ByZXByb2QuY2FzZXhlLmNvbTo4MA..&amp;hl=en&amp;v=r20160502112552&amp;theme=light&amp;size=normal&amp;cb=ftudccmhzbdi"-->
							<!--title="recaptcha widget" width="304" height="78" role="presentation"-->
							<!--frameborder="0" scrolling="no" name="undefined"></iframe>-->
							<!--</div>-->
							<!--<textarea id="g-recaptcha-response-2" name="g-recaptcha-response"-->
							<!--class="g-recaptcha-response"-->
							<!--style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea>-->
							<!--</div>-->
							<!--</div>-->
							<!--<p class="error-message"></p>-->
						</div>
						<button class="btn btn-primary" type="submit">send</button>
					</div>
				</form>
				<div class="success-msg">
					<div class="info">
						<img src="images/icon/icon-envelope.png" alt="">
						<strong class="text-light">thank you!</strong>
						<p class="text-light">We’ll get back to you soon</p>
					</div>
				</div>
				<div class="error-msg">
					<div class="info">
						<img src="images/icon/icon-envelope-error.png" alt="">
						<strong class="text-light">Something went wrong!!!</strong>
						<p class="text-light">We’ll fix it as soon as possible.</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="games-news">
		<div class="container">
			<h2 class="title-line">
				<span>new games</span>
				<a href="">all games</a>
			</h2>
			<ul class="games-list new-games-list slider slider-dark">
				<li>
					<a class="game" href="">
						<img src="images/article/square-gif.gif" alt="game">
						<div class="hover">
							<!--<div class="icon-play">-->
							<!--<img src="images/icon/icon-play.png" alt="play">-->
							<!--<span>Play now</span>-->
							<!--</div>-->
						</div>
					</a>
					<!--<h3>Devil’s fortune</h3>-->
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-02.jpg" alt="game">
						<div class="hover">
							<!--<div class="icon-play">-->
							<!--<img src="images/icon/icon-play.png" alt="play">-->
							<!--<span>Play now</span>-->
							<!--</div>-->
						</div>
					</a>
					<!--<h3>the ninja</h3>-->
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-01.jpg" alt="game">
						<div class="hover">
							<!--<div class="icon-play">-->
							<!--<img src="images/icon/icon-play.png" alt="play">-->
							<!--<span>Play now</span>-->
							<!--</div>-->
						</div>
					</a>
					<!--<h3>safari</h3>-->
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-02.jpg" alt="game">
						<div class="hover">
							<!--<div class="icon-play">-->
							<!--<img src="images/icon/icon-play.png" alt="play">-->
							<!--<span>Play now</span>-->
							<!--</div>-->
						</div>
					</a>
					<!--<h3>twerk</h3>-->
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-01.jpg" alt="game">
						<div class="hover">
							<!--<div class="icon-play">-->
							<!--<img src="images/icon/icon-play.png" alt="play">-->
							<!--<span>Play now</span>-->
							<!--</div>-->
						</div>
					</a>
					<!--<h3>safari</h3>-->
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-02.jpg" alt="game">
						<div class="hover">
							<!--<div class="icon-play">-->
							<!--<img src="images/icon/icon-play.png" alt="play">-->
							<!--<span>Play now</span>-->
							<!--</div>-->
						</div>
					</a>
					<!--<h3>twerk bitch</h3>-->
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
			</ul>
		</div>
	</section>

	<section class="partners">
		<div class="container">
			<h2 class="title-line">
				<span>Where to find us</span>
			</h2>
			<ul>
				<li class="partner">
					<a href=""><img src="images/partner/partner-01.png" alt=""></a>
				</li>
				<li class="partner">
					<a href=""><img src="images/partner/partner-02.png" alt=""></a>
				</li>
				<li class="partner">
					<a href=""><img src="images/partner/partner-01.png" alt=""></a>
				</li>
				<li class="partner">
					<a href=""><img src="images/partner/partner-02.png" alt=""></a>
				</li>
				<li class="partner">
					<a href=""><img src="images/partner/partner-01.png" alt=""></a>
				</li>
				<li class="partner">
					<a href=""><img src="images/partner/partner-01.png" alt=""></a>
				</li>
				<li class="partner">
					<a href=""><img src="images/partner/partner-02.png" alt=""></a>
				</li>
				<li class="partner">
					<a href=""><img src="images/partner/partner-01.png" alt=""></a>
				</li>
			</ul>
		</div>
	</section>

	<section class="advantages">
		<div class="container">
			<!--<ul data-animation="anim01">-->
			<ul>
				<li>
					<div class="circle">
						<img src="images/gif/icon-gamepad.gif" alt="icon">
						<img class="gif" src="images/gif/icon-gamepad.png" alt="icon">
					</div>
					<h3>50+ Games</h3>
				</li>
				<li>
					<div class="circle">
						<img src="images/gif/icon-html5.gif" alt="icon">
						<img class="gif" src="images/gif/icon-html5.png" alt="icon">
					</div>
					<h3>HTML5</h3>
				</li>
				<li>
					<div class="circle">
						<img src="images/gif/icon-star.gif" alt="icon">
						<img class="gif" src="images/gif/icon-star.png" alt="icon">
					</div>
					<h3>Certification</h3>
				</li>
				<li>
					<div class="circle">
						<img src="images/gif/icon-badge.gif" alt="icon">
						<img class="gif" src="images/gif/icon-badge.png" alt="icon">
					</div>
					<h3>Games Quality</h3>
				</li>
			</ul>
		</div>
	</section>

	<section class="send-email">
		<div id="plexus02" class="plexus"></div>

		<div class="container">

			<h2 class="title-line title-light title-bg">
				<span>drop us a line</span>
				<strong class="line"></strong>
			</h2>
			<p class="title-info">Get in touch with us to get the ball rolling</p>
			<div class="form-wrapper">
				<form class="form-light" action="">
					<div class="input error">
						<input type="text" placeholder="name*">
					<span class="icon">
						<i class="fa fa-user"></i>
					</span>

						<div class="error-message">Input cannot be blank.</div>
					</div>
					<div class="input">
						<input type="email" placeholder="e-mail address*">
					<span class="icon">
						<i class="fa fa-envelope-o"></i>
					</span>

						<div class="error-message">Input cannot be blank.</div>
					</div>
					<div class="input input-textarea">
						<textarea placeholder="comment*"></textarea>
					<span class="icon">
						<i class="fa fa-commenting-o"></i>
					</span>

						<div class="error-message">Input cannot be blank.</div>
					</div>
					<p class="form-info"><span>*</span> Required fields</p>

					<div class="buttons">
						<div class="input input-recaptcha">
							<!--<input type="hidden" id="subscriber-recaptcha-3" name="Subscriber[reCaptcha]">-->
							<!--<div id="recaptcha-3" data-sitekey="6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU"-->
							<!--data-recaptcha-object="recaptcha2" data-callback="recaptchaCallback">-->
							<!--<div>-->
							<!--<div style="width: 304px; height: 78px;">-->
							<!--<iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU&amp;co=aHR0cDovL3ByZXByb2QuY2FzZXhlLmNvbTo4MA..&amp;hl=en&amp;v=r20160502112552&amp;theme=light&amp;size=normal&amp;cb=ftudccmhzbdi"-->
							<!--title="recaptcha widget" width="304" height="78" role="presentation"-->
							<!--frameborder="0" scrolling="no" name="undefined"></iframe>-->
							<!--</div>-->
							<!--<textarea id="g-recaptcha-response-3" name="g-recaptcha-response"-->
							<!--class="g-recaptcha-response"-->
							<!--style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea>-->
							<!--</div>-->
							<!--</div>-->
							<!--<p class="error-message"></p>-->
						</div>
						<button class="btn btn-primary" type="submit">send</button>
					</div>
				</form>
				<div class="success-msg">
					<div class="info">
						<img src="images/icon/icon-envelope-white-big.png" alt="">
						<h2 class="text-light">Your message has been sent successfully</h2>
					</div>
				</div>
				<div class="error-msg">
					<div class="info">
						<img src="images/icon/icon-envelope-white-big-error.png" alt="">
						<h2 class="text-light">Something went wrong!!!</h2>
					</div>
				</div>
			</div>
		</div>
	</section>

</main>

<div class="social social-dark social-vertical social-fixed">
	<ul>
		<li><a class="fa fa-twitter" href=""></a></li>
		<li><a class="fa fa-periscope" href=""></a></li>
		<li><a class="fa fa-youtube" href=""></a></li>
		<li><a class="fa fa-linkedin" href=""></a></li>
		<li><a class="fa fa-facebook" href=""></a></li>
	</ul>
</div>

<div class="modal-wrapper"></div>

<?php include("footer.html"); ?>

<script src="js/libs/masonry.pkgd.min.js"></script>
<script src="js/libs/particles.min.js"></script>
<script>
	/* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
	particlesJS.load('plexus01', 'json/particlesjs.json');
	particlesJS.load('plexus02', 'json/particlesjs02.json');
</script>

</body>

</html>