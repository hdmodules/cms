<!DOCTYPE html>
<html lang="ru">

<?php include ("head.html"); ?>

<body>

<?php include("header.html"); ?>

<main class="page-news-detail">

	<section class="breadcrumbs">
		<div class="container">
			<ul>
				<li><a href="">Home</a></li>
				<li><a href="">All News</a></li>
				<li><span>Events</span></li>
			</ul>
		</div>
	</section>

	<section class="content">
		<div class="container">

			<h1 class="title-line">
				<span>Big Plans for G2E Asia</span>
			</h1>

			<div class="news-slider">
				<ul class="slider-for">
					<li><img src="images/article/news-gallery-01.jpg" alt=""></li>
					<li><img src="images/article/news-gallery-02.jpg" alt=""></li>
					<li><img src="images/partner/partner-03.png" alt=""></li>
					<li><img src="images/game/game-group-04.jpg" alt=""></li>
					<li><img src="images/article/news-gallery-01.jpg" alt=""></li>
					<li><img src="images/article/news-gallery-02.jpg" alt=""></li>
					<li><img src="images/article/news-gallery-01.jpg" alt=""></li>
					<li><img src="images/article/news-gallery-02.jpg" alt=""></li>
				</ul>
				<ul class="slider-nav">
					<li><img src="images/article/news-gallery-01.jpg" alt=""></li>
					<li><img src="images/article/news-gallery-02.jpg" alt=""></li>
					<li><img src="images/partner/partner-03.png" alt=""></li>
					<li><img src="images/game/game-group-04.jpg" alt=""></li>
					<li><img src="images/article/news-gallery-01.jpg" alt=""></li>
					<li><img src="images/article/news-gallery-02.jpg" alt=""></li>
					<li><img src="images/article/news-gallery-01.jpg" alt=""></li>
					<li><img src="images/article/news-gallery-02.jpg" alt=""></li>
				</ul>
			</div>

			<time class="fa fa-calendar">23/06/16</time>

			<div class="news-info text">

				<p><b>Global Gaming Expo Asia</b> is a massive 3 day event that will be held from 17th to the 19th May
					2016 at
					the The Venetian Macao Convention & Exhibition Center in Macau, China(Macau S.A.R.).Among the
					leading-edge companies,groundbreaking new technologies and never-before-seen products this year
					Endorphina will be showcased as well.</p>

				<blockquote>
					<p>We will bring our unique slot Twerk to Asia. Since its introduction at ICE where it was received
						with standing ovations earlier this year, we are determined to gain the same
						response from Asian gaming crowd. What makes the game so appealing is not only it’s unique theme
						but the Bonus Game. It consists of 6 Dance Battles, where a dancer gets
						to compete with the other 6 girls. Each Battle victory awards player doubling all his current
						winnings, giving chance to multiply all wins from Battle to Battle progressively
						during the Bonus.</p>
					<p>Moreover, our team will be glad to present our top games to the public, which have already proved
						internationally to be enticing and lucrative, like <b>The Ninja, Safari, Satoshi’s Secret</b>
						etc. We look forward to meeting you at our booth #1907 to present our games and talk everything
						igaming. </p>
				</blockquote>

				<img src="images/article/news-gallery-01.jpg" alt="">

				<p>Global Gaming Expo Asia is a massive 3 day event that will be held from 17th to the 19th May 2016 at
					the The Venetian Macao Convention & Exhibition Center in Macau, China(Macau S.A.R.).Among the
					leading-edge companies,groundbreaking new technologies and never-before-seen products this year
					Endorphina will be showcased as well. Global Gaming Expo Asia is a massive 3 day event that will be
					held from 17th to the 19th May 2016 at the The Venetian Macao Convention & Exhibition Center in
					Macau, China(Macau S.A.R.).Among the leading-edge companies,groundbreaking new technologies and
					never-before-seen products this year Endorphina will be showcased as well.</p>
			</div>

			<div class="tags">
				<span>Tags:</span>
				<ul>
					<li>Popular,</li>
					<li>9 line,</li>
					<li>Horror</li>
				</ul>
			</div>
		</div>
	</section>

	<section class="subscriber subscriber-dark">
		<div id="plexus01" class="plexus"></div>

		<div class="container">
			<h3>news subscription</h3>
			<form class="form-light" action="">
				<div class="input">
					<input type="text" placeholder="e-mail address">
					<span class="icon">
						<i class="fa fa-envelope-o"></i>
					</span>

					<div class="error-message">Input cannot be blank.</div>
				</div>

				<div class="buttons">
					<div class="input input-recaptcha">
						<input type="hidden" id="subscriber-recaptcha-2" name="Subscriber[reCaptcha]">
						<div id="recaptcha-2" data-sitekey="6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU"
							 data-recaptcha-object="recaptcha2" data-callback="recaptchaCallback">
							<div>
								<div style="width: 304px; height: 78px;">
									<iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU&amp;co=aHR0cDovL3ByZXByb2QuY2FzZXhlLmNvbTo4MA..&amp;hl=en&amp;v=r20160502112552&amp;theme=light&amp;size=normal&amp;cb=ftudccmhzbdi"
											title="recaptcha widget" width="304" height="78" role="presentation"
											frameborder="0" scrolling="no" name="undefined"></iframe>
								</div>
								<textarea id="g-recaptcha-response-2" name="g-recaptcha-response"
										  class="g-recaptcha-response"
										  style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea>
							</div>
						</div>
						<p class="error-message"></p>
					</div>
					<button class="btn btn-primary" type="submit">send</button>
				</div>
			</form>
		</div>
	</section>

	<section class="recommended-news">
		<div class="container">
			<h2 class="title-dots">
				<span>Recommended for you</span>
			</h2>
			<ul class="articles-list slider slider-dark">
				<li>
					<a href=""><img src="images/article/article-square.png" alt=""></a>
					<div class="details ellipsis">
						<h3><a href="">Endorphina is heading to Sweden</a></h3>
						<p>i-Gaming Forumin Stockholm is the next stop on our unmissable events calendar. We are
							thrilled to announce that Endorphina has become a partner of this even</p>
					</div>
					<a class="read-more" href="">Read More</a>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a href=""><img src="images/article/article-square.png" alt=""></a>
					<div class="details ellipsis">
						<h3><a href="">Endorphina is heading to Sweden</a></h3>
						<p class="ellipsis">i-Gaming Forumin Stockholm is the next stop on our unmissable events
							calendar. We are thrilled to announce that Endorphina has become a partner of this even</p>
					</div>
					<a class="read-more" href="">Read More</a>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a href=""><img src="images/article/article-square.png" alt=""></a>
					<div class="details ellipsis">
						<h3><a href="">Endorphina is heading to Sweden</a></h3>
						<p class="ellipsis">i-Gaming Forumin Stockholm is the next stop on our unmissable events
							calendar. We are thrilled to announce that Endorphina has become a partner of this even</p>
					</div>
					<a class="read-more" href="">Read More</a>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a href=""><img src="images/article/article-square.png" alt=""></a>
					<div class="details ellipsis">
						<h3><a href="">Endorphina is heading to Sweden</a></h3>
						<p class="ellipsis">i-Gaming Forumin Stockholm is the next stop on our unmissable events
							calendar. We are thrilled to announce that Endorphina has become a partner of this even</p>
					</div>
					<a class="read-more" href="">Read More</a>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a href=""><img src="images/article/article-square.png" alt=""></a>
					<div class="details ellipsis">
						<h3><a href="">Endorphina is heading to Sweden</a></h3>
						<p class="ellipsis">i-Gaming Forumin Stockholm is the next stop on our unmissable events
							calendar. We are thrilled to announce that Endorphina has become a partner of this even</p>
					</div>
					<a class="read-more" href="">Read More</a>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
			</ul>
		</div>
	</section>

</main>

<div class="modal-wrapper"></div>

<?php include("footer.html"); ?>

<script src="js/libs/particles.min.js"></script>
<script>
	/* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
	particlesJS.load('plexus01', 'json/particlesjs02.json');
</script>

</body>

</html>