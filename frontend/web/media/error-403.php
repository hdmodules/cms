<!DOCTYPE html>
<html lang="ru">

<?php include ("head.html"); ?>

<body>

<?php include("header.html"); ?>

<main class="page-error error-403 bg-dark">

	<section class="breadcrumbs">
		<div class="container">
			<ul>
				<li><a href="">Home</a></li>
				<li><span>error 403</span></li>
			</ul>
		</div>
	</section>

	<section class="content">
		<div class="container">

			<h1><span>Oh Snap!</span></h1>
			<img src="images/error/error-403.png" alt="">

			<p>Please, visit our <a href="">home page</a></p>
		</div>
	</section>

	<section class="report-error send-email">
		<div class="container">
			<h2 class="title-dots">
				<span>Report this issue</span>
			</h2>
			<p class="title-info">If you seeing this issue, if you seeing this...</p>
			<div class="form-wrapper">
				<form class="" action="">
					<div class="input error">
						<input type="text" placeholder="name*">
					<span class="icon">
						<i class="fa fa-user"></i>
					</span>

						<div class="error-message">Input cannot be blank.</div>
					</div>
					<div class="input">
						<input type="email" placeholder="e-mail address*">
					<span class="icon">
						<i class="fa fa-envelope-o"></i>
					</span>

						<div class="error-message">Input cannot be blank.</div>
					</div>
					<div class="input input-textarea">
						<textarea placeholder="comment*"></textarea>
					<span class="icon">
						<i class="fa fa-commenting-o"></i>
					</span>

						<div class="error-message">Input cannot be blank.</div>
					</div>
					<p class="form-info"><span>*</span> Required fields</p>

					<div class="buttons">
						<div class="input input-recaptcha">
							<input type="hidden" id="subscriber-recaptcha-3" name="Subscriber[reCaptcha]">
							<div id="recaptcha-3" data-sitekey="6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU"
								 data-recaptcha-object="recaptcha2" data-callback="recaptchaCallback">
								<div>
									<div style="width: 304px; height: 78px;">
										<iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU&amp;co=aHR0cDovL3ByZXByb2QuY2FzZXhlLmNvbTo4MA..&amp;hl=en&amp;v=r20160502112552&amp;theme=light&amp;size=normal&amp;cb=ftudccmhzbdi"
												title="recaptcha widget" width="304" height="78" role="presentation"
												frameborder="0" scrolling="no" name="undefined"></iframe>
									</div>
								<textarea id="g-recaptcha-response-3" name="g-recaptcha-response"
										  class="g-recaptcha-response"
										  style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea>
								</div>
							</div>
							<p class="error-message"></p>
						</div>
						<button class="btn btn-primary" type="submit">send</button>
					</div>
				</form>
				<div class="success-msg">
					<div class="info">
						<img src="images/icon/icon-envelope-big.png" alt="">
						<h2>Your message has been sent successfully</h2>
					</div>
				</div>
				<div class="error-msg">
					<div class="info">
						<img src="images/icon/icon-envelope-big-error.png" alt="">
						<h2>Something went wrong!!!</h2>
					</div>
				</div>
			</div>
		</div>
	</section>

</main>

<div class="modal-wrapper"></div>

<?php include("footer.html"); ?>

</body>

</html>