<!DOCTYPE html>
<html lang="ru">

<?php include ("head.html"); ?>

<body>

<?php include("header.html"); ?>

<main class="page-contact">

	<section class="breadcrumbs">
		<div class="container">
			<ul>
				<li><a href="">Home</a></li>
				<li><span>Contact Us</span></li>
			</ul>
		</div>
	</section>

	<section class="content">
		<div class="container">
			<h1 class="title-line">
				<span>Contact Us</span>
			</h1>

			<div class="contact-details">
				<div id="plexus02" class="plexus"></div>

				<ul>
					<li>
						<p class="label">tel. number:</p>
						<a class="fa fa-phone" href="">+420 222 564 222</a>
					</li>
					<li>
						<p class="label">e-mail address:</p>
						<a class="fa fa-envelope" href="">mail@endorphina.com</a>
					</li>
					<li>
						<p class="label">skype:</p>
						<a class="fa fa-skype" href="">endorphina.com</a>
					</li>
					<li>
						<p class="label">follow us:</p>
						<div class="social">
							<ul>
								<li><a class="fa fa-twitter" href=""></a></li>
								<li><a class="fa fa-periscope" href=""></a></li>
								<li><a class="fa fa-youtube" href=""></a></li>
								<li><a class="fa fa-linkedin" href=""></a></li>
								<li><a class="fa fa-facebook" href=""></a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
			<div class="contact-form send-email">
				<div class="form-wrapper">
					<form class="form-dark" action="">
						<h2 class="title-line">
							<span>We are just a click away</span>
						</h2>
						<p class="title-info">We would really appreciate your feedback on both our website and games we
							offer.</p>
						<div class="col-2">
							<div class="input">
								<input type="text" placeholder="name*">
							<span class="icon">
								<i class="fa fa-user"></i>
							</span>

								<div class="error-message">Input cannot be blank.</div>
							</div>
							<div class="input">
								<input type="email" placeholder="e-mail address*">
							<span class="icon">
								<i class="fa fa-envelope-o"></i>
							</span>

								<div class="error-message">Input cannot be blank.</div>
							</div>
						</div>
						<div class="col-2">
							<div class="input input-textarea">
								<textarea placeholder="comment*"></textarea>
							<span class="icon">
								<i class="fa fa-commenting-o"></i>
							</span>

								<div class="error-message">Input cannot be blank.</div>
							</div>
						</div>
						<p class="form-info"><span>*</span> Required fields</p>

						<div class="buttons">
							<div class="input input-recaptcha">
								<input type="hidden" id="subscriber-recaptcha-3" name="Subscriber[reCaptcha]">
								<div id="recaptcha-3" data-sitekey="6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU"
									 data-recaptcha-object="recaptcha2" data-callback="recaptchaCallback">
									<div>
										<div style="width: 304px; height: 78px;">
											<iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU&amp;co=aHR0cDovL3ByZXByb2QuY2FzZXhlLmNvbTo4MA..&amp;hl=en&amp;v=r20160502112552&amp;theme=light&amp;size=normal&amp;cb=ftudccmhzbdi"
													title="recaptcha widget" width="304" height="78" role="presentation"
													frameborder="0" scrolling="no" name="undefined"></iframe>
										</div>
								<textarea id="g-recaptcha-response-3" name="g-recaptcha-response"
										  class="g-recaptcha-response"
										  style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea>
									</div>
								</div>
								<p class="error-message"></p>
							</div>
							<button class="btn btn-primary" type="submit">send</button>
						</div>
					</form>
					<div class="success-msg">
						<div class="info">
							<img src="images/icon/icon-envelope-big.png" alt="">
							<h2>Your message has been sent successfully</h2>
						</div>
					</div>
					<div class="error-msg">
						<div class="info">
							<img src="images/icon/icon-envelope-big-error.png" alt="">
							<h2>Something went wrong!!!</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="events-slider">
		<div id="plexus01" class="plexus"></div>

		<div class="container">
			<ul class="slider-big slider-dark" data-animation="anim03">
				<li data-meeting="Not Best meeting ever">
					<div class="illustration">
						<img src="images/partner/partner-03.png" alt="">
					</div>
					<div class="details">
						<div class="text">
							<p class="ellipsis">
								1 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
								incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nos
								trud exercitation ullamco laboris nisi ut aliquip ex eaincididunt ut labore et dol
								ore magna aliqua. Ut enim ad minim veniam, quis nostrud...
							</p>
							<strong>
								<span class="place">berlin. arena hall</span>
								<span class="dash">/</span>
								<span class="date">20-22 october 2015</span>
							</strong>
						</div>
					</div>
					<div class="buttons">
						<a class="btn btn-default" data-open-modal="event-modal" href="">Arrange a meeting</a>
					</div>
				</li>
				<li data-meeting="Best meeting ever">
					<div class="illustration">
						<img src="images/partner/partner-02.png" alt="">
					</div>
					<div class="details">
						<div class="text">
							<p class="ellipsis">
								2 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
								incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nos
								trud exercitation ullamco laboris nisi ut aliquip ex eaincididunt ut labore et dol
								ore magna aliqua. Ut enim ad minim veniam, quis nostrud...Lorem ipsum dolor sit amet,
								consectetur adipisicing elit, sed do eiusmod tempor
								incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nos
								trud exercitation ullamco laboris nisi ut aliquip ex eaincididunt ut labore et dol
								ore magna aliqua. Ut enim ad minim veniam, quis nostrud...Lorem ipsum dolor sit amet,
								consectetur adipisicing elit, sed do eiusmod tempor
								incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nos
								trud exercitation ullamco laboris nisi ut aliquip ex eaincididunt ut labore et dol
								ore magna aliqua. Ut enim ad minim veniam, quis nostrud...
							</p>
							<strong><span class="place">berlin. arena hall</span><span class="dash">/</span><span
									class="date">20-22 october 2015</span></strong>
						</div>
					</div>
					<div class="buttons">
						<a class="btn btn-default" data-open-modal="event-modal" href="">Arrange a meeting</a>
					</div>
				</li>
				<li data-meeting="Not Best meeting ever">
					<div class="illustration" data-animation="anim03">
						<img src="images/partner/partner-03.png" alt="">
					</div>
					<div class="details">
						<div class="text">
							<p class="ellipsis">
								3 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
								incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nos
								trud exercitation ullamco laboris nisi ut aliquip ex eaincididunt ut labore et dol
								ore magna aliqua. Ut enim ad minim veniam, quis nostrud...
							</p>
							<strong><span class="place">berlin. arena hall</span><span class="dash">/</span><span
									class="date">20-22 october 2015</span></strong>
						</div>
					</div>
					<div class="buttons">
						<a class="btn btn-default" data-open-modal="event-modal" href="">Arrange a meeting</a>
					</div>
				</li>
				<li data-meeting="Best meeting ever">
					<div class="illustration">
						<img src="images/partner/partner-02.png" alt="">
					</div>
					<div class="details">
						<div class="text">
							<p class="ellipsis">
								4 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
								incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nos
								trud exercitation ullamco laboris nisi ut aliquip ex eaincididunt ut labore et dol
								ore magna aliqua. Ut enim ad minim veniam, quis nostrud...Lorem ipsum dolor sit amet,
								consectetur adipisicing elit, sed do eiusmod tempor
								incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nos
								trud exercitation ullamco laboris nisi ut aliquip ex eaincididunt ut labore et dol
								ore magna aliqua. Ut enim ad minim veniam, quis nostrud...Lorem ipsum dolor sit amet,
								consectetur adipisicing elit, sed do eiusmod tempor
								incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nos
								trud exercitation ullamco laboris nisi ut aliquip ex eaincididunt ut labore et dol
								ore magna aliqua. Ut enim ad minim veniam, quis nostrud...
							</p>
							<strong><span class="place">berlin. arena hall</span><span class="dash">/</span><span
									class="date">20-22 october 2015</span></strong>
						</div>
					</div>
					<div class="buttons">
						<a class="btn btn-default" data-open-modal="event-modal" href="">Arrange a meeting</a>
					</div>
				</li>
			</ul>
		</div>
	</section>

</main>

<div class="modal-wrapper">
	<section class="modal modal-form" data-modal="event-modal">
		<div class="links">
			<button data-close-modal="" class="close btn-close-game">Close<span>×</span></button>
		</div>
		<div class="modal-body add-review">

			<div class="form-wrapper transition" style="height: 489px;">

				<form id="event-form" class="form-dark" action="/service/save-universal-data" method="post"
					  style="height: 489px;">
					<input type="hidden" name="_csrf" value="eHdtUkdWZWE0AzoXEC81Dgk6XQg3FSMGTiUcAzEgFjEADz8oCgADCg==">
					<h2>Arrange a meeting</h2>
					<input class="meeting-name" type="hidden" value="">
					<div class="form-group field-request-title">

						<input type="hidden" id="request-title" class="form-control" name="Request[title]"
							   value="Arrange a meeting">

						<div class="help-block"></div>
					</div>
					<div class="form-group field-request-type_id">

						<input type="hidden" id="request-type_id" class="form-control" name="Request[type_id]"
							   value="11">

						<div class="help-block"></div>
					</div>
					<div class="form-group field-request-page">

						<input type="hidden" id="request-page" class="form-control" name="Request[page]"
							   value="/en/contact-us">

						<div class="help-block"></div>
					</div>
					<div class="form-group field-request-lang">

						<input type="hidden" id="request-lang" class="form-control" name="Request[lang]" value="en">

						<div class="help-block"></div>
					</div>
					<div class="form-group field-request-model_id">

						<input type="hidden" id="request-model_id" class="form-control" name="Request[model_id]">

						<div class="help-block"></div>
					</div>
					<div class="input field-request-name required">
						<input type="text" id="request-name" class="" name="Request[name]" placeholder="Name *"> <span
							class="icon"><i class="fa fa-user"></i></span>
						<div class="error-message"></div>
					</div>
					<div class="input field-request-email required">
						<input type="text" id="request-email" class="" name="Request[email]" placeholder="E-mail *">
						<span class="icon"><i class="fa fa-envelope-o"></i></span>
						<div class="error-message"></div>
					</div>
					<div class="input field-request-company required">
						<input type="text" id="request-company" class="" name="Request[company]"
							   placeholder="Company *"> <span class="icon"><i class="fa fa-group"></i></span>
						<div class="error-message"></div>
					</div>
					<div class="input input-textarea field-request-text required">
						<textarea id="request-text" class="form-control" name="Request[text]"
								  placeholder="Comment *"></textarea> <span class="icon"><i
							class="fa fa-commenting-o"></i></span>
						<div class="error-message"></div>
					</div>
					<p class="form-info"><span>*</span> Required fields</p>
					<div class="buttons">

						<div class="input input-recaptcha field-request-recaptcha">

							<input type="hidden" id="request-recaptcha" name="Request[recaptcha]">
							<div id="recaptcha-2" data-sitekey="6LdZJioTAAAAAG5EUAANAEf4wf06AjEBwzT55vhO"
								 data-callback="recaptchaCallback">
								<div style="width: 304px; height: 78px;">
									<div>
										<iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LdZJioTAAAAAG5EUAANAEf4wf06AjEBwzT55vhO&amp;co=aHR0cDovL3Rlc3Qub3BoaW5lLnNwYWNlOjgw&amp;hl=en&amp;v=r20160921114513&amp;theme=light&amp;size=normal&amp;cb=2q72cs61pywh"
												title="recaptcha widget" width="304" height="78" role="presentation"
												frameborder="0" scrolling="no" name="undefined"></iframe>
									</div>
									<textarea id="g-recaptcha-response-1" name="g-recaptcha-response"
											  class="g-recaptcha-response"
											  style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea>
								</div>
							</div>

							<div class="error-message"></div>
						</div>
						<button type="submit" class="btn btn-default">Send</button>
					</div>
				</form>
				<div class="success-msg" style="display: table; height: 489px;">
					<div class="info">
						<img src="/media/images/icon/icon-envelope-big.png" alt="">
						<h2>Your message has been sent successfully</h2>
					</div>
				</div>
				<div class="error-msg" style="display: table; height: 489px;">
					<div class="info">
						<img src="/media/images/icon/icon-envelope-big-error.png" alt="">
						<h2>Something went wrong!</h2>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php include("footer.html"); ?>

<script>
	$('.events-slider li .buttons .btn').on('click', function () {
		var thisDataAttr = $(this).closest('li').data('meeting');
		$('.modal-form .meeting-name').val(thisDataAttr);
	});
</script>

<script src="js/libs/particles.min.js"></script>
<script>
	/* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
	particlesJS.load('plexus01', 'json/particlesjs.json');
	particlesJS.load('plexus02', 'json/particlesjs02.json');
</script>

</body>

</html>