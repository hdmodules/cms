<!DOCTYPE html>
<html lang="ru">

<?php include ("head.html"); ?>

<body>

<?php include("header.html"); ?>

<main class="page-partners bg-dark">

	<section class="breadcrumbs">
		<div class="container">
			<ul>
				<li><a href="">Home</a></li>
				<li><span>Partners</span></li>
			</ul>
		</div>
	</section>

	<section class="content">
		<div class="container">
			<h1 class="title-line title-light title-bg">
				<span>Partners</span>
				<strong class="line"></strong>
			</h1>

			<h2 class="title-dots title-orange"><span>our clients</span></h2>

			<ul class="partners-list">
				<li>
					<div class="illustration">
						<a href=""><img src="images/partner/partner-01.png" alt=""></a>
					</div>
					<div class="details">
						<p>We will bring our unique slot Twerk to Asia.Since its introduction at ICE where it was
							response from Asian gaming crowd. What makes the game so appealing is not only it’s unique
							theme but the Bonus Game. It consists of 6 Dance Battles, received with standing ovations
							earlier this year, we are determined to gain the same where a dancer gets to compete with
							the other 6 girls. Each mBattle victory awards player doublin all his current winnings,
							giving chance to multiply all wins from Battle to Battle progressively during the
							Bonus.
						</p>
						<strong>
							<span class="name">William Jamieson,</span>
							<span class="company">online slots</span>
						</strong>
					</div>
				</li>
				<li>
					<div class="illustration">
						<a href=""><img src="images/partner/partner-02.png" alt=""></a>
					</div>
					<div class="details">
						<p>We will bring our unique slot Twerk to Asia.Since its introduction at ICE where it was
							response from Asian gaming crowd. What makes the game so appealing is not only it’s unique
							theme but the Bonus Game. It consists of 6 Dance Battles, received with standing ovations
							earlier this year, we are determined to gain the same where a dancer gets to compete with
							the other 6 girls. Each mBattle victory awards player doublin all his current winnings,
							giving chance to multiply all wins from Battle to Battle progressively during the
							Bonus. It consists of 6 Dance Battles, received with standing ovations
							earlier this year, we are determined to gain the same where a dancer gets to compete with
							the other 6 girls. Each mBattle victory awards player doublin all his current winnings,
							giving chance to multiply all wins from Battle to Battle progressively during the
							Bonus.
						</p>
						<strong>
							<span class="name">William Jamieson,</span>
							<span class="company">online slots</span>
						</strong>
					</div>
				</li>
				<li>
					<div class="illustration">
						<a href=""><img src="images/partner/partner-03.png" alt=""></a>
					</div>
					<div class="details">
						<p>We will bring our unique slot Twerk to Asia.Since its introduction at ICE where it was
							response from Asian gaming crowd. What makes the game so appealing is not only it’s unique
							theme but the Bonus Game. It consists of 6 Dance Battles, received with standing ovations
							earlier this year, we are determined to gain the same where a dancer gets to compete with
							the other 6 girls.
						</p>
						<strong>
							<span class="name">William Jamieson,</span>
							<span class="company">online slots</span>
						</strong>
					</div>
				</li>
				<li>
					<div class="illustration">
						<a href=""><img src="images/partner/partner-01.png" alt=""></a>
					</div>
					<div class="details">
						<p>We will bring our unique slot Twerk to Asia.Since its introduction at ICE where it was
							response from Asian gaming crowd. What makes the game so appealing is not only it’s unique
							theme but the Bonus Game. It consists of 6 Dance Battles, received with standing ovations
							earlier this year, we are determined to gain the same where a dancer gets to compete with
							the other 6 girls. Each mBattle victory awards player doublin all his current winnings,
							giving chance to multiply all wins from Battle to Battle progressively during the
							Bonus.
						</p>
						<strong>
							<span class="name">William Jamieson,</span>
							<span class="company">online slots</span>
						</strong>
					</div>
				</li>
			</ul>
			<div class="buttons">
				<a class="btn btn-primary" href="">become a client</a>
			</div>
		</div>
	</section>

	<section class="media-partners">
		<div class="container">
			<h2 class="title-dots"><span>media partners</span></h2>

			<ul class="partners-img-list">
				<li><a href=""><img src="images/partner/partner-01.png" alt=""></a></li>
				<li><a href=""><img src="images/partner/partner-02.png" alt=""></a></li>
				<li><a href=""><img src="images/partner/partner-03.png" alt=""></a></li>
				<li><a href=""><img src="images/partner/partner-01.png" alt=""></a></li>
				<li><a href=""><img src="images/partner/partner-02.png" alt=""></a></li>
				<li><a href=""><img src="images/partner/partner-03.png" alt=""></a></li>
				<li><a href=""><img src="images/partner/partner-01.png" alt=""></a></li>
				<li><a href=""><img src="images/partner/partner-02.png" alt=""></a></li>
				<li><a href=""><img src="images/partner/partner-03.png" alt=""></a></li>
				<li><a href=""><img src="images/partner/partner-01.png" alt=""></a></li>
				<li><a href=""><img src="images/partner/partner-02.png" alt=""></a></li>
				<li><a href=""><img src="images/partner/partner-03.png" alt=""></a></li>
				<li><a href=""><img src="images/partner/partner-01.png" alt=""></a></li>
				<li><a href=""><img src="images/partner/partner-02.png" alt=""></a></li>
				<li><a href=""><img src="images/partner/partner-03.png" alt=""></a></li>
			</ul>
			<div class="buttons">
				<a class="btn btn-default" href="">become a partner</a>
			</div>
		</div>
	</section>

	<section class="exhibitions">
		<div id="plexus01" class="plexus"></div>

		<div class="container">
			<h2 class="title-dots"><span>exhibitions</span></h2>

			<ul class="exhibitions-list">
				<li><a href=""><img src="images/partner/partner-eig-white.png" alt=""></a></li>
				<li><a href=""><img src="images/partner/partner-ice-white.png" alt=""></a></li>
			</ul>
		</div>
	</section>

</main>

<div class="modal-wrapper"></div>

<?php include("footer.html"); ?>

<script src="js/libs/particles.min.js"></script>
<script>
	/* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
	particlesJS.load('plexus01', 'json/particlesjs.json');
</script>

</body>

</html>