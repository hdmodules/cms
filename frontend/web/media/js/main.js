$(document).ready(function () {

    /*INIT*/
    //select();

    /*VARIABLES*/
    var $html = $('html');
    var $subscriber = $('.subscriber');
    var $mailer = $('.send-email');
    var $productPresentation = $(".product-presentation");
    var $formWrapper = $('.form-wrapper');

    /*SELECTS*/
    /* function select() {
     $("#lt-type").not(".multiselect").selectric({
     disableOnMobile: false,
     onChange: function () {
     $("#request-type_id").val($("#lt-type").val());
     }
     });
     }*/

    /*MAIN PAGE*/
    $('.product-presentation .stage-01 .btn').on('click', function () {
        $('.product-presentation').addClass('form');
    });

    $html.on('click', function (e) {
        if (!$(e.target).closest(".product-presentation").length && $productPresentation.hasClass('form')) {
            $productPresentation.removeClass('form');
        }
    });

    /*TITLE-LINE BG*/
    $.fn.titleLineWidth = function () {

        $('.title-bg').each(function (key, thisTitle) {
            setTimeout(function () {
                var titleSpanWidth = $(thisTitle).find('span').outerWidth();
                var titleWidth = $(thisTitle).width();
                var lineWidth = titleWidth - titleSpanWidth;

                $(thisTitle).find('strong').css('width', lineWidth);
            }, 500);
        });

    };

    $(window).titleLineWidth();

    /*ARTICLES*/
    // $('.articles-list a').on('mouseover', function () {
    //     $(this).closest('li').addClass('hover');
    // }).on('mouseout', function () {
    //     $(this).closest('li').removeClass('hover');
    // });
    //
    // $('.articles-list a').on('click', function () {
    //     $(this).closest('li').addClass('active');
    // });

    /*SUBSCRIBER*/
    $('.subscriber input').on('focus', function () {
        $(this).closest('.subscriber').addClass('focused');
    });

    $('.subscriber button').on('click', function () {
        $(this).closest('.subscriber').addClass('focused');
    });

    $html.on('click', function (e) {
        if (!$(e.target).closest(".subscriber").length && $subscriber.hasClass('focused')) {
            $subscriber.removeClass('focused');
        }
    });

    /*SEND MAIL*/
    $('.send-email input, .send-email textarea').on('focus', function () {
        $(this).closest('.send-email').addClass('focused');
    });

    $('.send-email button').on('click', function () {
        $(this).closest('.send-email').addClass('focused');
    });

    $html.on('click', function (e) {
        if (!$(e.target).closest(".send-email").length && $mailer.hasClass('focused')) {
            $mailer.removeClass('focused');
        }
    });

    /*FORM*/
    $('.form-wrapper input, .form-wrapper textarea').on('focus', function () {
        $(this).closest('.form-wrapper').addClass('focused');
    });

    $('.form-wrapper button').on('click', function () {
        $(this).closest('.form-wrapper').addClass('focused');
    });

    $html.on('click', function (e) {
        if (!$(e.target).closest(".form-wrapper").length && $formWrapper.hasClass('focused')) {
            $formWrapper.removeClass('focused');
        }
    });

    $.fn.formHeightFunction = function () {

        setTimeout(function () {

            $formWrapper.each(function (key, thisForm) {

                $(thisForm).css('height', '').removeClass('transition');
                $(thisForm).find('form').css('height', '');

                $(thisForm).find('.success-msg, .error-msg').css({
                    height: '',
                    display: 'none'
                });

                var realWrapperHeight = $(thisForm).height();

                $(thisForm).css('height', realWrapperHeight);
                $(thisForm).find('form').css('height', realWrapperHeight);

                $(thisForm).find('.success-msg, .error-msg').css('display', 'table');

                var successHeight = $(thisForm).find('.success-msg').height();
                var errorHeight = $(thisForm).find('.error-msg').height();
                var highestHeight = Math.max(realWrapperHeight, successHeight, errorHeight);

                $(thisForm).css('height', highestHeight);
                $(thisForm).find('form').css('height', highestHeight);
                $(thisForm).find('.success-msg, .error-msg').css('height', highestHeight);

                setTimeout(function () {
                    $(thisForm).addClass('transition');
                }, 50);

            });

        }, 50);
    };

    $(window).formHeightFunction();

    $('form').on("afterValidateAttribute", function (event, attribute, messages) {
        $(window).formHeightFunction();
    });

    $(document).on('click', '.slick-slider button', function () {
        $('.ellipsis').trigger("destroy");
        $(window).initDotDotDot();
    });

    /*SLICK*/
    var $membersSlider = $('.slider.team-members-list');
    var $partnersSlider = $('.page-main .partners ul');

    if ($membersSlider.length) {
        $membersSlider.slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            draggable: false,
            pauseOnHover: false,
            dots: true,
            autoplay: true,
            autoplaySpeed: 4000,
            prevArrow: "<button class='slick-prev fa fa-angle-left'></button>",
            nextArrow: "<button class='slick-next fa fa-angle-right'></button>",
            responsive: [
                {
                    breakpoint: 879,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 659,
                    settings: {
                        slidesToShow: 2
                    }
                }
            ]
        });
    }

    if (!$membersSlider.length) {
        $('.slider').not('.cols-3, .new-games-list').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            draggable: false,
            pauseOnHover: false,
            dots: true,
            autoplay: true,
            autoplaySpeed: 4000,
            prevArrow: "<button class='slick-prev fa fa-angle-left'></button>",
            nextArrow: "<button class='slick-next fa fa-angle-right'></button>",
            responsive: [
                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 499,
                    settings: {
                        slidesToShow: 2
                    }
                }
            ]
        });
    }

    $('.new-games-list').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        draggable: false,
        pauseOnHover: false,
        dots: true,
        autoplay: true,
        autoplaySpeed: 4000,
        prevArrow: "<button class='slick-prev fa fa-angle-left'></button>",
        nextArrow: "<button class='slick-next fa fa-angle-right'></button>",
        responsive: [
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 499,
                settings: 'unslick'
            }
        ]
    });

    $('.slider.cols-3').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        draggable: false,
        pauseOnHover: false,
        dots: true,
        autoplay: true,
        autoplaySpeed: 4000,
        prevArrow: "<button class='slick-prev fa fa-angle-left'></button>",
        nextArrow: "<button class='slick-next fa fa-angle-right'></button>",
        responsive: [
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });

    $partnersSlider.slick({
        slidesToShow: 4,
        slidesToScroll: 2,
        draggable: false,
        pauseOnHover: false,
        dots: true,
        autoplay: true,
        autoplaySpeed: 4000,
        prevArrow: "<button class='slick-prev fa fa-angle-left'></button>",
        nextArrow: "<button class='slick-next fa fa-angle-right'></button>",
        responsive: [
            {
                breakpoint: 699,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });

    $('.main-slider .slider-big').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        draggable: false,
        pauseOnHover: false,
        dots: true,
        autoplay: true,
        speed: 800,
        autoplaySpeed: 4000,
        prevArrow: "<button class='slick-prev fa fa-angle-left'></button>",
        nextArrow: "<button class='slick-next fa fa-angle-right'></button>"
    });

    $('.events-slider .slider-big').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        draggable: false,
        dots: true,
        prevArrow: "<button class='slick-prev fa fa-angle-left'></button>",
        nextArrow: "<button class='slick-next fa fa-angle-right'></button>"
    });

    $.fn.positionOfArrows = function () {
        $('.slider, .slider-big, .page-main .partners ul.slick-initialized').not('.slider-nav').each(function (key, thisSlider) {
            var dotsWidth = $(thisSlider).find('.slick-dots').outerWidth();
            var positionOfArrow = dotsWidth + 8;

            $(thisSlider).find('.slick-prev').css('right', positionOfArrow);
            $(thisSlider).find('.slick-next').css('left', positionOfArrow);
        });
    };

    $(window).positionOfArrows();

    /*POPUPS*/
    $(document).on("click", "[data-open-modal]", function (e) {
        e.preventDefault();
        openModal(e);
    });

    $(document).on("click", "[data-close-modal]", function (e) {
        closeModal(e);
    });

    $(document).on("click", ".modal-wrapper", function (e) {
        checkModalWrapperClick(e);
    });

    $(document).keyup(function (e) {
        if (e.keyCode == 27) closeModal(); // Escape
    });

    function openModal(e) {
        // Open modal wrapper and show modal window (by target)

        var target = $(e.target);
        var modalName = target.attr("data-open-modal");
        var modal = $("[data-modal='" + modalName + "']");
        actualModal = modalName;

        modal.addClass("displayed");
        modal.closest(".modal-wrapper").addClass("displayed");
    }

    function openModalByName(name) {
        // Open modal wrapper and show modal window (by name)

        var modal = $("[data-modal='" + name + "']");
        actualModal = name;

        modal.add($(".modal-wrapper")).addClass("displayed");
    }

    function closeModal(e) {
        // Close modal wrapper and hide modal window

        if (actualModal) {
            $("[data-modal='" + actualModal + "']").trigger("closeModal");
        }

        actualModal = null;

        var modal = $(".modal");
        modal.add($(".modal-wrapper")).removeClass("displayed");
    }

    function checkModalWrapperClick(e) {
        // If clicked on modal wrapper (but not the modal window itself), close the modal window.

        var target = $(e.target);

        if (target.closest(".modal").length == 0) {
            closeModal(e);
        }
    }

    /*STICKY FOOTER*/
    $.fn.stickyFooter = function () {
        var headerHeight = $('body header').outerHeight();
        var footerHeight = $('body footer').outerHeight();
        var windowHeight = $(window).height();
        var mainHeight = windowHeight - headerHeight - footerHeight;

        $('main').css('min-height', mainHeight);
    };

    $(window).stickyFooter();

    widthIndex = 0;

    /*RESPONSIVITY*/
    //Main page
    $.fn.windowWidthCheck = function () {
        var documentWidth = window.innerWidth;
        var windowHeight = $(window).height();
        var windowHeightPlus = windowHeight + 100;
        var subscriberIndex = 0;

        if (documentWidth < 999) {

            if (subscriberIndex != 1) {
                $('.page-main .subscriber').insertAfter('.page-main .games-news');
                $('.btn-lets-talk').appendTo('.header-nav .container').removeClass('btn-default').addClass('btn-secondary');
                $('.header-nav').css('height', windowHeightPlus);
                //Rewrite home-news link
                var home_news = $('#home-news').attr('href');
                if (home_news) {
                    var r = home_news.replace('/#news', '');
                    $('#home-news').attr('href', r);
                }

                subscriberIndex = 1;
            }

        } else {

            if (subscriberIndex != 2) {
                $('.page-main .subscriber').insertBefore('.page-main .games-news');
                $('.btn-lets-talk').appendTo('.header-main .buttons').removeClass('btn-secondary').addClass('btn-default');
                $('.header-nav').css('height', '');

                subscriberIndex = 2;
            }

        }

        if (documentWidth < 769) {

            if ($('.games-categories').length) {

                $('.games-categories ul').masonry({
                    itemSelector: 'li',
                    gutter: 0
                });

            }
        } else {

            if ($('.games-categories').length) {

                $('.games-categories ul').masonry({
                    itemSelector: 'li',
                    gutter: 13.333333
                });

            }
        }

        if (documentWidth < 519) {
            $('.social-footer').appendTo('.header-nav .container');
        } else {
            $('.social-footer').appendTo('body > footer .container');
        }

        if (documentWidth > 700 && documentWidth < 1000) {
            var imgWidth = $('.news-list .news-04 img').width();
            $('.news-list .news-04 img, .news-list .news-02 img').css('height', imgWidth);
        } else {
            $('.news-list .news-04 img, .news-list .news-02 img').css('height', '');
        }

        if (documentWidth < 700) {
            //News page
            $('.page-news .subscriber').insertAfter('.content');
            if (!$('.open-filters').length) {
                var filterText = $('.filters').data('filters');
                $("<button class='open-filters btn btn-default' type='button'>" + filterText + "</button>").prependTo(".page-game .filters, .page-news .filters");
            }
        } else {
            //News page
            $('.page-news .subscriber').insertAfter('.news-list li:nth-of-type(5)');
            $(".open-filters").remove();
        }

        if (documentWidth < 700) {

            $('.news-list li').each(function (key, thisLi) {

                var $thisImg = $(thisLi).find('.illustration img');
                var stringBefore = $thisImg.attr('data-img-src');

                var m = stringBefore.indexOf('_');
                var indexOf = m + 1;
                var replaceSymbol = '3';

                replaceFce(stringBefore, indexOf, replaceSymbol);

                function replaceFce(s, n, t) {
                    var stringAfter = s.substring(0, n) + t + s.substring(n + 1);
                    $thisImg.attr('src', stringAfter);
                }

            });

        } else {

            $('.news-list li').each(function (key, thisLi) {

                var $thisImg = $(thisLi).find('.illustration img');

                if ($thisImg.attr('data-img-src')) {
                    var oldSrc = $thisImg.attr('data-img-src');
                    $thisImg.attr('src', oldSrc);
                }
            });

        }

        if (widthIndex == 0) {

            if (documentWidth < 620) {

                $('.game-slider .slider-for').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                    arrows: true,
                    prevArrow: "<button class='slick-prev fa fa-angle-left'></button>",
                    nextArrow: "<button class='slick-next fa fa-angle-right'></button>"
                });

                $('.news-slider .slider-for').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true,
                    asNavFor: '.slider-nav',
                    prevArrow: "<button class='slick-prev fa fa-angle-left'></button>",
                    nextArrow: "<button class='slick-next fa fa-angle-right'></button>",
                    dots: true
                });

                widthIndex = 1;

            } else {

                $('.game-slider .slider-for').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    asNavFor: '.slider-nav'
                });

                $('.game-slider .slider-nav').slick({
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    asNavFor: '.slider-for',
                    focusOnSelect: true,
                    prevArrow: "<button class='slick-prev fa fa-angle-left'></button>",
                    nextArrow: "<button class='slick-next fa fa-angle-right'></button>"
                });

                $('.news-slider .slider-for').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true,
                    asNavFor: '.slider-nav',
                    prevArrow: "<button class='slick-prev fa fa-angle-left'></button>",
                    nextArrow: "<button class='slick-next fa fa-angle-right'></button>"
                });

                $('.news-slider .slider-nav').slick({
                    slidesToShow: 6,
                    slidesToScroll: 1,
                    asNavFor: '.slider-for',
                    focusOnSelect: true,
                    arrows: false,
                    responsive: [
                        {
                            breakpoint: 859,
                            settings: {
                                slidesToShow: 5
                            }
                        },
                        {
                            breakpoint: 699,
                            settings: {
                                slidesToShow: 4
                            }
                        }
                    ]
                });

                widthIndex = 2;

            }

        } else if (documentWidth < 620 && widthIndex == 2) {

            if ($('.slider-for.slick-initialized').length) {
                $('.slider-for').slick('unslick');
            }

            if ($('.slider-nav.slick-initialized').length) {
                $('.slider-nav').slick('unslick');
            }

            $('.game-slider .slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: true,
                prevArrow: "<button class='slick-prev fa fa-angle-left'></button>",
                nextArrow: "<button class='slick-next fa fa-angle-right'></button>"
            });

            $('.news-slider .slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                asNavFor: '.slider-nav',
                prevArrow: "<button class='slick-prev fa fa-angle-left'></button>",
                nextArrow: "<button class='slick-next fa fa-angle-right'></button>",
                dots: true
            });

            widthIndex = 1;

        } else if (documentWidth >= 620 && widthIndex == 1) {

            if ($('.slider-for.slick-initialized').length) {
                $('.slider-for').slick('unslick');
            }

            if ($('.slider-nav.slick-initialized').length) {
                $('.slider-nav').slick('unslick');
            }

            $('.game-slider .slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                asNavFor: '.slider-nav'
            });

            $('.game-slider .slider-nav').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                asNavFor: '.slider-for',
                focusOnSelect: true,
                prevArrow: "<button class='slick-prev fa fa-angle-left'></button>",
                nextArrow: "<button class='slick-next fa fa-angle-right'></button>"
            });

            $('.news-slider .slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                asNavFor: '.slider-nav',
                prevArrow: "<button class='slick-prev fa fa-angle-left'></button>",
                nextArrow: "<button class='slick-next fa fa-angle-right'></button>"
            });

            $('.news-slider .slider-nav').slick({
                slidesToShow: 6,
                slidesToScroll: 1,
                asNavFor: '.slider-for',
                focusOnSelect: true,
                arrows: false,
                responsive: [
                    {
                        breakpoint: 859,
                        settings: {
                            slidesToShow: 5
                        }
                    },
                    {
                        breakpoint: 699,
                        settings: {
                            slidesToShow: 4
                        }
                    }
                ]
            });

            widthIndex = 2;

        }

    };

    $(window).windowWidthCheck();

    $(document).on('click', '.open-filters', function () {
        if ($('.filters').hasClass('open')) {
            $(this).closest('.filters').removeClass('open');
            $('.modal-wrapper').removeClass('displayed');
        } else {
            $(this).closest('.filters').addClass('open');
            $('.modal-wrapper').addClass('displayed');
        }
    });

    //Game Details
    if ($('.page-game-detail .game-slider .slider-nav').length == 0) {
        $('.page-game-detail .game-slider, .page-game-detail .game-details').css('height', '476');
    }

    $('.open-menu').on('click', function () {
        if ($('.header-nav').hasClass('open')) {
            $('.header-nav, html').removeClass('open');
            $('.modal-wrapper').removeClass('displayed');
        } else {
            $('.header-nav, html').addClass('open');
            $('.modal-wrapper').addClass('displayed');
        }
    });

    //Games page
    $(document).on('click', '.filters ul li a', function (e) {

        var $thisLi = $(this).closest('li');
        var documentWidth = window.innerWidth;

        if ((documentWidth < 700) && ($thisLi.find('ul').length)) {
            e.preventDefault();
            if ($thisLi.hasClass('active')) {
                $thisLi.removeClass('active');
            } else {
                $thisLi.addClass('active');
            }
        }

    });

    /*REVIEWS COUNTER*/
    $.fn.reviewsSliders = function () {
        var documentWidth = window.innerWidth;
        var sliderWrapper = null;

        $('.reviews').each(function (key, thisSlider) {

            if ($(thisSlider).find('.games-list').hasClass('slick-initialized')) {
                var slickTrack = $(thisSlider).find('.slick-track');
            } else {
                var slickTrack = $(thisSlider).find('.games-list');
            }

            var sliderWrapper = $(thisSlider).find('.games-slider');
            var count = slickTrack.find('li').length;
            var thisSliderWrap = slickTrack.closest('.games-slider');
            var thisAddButton = $(thisSlider).find('.add-button');
            var $thisSliderInit = $(thisSlider).find('.games-list');

            if ((count < 1) || (sliderWrapper == null)) {

                thisAddButton.css("float", "none");

                thisSliderWrap.css({
                    width: 0,
                    float: "none"
                });

            } else if (count == 1) {

                if (documentWidth >= 769) {

                    thisSliderWrap.css({
                        width: 24.5 + "%",
                        float: "none",
                        marginRight: "18px"
                    }).css('width', '-=10px');

                } else if (documentWidth < 769 && documentWidth >= 499) {

                    thisSliderWrap.css({
                        width: 32.8 + "%",
                        float: "none",
                        marginRight: "18px"
                    }).css('width', '-=10px');

                } else {

                    thisSliderWrap.css({
                        width: 50 + "%",
                        float: "none",
                        marginRight: "0"
                    }).css('width', '-=9px');

                }

                thisAddButton.css("float", "none");

                if ($thisSliderInit.hasClass('slick-initialized')) {
                    $thisSliderInit.slick("unslick");
                }

                $thisSliderInit.slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    draggable: false,
                    pauseOnHover: false,
                    dots: true,
                    autoplay: true,
                    autoplaySpeed: 4000,
                    prevArrow: "<button class='slick-prev fa fa-angle-left'></button>",
                    nextArrow: "<button class='slick-next fa fa-angle-right'></button>"
                });

            } else if (count == 2) {

                if (documentWidth >= 769) {

                    thisSliderWrap.css({
                        width: 50 + "%",
                        float: "none",
                        marginRight: "19px"
                    }).css('width', '-=10px');

                } else if (documentWidth < 769 && documentWidth >= 499) {

                    thisSliderWrap.css({
                        width: 67.15 + "%",
                        float: "none",
                        marginRight: "18px"
                    }).css('width', '-=10px');

                } else {

                    thisSliderWrap.css({
                        width: 100 + "%",
                        float: "none"
                    });

                }

                thisAddButton.css("float", "none");

                if ($thisSliderInit.hasClass('slick-initialized')) {
                    $thisSliderInit.slick("unslick");
                }

                $thisSliderInit.slick({
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    draggable: false,
                    pauseOnHover: false,
                    dots: true,
                    autoplay: true,
                    autoplaySpeed: 4000,
                    prevArrow: "<button class='slick-prev fa fa-angle-left'></button>",
                    nextArrow: "<button class='slick-next fa fa-angle-right'></button>"
                });

            } else {

                thisSliderWrap.css("display", "");
                thisAddButton.css("display", "");

                if ($thisSliderInit.hasClass('slick-initialized')) {
                    $thisSliderInit.slick("unslick");
                }

                $thisSliderInit.slick({
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    draggable: false,
                    pauseOnHover: false,
                    dots: true,
                    autoplay: true,
                    autoplaySpeed: 4000,
                    prevArrow: "<button class='slick-prev fa fa-angle-left'></button>",
                    nextArrow: "<button class='slick-next fa fa-angle-right'></button>",
                    responsive: [
                        {
                            breakpoint: 769,
                            settings: {
                                slidesToShow: 2
                            }
                        }
                    ]
                });

            }
        });

    };

    $(window).reviewsSliders();

    /*iShit*/
    $('.breadcrumbs a, footer nav a, footer .contacts a, .contact-details a.fa-phone, .contact-details a.fa-envelope, .contact-details a.fa-skype').on('click touchend', function (e) {
        var el = $(this);
        var elLink = el.attr('href');
        window.location = elLink;
    });

    /*IS ON THE SCREEN*/
    $.fn.animation = function () {
        var animElement = $("[data-animation='" + dataAnim + "']");
        var animTop = animElement.offset().top;
        var animElementHeight = animElement.outerHeight();
        var animBottom = animTop + animElementHeight;

        if (animBottom < windowBottomPosition) {
            eval(dataAnim + "()");
        }
    };

    $.fn.isOnTheScreen = function () {

        var windowTopPosition = $(window).scrollTop();
        var windowHeight = $(window).height();
        windowBottomPosition = windowTopPosition + windowHeight;

        $('[data-animation^="anim"]').each(function (key, thisAnim) {
            dataAnim = $(thisAnim).data('animation');
            $(window).animation();
        });

    };

    function anim01() {
        var time = 400;

        $('.advantages li').each(function (key, thisLi) {

            var imgString = $(thisLi).find('img.gif').attr('src');
            var imgFormat = imgString.substr(imgString.length - 3);

            if (imgFormat != "gif") {
                setTimeout(function () {
                    var imgSrc = $(thisLi).find('img.gif').prev('img').attr('src');
                    //var imgSrc = $(thisLi).find('img.gif').data('src');
                    $(thisLi).find('img.gif').attr('src', imgSrc);
                }, time);

                time += 1300;
            }
        });
    }

    function anim02() {

        // var parentSelector = null;
        var parentSelector = ".four-points";
        var time = 100;

        // if ($('.four-points').length) {
        //     parentSelector = ".four-points";
        // } else {
        //     parentSelector = ".our-awards";
        // }

        $(parentSelector + ' li').each(function (key, thisLi) {

            setTimeout(function () {
                $(thisLi).addClass('show');
            }, time);

            time += 200;
        });

        setTimeout(function () {
            $(parentSelector).addClass('show');
        }, 1200);

        setTimeout(function () {
            $(parentSelector + ' li p').addClass('show');
        }, 1500);

    }

    function anim03() {
        var $eventsSlider = $('.events-slider .slider-big');

        if (!$eventsSlider.hasClass('auto-play')) {

            if ($('.events-slider .slider-big.slick-initialized').length) {
                $('.events-slider .slider-big').slick('unslick');
            }

            $eventsSlider.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                draggable: false,
                dots: true,
                autoplay: true,
                pauseOnHover: false,
                autoplaySpeed: 5000,
                prevArrow: "<button class='slick-prev fa fa-angle-left'></button>",
                nextArrow: "<button class='slick-next fa fa-angle-right'></button>"
            });

            $(window).positionOfArrows();

            $eventsSlider.addClass('auto-play');

        }

    }

    function anim04() {

        var parentSelector = ".our-awards";
        var time = 400;

        $('.our-awards li').each(function (key, thisLi) {

            var imgString = $(thisLi).find('img.gif').attr('src');
            var imgFormat = imgString.substr(imgString.length - 3);

            if (imgFormat != "gif") {
                setTimeout(function () {
                    var imgSrc = $(thisLi).find('img.gif').prev('img').attr('src');
                    //var imgSrc = $(thisLi).find('img.gif').data('src');
                    $(thisLi).find('img.gif').attr('src', imgSrc);
                }, time);

                time += 1300;
            }
        });

        setTimeout(function () {
            $(parentSelector).addClass('show');
        }, 5000);
    }

    /*RESIZE X ONLY*/
    var width = $(window).width();

    $(window).on('resize', function () {
        if ($(window).width() == width) return;
        width = $(window).width();

        //Triggers
        $(window).titleLineWidth();
        $(window).windowWidthCheck();
        $(window).formHeightFunction();
        $(window).reviewsSliders();
        $(window).initDotDotDot();
    });

    /*DOTDOTDOT*/
    $.fn.initDotDotDot = function () {

        $('.ellipsis').dotdotdot();
        $('li.team-member .quote').dotdotdot();

        setTimeout(function () {
            $('.page-game-detail .games-list li .game-info').dotdotdot();
        }, 200);

    };

    $(window).initDotDotDot();

});

$(window).on('load', function () {

    $(window).windowWidthCheck();
    $(window).stickyFooter();
    $(window).formHeightFunction();
    $(window).initDotDotDot();
    $(window).positionOfArrows();
    $(window).titleLineWidth();
    $(window).isOnTheScreen();

});

$(window).on('scroll', function () {

    $(window).isOnTheScreen();

});

$(window).on('resize', function () {

    $(window).stickyFooter();
    setTimeout(function () {

        $(window).positionOfArrows();

    }, 100);

});

$(window).on('orientationchange', function () {

    $(window).windowWidthCheck();
    $(window).initDotDotDot();
    $(window).stickyFooter();
    $(window).formHeightFunction();
    $(window).positionOfArrows();
    $(window).titleLineWidth();
    $(window).reviewsSliders();

});