<!DOCTYPE html>
<html lang="ru">

<?php include ("head.html"); ?>

<body>

<?php include("header.html"); ?>

<main class="page-career-detail bg-dark">

	<section class="breadcrumbs">
		<div class="container">
			<ul>
				<li><a href="">Home</a></li>
				<li><a href="">Career</a></li>
				<li><span>Head of Product</span></li>
			</ul>
		</div>
	</section>

	<section class="content">
		<div class="container">
			<h1 class="title-line title-light title-bg">
				<span>Career</span>
				<strong class="line"></strong>
			</h1>

			<h2 class="title-dots title-orange"><span>head of product</span></h2>

			<div class="text">
				<h3>ABOUT US AND WHY YOU’LL WANT TO WORK FOR US</h3>
				<p>Every so often, you come across a job so exciting; it makes you spill your coffee. So please – for
					your own safety – STOP. Put down your hot beverage (should you have one
					to hand) and only then resume reading the description below.<br>
					Our People are as unique as our products, that said we all share common values which keep us as one
					of the UK’s most high profile e-commerce businesses! As our Partner
					are creative businesses we in turn, appreciate and value creativity. Our true creative instinct,
					having fun with what we do and always staying true to the brand is what helps
					drive us. We are brave everyday which has meant we are experiencing explosive growth. To keep
					momentum we are continuously pushing boundaries, not afraid to speak
					up and are honest with one another. At NOTHS House change is championed, opportunities are sought
					and solutions are realised. We offer customers the chance to source,
					create and own beautiful out the ordinary things and love how each of our individual roles
					contribute to this. Our roles are our passion, we go above and beyond with
					infectious enthusiasm and energy. Together Team NOTHS collaborate and look out for each other. This
					unique blend makes us who we are and we are on the lookout for
					similar types who can add value to our business.</p>

				<h3>WHAT WE NEED IN A NUTSHELL</h3>
				<p>We’re looking for a Head of Product Management to drive the success of NOTHS’ current and future
					digital experience, across all devices by leading and inspiring a team of
					passionate product people and who can drive the product strategy of the business.</p>

				<h3>WHAT THE ROLE ENTAILS</h3>

				<p>Reporting into and working closely with the CTO, you’ll champion our users and partners and will lead
					a highly collaborative and a cross-functional Digital Product team.</p>

				<p><strong>Responsibilities will include:</strong></p>

				<ul>
					<li>You’ll drive the company’s product vision, strategy and roadmap so that everyone in the business
						is clear about our product direction and value delivered
					</li>
					<li>Act as the ultimate ‘product champion’, making sure that business, customer and partner needs
						are constantly driven forward by dedicated product teams within
					</li>
					<li>You'll establish, own and report on KPIs which best measure success for digital product, derived
						from the overall business objectives, making sure that your team of
						product managers and owners is fully set up to deliver against these objectives
					</li>
					<li>Driving of a companywide product roadmap will help us to deliver business and customer value on
						a continuous basis, resulting in consistent flow of great products and
						services for our customers and partners
					</li>
					<li>Forward and innovative thinking comes natural to you, encouraging your product team to always be
						curious and looking for new problems to solve
					</li>
				</ul>
			</div>

			<div class="buttons">
				<a class="btn btn-secondary" href="">recomend a friend</a>
				<a class="btn btn-primary" href="">aplly for this position</a>
			</div>
		</div>
	</section>

	<section class="subscriber subscriber-light">
		<div id="plexus01" class="plexus"></div>

		<div class="container">
			<h3>jobs subscription</h3>
			<form class="form-light" action="">
				<div class="input">
					<input type="text" placeholder="e-mail address">
					<span class="icon">
						<i class="fa fa-envelope-o"></i>
					</span>

					<div class="error-message">Input cannot be blank.</div>
				</div>

				<div class="buttons">
					<div class="input input-recaptcha">
						<input type="hidden" id="subscriber-recaptcha-2" name="Subscriber[reCaptcha]">
						<div id="recaptcha-2" data-sitekey="6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU"
							 data-recaptcha-object="recaptcha2" data-callback="recaptchaCallback">
							<div>
								<div style="width: 304px; height: 78px;">
									<iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU&amp;co=aHR0cDovL3ByZXByb2QuY2FzZXhlLmNvbTo4MA..&amp;hl=en&amp;v=r20160502112552&amp;theme=light&amp;size=normal&amp;cb=ftudccmhzbdi"
											title="recaptcha widget" width="304" height="78" role="presentation"
											frameborder="0" scrolling="no" name="undefined"></iframe>
								</div>
								<textarea id="g-recaptcha-response-2" name="g-recaptcha-response"
										  class="g-recaptcha-response"
										  style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea>
							</div>
						</div>
						<p class="error-message"></p>
					</div>
					<button class="btn btn-default" type="submit">send</button>
				</div>
			</form>
		</div>
	</section>

	<section class="other-jobs">
		<div class="container">
			<h2 class="title-dots"><span>other job vacacy</span></h2>

			<ul class="games-list slider slider-dark">
				<li>
					<h3 class="job-title"><a href="">Filip's slut</a></h3>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<h3 class="job-title"><a href="">HTML Dog</a></h3>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<h3 class="job-title"><a href="">Table Football Master</a></h3>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<h3 class="job-title"><a href="">php programmer</a></h3>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<h3 class="job-title"><a href="">cleaning lady</a></h3>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
			</ul>
		</div>
	</section>

</main>

<div class="modal-wrapper"></div>

<?php include("footer.html"); ?>

<script src="js/libs/particles.min.js"></script>
<script>
	/* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
	particlesJS.load('plexus01', 'json/particlesjs.json');
</script>

</body>

</html>