<!DOCTYPE html>
<html lang="ru">

<?php include ("head.html"); ?>

<body>

<?php include("header.html"); ?>

<main class="page-game">

	<section class="breadcrumbs">
		<div class="container">
			<ul>
				<li><a href="">Home</a></li>
				<li><a href="">games</a></li>
				<li><a href="">Themes</a></li>
				<li><span>Horror</span></li>
			</ul>
		</div>
	</section>

	<section class="content">
		<div class="container">
			<h1 class="title-line">
				<span>Games</span>
			</h1>

			<div class="filters no-submenu" data-filters="Filter Games">
				<ul>
					<li><a href="">all</a></li>
					<li><a href="">paylines</a></li>
					<li class="">
						<a href="">themes</a>
						<ul>
							<li><a href="">Oriental</a></li>
							<li><a href="">Fruit</a></li>
							<li class="active"><a href="">Horror</a></li>
							<li><a href="">cute</a></li>
							<li><a href="">unique</a></li>
							<li><a href="">ethnic</a></li>
						</ul>
					</li>
					<li><a href="">features</a></li>
					<li><a href="">popular</a></li>
					<li><a href="">new</a></li>
					<li><a href="">3d</a></li>
				</ul>
			</div>

			<ul class="games-list cols-3">
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-01.jpg" alt="game">
						<div class="hover">
							<div class="icon-play">
								<img src="images/icon/icon-play.png" alt="play">
								<span>Play now</span>
							</div>
						</div>
					</a>
					<h2><a href="">vampires</a></h2>
					<a class="btn btn-tertiary" href=""><span>find out </span>more</a>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-02.jpg" alt="game">
						<div class="hover">
							<div class="icon-play">
								<img src="images/icon/icon-play.png" alt="play">
								<span>Play now</span>
							</div>
						</div>
					</a>
					<h2><a href="">Devil’s fortune</a></h2>
					<a class="btn btn-tertiary" href=""><span>find out </span>more</a>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-01.jpg" alt="game">
						<div class="hover">
							<div class="icon-play">
								<img src="images/icon/icon-play.png" alt="play">
								<span>Play now</span>
							</div>
						</div>
					</a>
					<h2><a href="">the ninja</a></h2>
					<a class="btn btn-tertiary" href=""><span>find out </span>more</a>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-02.jpg" alt="game">
						<div class="hover">
							<div class="icon-play">
								<img src="images/icon/icon-play.png" alt="play">
								<span>Play now</span>
							</div>
						</div>
					</a>
					<h2>safari</h2>
					<a class="btn btn-tertiary" href=""><span>find out </span>more</a>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-01.jpg" alt="game">
						<div class="hover">
							<div class="icon-play">
								<img src="images/icon/icon-play.png" alt="play">
								<span>Play now</span>
							</div>
						</div>
					</a>
					<h2>twerk</h2>
					<a class="btn btn-tertiary" href=""><span>find out </span>more</a>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-02.jpg" alt="game">
						<div class="hover">
							<div class="icon-play">
								<img src="images/icon/icon-play.png" alt="play">
								<span>Play now</span>
							</div>
						</div>
					</a>
					<h2>twerk bitch</h2>
					<a class="btn btn-tertiary" href=""><span>find out </span>more</a>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
			</ul>
		</div>
	</section>

	<section class="subscriber subscriber-light">
		<div id="plexus01" class="plexus"></div>

		<div class="container">
			<h3>jobs subscription</h3>
			<form class="form-light" action="">
				<div class="input">
					<input type="text" placeholder="e-mail address">
					<span class="icon">
						<i class="fa fa-envelope-o"></i>
					</span>

					<div class="error-message">Input cannot be blank.</div>
				</div>

				<div class="buttons">
					<div class="input input-recaptcha">
						<input type="hidden" id="subscriber-recaptcha-2" name="Subscriber[reCaptcha]">
						<div id="recaptcha-2" data-sitekey="6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU"
							 data-recaptcha-object="recaptcha2" data-callback="recaptchaCallback">
							<div>
								<div style="width: 304px; height: 78px;">
									<iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU&amp;co=aHR0cDovL3ByZXByb2QuY2FzZXhlLmNvbTo4MA..&amp;hl=en&amp;v=r20160502112552&amp;theme=light&amp;size=normal&amp;cb=ftudccmhzbdi"
											title="recaptcha widget" width="304" height="78" role="presentation"
											frameborder="0" scrolling="no" name="undefined"></iframe>
								</div>
								<textarea id="g-recaptcha-response-2" name="g-recaptcha-response"
										  class="g-recaptcha-response"
										  style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea>
							</div>
						</div>
						<p class="error-message"></p>
					</div>
					<button class="btn btn-default" type="submit">send</button>
				</div>
			</form>
		</div>
	</section>

</main>

<div class="modal-wrapper"></div>

<?php include("footer.html"); ?>

<script src="js/libs/particles.min.js"></script>
<script>
	/* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
	particlesJS.load('plexus01', 'json/particlesjs.json');
</script>

</body>

</html>