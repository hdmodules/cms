<!DOCTYPE html>
<html lang="ru">

<?php include ("head.html"); ?>

<body>

<?php include("header.html"); ?>

<main class="page-lets-talk">

	<section class="breadcrumbs">
		<div class="container">
			<ul>
				<li><a href="">Home</a></li>
				<li><span>Let's Talk</span></li>
			</ul>
		</div>
	</section>

	<section class="content">
		<div class="container">
			<h1 class="title-line title-grey">
				<span>Let's Talk</span>
				<select id="lt-type" class="theme" name="">
					<option value="">career</option>
					<option value="">sales</option>
					<option value="">marketing</option>
					<option value="">partner offer</option>
				</select>
			</h1>
			<div class="form-wrapper">
				<form class="form-dark" action="" enctype="multipart/form-data">
					<p class="info-text">Would you like to speak with an Endorphina representative? Just send us your
						contact information and we would be happy to call you shortly</p>
					<input type="hidden" value="">
					<div class="col-2">
						<div class="input">
							<input type="text" placeholder="name*">
						<span class="icon">
							<i class="fa fa-user"></i>
						</span>

							<div class="error-message">Input cannot be blank.</div>
						</div>
						<div class="input">
							<input type="email" placeholder="e-mail address*">
						<span class="icon">
							<i class="fa fa-envelope-o"></i>
						</span>

							<div class="error-message">Input cannot be blank.</div>
						</div>
						<div class="input">
							<input type="email" placeholder="tel number*">
						<span class="icon">
							<i class="fa fa-phone"></i>
						</span>

							<div class="error-message">Input cannot be blank.</div>
						</div>

						<div class="input input-file">
							<!--<a href="#" class="btn-upload">-->
								<!--<span class="btn fa fa-plus">Attach file</span>-->
								<!--<input type="file" id="" multiple name="">-->
								<!--<span class="icon">-->
									<!--<i class="fa fa-file"></i>-->
								<!--</span>-->
							<!--</a>-->
							<input type="file" name="file" id="file" class="inputfile" data-multiple-caption="{count} files selected" multiple />
							<label for="file"><strong> Choose a file</strong> <span></span></label>
							<span class="icon">
								<i class="fa fa-file"></i>
							</span>
							<div class="error-message">Input cannot be blank.</div>
						</div>
					</div>
					<div class="col-2">
						<div class="input input-textarea">
							<textarea placeholder="comment*"></textarea>
							<span class="icon">
								<i class="fa fa-commenting-o"></i>
							</span>
							<div class="error-message">Input cannot be blank.</div>
						</div>
					</div>
					<div class="col-1">
						<p class="form-info"><span>*</span> Required fields</p>

						<div class="buttons">
							<div class="input input-recaptcha">
								<input type="hidden" id="subscriber-recaptcha-3" name="Subscriber[reCaptcha]">
								<div id="recaptcha-3" data-sitekey="6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU"
									 data-recaptcha-object="recaptcha2" data-callback="recaptchaCallback">
									<div>
										<div style="width: 304px; height: 78px;">
											<iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU&amp;co=aHR0cDovL3ByZXByb2QuY2FzZXhlLmNvbTo4MA..&amp;hl=en&amp;v=r20160502112552&amp;theme=light&amp;size=normal&amp;cb=ftudccmhzbdi"
													title="recaptcha widget" width="304" height="78" role="presentation"
													frameborder="0" scrolling="no" name="undefined"></iframe>
										</div>
								<textarea id="g-recaptcha-response-3" name="g-recaptcha-response"
										  class="g-recaptcha-response"
										  style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea>
									</div>
								</div>
								<p class="error-message"></p>
							</div>
							<button class="btn btn-primary" type="submit">send</button>
						</div>
					</div>
				</form>
				<div class="success-msg">
					<div class="info">
						<img src="images/logo/logo-main-no-text.png" alt="">
						<strong>thank you, Andrejko!</strong>
						<p class="title">We’ll get back to you soon</p>

						<p>Your email has been sent! We appreciate you reaching out and are stoked
							about the opportunity to work together.Lorem ipsum dolor sit amet, consectetur adipisicing
							elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
							veniam</p>

						<a class="read-more" href="">Send another message >></a>
					</div>
				</div>
			</div>
		</div>
	</section>

</main>

<div class="modal-wrapper"></div>

<?php include("footer.html"); ?>

<script src="js/libs/jquery.custom-file-input.js"></script>

<script>
	/*SELECTS*/
	$("#lt-type").not(".multiselect").selectric({
		disableOnMobile: false
	});
</script>

</body>

</html>