<!DOCTYPE html>
<html lang="ru">

<?php include ("head.html"); ?>

<body>

<?php include("header.html"); ?>

<main class="page-career">

	<section class="breadcrumbs">
		<div class="container">
			<ul>
				<li><a href="">Home</a></li>
				<li><span>Career</span></li>
			</ul>
		</div>
	</section>

	<section class="content">
		<div class="container">

			<h1 class="title-line">
				<span>Career</span>
			</h1>

			<h2 class="title-dots">
				<span>Why join us?</span>
			</h2>

			<ol class="four-points">
				<li>
					<strong class="number" data-animation="anim02">1</strong>
					<p>We are transforming our platform and want you to be part of it! We are on the hunt for a talented
						Java Developer who!</p>
				</li>
				<li>
					<strong class="number">2</strong>
					<p>We are transforming our platform and want you to be part of it! We are on the hunt for a talented
						Java Developer who!</p>
				</li>
				<li>
					<strong class="number">3</strong>
					<p>We are transforming our platform and want you to be part of it! We are on the hunt for a talented
						Java Developer who!</p>
				</li>
				<li>
					<strong class="number">4</strong>
					<p>We are transforming our platform and want you to be part of it! We are on the hunt for a talented
						Java Developer who!</p>
				</li>
			</ol>

			<ul class="vacancy-list">
				<li class="block-01 block-vacancy block-blue">
					<h3>Programme manager</h3>
					<p class="ellipsis">Reporting into the Programme Management team, this is a unique opportunity to help establis
						and champion best practice across the organisation. It requires that rare individual who can su
						ccessfully deliver programmes of work to high standards, while simultaneously helping define
						methods of working together that strive to evolve the company as a whole...</p>
					<a class="btn btn-primary" href="">Aply <span>for this position</span></a>
				</li>
				<li class="block-02 team-member">
					<img src="images/team/image-01.jpg" alt="">
					<h3>Jan Miklosh <span>Sale Manager sdfasdfsad fsdafsadf sad</span></h3>
					<p class="quote">The Endorphina team consists of highly qualified international the Endorphina team
						consists of highly qualified international</p>
				</li>
				<li class="block-03 team-member">
					<img src="images/team/image-02.jpg" alt="">
					<h3>Jan Miklosh <span>Sale Manager</span></h3>
					<p class="quote">The Endorphina team consists of highly qualified international the Endorphina team
						consists of highly qualified international</p>
				</li>
				<li class="block-04 block-vacancy block-orange">
					<h3>Java Developer</h3>
					<p class="ellipsis">We are transforming our platform and want
						you to be part of it! We are on the hunt for a
						talented Java Developer who are enthusiastic
						and passionate about java to work with our
						agile and cross-functional team. Click and
						have a look at this fantastic opportunity!</p>
					<a class="btn btn-default" href="">Aply</a>
				</li>
				<li class="block-05 block-vacancy block-grey">
					<h3>RUBY ENGINEER</h3>
					<p class="ellipsis">We are transforming our platform and want you to be
						part of it! We are on the hunt for a talented Java
						Developer who are enthusiastic and passionate about ja
						va to work with our agile and cross-functional team.
						Click and have a look at this fantastic opportunity!
						In this role you would define and deliver the strategic!</p>
					<a class="btn btn-default" href="">Aply</a>
				</li>
				<li class="block-06 linked-in">
					<a href="">
						<i class="fa fa-linkedin-square"></i>
					</a>
				</li>
				<li class="block-07 block-vacancy block-grey block-big">
					<h3>RUBY ENGINEER</h3>
					<p class="ellipsis">We are transforming our platform and want you to be
						part of it! We are on the hunt for a talented Java
						Developer who are enthusiastic and passionate about ja
						va to work with our agile and cross-functional team.
						Click and have a look at this fantastic opportunity!
						In this role you would define and deliver the strategic!</p>
					<a class="btn btn-primary" href="">Aply <span>for this position</span></a>
				</li>
			</ul>

			<section class="subscriber subscriber-dark">
				<div id="plexus01" class="plexus"></div>

				<div class="container">
					<h3>jobs subscription</h3>
					<form class="form-light" action="">
						<div class="input">
							<input type="text" placeholder="e-mail address">
					<span class="icon">
						<i class="fa fa-envelope-o"></i>
					</span>

							<div class="error-message">Input cannot be blank.</div>
						</div>

						<div class="buttons">
							<div class="input input-recaptcha">
								<input type="hidden" id="subscriber-recaptcha-2" name="Subscriber[reCaptcha]">
								<div id="recaptcha-2" data-sitekey="6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU"
									 data-recaptcha-object="recaptcha2" data-callback="recaptchaCallback">
									<div>
										<div style="width: 304px; height: 78px;">
											<iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU&amp;co=aHR0cDovL3ByZXByb2QuY2FzZXhlLmNvbTo4MA..&amp;hl=en&amp;v=r20160502112552&amp;theme=light&amp;size=normal&amp;cb=ftudccmhzbdi"
													title="recaptcha widget" width="304" height="78" role="presentation"
													frameborder="0" scrolling="no" name="undefined"></iframe>
										</div>
								<textarea id="g-recaptcha-response-2" name="g-recaptcha-response"
										  class="g-recaptcha-response"
										  style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea>
									</div>
								</div>
								<p class="error-message"></p>
							</div>
							<button class="btn btn-primary" type="submit">send</button>
						</div>
					</form>
				</div>
			</section>
		</div>
	</section>

	<section class="our-life">
		<div class="container">
			<h2 class="title-dots">
				<span>our life</span>
			</h2>
			<ul class="games-list slider slider-dark">
				<li>
					<img src="images/game/game-square-01.jpg" alt="game">
				</li>
				<li>
					<img src="images/game/game-square-02.jpg" alt="game">
				</li>
				<li>
					<img src="images/game/game-square-01.jpg" alt="game">
				</li>
				<li>
					<img src="images/game/game-square-02.jpg" alt="game">
				</li>
				<li>
					<img src="images/game/game-square-01.jpg" alt="game">
				</li>
				<li>
					<img src="images/game/game-square-02.jpg" alt="game">
				</li>
			</ul>
		</div>
	</section>

</main>

<div class="modal-wrapper"></div>

<?php include("footer.html"); ?>

<script src="js/libs/particles.min.js"></script>
<script>
	/* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
	particlesJS.load('plexus01', 'json/particlesjs02.json');
</script>

<div class="modal-wrapper">
	<div class="nodal"></div>
</div>

</body>

</html>