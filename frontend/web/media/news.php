<!DOCTYPE html>
<html lang="ru">

<?php include ("head.html"); ?>

<body>

<?php include("header.html"); ?>

<main class="page-news">

	<section class="breadcrumbs">
		<div class="container">
			<ul>
				<li><a href="">Home</a></li>
				<li><a href="">News</a></li>
				<li><span>All News</span></li>
			</ul>
		</div>
	</section>

	<section class="content">
		<div class="container">
			<h1 class="title-line">
				<span>News</span>
			</h1>

			<div class="filters filters-news" data-filters="fsdafsdfsadfasd">
				<ul>
					<li class="active"><a href="">all news</a></li>
					<li><a href="">events</a></li>
					<li><a href="">releases</a></li>
					<li><a href="">interviews</a></li>
					<li><a href="">social networks</a></li>
				</ul>
			</div>

			<ul class="news-list">
				<li class="news-01 news-grey">
					<a href="" class="illustration">
						<img src="" alt="" data-img-src="images/article/img_1.jpg">
					</a>
					<div class="details ellipsis">
						<h2><a href="">iGaming Super Show</a></h2>
						<time class="fa fa-calendar">23/06/16</time>
						<p>Endorphina is no newcomer at the iGaming Super Show and this year we confirm
							our belief that the expo is the where igaming industry gathers and it’s the best time to
							forge and renew alliances with partners to monitor fresh faces together. Endorphina is no
							newcomer at the iGaming Super Show and this year we confirm our belief that the expo is the
							where igaming industry gathers and it’s the best time to forge and renew alliances with
							partners to monitor fresh faces together.</p>
					</div>
					<a class="read-more" href="">Read more</a>
				</li>
				<li class="news-03">
					<a href="" class="illustration">
						<img src="" alt="" data-img-src="images/article/img_2.jpg">
					</a>
					<div class="details ellipsis">
						<h2><a href="">Big Plans for G2E Asia</a></h2>
						<time class="fa fa-calendar">23/06/16</time>
						<p>Endorphina is no newcomer at the iGaming Super Show and this year we confirm
							our belief that the expo is the where igaming industry gathers and it’s the best time to
							forge and renew alliances with partners to monitor fresh faces together. Endorphina is no
							newcomer at the iGaming Super Show and this year we confirm our belief that the expo is the
							where igaming industry gathers and it’s the best time to forge and renew alliances with
							partners to monitor fresh faces together.</p>
					</div>
					<a class="read-more" href="">Read more</a>
				</li>
				<li class="news-04">
					<a href="" class="illustration">
						<img src="" alt="" data-img-src="images/article/img_3.jpg">
					</a>
					<div class="details ellipsis">
						<h2><a href="">Big Plans for G2E Asia</a></h2>
						<time class="fa fa-calendar">23/06/16</time>
						<p>Endorphina is no newcomer at the iGaming Super Show and this year we confirm
							our belief that the expo is the where igaming industry gathers and it’s the best time to
							forge and renew alliances with partners to monitor fresh faces together. Endorphina is no
							newcomer at the iGaming Super Show and this year we confirm our belief that the expo is the
							where igaming industry gathers and it’s the best time to forge and renew alliances with
							partners to monitor fresh faces together.</p>
					</div>
					<a class="read-more" href="">Read more</a>
				</li>
				<li class="news-04">
					<a href="" class="illustration">
						<img src="" alt="" data-img-src="images/article/img_3.jpg">
					</a>
					<div class="details ellipsis">
						<h2><a href="">Endorphina is heading to Sweden</a></h2>
						<time class="fa fa-calendar">23/06/16</time>
						<p>Endorphina is no newcomer at the iGaming Super Show and this year we confirm
							our belief that the expo is the where igaming industry gathers and it’s the best time to
							forge and renew alliances with partners to monitor fresh faces together. Endorphina is no
							newcomer at the iGaming Super Show and this year we confirm our belief that the expo is the
							where igaming industry gathers and it’s the best time to forge and renew alliances with
							partners to monitor fresh faces together.</p>
					</div>
					<a class="read-more" href="">Read more</a>
				</li>
				<li class="news-02 news-orange">
					<a href="" class="illustration">
						<img src="" alt="" data-img-src="images/article/img_2.jpg">
					</a>
					<div class="details ellipsis">
						<h2><a href="">Big Plans for G2E Asia</a></h2>
						<time class="fa fa-calendar">23/06/16</time>
						<p>Endorphina is no newcomer at the iGaming Super Show and this year we confirm
							our belief that the expo is the where igaming industry gathers and it’s the best time to
							forge and renew alliances with partners to monitor fresh faces together. Endorphina is no
							newcomer at the iGaming Super Show and this year we confirm our belief that the expo is the
							where igaming industry gathers and it’s the best time to forge and renew alliances with
							partners to monitor fresh faces together.</p>
					</div>
					<a class="read-more" href="">Read more</a>
				</li>
				<li class="subscriber subscriber-dark">
					<div id="plexus01" class="plexus"></div>

					<div class="container">
						<h3>news subscription</h3>
						<p class="title-info">asohfojasndlkjfn asdkfnkjsd nfkln sdklfndsj</p>
						<form class="form-light" action="">
							<div class="input">
								<input type="text" placeholder="e-mail address">
					<span class="icon">
						<i class="fa fa-envelope-o"></i>
					</span>

								<div class="error-message">Input cannot be blank.</div>
							</div>

							<div class="buttons">
								<div class="input input-recaptcha">
									<input type="hidden" id="subscriber-recaptcha-2" name="Subscriber[reCaptcha]">
									<div id="recaptcha-2" data-sitekey="6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU"
										 data-recaptcha-object="recaptcha2" data-callback="recaptchaCallback">
										<div>
											<div style="width: 304px; height: 78px;">
												<iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU&amp;co=aHR0cDovL3ByZXByb2QuY2FzZXhlLmNvbTo4MA..&amp;hl=en&amp;v=r20160502112552&amp;theme=light&amp;size=normal&amp;cb=ftudccmhzbdi"
														title="recaptcha widget" width="304" height="78"
														role="presentation"
														frameborder="0" scrolling="no" name="undefined"></iframe>
											</div>
								<textarea id="g-recaptcha-response-2" name="g-recaptcha-response"
										  class="g-recaptcha-response"
										  style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea>
										</div>
									</div>
									<p class="error-message"></p>
								</div>
								<button class="btn btn-primary" type="submit">send</button>
							</div>
						</form>
					</div>
				</li>
				<li class="news-01 news-grey">
					<a href="" class="illustration">
						<img src="" alt="" data-img-src="images/article/img_1.jpg">
					</a>
					<div class="details ellipsis">
						<h2><a href="">iGaming Super Show</a></h2>
						<time class="fa fa-calendar">23/06/16</time>
						<p>Endorphina is no newcomer at the iGaming Super Show and this year we confirm
							our belief that the expo is the where igaming industry gathers and it’s the best time to
							forge and renew alliances with partners to monitor fresh faces together. Endorphina is no
							newcomer at the iGaming Super Show and this year we confirm our belief that the expo is the
							where igaming industry gathers and it’s the best time to forge and renew alliances with
							partners to monitor fresh faces together.</p>
					</div>
					<a class="read-more" href="">Read more</a>
				</li>
				<li class="news-03">
					<a href="" class="illustration">
						<img src="" alt="" data-img-src="images/article/img_2.jpg">
					</a>
					<div class="details ellipsis">
						<h2><a href="">Big Plans for G2E Asia</a></h2>
						<time class="fa fa-calendar">23/06/16</time>
						<p>Endorphina is no newcomer at the iGaming Super Show and this year we confirm
							our belief that the expo is the where igaming industry gathers and it’s the best time to
							forge and renew alliances with partners to monitor fresh faces together. Endorphina is no
							newcomer at the iGaming Super Show and this year we confirm our belief that the expo is the
							where igaming industry gathers and it’s the best time to forge and renew alliances with
							partners to monitor fresh faces together.</p>
					</div>
					<a class="read-more" href="">Read more</a>
				</li>
				<li class="news-04">
					<a href="" class="illustration">
						<img src="" alt="" data-img-src="images/article/img_3.jpg">
					</a>
					<div class="details ellipsis">
						<h2><a href="">Big Plans for G2E Asia</a></h2>
						<time class="fa fa-calendar">23/06/16</time>
						<p>Endorphina is no newcomer at the iGaming Super Show and this year we confirm
							our belief that the expo is the where igaming industry gathers and it’s the best time to
							forge and renew alliances with partners to monitor fresh faces together. Endorphina is no
							newcomer at the iGaming Super Show and this year we confirm our belief that the expo is the
							where igaming industry gathers and it’s the best time to forge and renew alliances with
							partners to monitor fresh faces together.</p>
					</div>
					<a class="read-more" href="">Read more</a>
				</li>
				<li class="news-04">
					<a href="" class="illustration">
						<img src="" alt="" data-img-src="images/article/img_3.jpg">
					</a>
					<div class="details ellipsis">
						<h2><a href="">Endorphina is heading to Sweden</a></h2>
						<time class="fa fa-calendar">23/06/16</time>
						<p>Endorphina is no newcomer at the iGaming Super Show and this year we confirm
							our belief that the expo is the where igaming industry gathers and it’s the best time to
							forge and renew alliances with partners to monitor fresh faces together. Endorphina is no
							newcomer at the iGaming Super Show and this year we confirm our belief that the expo is the
							where igaming industry gathers and it’s the best time to forge and renew alliances with
							partners to monitor fresh faces together.</p>
					</div>
					<a class="read-more" href="">Read more</a>
				</li>
				<li class="news-02 news-blue">
					<a href="" class="illustration">
						<img src="" alt="" data-img-src="images/article/img_2.jpg">
					</a>
					<div class="details ellipsis">
						<h2><a href="">Big Plans for G2E Asia</a></h2>
						<time class="fa fa-calendar">23/06/16</time>
						<p>Endorphina is no newcomer at the iGaming Super Show and this year we confirm
							our belief that the expo is the where igaming industry gathers and it’s the best time to
							forge and renew alliances with partners to monitor fresh faces together. Endorphina is no
							newcomer at the iGaming Super Show and this year we confirm our belief that the expo is the
							where igaming industry gathers and it’s the best time to forge and renew alliances with
							partners to monitor fresh faces together.</p>
					</div>
					<a class="read-more" href="">Read more</a>
				</li>
			</ul>

			<div class="buttons">
				<a class="btn btn-default" href="">Show more news</a>
			</div>
		</div>
	</section>

</main>

<div class="modal-wrapper"></div>

<?php include("footer.html"); ?>

<script src="js/libs/particles.min.js"></script>
<script>
	/* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
	particlesJS.load('plexus01', 'json/particlesjs02.json');
</script>

</body>

</html>