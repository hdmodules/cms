<!DOCTYPE html>
<html lang="ru">

<?php include ("head.html"); ?>

<body>

<?php include("header.html"); ?>

<main class="page-about-us bg-dark">

	<section class="breadcrumbs">
		<div class="container">
			<ul>
				<li><a href="">Home</a></li>
				<li><span>About Us</span></li>
			</ul>
		</div>
	</section>

	<section class="content">
		<div class="container">
			<h1 class="title-line title-light title-bg">
				<span>About us</span>
				<strong class="line"></strong>
			</h1>

			<h2 class="title-dots title-orange"><span>who we are</span></h2>

			<div class="video">
				<iframe src="//www.youtube.com/embed/3fumBcKC6RE" frameborder="0"
						allowfullscreen></iframe>
			</div>

			<h3>Endorphina is a software provider that offers a wide range of Flash-based slot games for online
				casinos</h3>
			<div class="text">
				<p>The Endorphina team consists of highly qualified international professionals with
					many years of experience in online casino games creation.
				</p>
				<p>The company unites the best online gaming experience and cutting-edge technology
					to create games that attract players with not only its beautiful design and authentic
					atmosphere, but the real thrill of gambling.
				</p>
				<p>Being a provider of games oriented on players of different playing preferences, from
					the beginners that are just starting to dive into the world of online casinos to High
					Rollers that prefer to bet and to win big, Endorphina pays high attention to the
					mathematical aspect in games creation.
				</p>
				<p>To create high quality software the team of the company carries out a complete
					analysis of the gaming market.This gives the possibility to define a product appealing
					to the players and profitable for the operators.To attract more players Endorphina
					creates games that are always based on most popular themes on the market.
				</p>
				<p>The mathematics and algorithms used by Endorphina are consistently reliable and
					thoroughly tested.</p>
				<p>While creating each game the team of designers and developers are focused on the
					details to make it more understandable and easy to play.
				</p>
				<p>Games provided by Endorphina are remarkable for the stability of the system,
					circumspect logic of the client side and versatile API which allows several types of
					integration, a full report with statistics sent to the Client’s back office, etc. Choosing
					Endorphina means choosing a trustworthy provider of profitable online casino
					software.
				</p>
			</div>
		</div>
	</section>

	<section class="subscriber subscriber-light">
		<div id="plexus01" class="plexus"></div>

		<div class="container">
			<h3>news subscription</h3>
			<p class="title-info">Subscribe to news. Be the first to know all fresh news about us!</p>
			<form class="form-light" action="">
				<div class="input">
					<input type="text" placeholder="e-mail address">
					<span class="icon">
						<i class="fa fa-envelope-o"></i>
					</span>

					<div class="error-message">Input cannot be blank.</div>
				</div>

				<div class="buttons">
					<div class="input input-recaptcha">
						<input type="hidden" id="subscriber-recaptcha-2" name="Subscriber[reCaptcha]">
						<div id="recaptcha-2" data-sitekey="6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU"
							 data-recaptcha-object="recaptcha2" data-callback="recaptchaCallback">
							<div>
								<div style="width: 304px; height: 78px;">
									<iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU&amp;co=aHR0cDovL3ByZXByb2QuY2FzZXhlLmNvbTo4MA..&amp;hl=en&amp;v=r20160502112552&amp;theme=light&amp;size=normal&amp;cb=ftudccmhzbdi"
											title="recaptcha widget" width="304" height="78" role="presentation"
											frameborder="0" scrolling="no" name="undefined"></iframe>
								</div>
								<textarea id="g-recaptcha-response-2" name="g-recaptcha-response"
										  class="g-recaptcha-response"
										  style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea>
							</div>
						</div>
						<p class="error-message"></p>
					</div>
					<button class="btn btn-default" type="submit">send</button>
				</div>
			</form>
		</div>
	</section>

	<section class="our-awards">
		<div class="container">
			<h2 class="title-dots"><span>Our awards</span></h2>

			<ul>
				<li>
					<div class="illustration" data-animation="anim04">
						<img src="images/gif/circle-award.gif" alt="icon">
						<img class="gif" src="images/gif/circle-award.png" alt="icon">
					</div>
					<p>Entertainment Arena Expo:<br>
						Product of the Year</p>
				</li>
				<li>
					<div class="illustration">
						<img src="images/gif/circle-slot.gif" alt="icon">
						<img class="gif" src="images/gif/circle-slot.png" alt="icon">
					</div>
					<p>Slots Guide: The Ninja and Satoshi’s<br>
						Secret Reader's choice Slot 2015</p>
				</li>
				<li>
					<div class="illustration">
						<img src="images/gif/circle-stars.gif" alt="icon">
						<img class="gif" src="images/gif/circle-stars.png" alt="icon">
					</div>
					<p>Slots Guide: The Ninja second place<br>
						in the Best New Online Slot 2015</p>
				</li>
				<li>
					<div class="illustration">
						<img src="images/gif/circle-check.gif" alt="icon">
						<img class="gif" src="images/gif/circle-check.png" alt="icon">
					</div>
					<p>Slots Guide: Best Online<br>
						Slot Developer 2015</p>
				</li>

			</ul>
		</div>
	</section>

	<section class="our-team">
		<div id="plexus02" class="plexus"></div>

		<div class="container">
			<h2 class="title-dots"><span>some of our team</span></h2>

			<ul class="team-members-list slider slider-light">

				<li class="member">
					<div class="flipper">
						<div class="front">
							<div class="illustration">
								<img src="images/team/honza.png" alt="">
							</div>
							<div class="details">
								<h3>Andrejka Kuzminova</h3>
								<p>PHP Looser</p>
							</div>
						</div>
						<div class="back">
							<div class="info">
								<div class="details">
									<h3>Andrejka Kuzminova</h3>
									<p>PHP Looser</p>
								</div>
								<ul class="contact">
									<li><a class="fa fa-envelope" href="mailto:mail@endorphina.com">mail@endorphina.com</a></li>
									<li><span class="fa fa-skype">endorphina.com</span></li>
									<li>
										<a class="linkedin" href="">
											<img src="images/icon/icon-linkedin.png" alt="">
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</li>
				<li class="member">
					<div class="flipper">
						<div class="front">
							<div class="illustration">
								<img src="images/team/jakub.png" alt="">
							</div>
							<div class="details">
								<h3>Andrejka Kuzminova</h3>
								<p>PHP Looser</p>
							</div>
						</div>
						<div class="back">
							<div class="info">
								<div class="details">
									<h3>Andrejka Kuzminova</h3>
									<p>PHP Looser</p>
								</div>
								<ul class="contact">
									<li><a class="fa fa-envelope" href="mailto:mail@endorphina.com">mail@endorphina.com</a></li>
									<li><span class="fa fa-skype" href="skype:endorphina.com?chat">endorphina.com</span></li>
									<li>
										<a class="linkedin" href="">
											<img src="images/icon/icon-linkedin.png" alt="">
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</li>
				<li class="member">
					<div class="flipper">
						<div class="front">
							<div class="illustration">
								<img src="images/team/lucie.png" alt="">
							</div>
							<div class="details">
								<h3>Andrejka Kuzminova</h3>
								<p>PHP Looser</p>
							</div>
						</div>
						<div class="back">
							<div class="info">
								<div class="details">
									<h3>Andrejka Kuzminova</h3>
									<p>PHP Looser</p>
								</div>
								<ul class="contact">
									<li><a class="fa fa-envelope" href="mailto:mail@endorphina.com">mail@endorphina.com</a></li>
									<li><span class="fa fa-skype" href="skype:endorphina.com?chat">endorphina.com</span></li>
									<li>
										<a class="linkedin" href="">
											<img src="images/icon/icon-linkedin.png" alt="">
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</li>
				<li class="member">
					<div class="flipper">
						<div class="front">
							<div class="illustration">
								<img src="images/team/miklos.png" alt="">
							</div>
							<div class="details">
								<h3>Andrejka Kuzminova</h3>
								<p>PHP Looser</p>
							</div>
						</div>
						<div class="back">
							<div class="info">
								<div class="details">
									<h3>Andrejka Kuzminova</h3>
									<p>PHP Looser</p>
								</div>
								<ul class="contact">
									<li><a class="fa fa-envelope" href="mailto:mail@endorphina.com">mail@endorphina.com</a></li>
									<li><span class="fa fa-skype" href="skype:endorphina.com?chat">endorphina.com</span></li>
									<li>
										<a class="linkedin" href="">
											<img src="images/icon/icon-linkedin.png" alt="">
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</li>
				<li class="member">
					<div class="flipper">
						<div class="front">
							<div class="illustration">
								<img src="images/team/zdenko.png" alt="">
							</div>
							<div class="details">
								<h3>Andrejka Kuzminova</h3>
								<p>PHP Looser</p>
							</div>
						</div>
						<div class="back">
							<div class="info">
								<div class="details">
									<h3>Andrejka Kuzminova</h3>
									<p>PHP Looser</p>
								</div>
								<ul class="contact">
									<li><a class="fa fa-envelope" href="mailto:mail@endorphina.com">mail@endorphina.com</a></li>
									<li><span class="fa fa-skype" href="skype:endorphina.com?chat">endorphina.com</span></li>
									<li>
										<a class="linkedin" href="">
											<img src="images/icon/icon-linkedin.png" alt="">
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</li>
				<li class="member">
					<div class="flipper">
						<div class="front">
							<div class="illustration">
								<img src="images/team/team-member-03.png" style="bottom: 45px" alt="">
							</div>
							<div class="details">
								<h3>Andrejka Kuzminova</h3>
								<p>PHP Looser</p>
							</div>
						</div>
						<div class="back">
							<div class="info">
								<div class="details">
									<h3>Andrejka Kuzminova</h3>
									<p>PHP Looser</p>
								</div>
								<ul class="contact">
									<li><a class="fa fa-envelope" href="mailto:mail@endorphina.com">mail@endorphina.com</a></li>
									<li><span class="fa fa-skype" href="skype:endorphina.com?chat">endorphina.com</span></li>
									<li>
										<a class="linkedin" href="">
											<img src="images/icon/icon-linkedin.png" alt="">
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</section>

</main>

<div class="modal-wrapper"></div>

<?php include("footer.html"); ?>

<script src="js/libs/particles.min.js"></script>
<script>
	/* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
	particlesJS.load('plexus01', 'json/particlesjs.json');
	particlesJS.load('plexus02', 'json/particlesjs.json');
</script>

</body>

</html>