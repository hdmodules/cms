<!DOCTYPE html>
<html lang="ru">

<?php include ("head.html"); ?>

<style>
	.slider-for img {
		height: 400px;
	}
</style>

<body>

<?php include("header.html"); ?>

<main class="page-game-detail">

	<section class="breadcrumbs">
		<div class="container">
			<ul>
				<li><a href="">Home</a></li>
				<li><a href="">games</a></li>
				<li><a href="">Themes</a></li>
				<li><a href="">Horror</a></li>
				<li><span>Devil’s fortune</span></li>
			</ul>
		</div>
	</section>

	<section class="content">
		<div class="container">
			<h1 class="title-line">
				<span>Devil’s fortune</span>
			</h1>

			<div class="game-slider">
				<ul class="slider-for">
					<li><img src="images/game/game-rectangle-01.jpg" alt=""></li>
					<li><img src="images/game/game-rectangle-02.jpg" alt=""></li>
					<li><img src="images/game/game-rectangle-01.jpg" alt=""></li>
				</ul>
				<ul class="slider-nav">
					<li><img src="images/game/game-rectangle-01.jpg" alt=""></li>
					<li><img src="images/game/game-rectangle-02.jpg" alt=""></li>
					<li><img src="images/game/game-rectangle-01.jpg" alt=""></li>
				</ul>
			</div>

			<div class="game-details">
				<ul>
					<li><span>9 lines</span></li>
					<li><span>3d graphics</span></li>
					<li><span>free spins</span></li>
					<li><span>something</span></li>
					<li><span>anything</span></li>
					<li><span>nothing</span></li>
				</ul>
			</div>

			<div class="buttons">
				<a class="btn btn-primary" data-open-modal="play-now" href="">Play now</a>
			</div>

			<div class="game-info text">
				<p>This amazing slot will take you into the depths of Dr. Frankenslot’slaboratory, where you can control
					the legendary monster in a thrilling bonus round! With scatter pays and electrifying Wilds, you
					won’t want to miss becoming part of this classic story!</p>

				<ul>
					<li>sdjfisdjfoasnof</li>
					<li>sdjfisdjfoasnoffd</li>
					<li>sdjfisdjfoasnoffsdf</li>
					<li>sdjfisdjfoasnofds</li>
				</ul>
			</div>

			<div class="tags">
				<span>Tags:</span>
				<ul>
					<li><a href="">Popular</a>,</li>
					<li><a href="">9 line</a>,</li>
					<li><a href="">Horror</a></li>
				</ul>
			</div>
		</div>
	</section>

	<section class="big-wins">
		<div class="container">
			<h2 class="title-dots title-light"><span>big wins</span></h2>
			<ul class="games-list slider">
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-01.jpg" alt="">
						<div class="hover">
							<div class="icon-play">
								<img src="images/icon/icon-play.png" alt="">
								<span>Play video</span>
							</div>
						</div>
					</a>
					<p class="game-info info-light">Satoshi's Secret first Bitcoin themed slot by Endorphina</p>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-02.jpg" alt="">
						<div class="hover">
							<div class="icon-play">
								<img src="images/icon/icon-play.png" alt="">
								<span>Play video</span>
							</div>
						</div>
					</a>
					<p class="game-info info-light">Satoshi's Secret first Bitcoin themed slot by Endorphina</p>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-01.jpg" alt="">
						<div class="hover">
							<div class="icon-play">
								<img src="images/icon/icon-play.png" alt="">
								<span>Play video</span>
							</div>
						</div>
					</a>
					<p class="game-info info-light">Satoshi's Secret first Bitcoin themed slot by Endorphina</p>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-02.jpg" alt="game">
						<div class="hover">
							<div class="icon-play">
								<img src="images/icon/icon-play.png" alt="play">
								<span>Play video</span>
							</div>
						</div>
					</a>
					<p class="game-info info-light">Satoshi's Secret first Bitcoin themed slot by Endorphina</p>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-01.jpg" alt="">
						<div class="hover">
							<div class="icon-play">
								<img src="images/icon/icon-play.png" alt="">
								<span>Play video</span>
							</div>
						</div>
					</a>
					<p class="game-info info-light">Satoshi's Secret first Bitcoin themed slot by Endorphina</p>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-02.jpg" alt="">
						<div class="hover">
							<div class="icon-play">
								<img src="images/icon/icon-play.png" alt="">
								<span>Play video</span>
							</div>
						</div>
					</a>
					<p class="game-info info-light">Satoshi's Secret first Bitcoin themed slot by Endorphina</p>
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
			</ul>
		</div>
	</section>

	<section class="reviews">
		<div class="container">
			<h2 class="title-dots">
				<span>video reviews</span>
			</h2>
			<div class="games-slider">
				<ul class="games-list cols-3 slider slider-dark">
					<li>
						<a class="game" data-open-modal="play-video" href="">
							<img src="images/game/game-square-01.jpg" alt="game">
							<div class="hover">
								<div class="icon-play">
									<img src="images/icon/icon-play.png" alt="play">
									<span>Play video</span>
								</div>
							</div>
						</a>
						<p class="game-info">Satoshi's Secret first Bitcoin themed slot by Endorphina</p>
						<span class="fa fa-heart-o">2039</span>
						<span class="fa fa-eye">38412</span>
					</li>
					<li>
						<a class="game" data-open-modal="play-video" href="">
							<img src="images/game/game-square-01.jpg" alt="game">
							<div class="hover">
								<div class="icon-play">
									<img src="images/icon/icon-play.png" alt="play">
									<span>Play video</span>
								</div>
							</div>
						</a>
						<p class="game-info">Satoshi's Secret first Bitcoin themed slot by Endorphina</p>
						<span class="fa fa-heart-o">2039</span>
						<span class="fa fa-eye">38412</span>
					</li>
					<li>
						<a class="game" data-open-modal="play-video" href="">
							<img src="images/game/game-square-01.jpg" alt="game">
							<div class="hover">
								<div class="icon-play">
									<img src="images/icon/icon-play.png" alt="play">
									<span>Play video</span>
								</div>
							</div>
						</a>
						<p class="game-info">Satoshi's Secret first Bitcoin themed slot by Endorphina</p>
						<span class="fa fa-heart-o">2039</span>
						<span class="fa fa-eye">38412</span>
					</li>
					<li>
						<a class="game" data-open-modal="play-video" href="">
							<img src="images/game/game-square-01.jpg" alt="game">
							<div class="hover">
								<div class="icon-play">
									<img src="images/icon/icon-play.png" alt="play">
									<span>Play video</span>
								</div>
							</div>
						</a>
						<p class="game-info">Satoshi's Secret first Bitcoin themed slot by Endorphina</p>
						<span class="fa fa-heart-o">2039</span>
						<span class="fa fa-eye">38412</span>
					</li>
				</ul>
			</div>
			<div class="add-button tiles">
				<a href="" data-open-modal="modal-01">
					<div class="icon-add">
						<i class="fa fa-plus"></i>
						<span>ADD REVIEW</span>
					</div>
				</a>
			</div>
		</div>
	</section>

	<section class="reviews">
		<div class="container">
			<h2 class="title-dots">
				<span>reviews</span>
			</h2>
			<div class="games-slider">
				<ul class="games-list cols-3 slider slider-dark">
					<li>
						<a class="game" data-open-modal="play-video" href="">
							<img src="images/game/game-square-01.jpg" alt="game">
							<div class="hover">
								<!--<div class="icon-play">-->
								<!--<img src="images/icon/icon-play.png" alt="play">-->
								<!--<span>Play now</span>-->
								<!--</div>-->
							</div>
						</a>
						<p class="game-info">Satoshi's Secret first Bitcoin themed slot by Endorphina</p>
						<span class="fa fa-heart-o">2039</span>
						<span class="fa fa-eye">38412</span>
					</li>
					<li>
						<a class="game" data-open-modal="play-video" href="">
							<img src="images/game/game-square-01.jpg" alt="game">
							<div class="hover">
								<div class="icon-play">
									<img src="images/icon/icon-play.png" alt="play">
									<span>Play video</span>
								</div>
							</div>
						</a>
						<p class="game-info">Satoshi's Secret first Bitcoin themed slot by Endorphina</p>
						<span class="fa fa-heart-o">2039</span>
						<span class="fa fa-eye">38412</span>
					</li>
					<li>
						<a class="game" data-open-modal="play-video" href="">
							<img src="images/game/game-square-02.jpg" alt="game">
							<div class="hover">
								<!--<div class="icon-play">-->
								<!--<img src="images/icon/icon-play.png" alt="play">-->
								<!--<span>Play now</span>-->
								<!--</div>-->
							</div>
						</a>
						<p class="game-info">Satoshi's Secret first Bitcoin themed slot by Endorphina</p>
						<span class="fa fa-heart-o">2039</span>
						<span class="fa fa-eye">38412</span>
					</li>
					<li>
						<a class="game" data-open-modal="play-video" href="">
							<img src="images/game/game-square-01.jpg" alt="game">
							<div class="hover">
								<!--<div class="icon-play">-->
								<!--<img src="images/icon/icon-play.png" alt="play">-->
								<!--<span>Play now</span>-->
								<!--</div>-->
							</div>
						</a>
						<p class="game-info">Satoshi's Secret first Bitcoin themed slot by Endorphina</p>
						<span class="fa fa-heart-o">2039</span>
						<span class="fa fa-eye">38412</span>
					</li>
				</ul>
			</div>
			<div class="add-button tiles">
				<a href="" data-open-modal="modal-01">
					<div class="icon-add">
						<i class="fa fa-plus"></i>
						<span>ADD REVIEW</span>
					</div>
				</a>
			</div>
		</div>
	</section>

	<section class="subscriber subscriber-dark">
		<div id="plexus01" class="plexus"></div>

		<div class="container">
			<h3>news subscription</h3>
			<form class="form-light" action="">
				<div class="input">
					<input type="text" placeholder="e-mail address">
					<span class="icon">
						<i class="fa fa-envelope-o"></i>
					</span>

					<div class="error-message">Input cannot be blank.</div>
				</div>

				<div class="buttons">
					<div class="input input-recaptcha">
						<input type="hidden" id="subscriber-recaptcha-2" name="Subscriber[reCaptcha]">
						<div id="recaptcha-2" data-sitekey="6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU"
							 data-recaptcha-object="recaptcha2" data-callback="recaptchaCallback">
							<div>
								<div style="width: 304px; height: 78px;">
									<iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU&amp;co=aHR0cDovL3ByZXByb2QuY2FzZXhlLmNvbTo4MA..&amp;hl=en&amp;v=r20160502112552&amp;theme=light&amp;size=normal&amp;cb=ftudccmhzbdi"
											title="recaptcha widget" width="304" height="78" role="presentation"
											frameborder="0" scrolling="no" name="undefined"></iframe>
								</div>
								<textarea id="g-recaptcha-response-2" name="g-recaptcha-response"
										  class="g-recaptcha-response"
										  style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea>
							</div>
						</div>
						<p class="error-message"></p>
					</div>
					<button class="btn btn-primary" type="submit">send</button>
				</div>
			</form>
		</div>
	</section>

	<section class="similar-games">
		<div class="container">
			<h2 class="title-dots">
				<span>similar games</span>
			</h2>
			<ul class="games-list slider slider-dark">
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-01.jpg" alt="game">
						<div class="hover">
							<!--<div class="icon-play">-->
							<!--<img src="images/icon/icon-play.png" alt="play">-->
							<!--<span>Play now</span>-->
							<!--</div>-->
						</div>
					</a>
					<!--<h3>Devil’s fortune</h3>-->
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-02.jpg" alt="game">
						<div class="hover">
							<!--<div class="icon-play">-->
							<!--<img src="images/icon/icon-play.png" alt="play">-->
							<!--<span>Play now</span>-->
							<!--</div>-->
						</div>
					</a>
					<!--<h3>the ninja</h3>-->
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-01.jpg" alt="game">
						<div class="hover">
							<!--<div class="icon-play">-->
							<!--<img src="images/icon/icon-play.png" alt="play">-->
							<!--<span>Play now</span>-->
							<!--</div>-->
						</div>
					</a>
					<!--<h3>safari</h3>-->
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-02.jpg" alt="game">
						<div class="hover">
							<!--<div class="icon-play">-->
							<!--<img src="images/icon/icon-play.png" alt="play">-->
							<!--<span>Play now</span>-->
							<!--</div>-->
						</div>
					</a>
					<!--<h3>twerk</h3>-->
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-01.jpg" alt="game">
						<div class="hover">
							<!--<div class="icon-play">-->
							<!--<img src="images/icon/icon-play.png" alt="play">-->
							<!--<span>Play now</span>-->
							<!--</div>-->
						</div>
					</a>
					<!--<h3>safari</h3>-->
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
				<li>
					<a class="game" href="">
						<img src="images/game/game-square-02.jpg" alt="game">
						<div class="hover">
							<!--<div class="icon-play">-->
							<!--<img src="images/icon/icon-play.png" alt="play">-->
							<!--<span>Play now</span>-->
							<!--</div>-->
						</div>
					</a>
					<!--<h3>twerk bitch</h3>-->
					<span class="fa fa-heart-o">2039</span>
					<span class="fa fa-eye">38412</span>
				</li>
			</ul>
		</div>
	</section>

</main>

<?php include("footer.html"); ?>

<script src="js/libs/particles.min.js"></script>
<script>
	/* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
	particlesJS.load('plexus01', 'json/particlesjs02.json');
</script>

<div class="modal-wrapper">

	<section class="modal modal-form" data-modal="modal-01">
		<button class="close" data-close-modal>close<span>×</span></button>

		<div class="modal-body ">
			<div class="form-wrapper">
				<form class="form-dark" action="">
					<h2>Add Review</h2>
					<div class="input error">
						<input type="text" placeholder="name*">
					<span class="icon">
						<i class="fa fa-user"></i>
					</span>

						<div class="error-message">Input cannot be blank.</div>
					</div>
					<div class="input">
						<input type="email" placeholder="e-mail address*">
					<span class="icon">
						<i class="fa fa-envelope-o"></i>
					</span>

						<div class="error-message">Input cannot be blank.</div>
					</div>
					<div class="input">
						<input type="text" placeholder="review url*">
					<span class="icon">
						<i class="fa fa-link"></i>
					</span>

						<div class="error-message">Input cannot be blank.</div>
					</div>
					<p class="form-info"><span>*</span> Required fields</p>
					<div class="buttons">
						<div class="input input-recaptcha">
							<input type="hidden" id="subscriber-recaptcha-3" name="Subscriber[reCaptcha]">
							<div id="recaptcha-3" data-sitekey="6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU"
								 data-recaptcha-object="recaptcha2" data-callback="recaptchaCallback">
								<div>
									<div style="width: 304px; height: 78px;">
										<iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LePTBETAAAAAERq6Mkf9NRwwYhhyxHKxdOsIieU&amp;co=aHR0cDovL3ByZXByb2QuY2FzZXhlLmNvbTo4MA..&amp;hl=en&amp;v=r20160502112552&amp;theme=light&amp;size=normal&amp;cb=ftudccmhzbdi"
												title="recaptcha widget" width="304" height="78" role="presentation"
												frameborder="0" scrolling="no" name="undefined"></iframe>
									</div>
								<textarea id="g-recaptcha-response-3" name="g-recaptcha-response"
										  class="g-recaptcha-response"
										  style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea>
								</div>
							</div>
							<p class="error-message"></p>
						</div>
						<button class="btn btn-primary" type="submit">send</button>
					</div>
				</form>
				<div class="success-msg">
					<div class="info">
						<strong>Your review has been submitted, once it is admin approved we will let you know.</strong>
						<div class="buttons">
							<a class="btn btn-default" href="">all our games</a>
						</div>
					</div>
				</div>
				<div class="error-msg">
					<div class="info">
						<strong>Blablablablabla, something went wrong!!!</strong>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="modal modal-play-now loading" data-modal="play-now">
		<div class="links">
			<a class="game-info" href="">game info<span>i</span></a>
			<button class="close" data-close-modal>close<span>×</span></button>
		</div>
		<div class="modal-body"></div>
	</section>

	<section class="modal modal-play-video" data-modal="play-video">
		<div class="links">
			<button class="close" data-close-modal>close<span>×</span></button>
		</div>
		<div class="modal-body ">
			<iframe width="680" height="380" src="//www.youtube.com/embed/3fumBcKC6RE" frameborder="0"
					allowfullscreen></iframe>
		</div>
	</section>

</div>

</body>

</html>