<?php

namespace common\models;

use Yii;
use yii\helpers\Json;
/**
 * This is the model class for table "log".
 *
 * @property integer $id
 * @property integer $create_time
 * @property string $message
 */
class Log extends \yii\db\ActiveRecord {
    
    const TYPE_INFO = 0;
    const TYPE_WARNING = 1;
    const TYPE_ERROR = 2;
    
    const STATUS_START = 1;
    const STATUS_END = 2;
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'log';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['create_time', 'type'], 'number'],
            ['create_time', 'default', 'value' => time()],
            [['message'], 'string'],
        ];
    }
        /**
     * @inheritdoc
     */
    public static function info($message) {
        $status = false;
        if($message){
            $model = new Log();
            $model->loadDefaultValues();
            $model->type = self::TYPE_INFO;
            $model->message = $message;
            
            if($model->save()){
                $status = true;
            } 
            /*else {
                Yii::info('Log error:' . print_r($model->getErrors(), 1));
            }*/
            
        } 
        
        return $status;
    }
        /**
     * @inheritdoc
     */
    public static function warning($message) {
        if($message){
            $model = new Log();
            $model->loadDefaultValues();
            $model->type = self::TYPE_WARNING;
            $model->message = $message;
            
            if($model->save()){
                Yii::info('Log save!');
            } else {
                Yii::info('Log error:' . print_r($model->getErrors(), 1));
            }
            
        }
    }
        /**
     * @inheritdoc
     */
    public static function error($message) {
        if($message){
            $model = new Log();
            $model->loadDefaultValues();
            $model->type = self::TYPE_ERROR;
            $model->message = $message;
            
            if($model->save()){
                Yii::info('Log save!');
            } else {
                Yii::info('Log error:' . print_r($model->getErrors(), 1));
            }
            
        }
    }
    /**
     * @inheritdoc
     */
    public static function getType($type = null) {
        $data = [
            self::TYPE_INFO => 'Info',
            self::TYPE_WARNING => 'Warning',
            self::TYPE_ERROR => 'Error'
        ];
        
        return isset($type) ? $data[$type] : $data;
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'create_time' => 'Create Time',
            'chat_id' => 'Chat ID',
            'params_post' => 'Params Post',
            'params_get' => 'Params Get',
            'params_raw' => 'Params Raw',
            'message' => 'Message',
        ];
    }

}
