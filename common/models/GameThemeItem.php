<?php

namespace common\models;

use hdmodules\content\models\Item;
use backend\behaviors\ThemeBehavior;
use backend\models\Theme;

class GameThemeItem extends Item {

    public $crop_info;

    public function behaviors() {
        $behaviors = parent::behaviors();
        return array_merge([
            'theme' => ThemeBehavior::className()
                ], $behaviors);
    }

    public function getTheme() {
        return $this->hasOne(Theme::className(), ['content_id' => 'id']);
    }

    public static function getCacheDependency() {
        return new \yii\caching\DbDependency(['sql' => 'SELECT CONCAT(MAX(`content_blocks_item`.update_time), "_", :language) FROM `content_blocks_item` 
                                                               LEFT JOIN `content_block` ON `content_blocks_item`.`block_id` = `content_block`.`id` 
                                                               WHERE `content_block`.`slug`="games-theme"', 'params' => ['language' => \Yii::$app->language]]);
    }
}
