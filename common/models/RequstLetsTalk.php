<?php
namespace  common\models;

use Yii;
use yii\helpers\Json;

class RequstLetsTalk extends Request{

    public $file;

    public function rules() {
        $rules = parent::rules();
        return array_merge([
            //[['file'], 'file',  'extensions' => ['png', 'jpg', 'doc', 'docs', 'pdf'], 'maxFiles' => 3, 'maxSize'=>'1024*1024*1'],
            [['file'], 'file',  'extensions' => ['png', 'jpg', 'doc', 'docs', 'pdf'], 'maxFiles' => 3, 'maxSize'=>'1048576'],
            //[['file'], 'file', 'maxFiles' => 3],

        ],$rules);
    }

    public static function getFiles($json_files)
    {
        $files = null;

        if($json_files && !empty($json_files)){
            $json_files = Json::decode($json_files);
            if(isset($json_files['file'])){
                foreach ($json_files['file'] as $file){
                    $files .= '<a href="/adminpanel/uploads/'.$file.'" target="_blank">'.$file.'</a><br>';
                }
            }
        }

        return $files;
    }
    
    public static function getCareerPage($json)
    {
        $careerPage = '';

        if(isset($json) && $json){
            $json_array = Json::decode($json);

            if(isset($json_array['career_slug'])){
                $career = \backend\models\CareerItemContent::find()->where(['slug' => $json_array['career_slug']])->one();
                if($career){
                    $careerPage = \yii\helpers\Html::a($career->title, Yii::$app->urlManagerFrontEnd->createUrl(['career/view', 'slug' => $career->slug]));
                }
            }
        }

        return $careerPage;
    }
}