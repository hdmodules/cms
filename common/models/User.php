<?php

  namespace common\models;

  use Yii;
  use yii\base\NotSupportedException;
  use yii\behaviors\TimestampBehavior;
  use yii\db\ActiveRecord;
  use yii\web\IdentityInterface;
  use common\models\Language;
  use yii\rbac\DbManager;

  /**
   * User model
   *
   * @property integer $id
   * @property string $username
   * @property string $password_hash
   * @property string $password_reset_token
   * @property string $email
   * @property string $auth_key
   * @property integer $role
   * @property integer $status
   * @property integer $created_at
   * @property integer $updated_at
   * @property string $password write-only password
   */
  class User extends ActiveRecord implements IdentityInterface {
    
    const DEFAULT_USER_ID = -1;
    
    const STATUS_DISABLED = 0;
    const STATUS_ENABLED = 1;
    
    const ROLE_GUEST = 0;
    const ROLE_USER = 10;
    const ROLE_ADMIN = 30;
    
    const SERVICE_SITE = -1;
    const SERVICE_VK = 1;
    const SERVICE_FACEBOOK = 2;
    
    const GENDER_FEMALE = 0;
    const GENDER_MALE = 1;

    public $change_password;
    public $password = '';
    public $password_repeat = '';
    /**
     * @inheritdoc
     */
    public static function tableName() {
      return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
      return [
          'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
      ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
      return [
          [['password', 'password_repeat', 'change_password'], 'safe'],
          [['password', 'password_repeat'], 'string', 'min' => 6],
          ['change_password', 'required', 'when' => function($model) {
                //Change password if option selected
                if(isset($this->change_password) && $this->change_password){
                    if(empty($this->password) || empty($this->password_repeat)){
                        $this->addError('change_password', 'Password and Repeat password can not be blank');
                    } else {
                        if($this->password == $this->password_repeat){
                            $this->generateAuthKey();
                            $this->setPassword($this->password);
                        } else {
                            $this->addError('change_password', 'Passwords are different');
                        }
                    }
                }
          }],        
          ['status', 'default', 'value' => self::STATUS_ENABLED],
          ['status', 'in', 'range' => [self::STATUS_ENABLED, self::STATUS_DISABLED]],
          ['service', 'default', 'value' => self::SERVICE_SITE],
          ['service', 'in', 'range' => [self::SERVICE_SITE, self::SERVICE_VK, self::SERVICE_FACEBOOK]],
          ['gender', 'in', 'range' => [self::GENDER_FEMALE, self::GENDER_MALE]],
          ['role', 'default', 'value' => self::ROLE_USER],
          ['role', 'in', 'range' => [self::ROLE_USER, self::ROLE_ADMIN]],
          [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'language_id']],
      ];
    }
    
    public function beforeSave($insert) {
      if (parent::beforeSave($insert)) {

        $user = self::find()->where(['id' => $this->id])->one();
        $newRole = intval($_POST['User']['role']);
        //Change role
        if ($newRole <> $user->role) {

          $item_name = User::getRoleDB($newRole);

          $auth = \backend\models\AuthAssignment::find()->where(['user_id' => $user->id])->one();
          $auth->item_name = $item_name;
          $auth->save();

        }
        return true;
      } else {
        return false;
      }
    }
    

    public static function getService($service = null) {

      $data = [
          self::SERVICE_SITE => 'Site', //Yii::t('album', 'STATUS_REJECTED')
          self::SERVICE_VK => 'Vkontakte',
          self::SERVICE_FACEBOOK => 'Facebook',
      ];

      if (isset($service)) {
        return isset($data[$service]) ? $data[$service] : null;
      } else {
        return $data;
      }
    }

    public static function getStatus($status = null) {

      $data = [
          self::STATUS_DISABLED => 'Disabled', //Yii::t('album', 'STATUS_REJECTED')
          self::STATUS_ENABLED => 'Enabled',
      ];

      if (isset($status)) {
        return isset($data[$status]) ? $data[$status] : null;
      } else {
        return $data;
      }
    }
    
    public static function getRole($role = null) {

      $data = [
          self::ROLE_GUEST => 'Guest',
          self::ROLE_USER => 'User',
          self::ROLE_ADMIN => 'Administrator',
      ];

      if (isset($role)) {
        return isset($data[$role]) ? $data[$role] : null;
      } else {
        return $data;
      }
    }
    
    public static function getRoleDB($role = null) {

      $data = [
          self::ROLE_USER => 'user',
          self::ROLE_ADMIN => 'admin',
      ];

      if (isset($role)) {
        return isset($data[$role]) ? $data[$role] : null;
      } else {
        return $data;
      }
    }
        /**
     * @inheritdoc
     */
    public function attributeLabels() {
      return [
          'id' => 'ID',
          'name' => 'Name',
          'surname' => 'Surname',
          'service' => 'Service',
          'role' => 'Role',
          'language_id' => 'Language',
          'created_at' => 'Registred',
          'updated_at' => 'Last time online',
          'status' => 'Status',
          'change_password' => 'Change password',
          'password' => 'Password',
          'password_repeat' => 'Repeat password',
      ];
    }

    public function getLanguage() {
      return $this->hasOne(Language::className(), ['language_id' => 'language_id']);
    }

    public static function getUsers() {
      $users = [];
      $dataArray = User::find()->select('id,username')->asArray()->all();
      if($dataArray){
        foreach($dataArray as $data){
          $users[$data['id']] = sprintf('(#%s) %s', $data['id'], $data['username']);
        }
      }
      return $users;
    }

    public static function findByEAuth($service) {
      if (!$service->getIsAuthenticated()) {
        throw new ErrorException('EAuth user should be authenticated before creating identity.');
      }
      
      Yii::info('User.findByEAuth(): service_id:' . $service->getAttribute('service_id'));
      Yii::info('User.findByEAuth(): service:' . $service->getAttribute('service'));
      $user = User::find()->where(['service_id' => $service->getAttribute('service_id'), 'service' => $service->getAttribute('service')])->one();
      //Register user if not exist
      Yii::info('User.findByEAuth(): service:' . $service->getAttribute('service'));
      if (!$user) {
        //Create new user
        $user = new User();
        
        $user->service = $service->getAttribute('service');
        $user->service_id = $service->getAttribute('service_id');
        $user->service_token = $service->getAttribute('token');
        $user->gender = $service->getAttribute('gender');
        $user->name = $service->getAttribute('name');
        $user->surname = $service->getAttribute('surname');
        $user->email = $service->getAttribute('email') ? $service->getAttribute('email') : 'no email';
        $user->setPassword(Yii::$app->security->generateRandomString(8));
        $user->generateAuthKey();

                //Set language
        $lang = \Yii::$app->getRequest()->getCookies()->getValue('lang');
        if (isset($lang)) {
          $language = Language::getLangByUrl($lang);
        } else {
          $language = Language::getDefaultLang();
        }
        $user->language_id = $language->id;
        
        $user->save();

        //Set user name
        $user->username = sprintf('(ID:%s) %s %s', $user->id, $service->getAttribute('surname'), $service->getAttribute('name'));
        $user->update();

        //Set user role
        $r = new DbManager;
        $r->init();
        $userRole = $r->getRole('user');
        $r->assign($userRole, $user->id);

      } else {
        $user->service_token = $service->getAttribute('token');
        $user->name = $service->getAttribute('name');
        $user->surname = $service->getAttribute('surname');
        //$user->email = isset($service->getAttribute('email')) ? $service->getAttribute('email') :  'no email';
        $user->updated_at = time();
        $user->update();
      }

      //Yii::$app->getSession()->set('user-' . $user->id, $service->getAttributes());

      return $user;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
       //Lock adminpanel
      /*if(!in_array($id, [73, 76])){
          return false;
      }*/
      return static::findOne(['id' => $id, 'status' => self::STATUS_ENABLED]);
    }

  /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
      throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($email) {
      return static::findOne(['email' => $email, 'status' => self::STATUS_ENABLED]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
      if (!static::isPasswordResetTokenValid($token)) {
        return null;
      }

      return static::findOne([
          'password_reset_token' => $token,
          'status' => self::STATUS_ENABLED,
      ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token) {
      if (empty($token)) {
        return false;
      }
      $expire = Yii::$app->params['user.passwordResetTokenExpire'];
      $parts = explode('_', $token);
      $timestamp = (int) end($parts);
      return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId() {
      return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
      return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
      return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
      return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
      $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
      $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
      $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
      $this->password_reset_token = null;
    }

  }
  