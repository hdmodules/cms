<?php
namespace  common\models;

use backend\models\Game;
use Yii;
use common\models\Request;
use hdmodules\base\models\Setting;
use yii\helpers\Html;
use yii\helpers\Json;

class RequestReview extends Request{

    public $url;

    public $type_review;

    public function behaviors() {
         $behaviors = parent::behaviors();
         return array_merge([
             'review'=>[
                 'class' => \frontend\behaviors\SaveReview::className(),
             ]
         ],$behaviors);
    }

    public function rules() {
        $rules = parent::rules();
        return array_merge([
            [['email', 'name', 'url'], 'required', 'on' => ['add-review']],
            [['url'], 'url', 'on' => ['add-review']],
            [['type_review'], 'safe', 'on' => ['add-review']],
        ],$rules);
    }

    public static function getJsonAttribute($json, $attr)
    {
        $value = null;

        if($json && !empty($json)){
            $json = Json::decode($json);
            if(isset($json[$attr])){
                $value = $json[$attr];
            }
        }
        return $value;
    }

    public static function getLinkReview($json)
    {
        $link = null;

        if($json && !empty($json)){
            $json = Json::decode($json);

            if(isset($json['model_id'])){
                $game = Game::find()->where(['id'=>$json['model_id']])->one();
                if($game){
                    $link =  Html::a($game->content->title, ['/game/reviews' , 'id'=>$game->content->id]);
                }
            }
        }
        return $link;
    }


}