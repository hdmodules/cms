<?php
namespace  common\models;

use hdmodules\base\models\Setting;

class Request extends \hdmodules\request\models\Request{

    public function behaviors() {
        $behaviors = parent::behaviors();

        if (!LOCALHOST) {
            $behaviors = array_merge([
                'sendMail' => [
                    'class' => \hdmodules\request\behaviors\SendMail::className(),
                    'mail_from' => 'noreply@endorphina.com',
                    'admin_emails' => [Setting::get('admin-emails')]
                ]
            ], $behaviors);
        }

        return array_merge([
            'task' => [
                'class' => \hdmodules\request\behaviors\TaskBehavior::className(),
                'params' => [
                    'name' => 'name',
                    'phone' => 'phone'
                ],
                'rm_params' => [
                    'Name' => 'name',
                    'Email' => 'email',
                    'Title' => 'title',
                    'Text' => 'text',
                    'Question' => 'question',
                    'Company' => 'company',
                    'Phone' => 'phone',
                    'Page' => 'page',
                    'Language' => 'lang'
                ],
            ],
        ], $behaviors);
    }

    public function rules() {
        $rules = parent::rules();
        return array_merge([
            ['phone', 'match', 'pattern' => '/[0-9]/', 'message' => \Yii::t('site', 'Phone must contain only digits')],
            [['email', 'name', 'text'], 'required', 'on' => ['main-page-footer', 'event-subscribe', 'error-page']],
            [['email', 'name', 'text', 'company'], 'required', 'on' => ['event-subscribe']],
            [['email', 'name'], 'required', 'on' => 'order-product-presentation'],
            [['email', 'name', 'text'], 'required', 'on' => ['main-contact-form', 'career-friend-recommend']],
            [['email', 'name', 'phone', 'text'], 'required', 'on' => ['lets-talk-career', 'lets-talk-sales', 'lets-talk-marketing', 'lets-talk-careerpartner-offer']]
        ],$rules);
    }

}