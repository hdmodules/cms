<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "language".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $local
 * @property integer $sort
 * @property integer $default
 * @property integer $status
 *
 * @property Clients[] $clients
 */
class Language extends \yii\db\ActiveRecord {

    const STATUS_DISABLE = 0;
    const STATUS_ENABLED = 1;

    static $current = null;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'language';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['language', 'language_id', 'status'], 'required'],
            [['sort', 'default_language', 'status'], 'integer'],
            [['name'], 'string', 'max' => 32],
            [['language'], 'string', 'max' => 2],
            [['language_id'], 'integer']
        ];
    }

    public static function getLanguages() {

        $data = [];

        $array = Language::find()->orderBy(['sort' => SORT_ASC])->asArray()->all();

        $data = \yii\helpers\ArrayHelper::map($array, 'language', 'name');

        return $data;
    }

    public function getLanguagesUrl() {

        $data = [];

        $array = Language::find()->orderBy(['sort' => SORT_ASC])->asArray()->all();

        $data = \yii\helpers\ArrayHelper::map($array, 'language', 'name');

        return $data;
    }

    public function getLanguageList() {

        $data = [];

        $array = Language::find()->where(['status' => self::STATUS_ENABLED])->asArray()->all();

        return $array;
    }

//Получение текущего объекта языка
    static function getCurrent() {
        if (self::$current === null) {
            self::$current = self::getDefaultLang();
        }
        return self::$current;
    }

//Установка текущего объекта языка и локаль пользователя
    static function setCurrent($url = null) {
        $language = self::getLangByUrl($url);
        self::$current = ($language === null) ? self::getDefaultLang() : $language;
        Yii::$app->language = self::$current->language_id;
        return $language;
    }

//Установка текущего объекта языка и локаль пользователя
    static function setCurrentById($id = null) {
        $language = Language::find()->where(['language_id' => $id])->one();
        self::$current = ($language === null) ? self::getDefaultLang() : $language;
        Yii::$app->language = self::$current->language_id;
    }

//Получения объекта языка по умолчанию
    static function getDefaultLang() {
        return Language::find()->where('`default_language` = :default_language', [':default_language' => 1])->one();
    }

//Получения объекта языка по буквенному идентификатору
    static function getLangByUrl($url = null) {
        if ($url === null) {
            return null;
        } else {
            $language = Language::find()->where('language = :url and status = :status', [':url' => $url, ':status' => self::STATUS_ENABLED])->one();
            if ($language === null) {
                return null;
            } else {
                return $language;
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'url' => Yii::t('app', 'Code'),
            'local' => Yii::t('app', 'Locale'),
            'sort' => Yii::t('app', 'Sort'),
            'default' => Yii::t('app', 'Default'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

}
