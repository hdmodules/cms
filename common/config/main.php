<?php

use common\models\User;

if (LOCALHOST) {
    $params = array_merge(
        require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
    );
} else {
    $params = require(__DIR__ . '/params.php');
}

return [
    'name' => 'Endorphina',
    'language' => 'en',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => ['debug'],
    'modules' => [
        'debug' => [
            'class' => 'yii\debug\Module',
            'allowedIPs' => ['127.0.0.1', '10.11.43.49', '89.190.54.62']
        ],
        'translatemanager' => [
            'class' => 'lajax\translatemanager\Module',
            'ignoredCategories' => ['yii', 'app', 'easyii', 'news', 'content', 'language', 'model', 'rbac-admin', 'easyii/file', 'easyii/file/api', 'easyii/text', 'base', 'easyii/landing', 
                                    'content/article', 'fileinput', 'kvdtime', 'kvdate', 'kvselect', 'kvcolor', 'array', 'javascript', 'kvbase', '{', 'eauth'],
            'roles' => ['@'],
            'allowedIPs' => ['127.0.0.1', '10.11.43.49', '89.190.54.81', '84.246.161.126', '94.230.144.40', '89.190.54.62'],
        ],
    ],
    'components' => [
        'db' => LOCALHOST ? require(__DIR__ . '/db-local.php') : require(__DIR__ . '/db.php'),
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest'], //здесь прописываем роли
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            
            'useFileTransport' => false,//set this property to false to send mails to real email addresses
            //comment the following array to send mail using php's mail function
            'enableSwiftMailerLogging' => true,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                /*'host' => '144.76.170.186',
                'username' => 'noreply@endorphina.com',
                'password' => 'U8Cc77yHoN81O9Oq',
                'port' => '587',
                'encryption' => 'tls',*/
            ],
         ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'db' => 'db',
                    'sourceLanguage' => 'en', // Developer language
                    'sourceMessageTable' => '{{%language_source}}',
                    'messageTable' => '{{%language_translate}}',
                    'cachingDuration' => 86400,
                    'enableCaching' => false,
                    'forceTranslation' => true,
                ]
            ]
        ],
        'formatter' => [
            'dateFormat' => 'php:d.m.Y',
            'timeFormat' => 'php:H:i',
            //'datetimeFormat' => 'dd.MM.yyyy HH:mm:ss',
            'datetimeFormat' => 'php:d.m.Y H:i',
            //'datetimeFormatFull' => 'php:d.m.Y H:i:s',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => 'Kč',
        ],
        'assetManager' => [
            'dirMode' => 0777,
            'fileMode' => 0777,
        ],
    ],
    'params' => $params,
];
