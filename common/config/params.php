<?php
return [
    'adminEmail' => 'noreply@endorphina.com',
    'sendEmail' => 'noreply@endorphina.com',
    'user.passwordResetTokenExpire' => 3600,
    'cachLiveTime' => 36000,
    'endorphinaAccountId' => 534,
    'mlConfig'=>[
        'default_language'=>'en',
        'languages'=>[
            'en'=>'Eng',
            //'ru'=>'Rus'
        ],
    ],
];
