<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use hdmodules\request\components\TaskResolver;
use hdmodules\request\models\RequestTask;
use hdmodules\base\models\Setting;

class TaskResolverController extends Controller {

    /**
     * tasks
     * @var array[]
     */
    public $tasks;

    /**
     * tasks to reresolve
     * @var array[]
     */
    public $error_tasks = array();

    /**
     * new tasks to resolve
     * @var array[]
     */
    public $new_tasks = array();

    /**
     * check_url
     * @var string
     */
    public $redmine_url = '';

    public function actionRedmine() {
   
        echo '<br>Redmine tasks: start!';
        
        //Yii::$app->cache->flush();
        //$status = Setting::get('enable-redmine-tasks-send');
        
        $status = Setting::find()->where(['name' => 'enable-redmine-tasks-send'])->one();
        
        if (isset($status) && $status->value) {
            $this->error_tasks = RequestTask::find()->where(['status' => RequestTask::STATUS_ERROR])->limit(50)->all();
            $errorTasksCount = count($this->error_tasks);

            echo '<br>Error tasks: ' . $errorTasksCount;

            //common\models\Log::info('Error tasks: ' . $errorTasksCount);
            
            $normalTasksCount = 50 - $errorTasksCount;
            $this->new_tasks = RequestTask::find()->where(['status' => RequestTask::STATUS_NEW])->limit($normalTasksCount)->all();

            $this->tasks = array_merge($this->error_tasks, $this->new_tasks);

            echo '<br>Tasks count: ' . count($this->tasks);
            
            //\common\models\Log::info('actionRedmine(): tasks:' . print_r($this->tasks, 1));
            //set in progress
            $this->setInProgress();

            echo '<br>Change status to progress.' . count($this->tasks);

            $this->redmine_url = RequestTask::REDMINE_URL;

            echo '<br>redmine_url:' . $this->redmine_url;
            
            TaskResolver::sendRedmineTasks($this->redmine_url, $this->tasks);

            return true;
        } else {
            echo '<br> Task sending disabled.';

            return true;
        }
    }

    /**
     * @param AbstractAffiliateAdapter $adapter
     *
     * @return mixed
     */
    protected function _checkServer($url) {
        //запилить это функцией
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl, CURLOPT_TIMEOUT, 5);
        curl_exec($curl);
        $result = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        return $result;
    }

    protected function setInProgress() {
        if($this->tasks){
            $taskIds = \yii\helpers\ArrayHelper::getColumn($this->tasks, 'id');
            RequestTask::updateAll(['status' => RequestTask::STATUS_IN_PROGRESS], ['id' => $taskIds]);
        }
    }

}
