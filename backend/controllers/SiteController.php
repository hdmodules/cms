<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public function beforeAction($event) {
        if(Yii::$app->user->isGuest || Yii::$app->user->identity->role <> \common\models\User::ROLE_ADMIN){
             $this->redirect(Yii::$app->urlManagerFrontEnd->createUrl(['site/index']));
        }
        return parent::beforeAction($event);
    }

    public function actions() {

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'view' => '@yiister/gentelella/views/error',
            ],
        ];
    }

    public function actionIndex()
    {
        $userCount = \common\models\User::find()->where(['status' => \common\models\User::STATUS_ENABLED])->count();

        \backend\models\GameSimilar::setSimilarGames(29);
        
        return $this->render('index', [
            'userCount' => $userCount,
        ]);
    }

    public function actionLogin() {

      return $this->redirect(Yii::$app->urlManagerFrontEnd->createUrl(['site/index']));

    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(Yii::$app->urlManagerFrontEnd->createUrl(['site/index']));
    }
}
