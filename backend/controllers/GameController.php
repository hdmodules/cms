<?php
namespace backend\controllers;

use backend\models\Game;
use backend\models\GamePoint;
use backend\models\GamePointRelation;
use backend\models\GameReview;
use hdmodules\content\models\Block;
use Yii;
use yii\data\ActiveDataProvider;
use hdmodules\content\controllers\ItemController;
use backend\models\GameItemContent;
use yii\widgets\ActiveForm;
use yii\web\NotFoundHttpException;


class GameController extends ItemController
{
    public $viewPath = '@backend/views/game';

    public $viewPathIndex = '@backend/views/game';

    public function init()
    {
        $this->class_model = GameItemContent::className();
        return parent::init();
    }

    public function actionIndex($id)
    {
        if(!($model = Block::findOne($id))){
            return $this->redirect(['/item/index']);
        }

        $provider = new \yii\data\ActiveDataProvider([
            'query' => GameItemContent::find()->where(['block_id' => $model->id])->orderBy(['order_num' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render($this->viewPathIndex.'/index', [
            'model' => $model,
            'provider'=>$provider
        ]);
    }

    public function actionPoints($id)
    {
        $class_model = $this->class_model;

        if(!($model = $class_model::findOne($id))){
            return $this->redirect(['/block/index']);
        }

        $point = new GamePoint();

        $points = new ActiveDataProvider([
            'query' => GamePoint::find(),
        ]);

        $game_point = new GamePointRelation();

        $game_points = new ActiveDataProvider([
            'query' => GamePointRelation::find()->where(['game_id'=>$model->game->id])->orderBy(['order_num' => SORT_DESC]),
        ]);

        if ($point->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else {
                if ($point->save()) {
                    $this->flash('success', Yii::t('game', 'Point created'));
                } else {
                    $this->flash('error', Yii::t('game', 'Create error. {0}', $point->formatErrors()));
                }
            }
        }

        if ($game_point->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else {
                if ($game_point->save()) {
                    $this->flash('success', Yii::t('game', 'Point added'));
                } else {
                    $this->flash('error', Yii::t('game', 'Create error. {0}', $point->formatErrors()));
                }
            }
        }

        return $this->render('points', [
            'model' => $model,
            'point'=>$point,
            'points'=>$points,
            'game_point'=>$game_point,
            'game_points'=>$game_points
        ]);
    }


    public function actionPointUp($id)
    {
        $game_point_rel = $this->findGamePointRelModel($id);
        return $this->move($id, 'up', ['game_id' => $game_point_rel->game_id]);
    }

    public function actionPointDown($id)
    {
        $game_point_rel = $this->findGamePointRelModel($id);
        return $this->move($id, 'down', ['game_id' => $game_point_rel->game_id]);
    }

    public function actionPointDelete($id)
    {
        $this->findGamePointRelModel($id)->delete();
        return $this->back();
    }

    public function actionReviews($id)
    {
        $class_model = $this->class_model;

        if(!($model = $class_model::findOne($id))){
            return $this->redirect(['/block/index']);
        }

        $review = new GameReview();

        if ($review->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($review);
            }
            else {
                if ($review->save()) {
                    $review = new GameReview();
                    $this->flash('success', Yii::t('game', 'Review created'));
                } else {
                    $this->flash('error', Yii::t('game', 'Create error. {0}', $review->formatErrors()));
                }
            }
        }

        return $this->render('reviews', [
            'model' => $model,
            'review'=>$review
        ]);
    }

    public function actionReviewUpdate($id)
    {
        $review = $this->findGameReviewModel($id);

        $model = $review->game->content;

        if ($review->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($review);
            }
            else {
                if ($review->save()) {
                    $this->flash('success', Yii::t('game', 'Review updated'));
                    return $this->redirect(['/game/reviews/'.$model->id, 'type'=>$review->type]);
                } else {
                    $this->flash('error', Yii::t('game', 'Update error. {0}', $review->formatErrors()));
                }
            }
        }

        return $this->render('update_review', [
            'model' => $model,
            'review'=>$review
        ]);
    }

    public function actionReviewUp($id)
    {
        $game_point_rel = $this->findGameReviewModel($id);
        return $this->move($id, 'up', ['game_id' => $game_point_rel->game_id]);
    }

    public function actionReviewDown($id)
    {
        $game_point_rel = $this->findGameReviewModel($id);
        return $this->move($id, 'down', ['game_id' => $game_point_rel->game_id]);
    }

    public function actionReviewDelete($id)
    {
        $this->findGameReviewModel($id)->delete();
        return $this->back();
    }


    public function actionClearReviewImage($id)
    {

        $model = GameReview::findOne($id);

        if($model === null){
            $this->flash('error', Yii::t('game', 'Not found'));
        }
        elseif($model->image){
            $model->image = '';
            if($model->save(false)){
                $this->flash('success', Yii::t('game', 'Image cleared'));
            } else {
                $this->flash('error', Yii::t('game', 'Update error. {0}', $model->formatErrors()));
            }
        }
        return $this->back();
    }

    protected function findGamePointRelModel($id)
    {
        if (($model = GamePointRelation::findOne($id)) !== null) {
            $sortable = $this->getBehavior('sortable');
            $sortable->model = GamePointRelation::className();
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findGameReviewModel($id)
    {
        if (($model = GameReview::findOne($id)) !== null) {
            $sortable = $this->getBehavior('sortable');
            $sortable->model = GameReview::className();
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}