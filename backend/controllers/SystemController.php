<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;

class SystemController extends Controller
{
    public $rootActions = ['*'];

    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionLogs()
    {
        return $this->render('logs');
    }  
    
    public function actionSlowLogs() {
        $logPath = Yii::getAlias('@backend/logs/fpm-php.slow.log');
        Yii::info('actionSlowLogs(): $logPath:' . $logPath);
        Yii::info('actionSlowLogs(): File is_readable:' . is_readable($logPath));
        Yii::info('actionSlowLogs(): File is_writable:' . is_writable($logPath));
        if (file_exists($logPath)) {
            Yii::info('actionSlowLogs(): File exist!');

            Yii::info('Content-Disposition: attachment; filename="' . basename($logPath) . '"');

            ini_set('memory_limit', '400M');

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($logPath) . '"');
            header('Content-Length: ' . filesize($logPath));
            header('Cache-Control: must-revalidate');
            header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
            header('Pragma: public');

            readfile($logPath);
        } else {
            Yii::info('actionSlowLogs(): File NOT exist!');
            Yii::$app->getSession()->setFlash('error', 'Slow log file not found!');
            return $this->redirect(['logs']);
        }
    }
    
    public function actionErrorLogs() {
        $logPath = Yii::getAlias('@backend/logs/fpm-php.error.log');
        Yii::info('actionErrorLogs(): $logPath:' . $logPath);
        Yii::info('actionErrorLogs(): File is_readable:' . is_readable($logPath));
        Yii::info('actionErrorLogs(): File is_writable:' . is_writable($logPath));
        if (file_exists($logPath)) {
            Yii::info('actionErrorLogs(): File exist!');

            Yii::info('Content-Disposition: attachment; filename="' . basename($logPath) . '"');

            ini_set('memory_limit', '400M');

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($logPath) . '"');
            header('Content-Length: ' . filesize($logPath));
            header('Cache-Control: must-revalidate');
            header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
            header('Pragma: public');

            readfile($logPath);
        } else {
            Yii::info('actionErrorLogs(): File NOT exist!');
            Yii::$app->getSession()->setFlash('error', 'Slow log file not found!');
            return $this->redirect(['logs']);
        }
    }
    
    public function actionAccessLogs() {
        $logPath = Yii::getAlias('@backend/logs/fpm-php.access.log');
        Yii::info('actionErrorLogs(): $logPath:' . $logPath);
        Yii::info('actionErrorLogs(): File is_readable:' . is_readable($logPath));
        Yii::info('actionErrorLogs(): File is_writable:' . is_writable($logPath));
        if (file_exists($logPath)) {
            Yii::info('actionErrorLogs(): File exist!');

            Yii::info('Content-Disposition: attachment; filename="' . basename($logPath) . '"');

            ini_set('memory_limit', '400M');

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($logPath) . '"');
            header('Content-Length: ' . filesize($logPath));
            header('Cache-Control: must-revalidate');
            header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
            header('Pragma: public');

            readfile($logPath);
        } else {
            Yii::info('actionErrorLogs(): File NOT exist!');
            Yii::$app->getSession()->setFlash('error', 'Slow log file not found!');
            return $this->redirect(['logs']);
        }
    }
    
    public function actionPhpInfo()
    {
        $this->layout = false;
        return $this->render('phpinfo');
    }
   /* public function actionUpdate()
    {
        $result = WebConsole::migrate();

        Setting::set('easyii_version', AdminModule::VERSION);
        Yii::$app->cache->flush();

        return $this->render('update', ['result' => $result]);
    }*/

    public function actionFlushCache()
    {
        Yii::$app->cache->flush();
        Yii::$app->getSession()->setFlash('success', 'Cache flushed');
        return $this->redirect(['index']);
    }

    public function actionRefreshGameStats()
    {
        $games = \backend\models\Game::find()->all();
        foreach ($games as $game){
            \backend\models\GameSimilar::setSimilarGames($game->id);
        }
        Yii::$app->getSession()->setFlash('success', 'Game stats refresh');
        return $this->redirect(['index']);
    }
    
    public function actionCopyPresentation()
    {
        
        $source = Yii::getAlias('@frontend/web/Endorphina_Presentation_Q4_2016.pdf');
        $target = Yii::getAlias('@frontend/web/uploads/Endorphina_Presentation_Q4_2016.pdf');
        
        if(copy($source, $target)){
            unlink($source);
            Yii::$app->getSession()->setFlash('success', 'Presentation copy.');
        }
        
        return $this->redirect(['index']);
    }
   /* public function actionClearThumbs() {
        $err = 0;

        foreach(glob($_SERVER['DOCUMENT_ROOT']."/uploads/thumbs/*.{jpg,png,gif,jpeg}",GLOB_BRACE) as $file) {
            if(!unlink($file)) {
                $err++;
            }
        }
        if($err) {
            $this->flash('error', Yii::t('easyii', 'Thumbs could not be deleted'));
        } else {
            $this->flash('success', Yii::t('easyii','Thumbs deleted'));
        }
        return $this->back();
    }*/

    public function actionClearAssets()
    {
        foreach(glob(Yii::getAlias('@frontend/web/assets') . DIRECTORY_SEPARATOR . '*') as $asset){
            if(is_link($asset)){
                unlink($asset);
            } elseif(is_dir($asset)){
                $this->deleteDir($asset);
            } else {
                unlink($asset);
            }
        }
        
        
        Yii::$app->getSession()->setFlash('success', 'Assets cleared');
        return $this->redirect(['index']);
    }

   /* public function actionLiveEdit($id)
    {
        Yii::$app->session->set('easyii_live_edit', $id);
        $this->back();
    }*/

    private function deleteDir($directory)
    {
        $iterator = new \RecursiveDirectoryIterator($directory, \RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new \RecursiveIteratorIterator($iterator, \RecursiveIteratorIterator::CHILD_FIRST);
        foreach($files as $file) {
            if ($file->isDir()){
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        return rmdir($directory);
    }

    public function recursiveChmod ($path, $filePerm=0644, $dirPerm=0755) {
        // Check if the path exists
        if (!file_exists($path)) {
            return(false);
        }

        // See whether this is a file
        if (is_file($path)) {
            // Chmod the file with our given filepermissions
            chmod($path, $filePerm);

            // If this is a directory...
        } elseif (is_dir($path)) {
            // Then get an array of the contents
            $foldersAndFiles = scandir($path);

            // Remove "." and ".." from the list
            $entries = array_slice($foldersAndFiles, 2);

            // Parse every result...
            foreach ($entries as $entry) {
                // And call this function again recursively, with the same permissions
                self::recursiveChmod($path."/".$entry, $filePerm, $dirPerm);
            }

            // When we are done with the contents of the directory, we chmod the directory itself
            chmod($path, $dirPerm);
        }

        // Everything seemed to work out well, return true
    }
}