<?php
namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use hdmodules\content\controllers\ItemController;
use backend\models\CareerItemContent;
use yii\widgets\ActiveForm;
use yii\web\NotFoundHttpException;


class CareerController extends ItemController
{
    public $viewPath = '@backend/views/career';

    public function init()
    {
        $this->class_model = CareerItemContent::className();
        return parent::init();
    }
    
    public function actionIndex($id)
    {
        if(!($model = \hdmodules\content\models\Block::findOne($id))){
            return $this->redirect(['/item/index']);
        }
        return $this->render('index', [
            'model' => $model
        ]);
    }

}