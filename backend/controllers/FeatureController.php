<?php
namespace backend\controllers;

use backend\models\GameFeatureItem;
use hdmodules\content\controllers\ItemController;

class FeatureController extends ItemController
{
    public $viewPath = '@backend/views/feature';

    public function init()
    {
        $this->class_model = GameFeatureItem::className();
        return parent::init();
    }

}