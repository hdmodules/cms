<?php
namespace backend\controllers;

use backend\models\GamePaylineItem;
use hdmodules\content\controllers\ItemController;

class PaylineController extends ItemController
{
    public $viewPath = '@backend/views/payline';

    public function init()
    {
        $this->class_model = GamePaylineItem::className();
        return parent::init();
    }

}