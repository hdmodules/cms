<?php
namespace backend\controllers;

use Yii;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

use hdmodules\base\helpers\Image;
use hdmodules\content\models\Block;
use hdmodules\content\models\Item;
use hdmodules\content\controllers\ItemController;
use backend\models\NewsItemContent;


class NewsController extends ItemController
{
    public $viewPath = '@backend/views/news';

    public function init()
    {
        $this->class_model = NewsItemContent::className();
        return parent::init();
    }

    public function actionCreate($id)
    {
        if(!($block = Block::findOne($id))){
            return $this->redirect(['/content/block/index']);
        }

        $model = new $this->class_model();
        $model->block_id = $block->primaryKey;

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else {
                if ($model->save()) {
                    $this->flash('success', Yii::t('content', 'Item created'));
                    return $this->redirect([($block->controller_name ? $block->controller_name.'/edit' :'/content/item/edit'), 'id' => $model->primaryKey]);
                } else {
                    $this->flash('error', Yii::t('content', 'Create error. {0}', $model->formatErrors()));
                    //return $this->refresh();
                }
            }
        }
        //else {
        return $this->render($this->viewPath.'/create', [
            'model' => $model,
            'block' => $block,
        ]);
        //}
    }

    /*
    public function actionEdit($id)
    {
        if(!($model = NewsItemContent::findOne($id))){
            return $this->redirect(['/block/index']);
        }

        $model->loadTranslations_custom();

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else {
                if (isset($_FILES)) {
                    $model->image = UploadedFile::getInstance($model, 'image');
                    if ($model->image && $model->validate(['image'])) {
                        $model->image = Image::upload($model->image, 'news');
                    } else {
                        $model->image = $model->oldAttributes['image'];
                    }
                }

                if ($model->save()) {
                    $this->flash('success', Yii::t('news', 'News updated'));
                    return $this->redirect([($model->block->controller_name ? $model->block->controller_name.'/edit' :'/content/item/edit'), 'id' => $model->primaryKey]);
                } else {
                    $this->flash('error', Yii::t('{', 'Update error. {0}', $model->formatErrors()));
                }
            }
        }

        return $this->render('edit', [
            'model' => $model,
        ]);

    }*/
    public function actionCrop($id) {

        if(!($model = NewsItemContent::findOne($id))){
            return $this->redirect(['/block/index']);
        }

        if (Yii::$app->request->isPost) {

            if (isset($_FILES) && !empty($_FILES)) {
                for ($i = 1; $i <= 3; $i++) {
                    $instance = UploadedFile::getInstance($model->news, 'cropped_' . $i);
                    if ($instance) {
                        $fileName = sprintf('%s_%s.%s', $id, $i, $instance->extension);
                        $model->news->{"cropped_" . $i} = Image::upload($instance, 'news', null, null, false, $fileName);
                    } else {
                        $model->news->{"cropped_" . $i} = null;
                    }
                }
            }
            
            if ($model->news->save(false)) {
                $this->flash('success', Yii::t('news', 'News updated'));
                return $this->redirect([($model->block->controller_name ? $model->block->controller_name . '/edit' : '/content/item/edit'), 'id' => $model->primaryKey]);
            } else {
                Yii::info('errors:' . print_r($model->news->getErrors(), 1));
                $this->flash('error', Yii::t('easyii', 'Update error. {0}', $model->formatErrors()));
                return $this->refresh();
            }
        }
        return $this->render('crop',['model' => $model, 'id' => $id]);
    }
    
    public function actionCrop123($id) {

        if(!($model = NewsItemContent::findOne($id))){
            return $this->redirect(['/block/index']);
        }

        if ($model->news->load(Yii::$app->request->post())) {
            /*if ($model->news->cropped_1 && $model->news->cropped_2 && $model->news->cropped_3) {

                /*for ($i = 1; $i <= 3; $i++) {
                     $imagename = $model->id;
                      $extension = pathinfo($model->{"image"},PATHINFO_EXTENSION);
                      $path = Yii::getAlias("@frontend/web/uploads/news/{$imagename}_$i.$extension");
                      if(!file_exists( Yii::getAlias("@frontend/web/uploads/news"))) {
                      mkdir( Yii::getAlias("@frontend/web/uploads/news"));
                      }
                      $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $model->news->{"cropped_$i"}));
                      file_put_contents($path,$data);+
                      $model->news->{"cropped_$i"} = str_replace(Yii::getAlias("@frontend/web"),'',$path); 
                    
                
                
                    $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
                    if ($model->upload()) {
                        // file is uploaded successfully
                        return;
                    }
                    
                    $model->news->{"cropped_$i"} = UploadedFile::getInstance($model->news, "cropped_$i");
                    if ($model->news->{"cropped_$i"} && $model->validate(["cropped_$i"])) {
                        $model->news->{"cropped_$i"} = Image::upload($model->news->{"cropped_$i"}, 'news');
                    }
                }
            }*/
            
            $model->news->cropped_1 = UploadedFile::getInstances($model->news, 'cropped_1');
            if ($model->news->upload()) {
                // file is uploaded successfully
                return;
            }
            
            if ($model->news->save()) {
                $this->flash('success', Yii::t('news', 'News updated'));
                return $this->redirect([($model->block->controller_name ? $model->block->controller_name . '/edit' : '/content/item/edit'), 'id' => $model->primaryKey]);
            } else {
                Yii::info('errors:' . print_r($model->news->getErrors(), 1));
                $this->flash('error', Yii::t('easyii', 'Update error. {0}', $model->formatErrors()));
                return $this->refresh();
            }
        }
        return $this->render('crop',['model' => $model, 'id' => $id]);
    }

    public function actionClearImage($id)
    {
        $model = NewsItemContent::findOne($id);

        if($model === null){
            $this->flash('error', Yii::t('content', 'Not found'));
        }
        elseif($model->image){

            $model->image = '';
            @unlink(Yii::getAlias("@frontend/web").$model->image);

            for($i=1; $i<=3; $i++){
                $model->news->{"cropped_$i"} = '';
                @unlink(Yii::getAlias("@frontend/web").$model->news->{"cropped_$i"});
            }

            if($model->update()){
                $this->flash('success', Yii::t('content', 'Image cleared'));
            } else {
                $this->flash('error', Yii::t('content', 'Update error. {0}', $model->formatErrors()));
            }
        }

        return $this->back();
    }
}