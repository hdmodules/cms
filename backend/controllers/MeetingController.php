<?php
namespace backend\controllers;

use Yii;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

use hdmodules\base\helpers\Image;
use hdmodules\content\models\Block;
use hdmodules\content\models\Item;
use hdmodules\content\controllers\ItemController;
use backend\models\MeetingItemContent;


class MeetingController extends ItemController
{
    public $viewPath = '@backend/views/meeting';

    public function init()
    {
        $this->class_model = MeetingItemContent::className();
        return parent::init();
    }

    public function actionClearImage($id)
    {
        $model = NewsItemContent::findOne($id);

        if($model === null){
            $this->flash('error', Yii::t('content', 'Not found'));
        }
        elseif($model->image){

            $model->image = '';
            @unlink(Yii::getAlias("@frontend/web").$model->image);

            for($i=1; $i<=3; $i++){
                $model->news->{"cropped_$i"} = '';
                @unlink(Yii::getAlias("@frontend/web").$model->news->{"cropped_$i"});
            }

            if($model->update()){
                $this->flash('success', Yii::t('content', 'Image cleared'));
            } else {
                $this->flash('error', Yii::t('content', 'Update error. {0}', $model->formatErrors()));
            }
        }

        return $this->back();
    }
}