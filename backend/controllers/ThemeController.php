<?php
namespace backend\controllers;

use Yii;
use backend\models\GameThemeItem;
use hdmodules\content\controllers\ItemController;
use yii\widgets\ActiveForm;

class ThemeController extends ItemController
{
    public $viewPath = '@backend/views/theme';

    public function init()
    {
        $this->class_model = GameThemeItem::className();
        return parent::init();
    }
    
    public function actionEdit($id)
    {
        $class_model = $this->class_model;

        if(!($model = $class_model::findOne($id))){
            return $this->redirect(['/block/index']);
        }

        $model->loadTranslations_custom();

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else {
                if ($model->save()) {
                    $this->flash('success', Yii::t('content', 'Item updated'));
                    return $this->redirect([($model->block->controller_name ? $model->block->controller_name.'/edit' :'/content/item/edit'), 'id' => $model->primaryKey]);
                } else {
                    $this->flash('error', Yii::t('content', 'Update error. {0}', $model->formatErrors()));
                }
            }
        }

        return $this->render($this->viewPath.'/edit', [
            'model' => $model,
        ]);

    }
    
    public function actionClearAnimationImage($id)
    {
        $class_model = $this->class_model;

        $model = $class_model::findOne($id);

        if($model === null){
            $this->flash('error', Yii::t('content', 'Not found'));
        }
        elseif($model->attribute_1){
            $model->attribute_1 = '';
            if($model->save(false)){
                $this->flash('success', Yii::t('content', 'Image cleared'));
            } else {
                $this->flash('error', Yii::t('content', 'Update error. {0}', $model->formatErrors()));
            }
        }
        return $this->back();
    }
    
    public function actionCrop($id) {

        if(!($model = GameThemeItem::findOne($id))){
            return $this->redirect(['/block/index']);
        }

        if ($model->theme->load(Yii::$app->request->post())) {
            if($model->theme->cropped_1 && $model->theme->cropped_2 && $model->theme->cropped_3) {
                for($i = 1; $i <= 3; $i++) {
                    $imagename = pathinfo($model->{"image"},PATHINFO_FILENAME);
                    $extension = pathinfo($model->{"image"},PATHINFO_EXTENSION);
                    $path = Yii::getAlias("@frontend/web/uploads/theme/{$imagename}_$i.$extension");
                    if(!file_exists( Yii::getAlias("@frontend/web/uploads/theme"))) {
                        mkdir( Yii::getAlias("@frontend/web/uploads/theme"));
                    }
                    $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $model->theme->{"cropped_$i"}));
                    file_put_contents($path,$data);
                    $model->theme->{"cropped_$i"} = str_replace(Yii::getAlias("@frontend/web"),'',$path);
                }
            }
            if ($model->theme->save()){
                $this->flash('success', Yii::t('theme', 'Theme updated'));
                return $this->redirect([($model->block->controller_name ? $model->block->controller_name.'/edit' :'/content/item/edit'), 'id' => $model->primaryKey]);
            } else {
                $this->flash('error', Yii::t('theme', 'Update error. {0}', $model->formatErrors()));
                return $this->refresh();
            }
        }
        return $this->render('crop',['model' => $model, 'id' => $id]);
    }


}