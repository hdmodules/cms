<?php
namespace backend\controllers;

use Yii;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

use hdmodules\base\helpers\Image;
use hdmodules\content\models\Block;
use hdmodules\content\models\Item;
use hdmodules\content\controllers\ItemController;
use backend\models\PartnerItemContent;


class PartnerController extends ItemController
{
    public $viewPath = '@backend/views/partner';
    
    public function init()
    {
        $this->class_model = PartnerItemContent::className();
        return parent::init();
    }

}