<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use backend\models\Materials;
use yii\web\UploadedFile;

class MaterialsController extends Controller
{

    public function actionIndex()
    {
        $model = new Materials();
        
        if (Yii::$app->request->isPost) {
            
            set_time_limit(0);
            ini_set('memory_limit', '500M');
            
            $model->materialsFile = UploadedFile::getInstance($model, 'materialsFile');

            if ($model->upload()) {
                Yii::$app->getSession()->setFlash('success', 'Materials was uploaded!');
            } else {
                Yii::$app->getSession()->setFlash('error', 'File not upload, some problem.');
            }
        }
        
        $filePath = Yii::getAlias('@frontend/materials') . '/GraphicKit.zip';
        $fileInfo = 0;
        $fileExist = file_exists($filePath);
        if($fileExist){
            $fileInfo = filesize($filePath);
        }

        return $this->render('index', [
            'model' => $model,
            'fileExist' => $fileExist,
            'filePath' => $filePath,
            'fileInfo' => $fileInfo
        ]);
    }

}
