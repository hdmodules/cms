<?php

namespace backend\controllers;

use Yii;
use common\models\Log;
use backend\models\LogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LogController implements the CRUD actions for Log model.
 */
class LogController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Log models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Log models.
     * @return mixed
     */
    public function actionFiles() {
        $directory = Yii::getAlias('@vendor/../logs');

        $action = isset($_POST['file_action']) ? $_POST['file_action'] : null;

        if (Yii::$app->request->isPost) {

            $file = isset($_POST['file_name']) ? $_POST['file_name'] : null;

            if ($action && $file) {
                if ($action == 'delete' && file_exists($directory . '/' . $file)) {
                    unlink($directory . '/' . $file);
                    Yii::$app->getSession()->setFlash('success', sprintf('File "%s" was delete!', $file));
                }

                if ($action == 'download' && file_exists($directory . '/' . $file)) {

                    $logPath = $directory . '/' . $file;
                    Yii::info('actionFiles(): $logPath:' . $logPath);
                    Yii::info('actionFiles(): File is_readable:' . is_readable($logPath));
                    Yii::info('actionFiles(): File is_writable:' . is_writable($logPath));

                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename="' . basename($logPath) . '"');
                    header('Content-Length: ' . filesize($logPath));
                    header('Cache-Control: must-revalidate');
                    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
                    header('Pragma: public');

                    readfile($logPath);

                }
            }
        }

        if ($action != 'download') {
            $logs = array_diff(scandir($directory), array('..', '.'));

            return $this->render('files', [
                        'logs' => $logs,
                        'directory' => $directory
            ]);
        }
    }

    /**
     * Displays a single Log model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Log model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Log();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Log model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Log model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    /**
     * Deletes an existing Log model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteAll()
    {
        Log::deleteAll();

        return $this->redirect(['index']);
    }
    /**
     * Finds the Log model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Log the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Log::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
