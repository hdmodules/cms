<?php

namespace backend\controllers;

use Yii;
use hdmodules\request\controllers\RequestController as RC;
use hdmodules\request\models\Request;

class ErequestController extends RC {
    
    public function init()
    {
        parent::init();
    }

    public function actionView($id) {

        $model = Request::findOne($id);
        $model->answer_email_from = Yii::$app->params['sendEmail'];

        if ($model === null) {
            $this->flash('error', Yii::t('request', 'Not found'));
            return $this->redirect(['erequest/index']);
        }

        if ($model->status == Request::STATUS_NEW) {
            $model->status = Request::STATUS_VIEW;
            $model->update(false);
        }

        $postData = Yii::$app->request->post('Request');
        if ($postData) {
            $model->answer_email_from = $postData['answer_email_from'];
            $model->answer_subject = $postData['answer_subject'];
            $model->answer_text = $postData['answer_text'];
            if ($model->sendAnswer()) {
                $model->status_answer = Request::STATUS_ANSWERED;
                $model->update(false);
                $this->flash('success', Yii::t('request', 'Answer successfully sent'));
            } else {
                $this->flash('error', Yii::t('request', 'An error has occurred while sending mail'));
            }

            return $this->refresh();
        } else {

            //$view = ($model->type && !empty($model->type->view)) ?  $model->type->view : 'view';

            return $this->render('view', ['model' => $model]);
        }
    }
    
    public function actionDelete($id) {
        if (($model = Request::findOne($id))) {
            \backend\models\GameReview::deleteAll(['request_id' => $model->id]);
            $model->delete();
        } else {
            $this->error = Yii::t('request', 'Not found');
        }
        return $this->formatResponse(Yii::t('request', 'Request dates deleted'));
    }

}
