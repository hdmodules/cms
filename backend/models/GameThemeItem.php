<?php
namespace backend\models;

use Yii;
use hdmodules\content\models\Item;
use backend\behaviors\ThemeBehavior;
use yii\web\UploadedFile;
use hdmodules\base\helpers\Image;

class GameThemeItem extends Item {

    public $crop_info;

    public function behaviors() {
        $behaviors = parent::behaviors();
        return array_merge([
            'theme' => ThemeBehavior::className()
        ],$behaviors);

    }
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('content', 'Title'),
            'text' => Yii::t('content', 'Text'),
            'short' => Yii::t('content/article', 'Short'),
            'image' => Yii::t('content', 'Image'),
            'time' => Yii::t('content', 'Date'),
            'slug' => Yii::t('content', 'Slug'),
            'attribute_1' => 'Gif animation',
            'attribute_3' => 'Tag'
        ];
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if (isset($_FILES) && !empty($_FILES)) {

                //Gif animation
                $this->attribute_1 = UploadedFile::getInstance($this, 'attribute_1');
                if ($this->attribute_1 && $this->validate(['attribute_1'])) {
                    $this->attribute_1 = Image::upload($this->attribute_1, 'item');
                } else {
                    if($this->isNewRecord){
                        $this->attribute_1 = '';
                    }else{
                        $this->attribute_1 = $this->oldAttributes['attribute_1'];
                    }
                }
            }

            if(!$insert && $this->image != $this->oldAttributes['image'] && $this->oldAttributes['image']){
                @unlink(Yii::getAlias('@webroot').$this->oldAttributes['image']);
            }

            if(!$insert && $this->attribute_1 != $this->oldAttributes['attribute_1'] && $this->oldAttributes['attribute_1']){
                @unlink(Yii::getAlias('@webroot').$this->oldAttributes['attribute_1']);
            }
            
            return true;
        } else {
            return false;
        }
    }
}