<?php
namespace backend\models;

use Yii;
use hdmodules\base\behaviors\Taggable;
use hdmodules\content\models\Item;
use backend\behaviors\GameBehavior;
use yii\web\UploadedFile;
use hdmodules\base\helpers\Image;

class GameItemContent extends Item {

    public function behaviors() {
        $behaviors = parent::behaviors();
        return array_merge([
            Taggable::className(),
            'game' => GameBehavior::className(),
        ],$behaviors);

    }

    public function rules() {
        $rules = parent::rules();
        return array_merge([
            //['image', 'image', 'minWidth'=>315, 'minHeight'=>315],
            //['image', 'image', 'minWidth'=>640, 'minHeight'=>471],
            [['tagNames'], 'safe'],
        ],$rules);

    }
    
    public static function getCacheDependency() {
        return new \yii\caching\DbDependency(['sql' => 'SELECT CONCAT(MAX(`content_blocks_item`.update_time), "_", :language) FROM `content_blocks_item` 
                                                        LEFT JOIN `content_block` ON `content_blocks_item`.`block_id` = `content_block`.`id` 
                                                        WHERE `content_block`.`slug`="game"', 'params' => ['language' => \Yii::$app->language]]);
    }
}