<?php

namespace backend\models;

use Yii;
use backend\models\Game;
use hdmodules\content\models\Item;;
/**
 * This is the model class for table "game_theme".
 *
 * @property integer $id
 * @property integer $game_id
 * @property integer $theme_id
 */
class GameTheme extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'game_theme';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['game_id', 'theme_id'], 'required'],
            [['game_id', 'theme_id'], 'integer'],
            [['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => Game::className(), 'targetAttribute' => ['game_id' => 'id']],
            [['theme_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['theme_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'game_id' => 'Game ID',
            'theme_id' => 'Theme ID',
        ];
    }

}
