<?php
namespace backend\models;

use Yii;
use hdmodules\content\models\Item;

class GameFeatureItem extends Item {

    public function attributeLabels()
    {
        return [
            'title' => Yii::t('content', 'Title'),
            'text' => Yii::t('content', 'Text'),
            'short' => Yii::t('content/article', 'Short'),
            'image' => Yii::t('content', 'Image'),
            'time' => Yii::t('content', 'Date'),
            'slug' => Yii::t('content', 'Slug'),
            'attribute_3' => 'Tag'
        ];
    }
}