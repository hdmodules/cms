<?php
namespace backend\models;

use hdmodules\base\components\ActiveRecord;
use hdmodules\base\behaviors\SortableModel;


class GamePointRelation extends ActiveRecord
{
    public static function tableName()
    {
        return 'game_point_rel';
    }

    public function rules()
    {
        $rules = [
            [['game_id', 'point_id'], 'required'],
            [['game_id', 'point_id', 'order_num'], 'integer'],
            [['color'], 'string'],
            [['game_id', 'point_id', 'order_num', 'color'], 'safe'],
        ];

        return $rules;
    }

    public function behaviors()
    {
        return [
            'sortable' => [
                'class' => SortableModel::className(),
                'par_id' => 'game_id'
            ]
        ];
    }


    public function getGame(){
        return $this->hasOne(Game::className(), ['id' => 'game_id']);
    }


    public function getPoint(){
        return $this->hasOne(GamePoint::className(), ['id' => 'point_id']);
    }
}