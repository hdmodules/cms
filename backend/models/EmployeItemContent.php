<?php
namespace backend\models;

use hdmodules\base\behaviors\Taggable;
use backend\behaviors\NewsBehavior;
use hdmodules\content\models\Item;


class EmployeItemContent extends Item {

    public function rules() {
        $rules = parent::rules();
        return array_merge([
            ['image', 'image', 'minWidth' => 215],
            ['time', 'required', 'when' => function($model) {
                $this->time = strtotime($this->time);
            }],
            [['attribute_1', 'attribute_2', 'attribute_3'], 'safe'],
        ],$rules);

    }
    
    public static function getCacheDependency() {
        return new \yii\caching\DbDependency(['sql' => 'SELECT CONCAT(MAX(`content_blocks_item`.update_time), "_", :language) FROM `content_blocks_item` 
                                                        LEFT JOIN `content_block` ON `content_blocks_item`.`block_id` = `content_block`.`id` 
                                                        WHERE `content_block`.`slug`="our-team"', 'params' => ['language' => \Yii::$app->language]]);
    }
}