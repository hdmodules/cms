<?php
namespace backend\models;

use hdmodules\base\components\ActiveRecord;


class GameCategoryRelation extends ActiveRecord
{
    public static function tableName()
    {
        return 'game_category';
    }

    public function rules()
    {

        $rules = [
            [['game_id', 'category_id'], 'integer'],
        ];

        return $rules;
    }
}