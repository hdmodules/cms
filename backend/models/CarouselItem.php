<?php
namespace backend\models;

use Yii;
use hdmodules\carousel\models\CarouselItem as CI;


class CarouselItem extends CI
{

    public function rules() {
        $rules = parent::rules();
        return array_merge([
            [['image'], 'image'],
            ['image', 'required', 'on' => 'create'],
        ],$rules);
    }

}