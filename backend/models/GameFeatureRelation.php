<?php
namespace backend\models;

use hdmodules\base\components\ActiveRecord;


class GameFeatureRelation extends ActiveRecord
{
    public static function tableName()
    {
        return 'game_feature';
    }

    public function rules()
    {

        $rules = [
            [['game_id', 'feature_id'], 'integer'],
        ];

        return $rules;
    }
}