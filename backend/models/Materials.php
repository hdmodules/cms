<?php

namespace backend\models;

use Yii;

class Materials extends \yii\base\Model {

    public $materialsFile;

    public function rules() {
        return [
           // ['materialsFile', 'required'],
            [['materialsFile'], 'file', 'extensions' => 'zip'],
        ];
    }

    public function upload() {
        Yii::info('materialsFile:' . print_r($this->materialsFile, 1));
        if ($this->validate()) {
            $this->materialsFile->saveAs(Yii::getAlias('@frontend/materials') . '/GraphicKit.zip');
            return true;
        } else {
            return false;
        }
    }

}
