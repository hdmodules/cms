<?php
namespace backend\models;

use backend\behaviors\NewsBehavior;
use hdmodules\content\models\Item;


class PartnerItemContent extends Item {

    public function rules() {
        $rules = parent::rules();
        return array_merge([  
            //['image', 'image', 'minWidth' => 300],
        ],$rules);
    }

    public static function getCacheDependency($slug) {
        return new \yii\caching\DbDependency(['sql' => 'SELECT CONCAT(MAX(`content_blocks_item`.update_time), "_", :language) FROM `content_blocks_item` 
                                                        LEFT JOIN `content_block` ON `content_blocks_item`.`block_id` = `content_block`.`id` 
                                                        WHERE `content_block`.`slug`="' . $slug . '"', 'params' => ['language' => \Yii::$app->language]]);
    }
}