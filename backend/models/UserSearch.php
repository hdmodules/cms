<?php

  namespace backend\models;

  use Yii;
  use yii\base\Model;
  use yii\data\ActiveDataProvider;
  use common\models\User;

  /**
   * UserSearch represents the model behind the search form about `common\models\User`.
   */
  class UserSearch extends User {

    public $created_date;
    public $updated_date;

    /**
     * @inheritdoc
     */
    public function rules() {
      return [
          [['id', 'language_id', 'service', 'service_id', 'role', 'status'], 'integer'],
          [['email', 'name', 'surname'], 'trim'],
          [['name', 'surname', 'email', 'created_date', 'updated_date'], 'safe'],
      ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
      // bypass scenarios() implementation in the parent class
      return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
      $query = User::find()->where('id <> :id', [':id' => User::DEFAULT_USER_ID]);

      // add conditions that should always apply here

      $dataProvider = new ActiveDataProvider([
          'query' => $query,
          'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
      ]);

      $this->load($params);

      if (!$this->validate()) {
        // uncomment the following line if you do not want to return any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
      }

      // grid filtering conditions
      $query->andFilterWhere([
          'id' => $this->id,
          'language_id' => $this->language_id,
          'service' => $this->service,
          'role' => $this->role,
          'status' => $this->status,
          //'created_at' => $this->created_at,
          //'updated_at' => $this->updated_at,
      ]);

      if (isset($this->created_date) && !empty($this->created_date)) {
        $query->andFilterWhere(['>=', 'created_at', strtotime($this->created_date . ' 00:00:00')]);
        $query->andFilterWhere(['<=', 'created_at', strtotime($this->created_date . ' 23:59:59')]);
      }
      if (isset($this->updated_date) && !empty($this->updated_date)) {
        $query->andFilterWhere(['>=', 'created_at', strtotime($this->updated_date . ' 00:00:00')]);
        $query->andFilterWhere(['<=', 'created_at', strtotime($this->updated_date . ' 23:59:59')]);
      }

      $query->andFilterWhere(['like', 'service_token', $this->service_token])
      ->andFilterWhere(['like', 'name', $this->name])
      ->andFilterWhere(['like', 'surname', $this->surname])
      ->andFilterWhere(['like', 'email', $this->email]);

      return $dataProvider;
    }

  }
  