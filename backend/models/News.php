<?php

namespace backend\models;

use Yii;
use hdmodules\content\models\Item;
use hdmodules\base\components\ActiveRecord;
use hdmodules\base\multilanguage\MultiLanguageBehavior;
use hdmodules\base\multilanguage\MultiLanguageTrait;

class News extends ActiveRecord {

    //use MultiLanguageTrait;

    public $croppedImage;
    
    public static function tableName() {
        return "news";
    }

   /* public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if (isset($_FILES) && !empty($_FILES)) {
                $this->cropped_1 = UploadedFile::getInstance($this, 'cropped_1');
                if ($this->cropped_1 && $this->validate(['cropped_1'])) {
                    $this->cropped_1 = Image::upload($this->cropped_1, 'news');
                } else {
                    if($this->isNewRecord){s
                        $this->cropped_1 = '';
                    }else{
                        $this->cropped_1 = $this->oldAttributes['cropped_1'];
                    }
                }
            }

            if(!$insert && $this->cropped_1 != $this->oldAttributes['cropped_1'] && $this->oldAttributes['cropped_1']){
                @unlink(Yii::getAlias('@webroot').$this->oldAttributes['cropped_1']);
            }

            return true;
        } else {
            return false;
        }
    }**/

    public function rules()	{
        return [
            [['video'], 'url'],
            [['video'],'safe'],
            ['cropped_1', 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif', 'minWidth' => 420, 'maxWidth' => 420,'minHeight' => 330, 'maxHeight' => 330],
            ['cropped_2', 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif', 'minWidth' => 450, 'maxWidth' => 450,'minHeight' => 235, 'maxHeight' => 235],
            ['cropped_3', 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif', 'minWidth' => 235, 'maxWidth' => 235,'minHeight' => 235, 'maxHeight' => 235]
        ];
    }
    
    public function upload() {
        
        if ($this->validate()) {
                    
            Yii::info('upload(): $this:' . print_r(Yii::getAlias('@frontend/web/uploads/') . $this->croppedImage->baseName . '.' . $this->croppedImage->extension, 1));
        
            $this->cropped_1->saveAs(Yii::getAlias('@frontend/web/uploads/') . $this->croppedImage->baseName . '.' . $this->croppedImage->extension);
            return true;
        } else {
            return false;
        }
    }

    public function attributeLabels() {
        return [
            "cropped_1" => "Cropped 1",
            "cropped_2" => "Cropped 2",
            "cropped_3" => "Cropped 3",
        ];
    }

}