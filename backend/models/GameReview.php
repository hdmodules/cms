<?php

namespace app\models;

namespace backend\models;

use hdmodules\base\components\ActiveRecord;
use hdmodules\base\behaviors\SortableModel;
use hdmodules\base\helpers\Image;
use yii\web\UploadedFile;

class GameReview extends ActiveRecord
{

    const STATUS_OFF = 0;
    const STATUS_ON = 1;
    const STATUS_NEW = 2;

    const REVIEW_BIG_WINS = 1;
    const REVIEW_VIDEO = 2;
    const REVIEW_SIMPLE = 3;


    public static function tableName()
    {
        return 'game_review';
    }


    public function rules()
    {
        return [
            [['type', 'status', 'game_id'], 'required'],
            [['url'], 'url'],
            [['type', 'status', 'game_id', 'order_num'], 'integer'],
            [['title', 'url'], 'string', 'max' => 255],
            ['image', 'image', 'minWidth'=>225, 'minHeight'=>225],

        ];
    }

    public function behaviors()
    {
        return [
            'sortable' => [
                'class' => SortableModel::className(),
                'par_id' => 'game_id'
            ]
        ];
    }

    public static function getTypeReviews(){
        return[
            self::REVIEW_BIG_WINS => 'BIG WINS REVIEW',
            self::REVIEW_VIDEO => 'VIDEO REVIEW',
            self::REVIEW_SIMPLE => 'SIMPLE REVIEW',
        ];
    }

    public static function getStatuses(){
        return[
            self::STATUS_OFF => 'OFF',
            self::STATUS_ON => 'ON',
            self::STATUS_NEW => 'NEW',
        ];
    }

    public static function getColorStatuses(){
        return[
            self::STATUS_OFF => 'danger',
            self::STATUS_ON => 'success',
            self::STATUS_NEW => 'warning',
        ];
    }

    public function getYotubeCode(){

        if(isset($this->url) && !empty($this->url)){

            $parts = parse_url($this->url);
            if(isset($parts['query'])){
                parse_str($parts['query'], $qs);
                if(isset($qs['v'])){
                    return $qs['v'];
                }else if(isset($qs['vi'])){
                    return $qs['vi'];
                }
            }
            if(isset($parts['path'])){
                $path = explode('/', trim($parts['path'], '/'));
                return $path[count($path)-1];
            }
            if (isset($this->codePattern) && preg_match($this->codePattern, $this->url, $match)) {
                return $match[0];
            }
        }

        return null;
    }

    public function getGame(){
        return $this->hasOne(Game::className(), ['id' => 'game_id']);
    }

    public function beforeSave($insert)
    {


        if (parent::beforeSave($insert)) {

            if (isset($_FILES) && !empty($_FILES)) {
                $this->image = UploadedFile::getInstance($this, 'image');
                if ($this->image && $this->validate(['image'])) {
                    $this->image = Image::upload($this->image, 'reviews');
                } else {
                    if($this->isNewRecord){
                        $this->image = '';
                    }else{
                        $this->image = $this->oldAttributes['image'];
                    }

                    if (!empty($this->url) && empty($this->image)){
                        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $this->url, $match)) {
                            $file = 'http://img.youtube.com/vi/'.$match[1].'/maxresdefault.jpg';
                            $newfile = $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/uploads/reviews/'.$match[1].'_maxresdefault.jpg';
                            if(@copy($file, $newfile)){
                                $this->image = '/uploads/reviews/'.$match[1].'_maxresdefault.jpg';
                            }
                        }
                    }
                }
            }

            return true;
        } else {
            return false;
        }
    }

}