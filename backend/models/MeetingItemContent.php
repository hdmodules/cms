<?php
namespace backend\models;

use hdmodules\base\behaviors\Taggable;
use backend\behaviors\NewsBehavior;
use hdmodules\content\models\Item;


class MeetingItemContent extends Item {

    public $crop_info;

   /* public function behaviors() {
        $behaviors = parent::behaviors();
        return array_merge([
            Taggable::className(),
            'meeting' => [
                'class'=>NewsBehavior::className()
            ]
        ],$behaviors);

    }*/

    public function rules() {
        $rules = parent::rules();
        return array_merge([
            ['time', 'required', 'when' => function($model) {
                $this->time = strtotime($this->time);
            }],
            ['attribute_1', 'required', 'when' => function($model) {
                $this->attribute_1 = strtotime($this->attribute_1);
            }],
            ['attribute_2', 'required', 'when' => function($model) {
                $this->attribute_2 = strtotime($this->attribute_2);
            }]
        ],$rules);

    }
    
    public function getNewsDetail()
    {
        return $this->hasOne(News::className(), ['content_id' => 'id']);
    }
    
    public static function getCacheDependency($slug) {
        return new \yii\caching\DbDependency(['sql' => 'SELECT CONCAT(MAX(`content_blocks_item`.update_time), "_", :language) FROM `content_blocks_item` 
                                                        LEFT JOIN `content_block` ON `content_blocks_item`.`block_id` = `content_block`.`id` 
                                                        WHERE `content_block`.`slug`="' . $slug . '"', 'params' => ['language' => \Yii::$app->language]]);
    }
}