<?php

namespace backend\models;

use common\helpers\Helper;
use OAuth\Common\Exception\Exception;
use Yii;
use hdmodules\base\components\ActiveRecord;
use hdmodules\base\multilanguage\MultiLanguageBehavior;
use hdmodules\base\multilanguage\MultiLanguageTrait;
use backend\models\GameTheme;

class Game extends ActiveRecord
{

    public static function tableName()
    {
        return "game";
    }

    public function rules()
    {
        return [
            [['category_ids', 'feature_ids', 'theme_ids'], 'each', 'rule' => ['integer']],
            [['content_id', 'theme_id', 'payline_id', 'key', 'html_game'], 'safe'],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        GameSimilar::setSimilarGames($this->id);
        parent::afterSave($insert, $changedAttributes);
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return array_merge([
            [
                'class' => \voskobovich\behaviors\ManyToManyBehavior::className(),
                'relations' => [
                    'category_ids' => 'categories',
                    'feature_ids' => 'features',
                    'theme_ids' => 'themes',
                ],
            ]
        ], $behaviors);
    }

    public static function getGameUrl($game_id, $slug, $html5 = 0)
    {
        $response = ['demoLink' => false, 'html5' => $html5];

        $account_id = Yii::$app->params['endorphinaAccountId'];

        $returnURL = urlencode(urlencode(\yii\helpers\Url::to(['games/view', 'slug' => $slug], true)));

        Yii::info('$returnURL:' . $returnURL);

        $gameName = $game_id;

        if ($html5) {
            $replacePart = '@ENDORPHINA';
            $gameName = str_replace($replacePart, '-html5', $game_id) . $replacePart;
        }

        $url = 'https://edemo.endorphina.com/api/link/accountId/' . $account_id . '/hash/'
                    . md5($gameName)
                    . '/returnURL/' . $returnURL;

        $data = Helper::sendGetData($url);

        if ($data['httpcode'] == 200) {
            $response['demoLink'] = $data['result'];
        }
        else{
            Yii::error('Game.getGameUrl(): $httpstatus:' . $data['httpcode']);
            Yii::error('Game.getGameUrl(): $result:' . $data['result']);

        }

        Yii::info('Game.getGameUrl(): $game_id:' . $game_id);
        Yii::info('Game.getGameUrl(): $gameName:' . $gameName);
        Yii::info('Game.getGameUrl(): $demoLink:' . $url);

        return $response;
    }

    public function getCategories()
    {
        return $this->hasMany(GameCategoryItem::className(), ['id' => 'category_id'])
            ->viaTable(GameCategoryRelation::tableName(), ['game_id' => 'id']);
    }

    public function getFeatures()
    {
        return $this->hasMany(GameFeatureItem::className(), ['id' => 'feature_id'])
            ->viaTable(GameFeatureRelation::tableName(), ['game_id' => 'id']);
    }

    public function getTagFeatures()
    {
        return $this->hasMany(GameFeatureItem::className(), ['id' => 'feature_id'])
            ->viaTable(GameFeatureRelation::tableName(),
                ['game_id' => 'id'])->where(['`content_blocks_item`.attribute_3' => 1]);
    }

    public function getGameFeatures()
    {
        return $this->hasMany(GameFeatureRelation::className(), ['game_id' => 'id']);
    }

    public function getThemes()
    {
        return $this->hasMany(GameThemeItem::className(), ['id' => 'theme_id'])
            ->viaTable(GameTheme::tableName(), ['game_id' => 'id']);
    }

    public function getTagThemes()
    {
        return $this->hasMany(GameThemeItem::className(), ['id' => 'theme_id'])
            ->viaTable(GameTheme::tableName(), ['game_id' => 'id'])->where(['`content_blocks_item`.attribute_3' => 1]);
    }

    public function getGameThemes()
    {
        return $this->hasMany(GameTheme::className(), ['game_id' => 'id']);
    }

    public function getPoints()
    {
        return $this->hasMany(GamePoint::className(), ['id' => 'point_id'])
            ->viaTable(GamePointRelation::tableName(), ['game_id' => 'id']);
    }

    public function getCategoriesRelation()
    {
        return $this->hasMany(GameCategoryRelation::className(), ['id' => 'game_id']);
    }

    public function getFeaturesRelation()
    {
        return $this->hasMany(GameFeatureRelation::className(), ['id' => 'game_id']);
    }

    public function getPointsRelation()
    {
        return $this->hasMany(GamePointRelation::className(), ['id' => 'game_id']);
    }

    public function getContent()
    {
        return $this->hasOne(GameItemContent::className(), ['id' => 'content_id']);
    }

    /* public function getTheme(){
         return $this->hasOne(GameItemContent::className(), ['id' => 'theme_id']);
     }*/

    public function getPayline()
    {
        return $this->hasOne(GameItemContent::className(), ['id' => 'payline_id']);
    }

    public function getReviews($type)
    {
        return $this->hasMany(GameReview::className(), ['game_id' => 'id'])->where([
            '`game_review`.type' => $type,
            '`game_review`.status' => GameReview::STATUS_ON
        ])->orderBy(['`game_review`.order_num' => SORT_DESC]);
    }

    public function getCountNewReviews()
    {
        return $this->hasMany(GameReview::className(),
            ['game_id' => 'id'])->where(['`game_review`.status' => GameReview::STATUS_NEW])->orderBy(['`game_review`.order_num' => SORT_DESC])->count();

    }

    public function getCountAllReviews()
    {
        return $this->hasMany(GameReview::className(),
            ['game_id' => 'id'])->orderBy(['`game_review`.order_num' => SORT_DESC])->count();

    }

}