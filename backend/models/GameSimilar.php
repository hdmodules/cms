<?php

namespace backend\models;

use Yii;
use backend\models\Game;
use yii\helpers\ArrayHelper;
use hdmodules\base\models\Setting;
/**
 * This is the model class for table "game_similar".
 *
 * @property integer $id
 * @property integer $game_id
 * @property integer $similar_game_id
 * @property integer $coefficient
 */
class GameSimilar extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'game_similar';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['game_id', 'similar_game_id', 'coefficient'], 'integer'],
            [['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => Game::className(), 'targetAttribute' => ['game_id' => 'id']],
            [['similar_game_id'], 'exist', 'skipOnError' => true, 'targetClass' => Game::className(), 'targetAttribute' => ['similar_game_id' => 'id']],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function setSimilarGames($game_id) {

        $targetGame = Game::find()->with(['gameThemes', 'gameFeatures'])->where(['id' => $game_id])->one();
        if ($targetGame) {
            $similarGames = [];

            $games = Game::find()->where('id <> :game_id and payline_id = :payline_id', ['game_id' => $game_id, 'payline_id' => $targetGame->payline_id])->all();
            if ($games) {
                foreach ($games as $game) {
                    $similarGames[$game->id] = 1;
                }
            }

            Yii::info('$similarGames paylines: ' . print_r($similarGames, 1));
            
            //Get target themes
            $targetThemes = ArrayHelper::getColumn($targetGame->gameThemes, 'theme_id');

            $themes = GameTheme::find()->select('game_id, count(theme_id) as themeCount')
                    ->where('game_id <> :game_id', ['game_id' => $game_id])
                    ->andWhere(['in','theme_id',$targetThemes])
                    ->groupBy(['game_id'])
                    ->orderBy(['game_id' => SORT_ASC])->asArray()->all();
            if ($themes) {
                foreach ($themes as $theme) {
                    if (isset($similarGames[$theme['game_id']])) {
                        $coefficient = $similarGames[$theme['game_id']] + $theme['themeCount'];
                        $similarGames[$theme['game_id']] = $coefficient;
                    } else {
                        $similarGames[$theme['game_id']] = $theme['themeCount'];
                    }
                }
            }

            Yii::info('$similarGames themes: ' . print_r($similarGames, 1));
            
            //Get target features
            $targetFeatures = ArrayHelper::getColumn($targetGame->gameFeatures, 'feature_id');

            $similarGames = [];
            $features = GameFeatureRelation::find()->select('game_id, count(feature_id) as featureCount')
                    ->where('game_id <> :game_id', ['game_id' => $game_id])
                    ->andWhere(['in','feature_id',$targetFeatures])
                    ->groupBy(['game_id'])
                    ->orderBy(['game_id' => SORT_ASC])->asArray()->all();
            if ($features) {
                foreach ($features as $feature) {
                    if (isset($similarGames[$feature['game_id']])) {
                        $coefficient = $similarGames[$feature['game_id']] + $feature['featureCount'];
                        $similarGames[$feature['game_id']] = $coefficient;
                    } else {
                        $similarGames[$feature['game_id']] = $feature['featureCount'];
                    }
                }
                arsort($similarGames);
                
                $count = Setting::get('game-page-similar-game-count');
                $count = isset($count) ? $count : 9;
                $similarGames = array_slice($similarGames, 0, $count, true);
            }

            Yii::info('$similarGames: ' . print_r($similarGames, 1));
            
            $data = [];
            if ($similarGames) {
                foreach ($similarGames as $game_id => $value) {
                    $data[] = [
                        'game_id' => $targetGame->id,
                        'similar_game_id' => $game_id,
                        'coefficient' => $value,
                    ];
                }
            }

            Yii::info('$data: ' . print_r($data, 1));
            GameSimilar::deleteAll(['game_id' => $targetGame->id]);
            if($data){
                Yii::$app->db->createCommand()->batchInsert(GameSimilar::tableName(), ['game_id', 'similar_game_id', 'coefficient'], $data)->execute();
            }

        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'game_id' => 'Game ID',
            'similar_game_id' => 'Similar Game ID',
            'coefficient' => 'Coefficient',
        ];
    }

}
