<?php

namespace backend\models;

use hdmodules\content\models\Item;
use hdmodules\base\components\ActiveRecord;
use hdmodules\base\multilanguage\MultiLanguageBehavior;
use hdmodules\base\multilanguage\MultiLanguageTrait;

class Theme extends ActiveRecord {

    public $crop_info;

    public static function tableName() {
        return "theme";
    }

    public function rules()	{
        return [
            [['cropped_1','cropped_2','cropped_3'],'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            "cropped_1" => "Cropped 1",
            "cropped_2" => "Cropped 2",
            "cropped_3" => "Cropped 3",
        ];
    }

}