<?php
namespace backend\models;

use hdmodules\base\components\ActiveRecord;


class GamePoint extends ActiveRecord
{
    public static function tableName()
    {
        return 'game_point';
    }

    public function rules()
    {

        $rules = [
            [['name'], 'string'],
            [['name'], 'required'],
        ];

        return $rules;
    }
}