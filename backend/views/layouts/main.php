<?php

/**
 * @var string $content
 * @var \yii\web\View $this
 */

use yii\helpers\Html;
use yii\helpers\Url;

//$bundle = \yiister\gentelella\assets\Asset::register($this);
$bundle = \backend\assets\AppAsset::register($this);

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="<?= Yii::$app->charset ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="icon" type="image/png" href="/images/icons/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="nav-md">
<?php $this->beginBody(); ?>
<div class="container body">

    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;" align="center">

                    <a href="<?= Yii::$app->urlManager->baseUrl ?>/" class="site_title">
                        <img src="/images/logos/logo-main.png" alt="logo" style="width: 100px">
                    </a>

                </div>
                <div class="clearfix"></div>

                <!-- menu prile quick info -->
                <!--<div class="profile">
                    <div class="profile_pic">
                        <img src="http://placehold.it/128x128" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2>John Doe</h2>
                    </div>
                </div>-->
                <!-- /menu prile quick info -->

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">

                        <?php ?>
                        <?=
                        \yiister\gentelella\widgets\Menu::widget(
                            [
                                "encodeLabels"=>false,
                                "items" => [
                                    ["label" => "Home", "url" => Yii::$app->urlManager->createUrl(['site/index']), "icon" => "home"],
                                    ["label" => "Content", "url" => ['/content/block/index'], "icon" => "list"],
                                    ["label" => "Menu", "url" => Yii::$app->urlManager->createUrl(['/base/menu/index']), "icon" => "th-large"],
                                    ["label" => "Carousel", "url" => Yii::$app->urlManager->createUrl(['/carousel/carousel/index']), "icon" => "desktop"],
                                    ["label" => "Translations", "url" => Yii::$app->urlManager->createUrl(['/translatemanager/language/list']), "icon" => "text-width", ['target' => 'blank']],
                                    ["label" => "Files", "url" => ['/file/file/index'], "icon" => "cloud-download"],
                                    ["label" => "Mail Templates", "url" => ['/universalmailtemplate/template/index'], "icon" => "envelope"],
                                    [
                                        "label" => "Requests",
                                        "icon" => "random",
                                        "url" => "#",
                                        "items" => [
                                            ["label" => "Request <span class='badge bg-red' style='float:right'>".@$new_request."</span>", "url" => ['/erequest/index'], "icon" => "random"],
                                            ["label" => "Request tasks <span class='badge bg-red' style='float:right'>".@$tascs_count."</span>", "url" => ['/request/request-task/index'], "icon" => "list"],
                                            ["label" => "Request Handler", "url" => ['/request/request-handler/index'], "icon" => "wrench"],
                                        ],
                                    ],
                                    [
                                        "label" => "Users",
                                        "icon" => "users",
                                        "url" => "#",
                                        "items" => [
                                            ["label" => "Users", "url" => Yii::$app->urlManager->createUrl(['user/index']), "icon" => "user"],
                                            ["label" => "Roles", "url" => ['/admin'], "icon" => "graduation-cap", ['target' => 'blank']],
                                        ],
                                    ],
                                    ["label" => "Materials", "url" => ['/materials/index'], "icon" => "file-zip-o"],
                                    [
                                        "label" => "System",
                                        "icon" => "gears",
                                        "url" => "#",
                                        "items" => [
                                            ["label" => "Settings", "url" => Yii::$app->urlManager->createUrl(['/base/settings/index']), "icon" => "gear"],
                                            ["label" => "Cache", "url" => Yii::$app->urlManager->createUrl(['system/index']), "icon" => "bolt"],
                                            ["label" => "Migrations", "url" => ['/migration/migration/index'], "icon" => "database"],
                                            ["label" => "Php Info", "url" => Yii::$app->urlManager->createUrl(['system/php-info']), "icon" => "info", ['target' => 'blank']],
                                            ["label" => "Logs", "url" => Yii::$app->urlManager->createUrl(['system/logs']), "icon" => "info", ['target' => 'blank']],
                                            ["label" => "MySql logs", "url" => Yii::$app->urlManager->createUrl(['log/index']), "icon" => "info", ['target' => 'blank']]
                                        ],
                                    ],
                                    ["label" => "Logout", "url" => Yii::$app->urlManager->createUrl(['site/logout']), "icon" => "sign-out"]
                                ],
                            ]
                        )
                        ?>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-user"></i> <?= Yii::$app->user->identity->username ?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
<!--                                <li>
                                    <a href="<?= yii\helpers\Url::toRoute(['content/block/index']) ?>"><i class="fa fa-list pull-right"></i> Content</a>
                                </li>
                                <li>
                                    <?= Html::a('<i class="fa fa-users pull-right"></i> Users', ['user/index']) ?>
                                </li>-->
                                <li>
                                    <a href="<?= yii\helpers\Url::to(['site/logout']) ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>
                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-green"><?= @$new_request ?></span>
                            </a>
                            
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <?php if(@$requests){ ?>
                                    <?php foreach($requests as $request){ ?>
                                <li>
                                    <a href="<?= Url::to(['/erequest/view', 'id' => $request->id]) ?>">
                                        <span class="image">
                                            <img src="/media/images/icon/favicon.png" alt="Requests" />
                                        </span>
                                        <span>
                                            <span><?= sprintf('(ID:%s) %s', $request->id, $request->title) ?></span><br>
                                            <span class="time"><?= Yii::$app->formatter->asDatetime($request->time, Yii::$app->formatter->datetimeFormat) ?></span>
                                        </span>
                                    </a>
                                </li>
                                    <?php } ?>
                                <li>
                                    <div class="text-center">
                                        <a href="<?= Url::to(['/request/request/index']) ?>">
                                            <strong>See All Requests</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                                <?php } else { ?>
                                <li>
                                    <div class="text-center">
                                            <h3>Not found</h3>
                                    </div>
                                </li>
                                <?php } ?>
                            </ul>
                            
                        </li>
                        <li class="">
                            <a href="/" target="blank">
                                <i class="fa fa-home"></i> Open site
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <?php if (isset($this->params['h1'])): ?>
                <div class="page-title">
                    <div class="title_left">
                        <h1><?= $this->params['h1'] ?></h1>
                    </div>
                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="clearfix"></div>

            <?php foreach(Yii::$app->session->getAllFlashes() as $key => $message) : ?>

                <div class="alert alert-<?= $key ?> alert-dismissible"  role="alert" align="center">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?= $message ?>
                </div>

            <?php endforeach; ?>

            <?= $content ?>
        </div>
        <!-- /page content -->
        <!-- footer content -->
        <footer>
            <div align="center">
                © Endorphina 2015 - <?= date('Y') ?>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
<!-- /footer content -->
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
