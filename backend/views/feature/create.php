<?php
$this->title = Yii::t('content', 'Create item');
?>
<?= $this->render('_menu', ['block' => $block]) ?>
<?= $this->render('_form', ['model' => $model]) ?>