<?php
use hdmodules\base\widgets\Photos;

$this->title = $model->title;
?>

<?= $this->render('_menu', ['block' => $model->block]) ?>
<?= $this->render('_submenu', ['model' => $model]) ?>

<?= Photos::widget(['model' => $model])?>