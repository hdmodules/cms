<?php
use hdmodules\base\helpers\Image;
use hdmodules\base\widgets\SeoForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use hdmodules\base\multilanguage\input_widget\MultiLanguageActiveField;
use hdmodules\base\widgets\RedactorMultiLanguageInput;

$module = $this->context->module->id;
?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

            <div class="x_title">
                <h2><?= Yii::t('content', 'Form') ?> <small><?= Yii::t('news', 'Create partner') ?></small></h2>
                <ul class="nav navbar-right">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                    'validateOnSubmit' => true,
                    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
                ]); ?>

                    <?= $form->field($model, 'title')->widget(MultiLanguageActiveField::className()) ?>
                    
                    <?= $form->field($model, 'attribute_1')->label('Name') ?>
                
                    <?= $form->field($model, 'attribute_2')->label('Partner Url') ?>
                
                    <?php if($model->image) : ?>
                        <img src="<?= Image::thumb($model->image, 240) ?>">
                        <a href="<?= Url::to([($model->block->controller_name ? $model->block->controller_name.'/clear-image' : '/'.$module.'/item/clear-image'), 'id' => $model->primaryKey]) ?>" class="text-danger confirm-delete" title="<?= Yii::t('content', 'Clear image')?>"><?= Yii::t('content', 'Clear image')?></a>
                    <?php endif; ?>

                    <?= $form->field($model, 'image')->fileInput() ?>

                    <?= $form->field($model, 'short')->textarea()->widget(MultiLanguageActiveField::className(), ['inputType' => 'textArea', 'inputOptions'=>['rows' => '5', 'style'=>['width'=>'100%']]]) ?>

                    <?= RedactorMultiLanguageInput::widget($model, 'text', ['options' => [
                        'minHeight' => 400,
                        'imageUpload' => Url::to(['/redactor/upload', 'dir' => 'items']),
                        'fileUpload' => Url::to(['/redactor/upload', 'dir' => 'items']),
                        'plugins' => ['fullscreen']
                    ]]); ?>
                    <br>

                    <?= $form->field($model, 'slug') ?>
                    
                    <?= hdmodules\base\widgets\PublishInput::widget(['model' => $model]) ?>
                    
                    <?php if(Yii::$app->controller->action->id == 'create' || (Yii::$app->controller->action->id == 'edit')) : ?>
                        <?= SeoForm::widget(['model' => $model]) ?>
                    <?php endif; ?>

                    <div class="ln_solid"></div>

                    <?= Html::submitButton(Yii::t('content', 'Save'), ['class' => 'btn btn-primary']) ?>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>


