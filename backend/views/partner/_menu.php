<?php
use yii\helpers\Url;

$action = $this->context->action->id;

?>
<ul class="nav nav-pills">
    <?php if($action == 'index') : ?>
        <li><a href="<?= Url::to(['/content/block']) ?>"><i class="glyphicon glyphicon-chevron-left font-12"></i> <?= Yii::t('content', 'Blocks') ?></a></li>
    <?php endif; ?>
    <li <?= ($action == 'index') ? 'class="active"' : '' ?>><a href="<?= Url::to([($block->controller_name ? '/'.$block->controller_name : '/content/item') , 'id' => $block->primaryKey]) ?>"><?php if($action != 'index') echo '<i class="glyphicon glyphicon-chevron-left font-12"></i> ' ?><?= $block->title ?></a></li>
    <li <?= ($action == 'create') ? 'class="active"' : '' ?>><a href="<?= Url::to([($block->controller_name ? $block->controller_name.'/create' : '/content/item/create') , 'id' => $block->primaryKey]) ?>"><?= Yii::t('content', 'Add') ?></a></li>
</ul>
<br/>