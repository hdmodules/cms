<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;


$this->title = 'Game Points';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-point-index">

    <?= $this->render('_menu') ?>

    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'summary'=>'',
            'columns' => [
                [
                    'label'=>'id',
                    'attribute'=>'id',
                    'headerOptions' => ['width'=>'50'],
                ],
                'name',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>
