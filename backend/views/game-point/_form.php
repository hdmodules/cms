<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use \hdmodules\request\models\RequestType;

$module = $this->context->module->id;
$action = $this->context->action->id;

?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

            <div class="x_title">
                <h2><?= Yii::t('request', 'Form') ?> <small><?= $model->isNewRecord ? 'Create Point' : 'Update Point' ?></small></h2>
                <ul class="nav navbar-right">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <div class="ln_solid"></div>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' =>'btn btn-primary']) ?>
                </div>



                <?php ActiveForm::end(); ?>

            </div>

        </div>
    </div>
</div>