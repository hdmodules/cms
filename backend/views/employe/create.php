<?php
$this->title = Yii::t('news', 'Create news');
?>
<?= $this->render('_menu', ['block' => $block]) ?>
<?= $this->render('_form', ['model' => $model]) ?>