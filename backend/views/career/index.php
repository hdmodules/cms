<?php
use hdmodules\content\models\Item;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\SwitchInput;

$this->title = Yii::t('content', 'Items');

$module = $this->context->module->id;
?>
<?= $this->render('_menu', ['block' => $model]) ?>

<?php if(count($model->items)) : ?>

    <table class="table table-hover">

        <thead>
            <tr>
                <th width="5%">#</th>
                <th><?= Yii::t('content', 'Title') ?></th>
                <th width="10%"><?= Yii::t('content', 'Type') ?></th>
                <th width="10%"><?= Yii::t('content', 'Views') ?></th>
                <th width="10%"><?= Yii::t('content', 'Status') ?></th>
                <th width="10%"></th>
            </tr>
        </thead>

        <tbody>
            <?php foreach($model->items as $item) : ?>
                <tr data-id="<?= $item->primaryKey ?>">

                    <td><?= $item->primaryKey ?></td>
                    <td><a href="<?= Url::to([($item->block->controller_name ? $item->block->controller_name.'/edit' : '/content/item/edit'), 'id' => $item->primaryKey]) ?>"><?= $item->title ?></a></td>
                    <td><?= backend\models\CareerItemContent::getCareerType($item->attribute_1) ?></td>
                    <td><?= $item->views ?></td>
                    <td class="status" align="right">
                        <?= SwitchInput::widget([
                            'name' => 'status',
                            'options'=>[
                                'class'=>'switch-input',
                                'data-id' => $item->primaryKey,
                                'data-link' => Url::to(['/content/item']),
                            ],
                            'value' => $item->status == Item::STATUS_ON,
                            'pluginOptions' => [
                                'size' => 'small',
                                'onColor' => 'success',
                                'offColor' => 'danger',
                            ],
                        ]); ?>
                    </td>
                    <td class="text-right">
                        <div class="btn-group btn-group-sm" role="group">
                            <a href="<?= Url::to(['/content/item/up', 'id' => $item->primaryKey, 'block_id' => $model->primaryKey]) ?>" class="btn btn-default move-up" title="<?= Yii::t('content', 'Move up') ?>"><span class="glyphicon glyphicon-arrow-up"></span></a>
                            <a href="<?= Url::to(['/content/item/down', 'id' => $item->primaryKey, 'block_id' => $model->primaryKey]) ?>" class="btn btn-default move-down" title="<?= Yii::t('content', 'Move down') ?>"><span class="glyphicon glyphicon-arrow-down"></span></a>
                            <a href="<?= Url::to(['/content/item/delete', 'id' => $item->primaryKey]) ?>" class="btn btn-default confirm-delete" title="<?= Yii::t('content', 'Delete item') ?>"><span class="glyphicon glyphicon-remove"></span></a>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

<?php else : ?>
    <p><?= Yii::t('content', 'No records found') ?></p>
<?php endif; ?>

<script>



</script>

<?php  $this->registerJs('

$(\'.switch-input\').on(\'switchChange.bootstrapSwitch\', function(event, state){
        var checkbox = $(this);             
        $.getJSON(checkbox.data(\'link\') + \'/\' + (state ? \'on\' : \'off\') + \'/\' + checkbox.data(\'id\'), function(response){                    
            if(response.result === \'error\' || response.result == \'\'){                            
                checkbox.bootstrapSwitch(\'disabled\',true);
                alert(response.error);
            }        
        }).error(function(error) {         
            checkbox.bootstrapSwitch(\'disabled\',true);
            alert(error.statusText);
        });
    });
', \yii\web\View::POS_READY); ?>



