<?php
use hdmodules\base\helpers\Image;
use hdmodules\base\widgets\SeoForm;
use hdmodules\base\widgets\DateTimePicker;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use \common\helpers\Helper;
use kartik\widgets\DatePicker;
use backend\models\CareerItemContent;

use hdmodules\base\multilanguage\input_widget\MultiLanguageActiveField;
use hdmodules\base\widgets\RedactorMultiLanguageInput;

$module = $this->context->module->id;
?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

            <div class="x_title">
                <h2><?= Yii::t('content', 'Form') ?> <small><?= Yii::t('career', 'create item') ?></small></h2>
                <ul class="nav navbar-right">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                    'validateOnSubmit' => true,
                    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
                ]); ?>

                    <?= $form->field($model, 'title')->widget(MultiLanguageActiveField::className()) ?>

                    <div style="margin: 30px 0 20px 0">
                        <?php if($model->image) : ?>
                            <img src="<?= Image::thumb($model->image, 240) ?>">
                            <a href="<?= Url::to([($model->block->controller_name ? $model->block->controller_name.'/clear-image' : '/'.$module.'/item/clear-image'), 'id' => $model->primaryKey]) ?>" class="text-danger confirm-delete" title="<?= Yii::t('content', 'Clear image')?>" style="margin-left: 20px"><?= Yii::t('content', 'Clear image')?></a>
                        <?php endif; ?>

                        <?= $form->field($model, 'image')->fileInput() ?>
                    </div>


                    <?= $form->field($model, 'short')->textarea()->widget(MultiLanguageActiveField::className(), ['inputType' => 'textArea', 'inputOptions'=>['rows' => '5', 'style'=>['width'=>'100%']]]) ?>

                    <?= RedactorMultiLanguageInput::widget($model, 'text', ['options' => [
                        'minHeight' => 400,
                        'imageUpload' => Url::to(['/redactor/upload', 'dir' => 'items']),
                        'fileUpload' => Url::to(['/redactor/upload', 'dir' => 'items']),
                        'plugins' => ['fullscreen']
                    ]]); ?>
                    <br>
                    
                    <?= $form->field($model, 'text_1')->label('Url') ?>
                    
                    <?= $form->field($model, 'attribute_1')->dropDownList(CareerItemContent::getCareerType())->label('Type block') ?>
                    <?= $form->field($model, 'attribute_2')->label('Block CSS class') ?>
                    <?= $form->field($model, 'attribute_3')->label('Button CSS class') ?>

                    <?= $form->field($model, 'slug') ?>

                    <?php if(Yii::$app->controller->action->id == 'create' || (Yii::$app->controller->action->id == 'edit')) : ?>
                        <?= SeoForm::widget(['model' => $model]) ?>
                    <?php endif; ?>

                    <div class="ln_solid"></div>

                    <?= Html::submitButton(Yii::t('content', 'Save'), ['class' => 'btn btn-primary']) ?>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>


