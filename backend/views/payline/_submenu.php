<?php
use yii\helpers\Url;

$action = $this->context->action->id;
$module = $this->context->module->id;
?>

<ul class="nav nav-tabs">
    <li <?= ($action === 'edit') ? 'class="active"' : '' ?>><a href="<?= Url::to([($model->block->controller_name ? $model->block->controller_name.'/edit' : '/'.$module.'/item/edit'), 'id' => $model->primaryKey]) ?>"><?= Yii::t('easyii/landing', 'Edit game') ?></a></li>
</ul>
<br>