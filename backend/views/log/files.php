<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Log;
use kartik\date\DatePicker;
use yii\web\View;

$this->title = 'File logs';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('    
    function downloadFile(fileName){
        $("#file_action").val("download");
        $("#file_name").val(fileName);
        
        $("#file-form").submit();
    }', View::POS_END);

$this->registerJs('    
    function deleteFile(fileName){
        $("#file_action").val("delete");
        $("#file_name").val(fileName);
        
        $("#file-form").submit();
    }', View::POS_END);

?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-md-12">
        <div class="page-title title-center">
            <?=
            yii\widgets\Breadcrumbs::widget([
                'homeLink' => [
                    'label' => \Yii::t('site', 'Home'),
                    'url' => Yii::$app->urlManager->baseUrl,
                ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []])
            ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?php if(isset($logs)){ ?>
        <?php $i = 1; ?>
        <?= Html::beginForm('files', 'post', ['id' => 'file-form']) ?>
            <?= Html::hiddenInput('file_action', '', ['id' => 'file_action']) ?>
            <?= Html::hiddenInput('file_name', '', ['id' => 'file_name']) ?>
        <table class="table table-bordered">
            <?php foreach($logs as $log){ ?>
            <tr>
                <td width="2%"><?= $i++ ?>.</td>
                <td><?= $log ?></td>
                <td width="10%" align="center"><?= Html::button('Download', ['class' => 'btn', 'onclick' => 'downloadFile(\'' . $log . '\');']) ?></td>
                <td width="10%" align="center"><?= Html::button('Delete ', ['class' => 'btn btn-danger', 'onclick' => 'deleteFile(\'' . $log . '\');']) ?></td>
            </tr>
            <?php } ?>
        </table>
        
        <?= Html::endForm() ?>
        <?php } else { ?>
        <h3>Logs files not found</h3>
        <?php } ?>

    </div>
</div>
