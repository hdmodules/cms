<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Log;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\LogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'MySQL logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-md-12">
        <div class="page-title title-center">
            <?=
              yii\widgets\Breadcrumbs::widget([
                  'homeLink' => [
                      'label' => \Yii::t('site', 'Home'),
                      'url' => Yii::$app->urlManager->baseUrl,
                  ],
                  'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []])
            ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= Html::a('Delete all', ['delete-all'], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete all logs?'
            ],
        ]) ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'contentOptions' => ['width' => '5%'],
            ],
            [
                'attribute' => 'type',
                'contentOptions' => ['width' => '10%'],
                'value' => function($data) {
                    return Log::getType($data->type);
                },
                'filter' => Html::activeDropDownList(
                        $searchModel, 'type', Log::getType(), ['class' => 'form-control', 'prompt' => '']
                )
            ],
            [
                'attribute' => 'create_time',
                'contentOptions' => ['width' => '15%'],
                'value' => function ($data) {
                    return Yii::$app->formatter->asDatetime($data->create_time, Yii::$app->formatter->datetimeFormat);
                },
                'filter' => DatePicker::widget([
                              'model' => $searchModel,
                              'attribute' => 'create_time',
                              'type' => DatePicker::TYPE_INPUT,
                              'value' => '',
                              'pluginOptions' => [
                                  'autoclose' => true,
                                  'format' => 'dd.mm.yyyy'
                              ]
                ])
            ],
            'message:ntext',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}',
                'contentOptions' => ['width' => '4%']
            ],
        ],
    ]);
    ?>
</div>
    </div>
