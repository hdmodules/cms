<?php

use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;


$module = $this->context->module->id;
?>

<div class="col-md-6 col-xs-12">
    <div class="x_panel">

        <div class="x_title">
            <h2><?= Html::a(Yii::t('game', 'All points'), Url::to('/adminpanel/game-point/index') )?></h2>
            <ul class="nav navbar-right">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">

            <?= GridView::widget([
                'dataProvider' => $points,
                'summary'=>'',
                'columns' => [
                    [
                        'label'=>'id',
                        'attribute'=>'id',
                        'headerOptions' => ['width'=>'50'],
                    ],
                    'name',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width'=>'85'],
                        'template'=>'<div class="btn-group btn-group-sm">{update}{delete}</div>',
                        'urlCreator' => function ($action, $model, $key, $index) {
                            $url = '';
                            if ($action === 'update'){
                                $url = Url::to(['game-point/update', 'id'=>$model->id]);
                            }
                            if ($action === 'delete')
                                $url = Url::to(['game-point/delete', 'id'=>$model->id]);

                            return $url;
                        },
                        'buttons' => [
                            'update' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('game', 'Update'),
                                    'class'=>'btn btn-default update'
                                ]);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('game', 'Delete'),
                                    'class'=>'btn btn-danger delete',
                                    'data-confirm' => Yii::t('app', 'Are you sure you want to delete this Record?'),
                                    'data-method' => 'post'
                                ]);
                            },
                        ],
                    ],
                ],
            ]) ?>

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg"><?= Yii::t('game', 'Create point') ?></button>

        </div>


        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel"><?= Yii::t('game', 'Create point') ?></h4>
                    </div>
                    <div class="modal-body">

                        <?php $form = ActiveForm::begin([
                            'enableAjaxValidation' => true,
                            'validateOnSubmit' => true,
                            'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
                        ]); ?>

                        <?= $form->field($point, 'name')->label('Name') ?>

                        <div class="ln_solid"></div>

                        <?= Html::submitButton(Yii::t('game', 'Save'), ['class' => 'btn btn-primary']) ?>

                        <?php ActiveForm::end(); ?>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>


