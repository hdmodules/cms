<?php
use yii\helpers\Html;
use \backend\models\GameReview;
use yii\widgets\ActiveForm;

$this->title = $model->title;
?>
<?= $this->render('_menu', ['block' => $model->block]) ?>

<?= $this->render('_submenu', ['model' => $model]) ?>

<div class="row">

    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div style="margin: 10px 0 10px 0">
                <?php foreach (GameReview::getTypeReviews() as $key=>$type){ ?>
                    <?= $this->render('_menu_common_reviews',  ['model' => $model, 'type_id'=>$key, 'type_name'=>$type]) ?>
                <?php } ?>
            </div>
        </div>
    </div>


    <div class="col-md-8 col-xs-12">
        <?php foreach (GameReview::getTypeReviews() as $key=>$type){ ?>

            <?php if(!Yii::$app->request->get('type') || (Yii::$app->request->get('type') && Yii::$app->request->get('type') == $key)){ ?>
                <?= $this->render('_form_game_reviews', ['model' => $model, 'type_id'=>$key, 'type_name'=>$type]) ?>
            <?php } ?>

        <?php } ?>
    </div>

    <div class="col-md-4 col-xs-12">
        <div class="x_panel">

            <div class="x_title">
                <h2><?= Yii::t('game', 'Create Review') ?></h2>
                <ul class="nav navbar-right">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                    'validateOnSubmit' => true,
                    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
                ]); ?>

                    <?= $form->field($review, 'title')->input('field'); ?>

                    <?= $form->field($review, 'game_id')->hiddenInput(['value'=>$model->game->id])->label(false); ?>

                    <?php $slected = isset($_GET['type']) ? ([$_GET['type'] => ['Selected'=>'selected']]) : [] ?>

                    <?= $form->field($review, 'type')->dropDownList(GameReview::getTypeReviews(), ['options' => $slected, 'prompt'=>'Select type ...'])->label('Type')?>

                    <?= $form->field($review, 'url')->input('field')->label('Youtube') ?>

                    <div style="margin: 10px 0 20px 0;">
                        <?= $form->field($review, 'image')->fileInput() ?>
                    </div>


                    <?= $form->field($review, 'status')->dropDownList(GameReview::getStatuses(), ['prompt'=>'Select status ...'])->label('Status')?>

                    <div class="ln_solid"></div>

                    <?= Html::submitButton(Yii::t('game', 'Save'), ['class' => 'btn btn-primary']) ?>

                <?php ActiveForm::end(); ?>

            </div>

        </div>
    </div>

</div>


