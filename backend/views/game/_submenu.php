<?php
use yii\helpers\Url;

$action = $this->context->action->id;
$module = $this->context->module->id;

$new_review = $model->game->countNewReviews ? $model->game->countNewReviews : '';

?>

<ul class="nav nav-tabs">
    <li <?= ($action === 'edit') ? 'class="active"' : '' ?>><a href="<?= Url::to([($model->block->controller_name ? $model->block->controller_name.'/edit' : '/'.$module.'/item/edit'), 'id' => $model->primaryKey]) ?>"><?= Yii::t('game', 'Edit game') ?></a></li>
    <li <?= ($action === 'photos') ? 'class="active"' : '' ?>><a href="<?= Url::to([($model->block->controller_name ? $model->block->controller_name.'/photos' : '/'.$module.'/item/photos'), 'id' => $model->primaryKey]) ?>"><span class="glyphicon glyphicon-camera"></span> <?= Yii::t('game', 'Photos') ?></a></li>
    <li <?= ($action === 'points') ? 'class="active"' : '' ?>><a href="<?= Url::to([($model->block->controller_name.'/points'), 'id' => $model->primaryKey]) ?>"><span class="glyphicon glyphicon-record"></span> <?= Yii::t('game', 'Points') ?></a></li>
    <li <?= ($action === 'reviews' || $action === 'review-update') ? 'class="active"' : '' ?>><a href="<?= Url::to([($model->block->controller_name.'/reviews'), 'id' => $model->primaryKey]) ?>"><span class="glyphicon glyphicon-th-large"></span> <?= Yii::t('game', 'Reviews') . ' <span class="badge bg-red">'.$new_review.'</span>'?></a></li>
</ul>
<br>