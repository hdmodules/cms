<?php
use yii\helpers\Html;
use \backend\models\GameReview;
use yii\widgets\ActiveForm;

$this->title = $review->title;
?>

<?= $this->render('_menu', ['block' => $model->block]) ?>

<?= $this->render('_submenu', ['model' => $model]) ?>

<div class="row">

    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div style="margin: 10px 0 10px 0">
                <?php foreach (GameReview::getTypeReviews() as $key=>$type){ ?>
                    <?= $this->render('_menu_common_reviews',  ['model' => $model, 'type_id'=>$key, 'type_name'=>$type, 'review'=>$review]) ?>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12">

        <div class="x_panel">

            <div class="x_title">
                <h2><?= Yii::t('game', 'Update Review') ?></h2>
                <ul class="nav navbar-right">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                    'validateOnSubmit' => true,
                    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
                ]); ?>

                <?= $form->field($review, 'title')->input('field'); ?>

                <?= $form->field($review, 'game_id')->hiddenInput(['value'=>$model->game->id])->label(false); ?>

                <?php $slected = isset($_GET['type']) ? ([$_GET['type'] => ['Selected'=>'selected']]) : [] ?>

                <?= $form->field($review, 'type')->dropDownList(GameReview::getTypeReviews(), ['options' => $slected, 'prompt'=>'Select type ...'])->label('Type')?>

                <?= $form->field($review, 'url')->input('field')->label('Youtube') ?>

                <div style="margin: 20px 0 20px 0;">
                    <?php if($review->image) : ?>
                        <img src="<?= \hdmodules\base\helpers\Image::thumb($review->image, 225, 225) ?>">
                        <a href="<?= \yii\helpers\Url::to([ '/game/clear-review-image', 'id' => $review->primaryKey]) ?>" class="text-danger confirm-delete" title="<?= Yii::t('content', 'Clear image')?>" style="margin-left: 20px"><?= Yii::t('content', 'Clear image')?></a>
                    <?php endif; ?>

                    <?= $form->field($review, 'image')->fileInput() ?>
                </div>


                <?= $form->field($review, 'status')->dropDownList(GameReview::getStatuses(), ['prompt'=>'Select status ...'])->label('Status')?>

                <div class="ln_solid"></div>

                <?= Html::submitButton(Yii::t('game', 'Update'), ['class' => 'btn btn-primary']) ?>

                <?php ActiveForm::end(); ?>

            </div>

        </div>
    </div>
</div>
