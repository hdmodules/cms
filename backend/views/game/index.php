<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

$this->title = Yii::t('game', 'Games');

$module = $this->context->module->id;
?>
<?= $this->render('_menu', ['block' => $model]) ?>

<div class="col-md-12 col-xs-12">
    <div class="x_panel">

        <div class="x_title"  style="margin-bottom: 30px">
            <h2><?= Yii::t('game', 'Games') ?></h2>
            <ul class="nav navbar-right">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">

            <?= GridView::widget([
                'dataProvider' =>$provider,
                'summary'=>'',
                'columns' => [
                    [
                        'attribute' => 'id',
                        'contentOptions' => ['width' => '3%'],
                    ],
                    [
                        'label'=>'Title',
                        'content'=>function($data){
                            $title = $data->title . ' ' .($data->game->countNewReviews ? '<span class="btn btn-danger" style="float: right">'.Yii::t('game', 'new reviews: ').$data->game->countNewReviews . '</span>': '');
                            $url = [($data->block->controller_name ? $data->block->controller_name.'/edit' : '/content/item/edit'), 'id' => $data->primaryKey];
                            return Html::a($title, $url, [
                                'title' => Yii::t('hdmodules', 'Update')
                            ]);

                        }
                    ],
                    [
                        'label'=>'Photos',
                        'contentOptions' => ['width' => '30px'],
                        'content'=>function($data){
                            $title = count($data->photos)
                                        ? '<span class="btn btn-default glyphicon glyphicon-folder-open"> '.count($data->photos). '</span>'
                                        : '<span class="btn btn-default glyphicon glyphicon-folder-close"> 0</span> ';
                            $url = ['/game/photos', 'id' => $data->primaryKey];
                            return Html::a($title, $url);

                        },
                        'format'=>'raw'
                    ],
                    [
                        'label'=>'Points',
                        'contentOptions' => ['width' => '30px'],
                        'content'=>function($data){
                            $title = count($data->game->points)
                                        ? '<span class="btn btn-default glyphicon glyphicon-folder-open"> '.count($data->game->points). '</span>'
                                        : '<span class="btn btn-default glyphicon glyphicon-folder-close"> 0</span> ';
                            $url = ['/game/points', 'id' => $data->primaryKey];
                            return Html::a($title, $url);

                        },
                        'format'=>'raw'
                    ],
                    [
                        'label'=>'Reviews',
                        'contentOptions' => ['width' => '30px'],
                        'content'=>function($data){
                            $title = $data->game->countAllReviews
                                        ? '<span class="btn btn-default glyphicon glyphicon-folder-open"> '.$data->game->countAllReviews. '</span>'
                                        : '<span class="btn btn-default glyphicon glyphicon-folder-close"> 0</span> ';
                            $url = ['/game/reviews', 'id' => $data->primaryKey];
                            return Html::a($title, $url);

                        },
                        'format'=>'raw'
                    ],
                    [
                        'attribute'=>'status',
                        'contentOptions' => ['width' => '90px'],
                        'content'=>function($data){
                            return \kartik\widgets\SwitchInput::widget([
                                'name' => 'status',
                                'options'=>[
                                    'class'=>'switch-input',
                                    'data-id' => $data->primaryKey,
                                    'data-link' => Url::to(['/content/item']),
                                ],
                                'value' => $data->status == \backend\models\GameItemContent::STATUS_ON,
                                'pluginOptions' => [
                                    'size' => 'small',
                                    'onColor' => 'success',
                                    'offColor' => 'danger',
                                ],
                            ]);
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width'=>'85px'],
                        'template'=>'<div class="btn-group btn-group-sm">{up}{down}</div>',
                        'urlCreator' => function ($action, $model, $key, $index) {
                            $url = '';
                            if ($action === 'up')
                                $url = Url::to(['/content/item/up', 'id'=>$model->id, 'block_id' => $model->block->primaryKey]);
                            if ($action === 'down')
                                $url = Url::to(['/content/item/down', 'id'=>$model->id, 'block_id' => $model->block->primaryKey]);

                            return $url;
                        },
                        'buttons' => [
                            'up' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', $url, [
                                    'title' => Yii::t('hdmodules', 'Move up'),
                                    'class'=>'btn btn-default move-up'
                                ]);
                            },
                            'down' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-arrow-down"></span>', $url, [
                                    'title' => Yii::t('hdmodules', 'Move down'),
                                    'class'=>'btn btn-default move-down'
                                ]);
                            },
                        ],

                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width'=>'85px'],
                        'template'=>'<div class="btn-group btn-group-sm">{update}{delete}</div>',
                        'urlCreator' => function ($action, $model, $key, $index) {
                            $url = '';
                            if ($action === 'update')
                                $url = Url::to(['/game/edit', 'id'=>$model->id]);
                            if ($action === 'delete')
                                $url = Url::to(['/game/delete', 'id'=>$model->id]);

                            return $url;
                        },
                        'buttons' => [
                            'update' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('hdmodules', 'Update'),
                                    'class'=>'btn btn-default update'
                                ]);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('hdmodules', 'Delete'),
                                    'class'=>'btn btn-danger delete',
                                    'data-confirm' => Yii::t('app', 'Are you sure you want to delete this Record?'),
                                    'data-method' => 'post'
                                ]);
                            },
                        ],
                    ],
                ],
            ]);
            ?>

            <?php /*if(count($model->items)) : */?><!--

                <table class="table table-hover">

                    <thead>
                    <tr>
                        <th width="50">#</th>
                        <th><?/*= Yii::t('content', 'Title') */?></th>
                        <th width="120"><?/*= Yii::t('content', 'Views') */?></th>
                        <th width="100"><?/*= Yii::t('content', 'Status') */?></th>
                        <th width="120"></th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php /*foreach($model->items as $item) : */?>
                        <tr data-id="<?/*= $item->primaryKey */?>">

                            <td><?/*= $item->primaryKey */?></td>
                            <td><a href="<?/*= Url::to([($item->block->controller_name ? $item->block->controller_name.'/edit' : '/content/item/edit'), 'id' => $item->primaryKey]) */?>"><?/*= $item->title */?></a></td>
                            <td><?/*= $item->views */?></td>
                            <td class="status">
                                <?/*= SwitchInput::widget([
                                    'name' => 'status',
                                    'options'=>[
                                        'class'=>'switch-input',
                                        'data-id' => $item->primaryKey,
                                        'data-link' => Url::to(['/content/item']),
                                    ],
                                    'value' => $item->status == Item::STATUS_ON,
                                    'pluginOptions' => [
                                        'size' => 'small',
                                        'onColor' => 'success',
                                        'offColor' => 'danger',
                                    ],
                                ]); */?>
                            </td>
                            <td class="text-right">
                                <div class="btn-group btn-group-sm" role="group">
                                    <a href="<?/*= Url::to(['/content/item/up', 'id' => $item->primaryKey, 'block_id' => $model->primaryKey]) */?>" class="btn btn-default move-up" title="<?/*= Yii::t('content', 'Move up') */?>"><span class="glyphicon glyphicon-arrow-up"></span></a>
                                    <a href="<?/*= Url::to(['/content/item/down', 'id' => $item->primaryKey, 'block_id' => $model->primaryKey]) */?>" class="btn btn-default move-down" title="<?/*= Yii::t('content', 'Move down') */?>"><span class="glyphicon glyphicon-arrow-down"></span></a>
                                    <a href="<?/*= Url::to(['/content/item/delete', 'id' => $item->primaryKey]) */?>" class="btn btn-default confirm-delete" title="<?/*= Yii::t('content', 'Delete item') */?>"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                            </td>
                        </tr>
                    <?php /*endforeach; */?>
                    </tbody>
                </table>

            <?php /*else : */?>
                <p><?/*= Yii::t('content', 'No records found') */?></p>
            --><?php /*endif; */?>
        </div>
    </div>
</div>




<?php  $this->registerJs('

$(\'.switch-input\').on(\'switchChange.bootstrapSwitch\', function(event, state){
        var checkbox = $(this);             
        $.getJSON(checkbox.data(\'link\') + \'/\' + (state ? \'on\' : \'off\') + \'/\' + checkbox.data(\'id\'), function(response){                    
            if(response.result === \'error\' || response.result == \'\'){                            
                checkbox.bootstrapSwitch(\'disabled\',true);
                alert(response.error);
            }        
        }).error(function(error) {         
            checkbox.bootstrapSwitch(\'disabled\',true);
            alert(error.statusText);
        });
    });
', \yii\web\View::POS_READY); ?>



