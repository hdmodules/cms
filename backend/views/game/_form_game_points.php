<?php

use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
use \backend\models\GamePoint;


$module = $this->context->module->id;
?>

<div class="col-md-6 col-xs-12">
    <div class="x_panel">

        <div class="x_title">
            <h2><?= Yii::t('game', 'Game points') ?></h2>
            <ul class="nav navbar-right">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">

            <?= GridView::widget([
                'dataProvider' => $game_points,
                'summary'=>'',
                'columns' => [
                    [
                        'label'=>'Name',
                        'content'=>function($data){
                            return $data->point->name;
                        }

                    ],
                    'color',
                   /* [
                        'label'=>'Order',
                        'content'=>function($data){
                             return Html::a(Yii::t('content', 'Move up'), Url::to(['/content/item/up', 'id' => $data->primaryKey]));
                        }
                    ],*/
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width'=>'85'],
                        'template'=>'<div class="btn-group btn-group-sm">{up}{down}</div>',
                        'urlCreator' => function ($action, $model, $key, $index) {
                            $url = '';
                            if ($action === 'up')
                                $url = Url::to(['game/point-up', 'id'=>$model->id]);
                            if ($action === 'down')
                                $url = Url::to(['game/point-down', 'id'=>$model->id]);

                            return $url;
                        },
                        'buttons' => [
                            'up' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', $url, [
                                    'title' => Yii::t('game', 'Move up'),
                                    'class'=>'btn btn-default move-up'
                                ]);
                            },
                            'down' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-arrow-down"></span>', $url, [
                                    'title' => Yii::t('game', 'Move down'),
                                    'class'=>'btn btn-default move-down'
                                ]);
                            },
                        ],

                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width'=>'30'],
                        'buttonOptions'=>[
                            'class'=>'btn btn-danger'
                        ],
                        'template'=>'<div class="btn-group btn-group-sm">{delete}</div>',
                        'urlCreator' => function ($action, $model, $key, $index) {
                            $url = '';
                            if ($action === 'delete')
                                $url = Url::to(['game/point-delete', 'id'=>$model->id]);

                            return $url;
                        },

                    ],
                ],
            ]) ?>

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg-game-point"><?= Yii::t('game', 'Add point for game') ?></button>

        </div>


        <div class="modal fade bs-example-modal-lg-game-point" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel"><?= Yii::t('game', 'Add point for game') ?></h4>
                    </div>
                    <div class="modal-body">

                        <?php $form = ActiveForm::begin([
                            'enableAjaxValidation' => true,
                            'validateOnSubmit' => true,
                            'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
                        ]); ?>

                            <?= $form->field($game_point, 'game_id')->hiddenInput(['value'=>$model->game->id])->label(false); ?>

                            <?= $form->field($game_point, 'point_id')->dropDownList(\yii\helpers\ArrayHelper::map(GamePoint::find()->all(), 'id', 'name'), ['prompt'=>'Select point ...'])->label('Point')?>

                            <?= $form->field($game_point, 'color')->widget(\kartik\widgets\ColorInput::className(), [
                                'options' => ['placeholder' => 'Select color ...']
                            ]); ?>

                            <div class="ln_solid"></div>

                        <?= Html::submitButton(Yii::t('game', 'Save'), ['class' => 'btn btn-primary']) ?>

                        <?php ActiveForm::end(); ?>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>


