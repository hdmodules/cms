<?php
$this->title = Yii::t('content', 'Create game');
?>
<?= $this->render('_menu', ['block' => $block]) ?>
<?= $this->render('_form', ['model' => $model]) ?>