<?php
use hdmodules\base\helpers\Image;
use hdmodules\base\widgets\SeoForm;
use hdmodules\base\widgets\DateTimePicker;
use hdmodules\base\widgets\TagsInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use hdmodules\base\multilanguage\input_widget\MultiLanguageActiveField;
use hdmodules\base\widgets\RedactorMultiLanguageInput;
use hdmodules\content\models\Item;
use kartik\widgets\Select2;

$module = $this->context->module->id;
?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

            <div class="x_title">
                <h2><?= Yii::t('content', 'Form') ?> <small><?= Yii::t('content', 'create game') ?></small></h2>
                <ul class="nav navbar-right">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                    'validateOnSubmit' => true,
                    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
                ]); ?>

                    <?= $form->field($model, 'title')->widget(MultiLanguageActiveField::className()) ?>

                    <?= $form->field($model->game, 'key')->widget(MultiLanguageActiveField::className()) ?>

                    <?= $form->field($model->game, 'html_game')->checkbox() ?>

                    <?php
                        echo $form->field($model->game, 'category_ids')->widget(Select2::className(), [
                            'data' => Item::listAll('games-category'),
                            'size'    => Select2::MEDIUM,
                            'options' => ['multiple' => true, 'placeholder' => 'Select categoties ...', 'style'=>'margin-top:100px' ],
                            'pluginOptions' => [
                                'multiple' => true
                            ],
                        ])->label('Categoties');
                    ?>

                    <?php
                        echo $form->field($model->game, 'feature_ids')->widget(Select2::className(), [
                            'data' => Item::listAll('games-feature'),
                            'size'    => Select2::MEDIUM,
                            'options' => ['multiple' => true, 'placeholder' => 'Select features ...', 'style'=>'margin-top:100px' ],
                            'pluginOptions' => [
                                'multiple' => true
                            ],
                        ])->label('Features');
                    ?>

                    <?php $form->field($model->game, 'theme_id')->dropDownList(Item::listAll('games-theme'), ['prompt'=>'Select theme ...'])->label('Theme')?>
                    <?php
                        echo $form->field($model->game, 'theme_ids')->widget(Select2::className(), [
                            'data' => Item::listAll('games-theme'),
                            'size'    => Select2::MEDIUM,
                            'options' => ['multiple' => true, 'placeholder' => 'Select themes ...', 'style'=>'margin-top:100px' ],
                            'pluginOptions' => [
                                'multiple' => true
                            ],
                        ])->label('Themes');
                    ?>

                    <?= $form->field($model->game, 'payline_id')->dropDownList(Item::listAll('games-payline'), ['prompt'=>'Select payline ...'])->label('Payline')?>

                    <div style="margin: 30px 0 30px 0">
                        <?php if($model->image) : ?>
                            <img src="<?= Image::thumb($model->image, 313, 313) ?>">
                            <a href="<?= Url::to(['/content/item/clear-image', 'id' => $model->primaryKey]) ?>" class="text-danger confirm-delete" title="<?= Yii::t('content', 'Clear image')?>" style="margin-left: 20px">
                                <?= Yii::t('content', 'Clear image')?>
                            </a>
                        <?php endif; ?>

                        <?= $form->field($model, 'image')->fileInput() ?>
                    </div>

                    <?= $form->field($model, 'short')->textarea()->widget(MultiLanguageActiveField::className(), ['inputType' => 'textArea', 'inputOptions'=>['rows' => '5', 'style'=>['width'=>'100%']]]) ?>

                    <?= RedactorMultiLanguageInput::widget($model, 'text', ['options' => [
                        'minHeight' => 400,
                        'imageUpload' => Url::to(['/redactor/upload', 'dir' => 'items']),
                        'fileUpload' => Url::to(['/redactor/upload', 'dir' => 'items']),
                        'plugins' => ['fullscreen']
                    ]]); ?>
                    <br>

                    <?= $form->field($model, 'time')->widget(DateTimePicker::className()); ?>

                    <?= $form->field($model, 'slug') ?>

                    <?= SeoForm::widget(['model' => $model]) ?>

                    <?php $form->field($model, 'tagNames')->widget(TagsInput::className()) ?>

                    <div class="ln_solid"></div>

                    <?= Html::submitButton(Yii::t('content', 'Save'), ['class' => 'btn btn-primary']) ?>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>


