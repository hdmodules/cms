<?php
$this->title = $model->title;
?>
<?= $this->render('_menu', ['block' => $model->block]) ?>

<?= $this->render('_submenu', ['model' => $model]) ?>


<div class="row">
    <?= $this->render('_form_game_points', ['model' => $model, 'game_points'=>$game_points, 'game_point'=>$game_point]) ?>

    <?= $this->render('_form_all_points', ['model' => $model, 'points'=>$points, 'point'=>$point,]) ?>
</div>


