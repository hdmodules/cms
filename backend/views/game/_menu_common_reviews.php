<?php
use yii\helpers\Url;

$action = $this->context->action->id;
?>

<div class="btn-group" style="margin-right: 10px">

    <?php if($action == 'reviews'){?>

        <a href="<?= Url::to(['game/reviews/'.$model->id, 'type'=>$type_id]) ?>" type="button" class="btn btn-<?= Yii::$app->request->get('type') == $type_id ? 'danger' : 'default' ?>">
            <?= $type_name; ?>
        </a>

    <?php }elseif($action == 'review-update'){?>

        <a href="<?= Url::to(['game/reviews/'.$model->id, 'type'=>$type_id]) ?>" type="button" class="btn btn-<?= $review->type == $type_id ? 'danger' : 'default' ?>">
            <?= $type_name; ?>
        </a>

    <?php }?>

</div>

