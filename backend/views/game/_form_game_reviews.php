<?php

use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use \backend\models\GameReview;
use \hdmodules\base\helpers\Image;

?>


<div class="x_panel">

    <div class="x_title">
        <h2><?= $type_name ?></h2>
        <ul class="nav navbar-right">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">

        <?php
            $data_provider = new ActiveDataProvider([
                'query' => GameReview::find()->where(['game_id'=>$model->game->id, 'type'=>$type_id])->orderBy(['order_num' => SORT_DESC]),
            ]);
        ?>

        <?= GridView::widget([
            'dataProvider' => $data_provider,
            'summary'=>'',
            'columns' => [
                [
                    'label'=>'Title',
                    'content'=>function($data){
                        return Html::a($data->title, Url::to(['game/review-update', 'id'=>$data->id]), [
                            'title' => Yii::t('game', 'Update')
                        ]);
                    }

                ],
                [
                    'attribute'=>'image',
                    'headerOptions' => ['width'=>'150'],
                    'format' => 'raw',
                    'content'=>function($data){
                        return !empty($data->image) ?  Html::img(Image::thumb($data->image, 313, 313), ['style'=>'width:150px']) : null;
                    }
                ],
                [
                    'label'=>'Youtube',
                    'attribute'=>'url',
                    'headerOptions' => ['width'=>'200'],
                    'format' => 'raw',
                    'content'=>function($data){
                        return !empty($data->url) ? '<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'.$data->getYotubeCode().'" frameborder="0" allowfullscreen></iframe>' : null;
                    }
                ],
                [
                    'attribute'=>'status',
                    'format' => 'raw',
                    'headerOptions' => ['width'=>'60'],
                    'content'=>function($data){
                        return Html::a(GameReview::getStatuses()[$data->status], Url::to(['game/review-update', 'id'=>$data->id]), [
                            'title' => Yii::t('game', 'Update'),
                            'class'=>'btn btn-'.GameReview::getColorStatuses()[$data->status].' btn-sm'
                        ]);
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'headerOptions' => ['width'=>'85'],
                    'template'=>'<div class="btn-group btn-group-sm">{up}{down}</div>',
                    'urlCreator' => function ($action, $model, $key, $index) {
                        $url = '';
                        if ($action === 'up')
                            $url = Url::to(['game/review-up', 'id'=>$model->id]);
                        if ($action === 'down')
                            $url = Url::to(['game/review-down', 'id'=>$model->id]);

                        return $url;
                    },
                    'buttons' => [
                        'up' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', $url, [
                                'title' => Yii::t('game', 'Move up'),
                                'class'=>'btn btn-default move-up'
                            ]);
                        },
                        'down' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-arrow-down"></span>', $url, [
                                'title' => Yii::t('game', 'Move down'),
                                'class'=>'btn btn-default move-down'
                            ]);
                        },
                    ],

                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'headerOptions' => ['width'=>'85'],
                    'template'=>'<div class="btn-group btn-group-sm">{update}{delete}</div>',
                    'urlCreator' => function ($action, $model, $key, $index) {
                        $url = '';
                        if ($action === 'update'){
                            $url = Url::to(['game/review-update', 'id'=>$model->id]);
                        }
                        if ($action === 'delete')
                            $url = Url::to(['game/review-delete', 'id'=>$model->id]);

                        return $url;
                    },
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('game', 'Update'),
                                'class'=>'btn btn-default update'
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('game', 'Delete'),
                                'class'=>'btn btn-danger delete',
                                'data-confirm' => Yii::t('app', 'Are you sure you want to delete this Record?'),
                                'data-method' => 'post'
                            ]);
                        },
                    ],
                ],
            ],
        ]) ?>

    </div>

</div>



