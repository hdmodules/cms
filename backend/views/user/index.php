<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-md-12">
        <div class="page-title title-center">
            <?=
              yii\widgets\Breadcrumbs::widget([
                  'homeLink' => [
                      'label' => \Yii::t('site', 'Home'),
                      'url' => Yii::$app->urlManager->baseUrl,
                  ],
                  'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []])
            ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">

            <?=
          GridView::widget([
              'dataProvider' => $dataProvider,
              'filterModel' => $searchModel,
              'columns' => [
                  [
                      'class' => 'yii\grid\SerialColumn',
                      'contentOptions' => ['width' => '3%']
                  ],
                  [
                      'attribute' => 'id',
                      'contentOptions' => ['width' => '5%'],
                  ],
                  [
                      'attribute' => 'name',
                      'contentOptions' => ['width' => '10%'],
                  ],
                  [
                      'attribute' => 'surname',
                      'contentOptions' => ['width' => '10%'],
                  ],
                  [
                      'attribute' => 'language_id',
                      'format' => 'html',
                      'contentOptions' => ['width' => '7%'],
                      'value' => function($data) {
                        return $data->language->name;
                      },
                      'filter' => Html::activeDropDownList(
                          $searchModel, 'language_id', common\models\Language::getLanguages(), ['class' => 'form-control', 'prompt' => '']
                      )
                  ],
                  [
                      'attribute' => 'service',
                      'format' => 'html',
                      //'contentOptions' => ['width' => '10%'],
                      'value' => function($data) {
                        return common\models\User::getService($data->service);
                      },
                      'filter' => Html::activeDropDownList(
                          $searchModel, 'service', common\models\User::getService(), ['class' => 'form-control', 'prompt' => '']
                      )
                  ],
                   'email:email',
                  [
                      'attribute' => 'role',
                      'format' => 'html',
                      //'contentOptions' => ['width' => '10%'],
                      'value' => function($data) {
                        return common\models\User::getRole($data->role);
                      },
                      'filter' => Html::activeDropDownList(
                        $searchModel, 'role', common\models\User::getRole(), ['class' => 'form-control', 'prompt' => '']
                      )
                  ],
                  [
                      'attribute' => 'status',
                      'format' => 'html',
                      //'contentOptions' => ['width' => '10%'],
                      'value' => function($data) {
                        return common\models\User::getStatus($data->status);
                      },
                      'filter' => Html::activeDropDownList(
                          $searchModel, 'status', common\models\User::getStatus(), ['class' => 'form-control', 'prompt' => '']
                      )
                  ],
                  [
                      'attribute' => 'created_at',
                      'format' => 'html',
                      'contentOptions' => ['width' => '10%'],
                      'value' => function($data) {
                        return Yii::$app->formatter->asDatetime($data->created_at, Yii::$app->formatter->datetimeFormat);
                      },
                      'filter' => DatePicker::widget([
                          'model' => $searchModel,
                          'attribute' => 'created_date',
                          'type' => DatePicker::TYPE_INPUT,
                          'value' => '',
                          'pluginOptions' => [
                              'autoclose' => true,
                              'format' => 'dd.mm.yyyy'
                          ]
                      ])
                  ],
                  [
                      'attribute' => 'updated_at',
                      'format' => 'html',
                      'contentOptions' => ['width' => '10%'],
                      'value' => function($data) {
                        return Yii::$app->formatter->asDatetime($data->updated_at, Yii::$app->formatter->datetimeFormat);
                      },
                      'filter' => DatePicker::widget([
                          'model' => $searchModel,
                          'attribute' => 'updated_date',
                          'type' => DatePicker::TYPE_INPUT,
                          'value' => '',
                          'pluginOptions' => [
                              'autoclose' => true,
                              'format' => 'dd.mm.yyyy'
                          ]
                      ])
                  ],
                  [
                      'class' => 'yii\grid\ActionColumn',
                      'contentOptions' => ['width' => '5%'],
                  ],
              ],
          ]);
        ?>

    </div>
</div>
