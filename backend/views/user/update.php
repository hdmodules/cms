<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = sprintf('Update: (ID: %s) %s', $model->id, $model->username);
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edit';
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-md-12">
        <div class="page-title title-center">
            <?=
              yii\widgets\Breadcrumbs::widget([
                  'homeLink' => [
                      'label' => \Yii::t('site', 'Home'),
                      'url' => '/dashboard',
                  ],
                  'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []])
            ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <?=
          $this->render('_form', [
              'model' => $model,
          ])
        ?>
    </div>
</div>
