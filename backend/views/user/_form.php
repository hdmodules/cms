<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use common\models\User;
use yii\web\View;

$this->registerJs('    
    $("#change_password").change(function() {
        if($(this).is(":checked")) {
            $("#change-password-form").fadeIn();
        } else {
            $("#change-password-form").fadeOut();
        }
        
    });', View::POS_READY);

?>

<div class="user-form">

    <?php $form = ActiveForm::begin([]); ?>
    <?=
      DetailView::widget([
          'model' => $model,
          'attributes' => [
              'id',
              'name',
              'surname',
              [
                  'attribute' => 'service',
                  'value' => User::getService($model->service),
              ],
              [
                  'attribute' => 'service_id',
                  'value' => $model->service_id,
                  'visible' => isset($model->service_id) ? true : false,
              ],
              'email:email',
              [
                  'attribute' => 'created_at',
                  'value' => Yii::$app->formatter->asDatetime($model->created_at, Yii::$app->formatter->datetimeFormat)
              ],
              [
                  'attribute' => 'updated_at',
                  'value' => Yii::$app->formatter->asDatetime($model->updated_at, Yii::$app->formatter->datetimeFormat)
              ],
          ],
      ])
    ?>
    <?= $form->field($model, 'language_id')->dropDownList(common\models\Language::getLanguages()) ?>

    <?= $form->field($model, 'role')->dropDownList(User::getRole()) ?>

    <?= $form->field($model, 'status')->dropDownList(User::getStatus()) ?>

    <?= $form->field($model, 'change_password')->checkbox(['id' => 'change_password']) ?>
    
    <div id="change-password-form" style="<?= $model->change_password ? '' : 'display:none' ?>">
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'password_repeat')->passwordInput() ?>
    </div>
    
    <div class="form-group" align="right">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
