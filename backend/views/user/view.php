<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-md-12">
        <div class="page-title title-center">
            <?=
              yii\widgets\Breadcrumbs::widget([
                  'homeLink' => [
                      'label' => \Yii::t('site', 'Home'),
                      'url' => '/dashboard',
                  ],
                  'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []])
            ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <p>        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?=
              Html::a('Удалить', ['delete', 'id' => $model->id], [
                  'class' => 'btn btn-danger',
                  'data' => [
                      'confirm' => sprintf('Вы действительно хотите удалить (ID:%s) %s?', $model->id, $model->name),
                      'method' => 'post',
                  ],
              ])
            ?>
        </p>

        <?=
          DetailView::widget([
              'model' => $model,
              'attributes' => [
                  'id',
                  'name',
                  'surname',
                  [
                      'attribute' => 'service',
                      'value' => User::getService($model->service),
                  ],
                  [
                      'attribute' => 'service_id',
                      'value' => $model->service_id,
                      'visible' => isset($model->service_id) ? true : false,
                  ],
                  'email:email',
                  [
                      'attribute' => 'language_id',
                      'value' => $model->language->name,
                  ],
                  [
                      'attribute' => 'created_at',
                      'value' => Yii::$app->formatter->asDatetime($model->created_at, Yii::$app->formatter->datetimeFormat)
                  ],
                  [
                      'attribute' => 'updated_at',
                      'value' => Yii::$app->formatter->asDatetime($model->updated_at, Yii::$app->formatter->datetimeFormat)
                  ],
                  [
                      'attribute' => 'role',
                      'value' => User::getRole($model->role),
                  ],
                  [
                      'attribute' => 'status',
                      'value' => User::getStatus($model->status),
                  ],
              ],
          ])
        ?>
    </div>

</div>

