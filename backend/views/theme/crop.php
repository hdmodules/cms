<?php
use hdmodules\base\cropbox\Cropbox;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = $model->title;

?>
<?= $this->render('_menu', ['block' => $model->block]) ?>
<?= $this->render('_submenu', ['model' => $model]) ?>


<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">

			<div class="x_title">
				<h2><?= Yii::t('theme', 'Form') ?> <small><?= Yii::t('theme', 'Cropped images (230x230, 473.333333x230, 230x473.333333)') ?></small></h2>
				<ul class="nav navbar-right">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				<?php if($model->theme->{"cropped_1"} && $model->theme->{"cropped_2"} && $model->theme->{"cropped_3"}) { ?>
					<h2>Saved cropped images:</h2>
					<?php  for($i = 1; $i <= 3; $i++) { ?>
						<img src="<?= $model->theme->{"cropped_{$i}"} ?>"/>
					<?php } ?>
					<hr>
				<?php } ?>


				<?php if($model->image) { ?>

					<?php  $this->registerJs("

						$(document).ready(function () {
							var xhr = new XMLHttpRequest();
							xhr.open('GET', '". $model->image ."', true);
							xhr.responseType = 'blob';
							xhr.onload = function (e) {
								if (this.status == 200) {
									window.blob = new Blob([this.response], {type: '".image_type_to_mime_type(exif_imagetype($_SERVER['DOCUMENT_ROOT'] ."/frontend/web/". $model->image))."'});
									$('input[type=file]').trigger('change');
								}
							}
							xhr.send();
						
							var k = 0;
							$('.cropped').bind('DOMSubtreeModified', function () {
								if (k == 3) {
									$('#theme-cropped_1').val($('.img-thumbnail:eq(0)').prop('src'));
									$('#theme-cropped_2').val($('.img-thumbnail:eq(1)').prop('src'));
									$('#theme-cropped_3').val($('.img-thumbnail:eq(2)').prop('src'));
								}
								k++;
							});
						});
					
					", \yii\web\View::POS_READY); ?>


					<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

					<?= $form->field($model, 'image')->widget(Cropbox::className(), [
						'attributeCropInfo' => 'crop_info',
						'pluginOptions'=> [
							'variants'=>[
								[
									'width'=> 230,
									'height'=> 230
								],
								[
									'width'=> 473.333333,
									'height'=> 230
								],
								[
									'width'=> 230,
									'height'=> 473.333333
								]
							]
						]
					])->label('Cropped images (230x230, 473.333333x230, 230x473.333333)') ?>

					<?= $form->field($model->theme, 'cropped_1')->hiddenInput()->label(false) ?>
					<?= $form->field($model->theme, 'cropped_2')->hiddenInput()->label(false) ?>
					<?= $form->field($model->theme, 'cropped_3')->hiddenInput()->label(false) ?>

					<div class="ln_solid"></div>

					<?= Html::submitButton(Yii::t('easyii', 'Save'), ['class' => 'btn btn-primary']) ?>

					<?php  ActiveForm::end();?>

				<?php } else { ?>

					<p><?= \Yii::t('theme', 'No image found. You first need to upload an image.') ?></p>

				<?php } ?>
			</div>
		</div>
	</div>
</div>




