<?php
use hdmodules\base\helpers\Image;
use hdmodules\base\widgets\SeoForm;
use hdmodules\base\widgets\DateTimePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use hdmodules\base\multilanguage\input_widget\MultiLanguageActiveField;
use hdmodules\base\widgets\RedactorMultiLanguageInput;
use kartik\widgets\Select2;

$module = $this->context->module->id;
?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

            <div class="x_title">
                <h2><?= Yii::t('content', 'Form') ?> <small><?= Yii::t('content', 'create theme') ?></small></h2>
                <ul class="nav navbar-right">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                    'validateOnSubmit' => true,
                    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
                ]); ?>

                    <?= $form->field($model, 'title')->label('Name') ?>

                    <div style="margin: 30px 0 30px 0">
                        <?php if($model->image) : ?>
                            <img src="<?= Image::thumb($model->image, 240) ?>">
                            <a href="<?= Url::to(['/content/item/clear-image', 'id' => $model->primaryKey]) ?>" class="text-danger confirm-delete" title="<?= Yii::t('content', 'Clear image')?>"><?= Yii::t('content', 'Clear image')?></a>
                        <?php endif; ?>

                        <?= $form->field($model, 'image')->fileInput() ?>
                    </div>
                
                    <div style="margin: 30px 0 30px 0">
                        <?php if($model->attribute_1) : ?>
                            <img src="<?= $model->attribute_1 ?>">
                            <a href="<?= Url::to(['theme/clear-animation-image', 'id' => $model->primaryKey]) ?>" class="text-danger confirm-delete" title="<?= Yii::t('content', 'Clear image')?>"><?= Yii::t('content', 'Clear image')?></a>
                        <?php endif; ?>

                        <?= $form->field($model, 'attribute_1')->fileInput()->label('Gif animation') ?>
                    </div>
                
                    <?= $form->field($model, 'slug') ?>
                    
                    <?= $form->field($model, 'attribute_3')->checkbox() ?>
                
                    <?php if(Yii::$app->controller->action->id == 'create' || (Yii::$app->controller->action->id == 'edit')) : ?>
                        <?= SeoForm::widget(['model' => $model]) ?>
                    <?php endif; ?>

                    <div class="ln_solid"></div>

                    <?= Html::submitButton(Yii::t('content', 'Save'), ['class' => 'btn btn-primary']) ?>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>


