<?php
$this->title = Yii::t('content', 'Create theme');
?>
<?= $this->render('_menu', ['block' => $block]) ?>
<?= $this->render('_form', ['model' => $model]) ?>