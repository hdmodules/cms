<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'System';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-md-12">
        <div class="page-title title-center">
            <?=
              yii\widgets\Breadcrumbs::widget([
                  'homeLink' => [
                      'label' => \Yii::t('site', 'Home'),
                      'url' => '/adminpanel',
                  ],
                  'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []])
            ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">

<p>
    <?= Html::a('<i class="glyphicon glyphicon-flash"></i> Flush backend cache', ['flush-cache'], ['class' => 'btn btn-default']) ?>
</p>

<br>
<p>
    <?= Html::a('<i class="glyphicon glyphicon-flash"></i> Flush frontend cache', Yii::$app->urlManagerFrontEnd->createUrl('site/index'). '/?clearCache=1', ['class' => 'btn btn-default', 'target' => 'blank']) ?>
</p>

<br>
<p>
    <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Clear assets', ['clear-assets'], ['class' => 'btn btn-default']) ?>
</p>

<br>
<p>
    <?= Html::a('<i class="glyphicon glyphicon-flash"></i> Refresh similar game', ['refresh-game-stats'], ['class' => 'btn btn-default']) ?>
</p>
<br>

<!--<p>
    <a href="<?= Url::to(['clear-thumbs']) ?>" class="btn btn-default"><i class="glyphicon glyphicon-flash"></i> <?= Yii::t('easyii', 'Clear thumbs') ?></a>
</p>-->

    </div>
</div>
