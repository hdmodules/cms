<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Logs';
$this->params['breadcrumbs'][] = 'System';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-md-12">
        <div class="page-title title-center">
            <?=
              yii\widgets\Breadcrumbs::widget([
                  'homeLink' => [
                      'label' => \Yii::t('site', 'Home'),
                      'url' => '/adminpanel',
                  ],
                  'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []])
            ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">

<p>
    <?= Html::a('<i class="glyphicon glyphicon-flash"></i> Get fpm-php.slow.log', ['slow-logs'], ['class' => 'btn btn-default', 'target' => '_blank']) ?>
</p>

<br>
<p>
    <?= Html::a('<i class="glyphicon glyphicon-flash"></i> Get fpm-php.error.log',  ['error-logs'], ['class' => 'btn btn-default', 'target' => 'blank']) ?>
</p>

<br>
<p>
    <?= Html::a('<i class="glyphicon glyphicon-flash"></i> Get fpm-php.access.log', ['access-logs'], ['class' => 'btn btn-default']) ?>
</p>


    </div>
</div>
