<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Upload materials';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-md-12">
        <div class="page-title title-center">
            <?=
            yii\widgets\Breadcrumbs::widget([
                'homeLink' => [
                    'label' => \Yii::t('site', 'Home'),
                    'url' => '/adminpanel',
                ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []])
            ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        
        <?php $form = ActiveForm::begin(['id' => 'materials-form', 'options' => ['enctype' => 'multipart/form-data']]) ?>
        <table width="100%">
            <tr>
                <td width="15%">
                   <?= $form->field($model, 'materialsFile')->fileInput() ?> 
                </td>
                <td width="15%">
                    <button>Upload</button>
                </td>
                <td>
                    <h3>File info</h3>
                    <h4>File exist: <?= $fileExist ? 'Yes' : 'No' ?></h4>
                    
                    <?php if($fileExist){ ?>
                        <h4>File path: <?= $filePath ?></h4>
                        <h4>File size: <?= round($fileInfo / (1024 * 1024), 1) ?>Mb</h4>  
                    <?php } ?>
                </td>
            </tr>
        </table>
        


        

        


        

        <?php ActiveForm::end() ?>


    </div>
</div>
