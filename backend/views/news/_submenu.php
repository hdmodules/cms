<?php
use yii\helpers\Url;

$action = $this->context->action->id;
$module = $this->context->module->id;
?>

<ul class="nav nav-tabs">
    <li <?= ($action === 'edit') ? 'class="active"' : '' ?>><a href="<?= Url::to([($model->block->controller_name ? $model->block->controller_name.'/edit' : '/'.$module.'/item/edit'), 'id' => $model->primaryKey]) ?>"><?= Yii::t('news', 'Edit news') ?></a></li>
    <li <?= ($action === 'photos') ? 'class="active"' : '' ?>><a href="<?= Url::to([($model->block->controller_name ? $model->block->controller_name.'/photos' : '/'.$module.'/item/photos'), 'id' => $model->primaryKey]) ?>"><span class="glyphicon glyphicon-camera"></span> <?= Yii::t('news', 'Photos') ?></a></li>
    <li <?= ($action === 'crop') ? 'class="active"' : '' ?>><a href="<?= Url::to([($model->block->controller_name ? $model->block->controller_name.'/crop' : '/'.$module.'/item/crop'), 'id' => $model->primaryKey]) ?>"><span class="glyphicon glyphicon-image"></span> <?= Yii::t('news', 'Images') ?></a></li>
</ul>
<br>