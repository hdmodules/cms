<?php
use hdmodules\base\cropbox\Cropbox;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = $model->title;
?>
<?= $this->render('_menu', ['block' => $model->block]) ?>
<?= $this->render('_submenu', ['model' => $model]) ?>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">

			<div class="x_title">
				<h2><?= Yii::t('theme', 'Form') ?> <small><?= Yii::t('content', 'Upload images (420x330, 450x235, 235x235)') ?></small></h2>
				<ul class="nav navbar-right">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">

					<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

					<?= $form->field($model->news, 'cropped_1')->fileInput()->label('Crop 1 - 420x330px') ?>
					<?= $form->field($model->news, 'cropped_2')->fileInput()->label('Crop 2 - 450x235px') ?>
					<?= $form->field($model->news, 'cropped_3')->fileInput()->label('Crop 3 - 235x235px') ?>

					<div class="ln_solid"></div>

					<?= Html::submitButton(Yii::t('easyii', 'Save'), ['class' => 'btn btn-primary']) ?>

					<?php  ActiveForm::end();?>
                                        
                                        <h3>Saved images:</h3>
                                        <div class="col-md-12">
                                            <?php for ($i = 1; $i <= 3; $i++) { ?>
                                                <?php if ($model->news->{"cropped_{$i}"}) { ?>
                                                    <div class="col-md-4">
                                                        <h2>Image <?= $i ?></h2>
                                                        <img src="<?= $model->news->{"cropped_{$i}"} ?>?x=<?= rand(1, 100) ?>"/>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
					<hr>
			</div>
		</div>
	</div>
</div>



