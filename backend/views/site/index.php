<?php
/* @var $this yii\web\View */

$this->title = 'My Yii Application';

?>

<div class="row">
    <div class="col-xs-12 col-md-3">
        <?=
        \yiister\gentelella\widgets\StatsTile::widget(
            [
                'icon' => 'users',
                'header' => 'Users',
                'text' => 'Count of registered users',
                'number' => $userCount,
            ]
        )
        ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?=
        \yiister\gentelella\widgets\StatsTile::widget(
            [
                'icon' => 'list-alt',
                'header' => 'Just demo',
                'text' => 'Just demo',
                'number' => '7084',
            ]
        )
        ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?=
        \yiister\gentelella\widgets\StatsTile::widget(
            [
                'icon' => 'pie-chart',
                'header' => 'Just demo',
                'text' => 'Just demo',
                'number' => '1.5%',
            ]
        )
        ?>
    </div>
</div>
 
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome, <?= Yii::$app->user->identity->username ?>!</h1>

    </div>
</div>
