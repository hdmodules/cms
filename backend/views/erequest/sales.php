<?php
use yii\helpers\Html;
use hdmodules\request\models\Request;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\DetailView;

$this->title = Yii::t('request', 'View request');
$this->registerCss('.popup-view dt{margin-bottom: 10px;}');

if($model->status == Request::STATUS_ANSWERED) {
    $this->registerJs('$(".send-answer").click(function(){return confirm("'.Yii::t('request', 'Are you sure you want to resend the answer?').'");})');
}

$careerPage = \common\models\RequstLetsTalk::getCareerPage($model->json_attributes);
$files = \common\models\RequstLetsTalk::getFiles($model->json_attributes);

?>
<?= $this->render('@vendor/hdmodules/request/views/request/_menu', ['noanswer' => $model->status == Request::STATUS_ANSWERED]) ?>


<div class="row">
    <div class="col-md-6 col-xs-12">
        <div class="x_panel">

            <div class="x_title"  style="margin-bottom: 30px">
                <h2><?= Yii::t('request', 'Request detail'); ?></h2>
                <ul class="nav navbar-right">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        [
                            'attribute'=>'title',
                            'format'=>'html'
                        ],
                        [
                            'attribute'=>'status_answer',
                            'value'=> Request::getStatusAnswerLabel($model->status_answer),
                            'format'=>'html'
                        ],
                        'name',
                        'email',
                        'phone',
                        'text',
                        'class_name',
                        [
                            'attribute'=>'type',
                            'value'=> '<a href="'.Url::to(['/request/request/all', 'RequestSearch[type_id]'=>$model->type->id]).'"><span class="label label-success">'.strtoupper($model->type->name).'</span></a>',
                            'format'=>'html'
                        ],
                        [
                            'attribute'=>'page',
                            'value'=> $model->page ? \yii\helpers\Html::a($model->page, Url::base().$model->page) : '' ,
                            'format'=>'html'
                        ],
                        'lang',
                        [
                            'attribute'=>'ip',
                            'value'=> $model->ip.'  <a href="//freegeoip.net/?q='.$model->ip.'" class="label label-info" target="_blank">info</a>' ,
                            'format'=>'html'
                        ],
                        //'json_attributes',
                        [
                            'label'=>'Files',
                            'value'=> $files,
                            'visible' => $files ? true : false,
                            'format'=>'html'
                        ],
                        [
                            'label'=>'Career page',
                            'value'=> $careerPage,
                            'visible' => $careerPage ? true : false,
                            'format'=>'html'
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xs-12">
        <div class="x_panel">

            <div class="x_title"  style="margin-bottom: 30px">
                <h2><?= Yii::t('request', 'Answer') ?></h2>
                <ul class="nav navbar-right">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <?php $form = ActiveForm::begin() ?>
                <?= $form->field($model, 'answer_email_from')->label(Yii::t('request', 'E-mail from')) ?>
                <?= $form->field($model, 'email')->textInput(['readonly'=>true])->label(Yii::t('request', 'E-mail to')); ?>
                <?= $form->field($model, 'answer_subject') ?>
                <?= $form->field($model, 'answer_text')->textarea(['style' => 'height: 250px']) ?>
                <?= Html::submitButton(Yii::t('request', 'Send'), ['class' => 'btn btn-success send-answer']) ?>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

