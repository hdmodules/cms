<?php
use yii\helpers\Html;
use hdmodules\request\models\Request;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('request', 'View request');
$this->registerCss('.popup-view dt{margin-bottom: 10px;}');

if($model->status == Request::STATUS_ANSWERED) {
    $this->registerJs('$(".send-answer").click(function(){return confirm("'.Yii::t('request', 'Are you sure you want to resend the answer?').'");})');
}
?>
<?= $this->render('_menu', ['noanswer' => $model->status == Request::STATUS_ANSWERED]) ?>


<div class="row">
    <div class="col-md-6 col-xs-12">
        <div class="x_panel">

            <div class="x_title"  style="margin-bottom: 30px">
                <h2><?= Yii::t('request', 'Request detail'); ?></h2>
                <ul class="nav navbar-right">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <dl class="dl-horizontal popup-view">

                    <dt><?= Yii::t('request', 'Title Request') ?></dt>
                    <dd><?= $model->title ?></dd>

                    <dt><?= Yii::t('request', 'Status answer') ?></dt>
                    <dd><?= Request::getStatusAnswerLabel($model->status_answer); ?></dd>


                    <dt><?= Yii::t('request', 'Name') ?></dt>
                    <dd><?= $model->name ?></dd>

                    <dt><?= Yii::t('request', 'Last name') ?></dt>
                    <dd><?= $model->last_name ?></dd>

                    <dt>E-mail</dt>
                    <dd><?= $model->email ?></dd>

                    <dt><?= Yii::t('request', 'Phone') ?></dt>
                    <dd><?= $model->phone ?></dd>

                    <dt><?= Yii::t('request', 'Text') ?></dt>
                    <dd><?= nl2br($model->text) ?></dd>

                    <dt><?= Yii::t('request', 'Question') ?></dt>
                    <dd><?= nl2br($model->question) ?></dd>

                    <dt><?= Yii::t('request', 'Profession') ?></dt>
                    <dd><?= nl2br($model->profession) ?></dd>

                    <dt><?= Yii::t('request', 'Company') ?></dt>
                    <dd><?= nl2br($model->company) ?></dd>

                    <dt><?= Yii::t('request', 'City') ?></dt>
                    <dd><?= nl2br($model->city) ?></dd>

                    <dt><?= Yii::t('request', 'Country') ?></dt>
                    <dd><?= nl2br($model->country) ?></dd>

                    <dt><?= Yii::t('request', 'Class Name') ?></dt>
                    <dd><?= nl2br($model->class_name) ?></dd>

                    <dt><?= Yii::t('request', 'Model ID') ?></dt>
                    <dd><?= nl2br($model->model_id) ?></dd>

                    <dt><?= Yii::t('request', 'Type') ?></dt>
                    <dd><?= nl2br($model->type->name) ?></dd>

                    <dt><?= Yii::t('request', 'Page') ?></dt>
                    <dd><?= $model->page ? \yii\helpers\Html::a($model->page, Url::base().$model->page) : '' ?></dd>

                    <dt><?= Yii::t('request', 'Language') ?></dt>
                    <dd><?= nl2br($model->lang) ?></dd>

                    <dt>IP</dt>
                    <dd><?= $model->ip ?> <a href="//freegeoip.net/?q=<?= $model->ip ?>" class="label label-info" target="_blank">info</a></dd>

                    <dt><?= Yii::t('request', 'Date') ?></dt>
                    <dd><?= Yii::$app->formatter->asDatetime($model->time, 'medium') ?></dd>

                    <dt><?= Yii::t('request', 'Json attributes') ?></dt>
                    <dd><?= $model->json_attributes ?></dd>
                </dl>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xs-12">
        <div class="x_panel">

            <div class="x_title"  style="margin-bottom: 30px">
                <h2><?= Yii::t('request', 'Answer') ?></h2>
                <ul class="nav navbar-right">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <?php $form = ActiveForm::begin() ?>
                <?= $form->field($model, 'answer_email_from')->label(Yii::t('request', 'E-mail from')) ?>
                <?= $form->field($model, 'email')->textInput(['readonly'=>true])->label(Yii::t('request', 'E-mail to')); ?>
                <?= $form->field($model, 'answer_subject') ?>
                <?= $form->field($model, 'answer_text')->textarea(['style' => 'height: 250px']) ?>
                <?= Html::submitButton(Yii::t('request', 'Send'), ['class' => 'btn btn-success send-answer']) ?>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

