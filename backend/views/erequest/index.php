<?php
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Html;
use hdmodules\request\models\Request;

$this->title = Yii::t('request', 'Request dates');
$module = $this->context->module->id;
?>

<?= $this->render('_menu') ?>

<div class="x_panel">

    <div class="x_title"  style="margin-bottom: 30px">
        <h2><?= Yii::t('request', 'Requests'); ?></h2>
        <ul class="nav navbar-right">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'summary'=>'',
            'columns' => [
                [
                    'label'=>'id',
                    'attribute'=>'feedback_id',
                    'headerOptions' => ['width'=>'50'],
                    'content'=>function($data){
                        return \yii\helpers\Html::a($data->primaryKey, ['erequest/view', 'id'=>$data->primaryKey]);
                    }
                ],
                [
                    'attribute'=>'title',
                    'content'=>function($data){
                        return \yii\helpers\Html::a($data->title, ['erequest/view', 'id'=>$data->primaryKey]);
                    }

                ],
                [
                    'attribute'=>'type_id',
                    'content'=>function($data){
                        if($data->type){
                            return \yii\helpers\Html::a($data->type->name, ['request-type/update', 'id'=>$data->type_id]);
                        }
                    }

                ],
                'name',
                'email',
                [
                    'attribute'=>'page',
                    'content'=>function($data){
                        return $data->page ? \yii\helpers\Html::a($data->page, Url::base().'/'.$data->page) : '';
                    }
                ],
                [
                    'attribute'=>'lang',
                    'headerOptions' => ['width'=>'100'],
                ],
                [
                    'attribute'=>'time',
                    'format' => ['date', 'php:d/m/Y H:i:s'],
                    'filter' => \yii\jui\DatePicker::widget([
                        'model'=>$searchModel,
                        'attribute'=>'time',
                        'language' => 'ru',
                        'dateFormat' => 'dd/MM/yyyy',
                    ]),
                ],
                [
                    'label'=>'Viewed',
                    'attribute'=>'status',
                    'headerOptions' => ['width'=>'6%'],
                    'content'=>function($data){
                       return $data->status == Request::STATUS_VIEW ? '<span class="text-success">'.Yii::t("request", "Yes").'</span>' : '<span class="text-danger">'.Yii::t("request", "No").'</span>';
                    },
                    'filter' => Html::dropDownList('RequestSearch[status]', $searchModel->status, [Request::STATUS_VIEW => Yii::t("request", "Yes"), Request::STATUS_NEW => Yii::t("request", "No")], ['prompt' => ''])
                ],
                [
                    'label'=>'Answered',
                    'attribute'=>'status_answer',
                    'headerOptions' => ['width'=>'6%'],
                    'content'=>function($data){
                       return Request::getStatusAnswer($data->status_answer);
                    },
                    'filter' => Html::dropDownList('RequestSearch[status_answer]', $searchModel->status_answer, Request::getStatusAnswer(), ['prompt' => ''])
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'headerOptions' => ['width'=>'6%'],
                    'template'=>'<div class="btn-group btn-group-sm">{view}{delete}</div>',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('hdmodules', 'View'),
                                'class'=>'btn btn-default view'
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('hdmodules', 'Delete'),
                                'class'=>'btn btn-danger delete',
                                'data-confirm' => Yii::t('hdmodules', 'Are you sure you want to delete this Record?'),
                                'data-method' => 'post'
                            ]);
                        },
                    ],
                ],
            ],
        ]) ?>
    </div>
</div>
