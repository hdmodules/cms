<?php
use yii\helpers\Url;

$action = $this->context->action->id;
$module = $this->context->module->id;

$backTo = null;
$indexUrl = Url::to(['/erequest/index']);
$noanswerUrl = Url::to(['/erequest/noanswer']);
$handanswerUrl = Url::to(['/erequest/handanswer']);
$autoanswerUrl = Url::to(['/erequest/autoanswer']);
$allUrl = Url::to(['/erequest/all']);

if($action === 'view')
{
    $returnUrl = $this->context->getReturnUrl($indexUrl);

    if(strpos($returnUrl, 'noanswer') !== false){
        $backTo = 'noanswer';
        $noanswerUrl = $returnUrl;
    }elseif(strpos($returnUrl, 'handanswer') !== false) {
        $backTo = 'handanswer';
        $allUrl = $returnUrl;
    } elseif(strpos($returnUrl, 'autoanswer') !== false) {
        $backTo = 'autoanswer';
        $allUrl = $returnUrl;
    } elseif(strpos($returnUrl, 'all') !== false) {
        $backTo = 'all';
        $allUrl = $returnUrl;
    } else {
        $backTo = 'index';
        $indexUrl = $returnUrl;
    }
}
?>
<ul class="nav nav-pills">
    <li <?= ($action === 'index') ? 'class="active"' : '' ?>>
        <a href="<?= $indexUrl ?>">
            <?php if($backTo === 'index') : ?>
                <i class="glyphicon glyphicon-chevron-left font-12"></i>
            <?php endif; ?>
            <?= Yii::t('easyii', 'New') ?>
            <?php if($this->context->new > 0) : ?>
                <span class="badge"><?= $this->context->new ?></span>
            <?php endif; ?>
        </a>
    </li>
    <li <?= ($action === 'noanswer') ? 'class="active"' : '' ?>>
        <a href="<?= $noanswerUrl ?>">
            <?php if($backTo === 'noanswer') : ?>
                <i class="glyphicon glyphicon-chevron-left font-12"></i>
            <?php endif; ?>
            <?= Yii::t('easyii/popup', 'No answer') ?>
            <?php if($this->context->noAnswer > 0) : ?>
                <span class="badge"><?= $this->context->noAnswer ?></span>
            <?php endif; ?>
        </a>
    </li>
    <li <?= ($action === 'handanswer') ? 'class="active"' : '' ?>>
        <a href="<?= $handanswerUrl ?>">
            <?php if($backTo === 'handanswer') : ?>
                <i class="glyphicon glyphicon-chevron-left font-12"></i>
            <?php endif; ?>
            <?= Yii::t('easyii/popup', 'Hand answer') ?>
            <?php if($this->context->handAnswer > 0) : ?>
                <span class="badge"><?= $this->context->handAnswer ?></span>
            <?php endif; ?>
        </a>
    </li>
    <li <?= ($action === 'autoanswer') ? 'class="active"' : '' ?>>
        <a href="<?= $autoanswerUrl ?>">
            <?php if($backTo === 'autoanswer') : ?>
                <i class="glyphicon glyphicon-chevron-left font-12"></i>
            <?php endif; ?>
            <?= Yii::t('easyii/popup', 'Auto answer') ?>
            <?php if($this->context->autoAnswer > 0) : ?>
                <span class="badge"><?= $this->context->autoAnswer ?></span>
            <?php endif; ?>
        </a>
    </li>
    <li <?= ($action === 'all') ? 'class="active"' : '' ?>>
        <a href="<?= $allUrl ?>">
            <?php if($backTo === 'all') : ?>
                <i class="glyphicon glyphicon-chevron-left font-12"></i>
            <?php endif; ?>
            <?= Yii::t('easyii', 'All') ?>
            <?php if($this->context->all > 0) : ?>
                <span class="badge"><?= $this->context->all ?></span>
            <?php endif; ?>
        </a>
    </li>
    <?php if($action === 'view' && isset($noanswer) && !$noanswer) : ?>
        <li class="pull-right">
            <a href="<?= Url::to(['/admin/'.$module.'/a/set-answer', 'id' => Yii::$app->request->get('id')]) ?>" class="text-warning"><span class="glyphicon glyphicon-ok"></span> <?= Yii::t('easyii/popup', 'Mark as answered') ?></a>
        </li>
    <?php endif; ?>
</ul>
<br/>
