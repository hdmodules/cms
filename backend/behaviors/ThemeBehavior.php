<?php
namespace backend\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use backend\models\Theme;

class ThemeBehavior extends Behavior
{
    private $_model;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    public function afterInsert()
    {
        //if($this->news->load(Yii::$app->request->post())){
        $this->theme->save();
        //}
    }

    public function afterUpdate()
    {
        //if($this->news->load(Yii::$app->request->post())){
        $this->theme->update();
        //}

    }

    public function afterDelete()
    {
        if(!$this->theme->isNewRecord){
            $this->theme->delete();
        }
    }

    public function getPart()
    {
        return $this->owner->hasOne(Theme::className(), ['content_id' => $this->owner->primaryKey()[0]]);
    }

    public function getTheme()
    {
        if(!$this->_model)
        {
            $this->_model = $this->owner->part;
            if(!$this->_model){
                $this->_model = new Theme([
                    'content_id' => $this->owner->primaryKey
                ]);
            }
        }

        return $this->_model;
    }
}