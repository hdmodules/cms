<?php
namespace backend\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use backend\models\Game;

class GameBehavior extends Behavior
{
    private $_model;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    public function afterInsert()
    {
        if($this->game->load(Yii::$app->request->post())){
            $this->game->save();
        }
    }

    public function afterUpdate()
    {
        if($this->game->load(Yii::$app->request->post())){
            $this->game->update();
        }

    }

    public function afterDelete()
    {
        if(!$this->game->isNewRecord){
            $this->game->delete();
        }
    }

    public function getPart()
    {
        return $this->owner->hasOne(Game::className(), ['content_id' => $this->owner->primaryKey()[0]]);
    }

    public function getGame()
    {
        if(!$this->_model)
        {
            $this->_model = $this->owner->part;
            if(!$this->_model){
                $this->_model = new Game([
                    'content_id' => $this->owner->primaryKey
                ]);
            }
        }

        return $this->_model;
    }
}