<?php
namespace backend\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use backend\models\News;

class NewsBehavior extends Behavior
{
    private $_model;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    public function afterInsert()
    {
        if($this->news->load(Yii::$app->request->post())){
            $this->news->save();
        }
    }

    public function afterUpdate()
    {
        if($this->news->load(Yii::$app->request->post())){
            $this->news->update();
        }

    }

    public function afterDelete()
    {
        if(!$this->news->isNewRecord){
            $this->news->delete();
        }
    }

    public function getPart()
    {
        return $this->owner->hasOne(News::className(), ['content_id' => $this->owner->primaryKey()[0]]);
    }

    public function getNews()
    {
        if(!$this->_model)
        {
            $this->_model = $this->owner->part;
            if(!$this->_model){
                $this->_model = new News([
                    'content_id' => $this->owner->primaryKey
                ]);
            }
        }

        return $this->_model;
    }
}