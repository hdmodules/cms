<?php

use yii\db\Schema;
use yii\db\Migration;

class m160812_082736_game_feature extends Migration
{
    public function safeUp()
    {
        $this->createTable('game_feature', [
            'id' => Schema::TYPE_PK,
            'game_id' => Schema::TYPE_INTEGER .  " DEFAULT NULL",
            'feature_id' => Schema::TYPE_INTEGER .  " DEFAULT NULL",
        ], 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

        $this->createIndex('game_id', 'game_feature', ['game_id']);
        $this->createIndex('feature_id', 'game_feature', ['feature_id']);

        $this->addForeignKey('fk1_game_feature', 'game_feature', 'game_id', 'game', 'id', 'CASCADE');
        $this->addForeignKey('fk2_game_feature', 'game_feature', 'feature_id', 'content_blocks_item', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('game_feature');

        return true;
    }
}
