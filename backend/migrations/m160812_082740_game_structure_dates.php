<?php

use yii\db\Migration;
use yii\db\Schema;

class m160812_082740_game_structure_dates extends Migration
{

    public function safeUp()
    {
        $this->execute("
           INSERT INTO `content_block` (`id`, `title`, `image`, `short`, `text`, `text_1`, `text_2`, `order_num`, `slug`, `tree`, `lft`, `rgt`, `depth`, `create_time`, `update_time`, `status`, `time`, `controller_name`) VALUES
                (1, 'News', '', NULL, NULL, NULL, NULL, 1, 'news', 1, 1, 2, 0, NULL, '2016-08-16 12:48:39', 1, 0, 'news'),
                (2, 'Game', '', NULL, NULL, NULL, NULL, 2, 'game', 2, 1, 2, 0, NULL, '2016-08-16 12:48:58', 1, 0, 'game'),
                (3, 'Games category', '', NULL, NULL, NULL, NULL, 3, 'games-category', 3, 1, 2, 0, NULL, '2016-08-16 12:50:21', 1, 0, ''),
                (4, 'Games feature', '', NULL, NULL, NULL, NULL, 4, 'games-feature', 4, 1, 2, 0, NULL, '2016-08-16 12:50:40', 1, 0, ''),
                (5, 'Games theme', '', NULL, NULL, NULL, NULL, 5, 'games-theme', 5, 1, 2, 0, NULL, '2016-08-16 12:51:38', 1, 0, 'theme'),
                (6, 'Games payline', '', NULL, NULL, NULL, NULL, 6, 'games-payline', 6, 1, 2, 0, NULL, '2016-08-16 12:51:56', 1, 0, 'payline');
        ");

        $this->execute("
           INSERT INTO `content_blocks_item` (`id`, `block_id`, `title`, `image`, `short`, `text`, `text_1`, `text_2`, `order_num`, `slug`, `create_time`, `update_time`, `views`, `status`, `time`) VALUES
                (1, 3, 'Popular', '', '', '', '', '', 1, 'popular', NULL, '0000-00-00 00:00:00', 0, 1, 1471351920),
                (2, 3, 'New', '', '', '', '', '', 2, 'new', NULL, '0000-00-00 00:00:00', 0, 1, 1471351951),
                (3, 3, '3D', '', '', '', '', '', 3, '3d', NULL, '0000-00-00 00:00:00', 0, 1, 1471351968),
                (4, 4, 'SCATER', '', '', '', '', '', 1, 'scater', NULL, '0000-00-00 00:00:00', 0, 1, 1471352005),
                (5, 4, 'WILD', '', '', '', '', '', 2, 'wild', NULL, '0000-00-00 00:00:00', 0, 1, 1471352019),
                (6, 4, 'FREE GAMES', '', '', '', '', '', 3, 'free-games', NULL, '0000-00-00 00:00:00', 0, 1, 1471352026),
                (7, 4, 'GAMBLE GAME', '', '', '', '', '', 4, 'gamble-game', NULL, '0000-00-00 00:00:00', 0, 1, 1471352038),
                (8, 4, 'MULTIPLIER', '', '', '', '', '', 5, 'multiplier', NULL, '0000-00-00 00:00:00', 0, 1, 1471352054),
                (9, 4, 'BONUS GAME', '', '', '', '', '', 6, 'bonus-game', NULL, '0000-00-00 00:00:00', 0, 1, 1471352071),
                (10, 5, 'FRUIT', '', '', '', NULL, NULL, 1, 'fruit', NULL, '0000-00-00 00:00:00', 0, 1, 1471352127),
                (11, 5, 'UNIQUE', '', '', '', NULL, NULL, 2, 'unique', NULL, '0000-00-00 00:00:00', 0, 1, 1471352138),
                (12, 5, 'LIFESTYLE', '', '', '', NULL, NULL, 3, 'lifestyle', NULL, '0000-00-00 00:00:00', 0, 1, 1471352147),
                (13, 5, 'CUTE', '', '', '', NULL, NULL, 4, 'cute', NULL, '0000-00-00 00:00:00', 0, 1, 1471352163),
                (14, 5, 'ADVENTURE', '', '', '', NULL, NULL, 5, 'adventure', NULL, '0000-00-00 00:00:00', 0, 1, 1471352177),
                (15, 5, 'HORROR', '', '', '', NULL, NULL, 6, 'horror', NULL, '0000-00-00 00:00:00', 0, 1, 1471352185),
                (16, 5, 'ETHNIC', '', '', '', NULL, NULL, 7, 'ethnic', NULL, '0000-00-00 00:00:00', 0, 1, 1471352194),
                (17, 5, 'MIX', '', '', '', NULL, NULL, 8, 'mix', NULL, '0000-00-00 00:00:00', 0, 1, 1471352203),
                (18, 6, '5', '', '', '', NULL, NULL, 1, '5', NULL, '0000-00-00 00:00:00', 0, 1, 1471352235),
                (19, 6, '9', '', '', '', NULL, NULL, 2, '9', NULL, '0000-00-00 00:00:00', 0, 1, 1471352244),
                (20, 6, '10', '', '', '', NULL, NULL, 3, '10', NULL, '0000-00-00 00:00:00', 0, 1, 1471352248),
                (21, 6, '20', '', '', '', NULL, NULL, 4, '20', NULL, '0000-00-00 00:00:00', 0, 1, 1471352253),
                (22, 6, '21', '', '', '', NULL, NULL, 5, '21', NULL, '0000-00-00 00:00:00', 0, 1, 1471352256),
                (23, 6, '25', '', '', '', NULL, NULL, 6, '25', NULL, '0000-00-00 00:00:00', 0, 1, 1471352266),
                (24, 6, '40', '', '', '', NULL, NULL, 7, '40', NULL, '0000-00-00 00:00:00', 0, 1, 1471352271),
                (25, 6, '50', '', '', '', NULL, NULL, 8, '50', NULL, '0000-00-00 00:00:00', 0, 1, 1471352276);
        ");
    }

    public function safeDown()
    {
        return true;
    }

}
