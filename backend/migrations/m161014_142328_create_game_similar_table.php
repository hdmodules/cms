<?php

use yii\db\Schema;
use yii\db\Migration;

class m161014_142328_create_game_similar_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('game_similar', [
            'id' => Schema::TYPE_PK,
            'game_id' => Schema::TYPE_INTEGER,
            'similar_game_id' => Schema::TYPE_INTEGER,
            'similar_game_id' => Schema::TYPE_INTEGER,
            'coefficient' => Schema::TYPE_INTEGER . ' DEFAULT 0',
        ], 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

        $this->createIndex('game_id', 'game_similar', ['game_id']);
        $this->createIndex('similar_game_id', 'game_similar', ['similar_game_id']);

        $this->addForeignKey('fk1_game_id', 'game_similar', 'game_id', 'game', 'id', 'CASCADE');
        $this->addForeignKey('fk1_similar_game_id', 'game_similar', 'similar_game_id', 'game', 'id', 'CASCADE');
        
        $this->execute("INSERT INTO `settings` (`name`, `visibility`, `title`, `value`) VALUES ('game-page-similar-game-count', 2, 'Count similar games on game page', '9')");
    }

    public function safeDown()
    {

        $this->dropTable('game_similar');
        
        return true;
    }

}
