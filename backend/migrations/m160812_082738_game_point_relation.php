<?php

use yii\db\Schema;
use yii\db\Migration;

class m160812_082738_game_point_relation extends Migration
{
    public function safeUp()
    {
        $this->createTable('game_point_rel', [
            'id' => Schema::TYPE_PK,
            'game_id' => Schema::TYPE_INTEGER .  " DEFAULT NULL",
            'point_id' => Schema::TYPE_INTEGER .  " DEFAULT NULL",
            'order_num' => Schema::TYPE_INTEGER .  " DEFAULT NULL",
            'color' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
        ], 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

        $this->createIndex('game_id', 'game_point_rel', ['game_id']);
        $this->createIndex('point_id', 'game_point_rel', ['point_id']);

        $this->addForeignKey('fk1_game_point_rel', 'game_point_rel', 'game_id', 'game', 'id', 'CASCADE');
        $this->addForeignKey('fk2_game_point_rel', 'game_point_rel', 'point_id', 'game_point', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('game_point_rel');

        return true;
    }
}
