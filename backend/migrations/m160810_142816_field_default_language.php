<?php

use yii\db\Schema;
use yii\db\Migration;

class m160810_142816_field_default_language extends Migration
{
    public function safeUp()
    {
        $this->addColumn('language', 'default_language', Schema::TYPE_INTEGER. ' DEFAULT 0');
        $this->update('language', ['default_language' => 1, 'status' => 1], ['language_id' => 'cs-CZ']);
        $this->update('language', ['status' => 1], ['language_id' => ['cs-CZ', 'en-US', 'ru-RU']]);
    }

    public function safeDown()
    {
        echo "m160810_142816_field_default_language reverted.\n";
        
        $this->dropColumn('language', 'default_language');
        
        return true;
    }

}
