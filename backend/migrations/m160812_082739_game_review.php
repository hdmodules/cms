<?php

use yii\db\Schema;
use yii\db\Migration;

class m160812_082739_game_review extends Migration
{
    public function up()
    {
        $this->createTable('game_review', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'image' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'url' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'type' => Schema::TYPE_INTEGER .  " DEFAULT 0",
            'status' => Schema::TYPE_INTEGER .  " DEFAULT 0",
            'game_id' => Schema::TYPE_INTEGER .  " DEFAULT NULL",
            'order_num' => Schema::TYPE_INTEGER .  " DEFAULT NULL",
            'request_id' => Schema::TYPE_INTEGER .  " DEFAULT NULL",
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('game_id', 'game_review', ['game_id']);
        $this->createIndex('request_id', 'game_review', ['request_id']);

        $this->addForeignKey('fk1_game_review', 'game_review', 'game_id', 'game', 'id', 'CASCADE');
        $this->addForeignKey('fk2_game_review', 'game_review', 'request_id', 'request', 'id');
    }

    public function down()
    {
        $this->dropTable('game_review');
    }
}
