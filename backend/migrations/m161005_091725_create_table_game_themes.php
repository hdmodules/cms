<?php

use yii\db\Schema;
use yii\db\Migration;

class m161005_091725_create_table_game_themes extends Migration
{
    public function safeUp()
    {
        $this->createTable('game_theme', [
            'id' => Schema::TYPE_PK,
            'game_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'theme_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

        $this->createIndex('game_id', 'game_theme', ['game_id']);
        $this->createIndex('feature_id', 'game_theme', ['theme_id']);

        $this->addForeignKey('fk1_game_theme', 'game_theme', 'game_id', 'game', 'id', 'CASCADE');
        $this->addForeignKey('fk2_game_theme', 'game_theme', 'theme_id', 'content_blocks_item', 'id', 'CASCADE');
        
        $this->execute('INSERT INTO `game_theme`(`game_id`, `theme_id`)
                        SELECT id, theme_id FROM game;');
        
        //$this->dropForeignKey('fk2_game', 'game');
        //$this->dropIndex('fk2_game', 'game');
       // $this->dropColumn('game', 'theme_id');
    }

    public function safeDown()
    {
        //$this->addColumn('game', 'theme_id', $this->integer());
        $this->dropTable('game_theme');

        return true;
    }
}
