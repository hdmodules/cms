<?php

use yii\db\Schema;
use yii\db\Migration;

class m161014_142830_add_html_game_checkbox extends Migration
{
    public function safeUp()
    {
        $this->addColumn('game', 'html_game', Schema::TYPE_INTEGER. ' DEFAULT 0');

    }

    public function safeDown()
    {
        echo "m161014_142830_add_html_game_checkbox reverted.\n";

        $this->dropColumn('game', 'html_game');

        return true;
    }
}
