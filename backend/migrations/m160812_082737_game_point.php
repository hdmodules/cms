<?php

use yii\db\Schema;
use yii\db\Migration;

class m160812_082737_game_point extends Migration
{
    public function up()
    {
        $this->createTable('game_point', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down()
    {
        $this->dropTable('game_point');
    }
}
