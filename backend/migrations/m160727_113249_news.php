<?php

use yii\db\Schema;
use yii\db\Migration;

class m160727_113249_news extends Migration
{
    public function up()
    {
        $this->createTable('news', [
            'id' => Schema::TYPE_PK,
            'content_id' => Schema::TYPE_INTEGER,
            'cropped_1' =>$this->string(255)." DEFAULT NULL",
            'cropped_2' =>$this->string(255)." DEFAULT NULL",
            'cropped_3' =>$this->string(255)." DEFAULT NULL",
            'video' =>$this->string(255)." DEFAULT NULL",
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->addForeignKey('fk1_news', 'news', 'content_id', 'content_blocks_item', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('news');
    }
}
