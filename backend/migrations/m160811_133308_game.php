<?php

use yii\db\Schema;
use yii\db\Migration;

class m160811_133308_game extends Migration
{
    public function up()
    {
        $this->createTable('game', [
            'id' => Schema::TYPE_PK,
            'content_id' => Schema::TYPE_INTEGER,
            'theme_id' => Schema::TYPE_INTEGER,
            'payline_id' => Schema::TYPE_INTEGER
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->addForeignKey('fk1_game', 'game', 'content_id', 'content_blocks_item', 'id', 'CASCADE');
        $this->addForeignKey('fk2_game', 'game', 'theme_id', 'content_blocks_item', 'id');
        $this->addForeignKey('fk3_game', 'game', 'payline_id', 'content_blocks_item', 'id');
    }

    public function down()
    {
        $this->dropTable('game');
    }
}
