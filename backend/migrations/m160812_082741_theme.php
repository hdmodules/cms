<?php

use yii\db\Schema;
use yii\db\Migration;

class m160812_082741_theme extends Migration
{
    public function up()
    {
        $this->createTable('theme', [
            'id' => Schema::TYPE_PK,
            'content_id' => Schema::TYPE_INTEGER,
            'cropped_1' =>$this->string(255),
            'cropped_2' =>$this->string(255),
            'cropped_3' =>$this->string(255)
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->addForeignKey('fk1_theme', 'theme', 'content_id', 'content_blocks_item', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('theme');
    }
}
