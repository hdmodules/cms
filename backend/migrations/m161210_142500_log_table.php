<?php

use yii\db\Schema;
use yii\db\Migration;

class m161210_142500_log_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('log', [
            'id' => $this->primaryKey(11),
            'type' => $this->integer()->defaultValue(0),
            'create_time' => $this->integer()->notNull(),
            'message'=> $this->text(),
        ],  $tableOptions);

    }

    public function safeDown()
    {
        $this->dropTable('log');
        
        return true;
    }

}
