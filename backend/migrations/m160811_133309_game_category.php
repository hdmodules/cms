<?php

use yii\db\Schema;
use yii\db\Migration;

class m160811_133309_game_category extends Migration
{
    public function safeUp()
    {
        $this->createTable('game_category', [
            'id' => Schema::TYPE_PK,
            'game_id' => Schema::TYPE_INTEGER .  " DEFAULT NULL",
            'category_id' => Schema::TYPE_INTEGER .  " DEFAULT NULL",
        ], 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

        $this->createIndex('game_id', 'game_category', ['game_id']);
        $this->createIndex('category_id', 'game_category', ['category_id']);

        $this->addForeignKey('fk1_game_category', 'game_category', 'game_id', 'game', 'id', 'CASCADE');
        $this->addForeignKey('fk2_game_category', 'game_category', 'category_id', 'content_blocks_item', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('game_category');

        return true;
    }
}
