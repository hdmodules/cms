<?php

if (LOCALHOST) {
    $params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
    );
} else {
    $params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/params.php')
    );
}

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log', 'translatemanager'],
    'modules' => [
        'base' => 'hdmodules\base\BaseModule',
        'content' => 'hdmodules\content\ContentModule',
        'file' => 'hdmodules\file\FileModule',
        'migration' => 'hdmodules\migration\MigrationModule',
        'carousel' => [
            'class' =>'hdmodules\carousel\CarouselModule',
            'model_item'=>'backend\models\CarouselItem'
        ],
        'universalmailtemplate' =>[
            'class' =>'hdmodules\universalmailtemplate\UniversalMailTemplatesModule',
            'relation_classes' =>[
                'hdmodules\request\models\Request'
            ]
        ],
        'request' =>[
            'class' => 'hdmodules\request\RequestModule',
            'answer_email_from'=>'noreply@endorphina.com',
            'controllerMap'=>[
                'request-handler'=>[
                    'class' => 'backend\controllers\RequestHandlerController',
                ]
            ]
        ],
        'admin' => [
            'class' => 'mdm\admin\Module',
            'layout' => 'left-menu', // avaliable value 'left-menu', 'right-menu' and 'top-menu'
            'controllerMap' => [
                'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    'userClassName' => 'common\models\User',
                    'idField' => 'id'
                ]
            ],
            'menus' => [
                'assignment' => [
                    'label' => 'Grand Access' // change label
                ],
            // 'route' => null, // disable menu
            ],
        ]
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            //'admin/*', // add or remove allowed actions to this list
            'debug/*',
        ]
    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest'],
        ],
        'request' => [
            'baseUrl' => '/adminpanel',
            'enableCsrfValidation' => false,
            'cookieValidationKey' => 'M0NYURR7h--VRr39ZdSz1wrV368v6BOZ',
        ],
        'urlManagerFrontEnd' => [
            'class' => 'common\components\LangUrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => '/',
            'rules' => [
                '' => 'site/index',
                'login' => 'site/login',
                'career-detail/<slug:[\w-]+>' => 'career/view',
            ]
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            //'class' => 'backend\components\BackendUrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'baseUrl' => '/adminpanel',
            'rules' => [
                //'/' => 'site/index',
                //'/admin' => 'admin/site/index',
                //'<action>'=>'site/<action>',
                //'<controller:\w+>/<id:\d+>' => '<controller>/view',
                // <module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>] => <module>/<controller>/<action> 
                '' => 'site/index',
                'logout' => 'site/logout',
                'users' => 'user/index',
                
                'system' => 'system/index',
                'logs/fpm-php.slow.log' => 'system/slow-logs',
                'logs/fpm-php.error.log' => 'system/error-logs',
                'logs/fpm-php.access.log' => 'system/access-logs',
                
                //Modules
                'content' => 'content/block/index',
                'files' => 'file/file/index',
                'migrations' => 'migration/migration/index',
                'menu-item/create/<menu_id:\d+>/<parent_id:\d+>' => 'base/menu-item/create',
                'menu/index' => 'base/menu/index',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<module:\w+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>' => '<module>/<controller>/index',
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'assetManager' => [
            'dirMode' => 0777,
            'fileMode' => 0777,
        ],
    ],
    'params' => $params,
];
